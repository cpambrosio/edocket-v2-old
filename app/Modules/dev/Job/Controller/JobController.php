<?php namespace App\Modules\dev\Job\Controller;

use App\Http\Controllers\api\BaseApiController;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\dev\Job\Gateway\JobGateway;
use App\Modules\dev\Job\Gateway\TimesheetGateway;
use App\Modules\dev\Job\Gateway\RequirementGateway;
use App\Modules\dev\Job\Gateway\SiteChecklistGateway;
use App\Modules\dev\Job\Gateway\TCAttachmentGateway;
use App\Modules\dev\Job\Gateway\SignageAuditGateway;
use App\Modules\dev\Job\Gateway\JobImageGateway;
use App\Modules\dev\Job\Gateway\DocketGateway;
use App\Modules\dev\Job\Gateway\VideoGateway;
use App\Modules\dev\Job\Gateway\ReportGateway;
use App\Modules\dev\Authenticate\Services\TokenServices;
use App\Modules\dev\Authenticate\Services\ContactServices;
use App\Modules\dev\Job\Request\JobRequest;
use App\Modules\dev\Job\Request\GetTimesheetRequest;
use App\Modules\dev\Job\Request\GetAllJobDataRequest;
use App\Modules\dev\Job\Request\GenerateFailSafeReportRequest;
use App\Modules\dev\Job\Request\GenerateJobDocketReportRequest;
use App\Modules\dev\Job\Request\GenerateAppSafetyReportRequest;
use App\Modules\dev\Job\Request\UploadJobImageRequest;
use App\Modules\dev\Job\Request\UploadVideoRequest;

use Illuminate\Http\Request;
use DB, Response, Config;

class JobController extends Controller {

	protected $jobGateway;
	protected $timesheetGateway;
	protected $requirementGateway;
	protected $siteChecklistGateway;
	protected $tcAttachmentGateway;
	protected $signageAuditGateway;
	protected $jobImageGateway;
	protected $docketGateway;
	protected $videoGateway;
	protected $reportGateway;
	protected $tokenServices;
	protected $contactServices;

	function __construct(
		JobGateway $jobGateway,
		TimesheetGateway $timesheetGateway,
		RequirementGateway $requirementGateway,
		SiteChecklistGateway $siteChecklistGateway,
		TCAttachmentGateway $tcAttachmentGateway,
		SignageAuditGateway $signageAuditGateway,
		JobImageGateway $jobImageGateway,
		DocketGateway $docketGateway,
		VideoGateway $videoGateway,
		ReportGateway $reportGateway,
		TokenServices $tokenServices,
		ContactServices $contactServices){

		$this->jobGateway = $jobGateway;
		$this->timesheetGateway = $timesheetGateway;
		$this->requirementGateway = $requirementGateway;
		$this->siteChecklistGateway = $siteChecklistGateway;
		$this->tcAttachmentGateway = $tcAttachmentGateway;
		$this->signageAuditGateway = $signageAuditGateway;
		$this->jobImageGateway = $jobImageGateway;
		$this->docketGateway = $docketGateway;
		$this->videoGateway = $videoGateway;
		$this->reportGateway = $reportGateway;
		$this->tokenServices  = $tokenServices;
		$this->contactServices  = $contactServices;

	}

	public function getRelatedJobs()
	{

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->jobGateway->getRelatedJobs($tokenInfo['ContactID']);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function getTimesheetEmployees(GetTimesheetRequest $request){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->timesheetGateway->getTimesheetEmployees($tokenInfo['ContactID'],$request->all());

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function getRequirements(){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->requirementGateway->getRequirements($tokenInfo['ContactID']);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);
	}

	public function getSiteCheckList(){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->siteChecklistGateway->getSiteCheckList($tokenInfo['ContactID']);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);
	}

	public function getTCAttachment(){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->tcAttachmentGateway->getTCAttachment($tokenInfo['ContactID']);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);
	}

	public function getSignageAudit(){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->signageAuditGateway->getSignageAudit($tokenInfo['ContactID']);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);
	}

	public function getSign(){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->signageAuditGateway->getSign($tokenInfo['ContactID']);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);
	}

	public function getCarriageWay(){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->signageAuditGateway->getCarriageWay($tokenInfo['ContactID']);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);
	}

	public function getDirection(){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->signageAuditGateway->getDirection($tokenInfo['ContactID']);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);
	}

	public function getAllJobData(GetAllJobDataRequest $request){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->jobGateway->getAllJobData($tokenInfo['ContactID'],$request->all());

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function countJobSync($jobId){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->jobGateway->countJobSync($jobId);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function returnAll(){

		$tokenInfo = $this->tokenServices->info();
		$data = $this->jobGateway->returnAll($tokenInfo['ContactID']);
		$response['data'] = $data;

		return Response::json($response, 200);

	}

	public function uploadJobImage(UploadJobImageRequest $request){

		$responseData = $this->jobImageGateway->uploadJobImage($request->all());

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function checkConnectivity(){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->jobGateway->checkConnectivity($tokenInfo['ContactID']);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function checkJobExpiration($jobId){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->jobGateway->checkJobExpiration($tokenInfo['ContactID'], $jobId);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function clearJobSync($jobId){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->jobGateway->clearJobSync($tokenInfo['ContactID'], $jobId);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function getDocketAttachments(){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->docketGateway->getDocketAttachments($tokenInfo['ContactID']);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);
	}

	public function viewDocketLink(Request $request){

		$responseData = $this->docketGateway->viewDocketLink($request->all());

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function getNotes(){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->jobGateway->getNotes($tokenInfo['ContactID']);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function truncateDocketAttachments(){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->docketGateway->truncateDocketAttachments();

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);
	}

	public function uploadVideo(UploadVideoRequest $request){

		$responseData = $this->videoGateway->uploadVideo($request->all());

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function updateJob(JobRequest $request){

		$tokenInfo = $this->tokenServices->info();
		$responseData = $this->jobGateway->updateJob($request->all(), $tokenInfo['ContactID']);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function saveTCAttachment(Request $request){

		$responseData = $this->tcAttachmentGateway->saveTCAttachment($request->all());

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function deleteExtensionJob($jobId){

		$tokenInfo = $this->tokenServices->info();

		$responseData = $this->jobGateway->deleteExtensionJob($jobId);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function generateJobDocketReport(GenerateJobDocketReportRequest $request){

		$tokenInfo = $this->tokenServices->info();
		$contactInfo = $this->contactServices->findByCurrentToken();

		$responseData = $this->reportGateway->generateJobDocketReport($request->all(), $tokenInfo['ContactID'],$contactInfo['ContactCode']);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function generateAppSafetyReport(GenerateAppSafetyReportRequest $request){

		$tokenInfo = $this->tokenServices->info();
		$contactInfo = $this->contactServices->findByCurrentToken();

		$responseData = $this->reportGateway->generateAppSafetyReport($request->all(), $tokenInfo['ContactID'],$contactInfo['ContactCode']);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function generateJobImageReport(Request $request){

		$tokenInfo = $this->tokenServices->info();
		$contactInfo = $this->contactServices->findByCurrentToken();

		$responseData = $this->reportGateway->generateJobImageReport($request->all(), $tokenInfo['ContactID'],$contactInfo['ContactCode']);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function checkJobReport($type, $jobId = false){

		$responseData = $this->reportGateway->checkJobReport($type, $jobId);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

	public function generateFailSafeReport(Request $request){

		$tokenInfo = $this->tokenServices->info();
		$contactInfo = $this->contactServices->findByCurrentToken();

		$responseData = $this->reportGateway->generateFailSafeReport($request->all(), $tokenInfo['ContactID'],$contactInfo['ContactCode']);

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}



}