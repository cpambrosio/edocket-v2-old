<?php namespace App\Modules\dev\Job\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendVideoMail extends Mailable
{
    use Queueable, SerializesModels;

     /**
     * The video instance.
     *
     * @var Video
     */
    public $video;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($video)
    {
        $this->video = $video;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        if( isset( $this->mail['attachment'] ) && count( $this->mail['attachment'] ) > 0 ){

            foreach($this->mail['attachment'] as $attachmentInfo){

              $this->attach($attachmentInfo['path'],['as'=>$attachmentInfo['name'],'mime'=>$attachmentInfo['mime']])

            }

        }

        return $this->view('emails.videoupload')
                    ->subject('A video was successfully uploaded');
    }
}
