<?php namespace App\Modules\dev\Job\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendGenerateFailReportMail extends Mailable
{
    use Queueable, SerializesModels;

     /**
     * The mail instance.
     *
     * @var mail
     */
    public $mail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mail)
    {
        $this->mail = $mail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        if( isset( $this->mail['attachment'] ) && count( $this->mail['attachment'] ) > 0 ){

            foreach($this->mail['attachment'] as $attachInfo){

              $this->attach($attachInfo);

            }

        }

        return $this->view('emails.jobdocketlog')
                    ->subject('Job Docket no.'.$this->mail['jobID'].' was failed to submit');


    }
}
