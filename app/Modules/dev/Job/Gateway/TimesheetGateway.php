<?php namespace App\Modules\dev\Job\Gateway;

use App\Modules\dev\Job\Services\ManstatServices;
use App\Modules\dev\Job\Services\GoogleTimezoneServices;
use App\Modules\dev\Job\Repository\Interfaces\JobSyncInterface as JobSyncRepository;
use App\Modules\dev\Job\Helper\JobHelper;

use Config;
use Carbon\Carbon;
use DB;
use Request;
use Mail;
use PDF;
use Excel;
use Input;
use File;
use DateTime;

class TimesheetGateway
{

    protected $manstatServices;
    protected $googleTimezoneServices;
    protected $jobSyncRepository;
    protected $jobHelper;

    public function __construct(
        ManstatServices $manstatServices,
        GoogleTimezoneServices $googleTimezoneServices,
        JobSyncRepository $jobSyncRepository,
        JobHelper $jobHelper)
    {

        $this->manstatServices = $manstatServices;
        $this->googleTimezoneServices = $googleTimezoneServices;
        $this->jobSyncRepository = $jobSyncRepository;
        $this->jobHelper = $jobHelper;

    }


    public function getTimesheetEmployees($contactId, $input){

      $timezone = $this->googleTimezoneServices->setTimezone($input['geotag']);

      if( !$timezone ){

        $response['status'] = false;
        $response['statusCode'] = 422;
        $response['message'] = "Failed to set Timezone";

        return $response;

      }

      $timesheet = $this->manstatServices->getTimesheetEmployees($contactId);

      $timesheetList = [];

      if( $timesheet ){

        foreach( $timesheet as $key => $timesheetInfo ){

          $countJobSync = $this->jobSyncRepository->countByJobID($timesheetInfo['JobID']);

          $jobSync = Config::get('evolution.jobSyncEnable');

          if( $jobSync == false || $countJobSync == 0 ){

            array_push($timesheetList, $timesheet[$key]);

          }

        }

      }

      if( count( $timesheetList ) > 0 ){

        $response['status'] = true;
        $response['statusCode'] = 200;
        $response['employees'] = $timesheetList;

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 200;
        $response['message'] = "No Timesheet Found";

      }

      return $response;

    }

    

}

?>