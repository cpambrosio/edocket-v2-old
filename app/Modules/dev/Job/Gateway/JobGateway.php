<?php namespace App\Modules\dev\Job\Gateway;

use App\Modules\dev\Job\Services\ManstatServices;
use App\Modules\dev\Job\Services\DocushareServices;
use App\Modules\dev\Job\Services\GoogleTimezoneServices;
use App\Modules\dev\Job\Services\EmailServices;
use App\Modules\dev\Job\Services\DocketAttachmentServices as DocketAttachmentServices;
use App\Modules\dev\Job\Repository\Interfaces\JobSyncInterface as JobSyncRepository;
use App\Modules\dev\Job\Repository\Interfaces\JobShiftDetailInterface as JobShiftDetailRepository;
use App\Modules\dev\Job\Repository\Interfaces\JobHeaderInterface as JobHeaderRepository;
use App\Modules\dev\Job\Repository\Interfaces\ContactPhoneMobileEmailInterface as ContactPhoneMobileEmailRepository;
use App\Modules\dev\Job\Repository\Interfaces\NotesInterface as NotesRepository;
use App\Modules\dev\Job\Repository\Interfaces\RequestInterface as RequestRepository;
use App\Modules\dev\Job\Repository\Interfaces\JobSiteCheckListItemInterface as JobSiteCheckListItemRepository;
use App\Modules\dev\Job\Repository\Interfaces\SiteCheckListInputValueInterface as SiteCheckListInputValueRepository;
use App\Modules\dev\Job\Repository\Interfaces\DocketSignaturesInterface as DocketSignaturesRepository;
use App\Modules\dev\Job\Repository\Interfaces\AddContactCalendarInterface as AddContactCalendarRepository;
use App\Modules\dev\Job\Repository\Interfaces\AddTimesheetInterface as AddTimesheetRepository;
use App\Modules\dev\Job\Repository\Interfaces\JobClientNotifyInterface as JobClientNotifyRepository;
use App\Modules\dev\Job\Repository\Interfaces\AddJobHeaderInterface as AddJobHeaderRepository;
use App\Modules\dev\Job\Repository\Interfaces\AddHazardInterface as AddHazardRepository;
use App\Modules\dev\Job\Repository\Interfaces\TCAttachmentInterface as TCAttachmentRepository;
use App\Modules\dev\Job\Repository\Interfaces\SignageAuditInterface as SignageAuditRepository;
use App\Modules\dev\Job\Repository\Interfaces\AuditSignsInterface as AuditSignsRepository;
use App\Modules\dev\Job\Repository\Interfaces\ExtJobHeaderInterface as ExtJobHeaderRepository;
use App\Modules\dev\Job\Repository\Interfaces\ExtTimesheetInterface as ExtTimesheetRepository;
use App\Modules\dev\Job\Repository\Interfaces\ExtJobRequirementInterface as ExtJobRequirementRepository;
use App\Modules\dev\Job\Repository\Interfaces\ExtDocketInterface as ExtDocketRepository;
use App\Modules\dev\Job\Repository\Interfaces\ExtJobSiteCheckListItemInterface as ExtJobSiteCheckListItemRepository;
use App\Modules\dev\Job\Repository\Interfaces\ExtSiteCheckListInputValueInterface as ExtSiteCheckListInputValueRepository;
use App\Modules\dev\Job\Repository\Interfaces\PreSiteCheckListInterface as PreSiteCheckListRepository;
use App\Modules\dev\Job\Repository\Interfaces\PreSiteCheckListItemInterface as PreSiteCheckListItemRepository;
use App\Modules\dev\Job\Gateway\SiteChecklistGateway;
use App\Modules\dev\Job\Helper\JobHelper;
use App\Modules\dev\Job\Helper\InputHelper;

use Config;
use Carbon\Carbon;
use DB;
use Request;
use Mail;
use PDF;
use Excel;
use Input;
use File;
use DateTime;

class JobGateway
{

    protected $manstatServices;
    protected $docushareServices;
    protected $googleTimezoneServices;
    protected $emailServices;
    protected $jobSyncRepository;
    protected $jobShiftDetailRepository;
    protected $jobHeaderRepository;
    protected $contactPhoneMobileEmailRepository;
    protected $notesRepository;
    protected $requestRepository;
    protected $jobSiteCheckListItemRepository;
    protected $siteCheckListInputValueRepository;
    protected $docketSignaturesRepository;
    protected $addContactCalendarRepository;
    protected $addTimesheetRepository;
    protected $jobClientNotifyRepository;
    protected $addJobHeaderRepository;
    protected $addHazardRepository;
    protected $tcAttachmentRepository;
    protected $signageAuditRepository;
    protected $auditSignsRepository;
    protected $docketAttachmentServices;
    protected $extJobHeaderRepository;
    protected $extTimesheetRepository;
    protected $extJobRequirementRepository;
    protected $extDocketRepository;
    protected $extJobSiteCheckListItemRepository;
    protected $extSiteCheckListInputValueRepository;
    protected $siteChecklistGateway;
    protected $preSiteCheckListRepository;
    protected $preSiteCheckListItemRepository;
    protected $jobHelper;
    protected $inputHelper;

    public function __construct(
        ManstatServices $manstatServices,
        DocushareServices $docushareServices,
        GoogleTimezoneServices $googleTimezoneServices,
        EmailServices $emailServices,
        JobSyncRepository $jobSyncRepository,
        JobShiftDetailRepository $jobShiftDetailRepository,
        JobHeaderRepository $jobHeaderRepository,
        ContactPhoneMobileEmailRepository $contactPhoneMobileEmailRepository,
        NotesRepository $notesRepository,
        RequestRepository $requestRepository,
        JobSiteCheckListItemRepository $jobSiteCheckListItemRepository,
        SiteCheckListInputValueRepository $siteCheckListInputValueRepository,
        DocketSignaturesRepository $docketSignaturesRepository,
        AddContactCalendarRepository $addContactCalendarRepository,
        AddTimesheetRepository $addTimesheetRepository,
        JobClientNotifyRepository $jobClientNotifyRepository,
        AddJobHeaderRepository $addJobHeaderRepository,
        AddHazardRepository $addHazardRepository,
        TCAttachmentRepository $tcAttachmentRepository,
        SignageAuditRepository $signageAuditRepository,
        AuditSignsRepository $auditSignsRepository,
        DocketAttachmentServices $docketAttachmentServices,
        ExtJobHeaderRepository $extJobHeaderRepository,
        ExtTimesheetRepository $extTimesheetRepository,
        ExtJobRequirementRepository $extJobRequirementRepository,
        ExtDocketRepository $extDocketRepository,
        ExtJobSiteCheckListItemRepository $extJobSiteCheckListItemRepository,
        ExtSiteCheckListInputValueRepository $extSiteCheckListInputValueRepository,
        SiteChecklistGateway $siteChecklistGateway,
        PreSiteCheckListRepository $preSiteCheckListRepository,
        PreSiteCheckListItemRepository $preSiteCheckListItemRepository,
        JobHelper $jobHelper,
        InputHelper $inputHelper)
    {

        $this->manstatServices = $manstatServices;
        $this->docushareServices = $docushareServices;
        $this->googleTimezoneServices = $googleTimezoneServices;
        $this->emailServices = $emailServices;
        $this->jobSyncRepository = $jobSyncRepository;
        $this->jobShiftDetailRepository = $jobShiftDetailRepository;
        $this->jobHeaderRepository = $jobHeaderRepository;
        $this->contactPhoneMobileEmailRepository = $contactPhoneMobileEmailRepository;
        $this->notesRepository = $notesRepository;
        $this->requestRepository = $requestRepository;
        $this->jobSiteCheckListItemRepository = $jobSiteCheckListItemRepository;
        $this->siteCheckListInputValueRepository = $siteCheckListInputValueRepository;
        $this->docketSignaturesRepository = $docketSignaturesRepository;
        $this->addContactCalendarRepository = $addContactCalendarRepository;
        $this->addTimesheetRepository = $addTimesheetRepository;
        $this->jobClientNotifyRepository = $jobClientNotifyRepository;
        $this->addJobHeaderRepository = $addJobHeaderRepository;
        $this->addHazardRepository = $addHazardRepository;
        $this->tcAttachmentRepository = $tcAttachmentRepository;
        $this->signageAuditRepository = $signageAuditRepository;
        $this->auditSignsRepository = $auditSignsRepository;
        $this->docketAttachmentServices = $docketAttachmentServices;
        $this->extJobHeaderRepository = $extJobHeaderRepository;
        $this->extTimesheetRepository = $extTimesheetRepository;
        $this->extJobRequirementRepository = $extJobRequirementRepository;
        $this->extDocketRepository = $extDocketRepository;
        $this->extJobSiteCheckListItemRepository = $extJobSiteCheckListItemRepository;
        $this->extSiteCheckListInputValueRepository = $extSiteCheckListInputValueRepository;
        $this->preSiteCheckListRepository = $preSiteCheckListRepository;
        $this->preSiteCheckListItemRepository = $preSiteCheckListItemRepository;
        $this->siteChecklistGateway = $siteChecklistGateway;
        $this->jobHelper = $jobHelper;
        $this->inputHelper = $inputHelper;
    }

    public function getRelatedJobs($contactId){

        $relatedJobs = $this->manstatServices->getRelatedJobs($contactId);

        $jobList = [];

        if( $relatedJobs ){

          foreach( $relatedJobs as $key => $jobInfo ){

            $countJobSync = $this->jobSyncRepository->countByJobID($jobInfo['JobID']);
            $jobSync = Config::get('evolution.jobSyncEnable');

            if( $jobSync == false || $countJobSync == 0 ){

              //Get Job Shift Info
              $jobShiftDetailInfo = $this->jobShiftDetailRepository->getJobShiftDate($jobInfo['JobID']);
              $relatedJobs[$key] = array_merge($relatedJobs[$key],$jobShiftDetailInfo);

               //Get Job Info
              $jobHeaderInfo = $this->jobHeaderRepository->getCreateDate($jobInfo['JobID']);
              $relatedJobs[$key] = array_merge($relatedJobs[$key],$jobHeaderInfo);

              //Get Contact Info
              $contactInfo = $this->contactPhoneMobileEmailRepository->getContactInfo($jobInfo['ContactID']);
              $relatedJobs[$key] = array_merge($relatedJobs[$key],$contactInfo);

              array_push($jobList, $relatedJobs[$key]);

            }

          }

        }

        if( count( $jobList ) > 0 ){

          //Sort Job ID
          $relatedJobs = $this->jobHelper->arraySorter($jobList,'Id');

          $response['status'] = true;
          $response['statusCode'] = 200;
          $response['jobs'] = $relatedJobs;

        }
        else{

          $response['status'] = false;
          $response['statusCode'] = 200;
          $response['message'] = "No Jobs Found";

        }

        return $response;

    }

  public function getAllJobData($contactId, $input){

    $timezone = $this->googleTimezoneServices->setTimezone($input['geotag']);

    if( !$timezone ){

      $response['status'] = false;
      $response['statusCode'] = 422;
      $response['message'] = "Failed to set Timezone";

      return $response;

    }

    $jobData =  $this->manstatServices->getAllJobData($contactId);

    if( $jobData ){

      $jobList = [];

      foreach( $jobData as $key =>$jobInfo ){

        $countJobSync = $this->jobSyncRepository->countByJobID($jobInfo['JobID']);
        $jobSync = Config::get('evolution.jobSyncEnable');

        if( $jobSync == false || $countJobSync == 0 ){

          //Get Job Shift Info
          $jobShiftDetailInfo = $this->jobShiftDetailRepository->getJobShiftDate($jobInfo['JobID']);
          $jobData[$key] = array_merge($jobData[$key],$jobShiftDetailInfo);

           //Get Job Info
          $jobHeaderInfo = $this->jobHeaderRepository->getCreateDate($jobInfo['JobID']);
          $jobData[$key] = array_merge($jobData[$key],$jobHeaderInfo);

          //Get Contact Info
          $contactInfo = $this->contactPhoneMobileEmailRepository->getContactInfo($jobInfo['ContactID']);
          $jobData[$key] = array_merge($jobData[$key],$contactInfo);


          foreach( $jobData[$key]['DocketAttachments'] as $docketKey => $docketInfo ){

            $docpath = $jobData[$key]['DocketAttachments'][$docketKey]['ExternalLink'];
            $docpath = str_replace('https://', 'http://', $docpath);

            //Create App TMP Path
            $data = [
              'url' => $docpath
            ];

            $docketDocument = $this->docushareServices->get($data);

            if( !file_exists(public_path('tmp')) ){

              mkdir(public_path('tmp'));

            }


            if( !file_exists(public_path('tmp/'.$docketInfo['JobId'])) ){

              mkdir(public_path('tmp/'.$docketInfo['JobId']));

            }

            $filename = str_random(10);
            $pathInfo = pathinfo($jobData[$key]['DocketAttachments'][$docketKey]['ExternalLink']);

            $path = public_path('tmp/'.$docketInfo['JobId']."/".$filename.'.'.$pathInfo['extension']);

            if( file_exists($path) ){

              unlink($path);

            }

            $sitepath = url('/tmp/'.$docketInfo['JobId']."/".$filename.'.'.$pathInfo['extension']);

            if( file_put_contents($path, $docketDocument) ){

              $jobData[$key]['DocketAttachments'][$docketKey]['AppLink'] = $sitepath;

            }
            else{

              $jobData[$key]['DocketAttachments'][$docketKey]['AppLink'] = "";

            }

          }

          array_push($jobList, $jobData[$key]);

        }

      }

      $response['status'] = true;
      $response['statusCode'] = 200;
      $response['jobData'] = $jobList;

    }
    else{

      $response['status'] = false;
      $response['statusCode'] = 200;
      $response['message'] = "No Job Data Found";

    }

    return $response;

  }

  public function getNotes(){

    $notes = $this->notesRepository->getCurrentNotes();

    if( $notes ){

      $response['status'] = true;
      $response['statusCode'] = 200;
      $response['note'] = $notes['Notes'];


    }
    else{

        $response['status'] = false;
        $response['statusCode'] = 200;
        $response['message'] = "No Notice Found";

    }

    return $response;

  }


  public function updateJob($input, $contactId){

    //Set Timezone Data
    $timezone = $this->googleTimezoneServices->setTimezone($input['geotag']);

    if( !$timezone ){

      $timezone = Config::get('evolution.defaultTimezone');

      if( isset($timezone) && $timezone != '' ){

        date_default_timezone_set($timezone);

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 422;
        $response['message'] = "Failed to set Timezone";

        return $response;

      }

    }


    // Record request submit by the App for monitoring purposes
    $requestData = [];

    // $updateRequest = $this->requestRepository->create($requestData);

    $jobSiteCheckListItems = [];
    $siteCheckListInputValues = [];

    //filter inputs for IOS
    $input = $this->inputHelper->filterInput($input);

    $siteChecklistReportData = [];
    $additionalHazardReportData = [];
    $sinageAuditReportData = [];

    //Set site checklist
    if( $input['siteCheckList'] ){

      $checkSiteChecklist = $this->siteChecklistGateway->checkSiteChecklist($input,$contactId);

      if( $checkSiteChecklist['status'] ){

        $jobSiteCheckListItems = json_encode($checkSiteChecklist['jobSiteCheckListItems']);
        $siteCheckListInputValues = json_encode($checkSiteChecklist['siteCheckListInputValues']);

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 400;
        $response['message']['siteCheckList'] = $checkSiteChecklist['message'];

        return $response;

      }

    }


    $requestData = array(
      "IP" => Request::getClientIp(),
      "OperatorID" => $input['operatorID'],
      "JobID" => $input['jobID'],
      "Timesheet" => $input['timesheets'],
      "Assets" => $input['assets'],
      "Requirements" => $input['requirements'],
      "SiteCheckList" => $input['siteCheckList'],
      "SignageAudit" => $input['signageAudit'],
      "AdditionalHazard" => $input['additionalHazards'],
      "ClientName" => $input['clientName'],
      "ClientEmail" => $input['clientEmail'],
      "OrderNumber" => $input['orderNumber'],
      "GeoTag" => $input['geotag'],
      "Timezone" => $timezone
    );

    if( $input['parentJob'] ){

      $requestData['ParentJobID'] = $input['parentJob'];

    }

    //convert base64 images for tc attachment
    $tcData = json_decode($input['tcAttachment'],true);
    $tcAttachment = "";


    if( count($tcData) > 0 ){

      foreach( $tcData as $key => $tcInfo ){

        $filename_path = "tc_attachment_".md5(time().uniqid()).".jpg";
        $imageDecode = base64_decode($tcInfo['Attachment']);
        file_put_contents(public_path("image/tc_attachment/".$filename_path),$imageDecode);

        $tcData[$key]['Attachment'] = $filename_path;

      }

      $tcAttachment = json_encode($tcData);

    }

    $requestData['TCAttachment'] = $tcAttachment;

    //convert base64 images for docket
    $docketData = json_decode($input['dockets'],true);
    $dockets = "";

    if( count($docketData) > 0 ){

      $clientSignature = false;

      foreach( $docketData as $key => $docketInfo ){

        if( $docketInfo['AttachmentTypeID'] == 1 ){

          $clientSignature = true;

        }

      }

      //If no client signature found

      if( $clientSignature == false ){

        $attachedOn = ( strtotime( date('Y-m-d H:i:s') ) * 1000 );

        $requestClientAttachment = [
          'JobID' => $input['jobID'],
          'OperatorID' => $input['operatorID'],
          'Attachment' => '',
          'AttachmentTypeID' => 1,
          'AttachedOn' => $attachedOn
        ];

        $requestClientAttachment['Attachment'] = 'iVBORw0KGgoAAAANSUhEUgAAAMgAAAAkCAYAAADM3nVnAAAAAXNSR0IArs4c6QAAAAlwSFlzAAALEwAACxMBAJqcGAAAA/BpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIgogICAgICAgICAgICB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIKICAgICAgICAgICAgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiCiAgICAgICAgICAgIHhtbG5zOnRpZmY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vdGlmZi8xLjAvIj4KICAgICAgICAgPHhtcDpDcmVhdG9yVG9vbD5QaG90b3MgMS4wLjE8L3htcDpDcmVhdG9yVG9vbD4KICAgICAgICAgPHhtcE1NOkRvY3VtZW50SUQ+eG1wLmRpZDo4REYwNDE4NDdDNTcxMUU1OTBFREJERENBQkJFNTkyMTwveG1wTU06RG9jdW1lbnRJRD4KICAgICAgICAgPHhtcE1NOkRlcml2ZWRGcm9tIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgPHN0UmVmOmluc3RhbmNlSUQ+MUVFMUJGMjVFMzQxNEJBMDcwOTE1M0NFNzE1RUUxNDQ8L3N0UmVmOmluc3RhbmNlSUQ+CiAgICAgICAgICAgIDxzdFJlZjpkb2N1bWVudElEPjFFRTFCRjI1RTM0MTRCQTA3MDkxNTNDRTcxNUVFMTQ0PC9zdFJlZjpkb2N1bWVudElEPgogICAgICAgICA8L3htcE1NOkRlcml2ZWRGcm9tPgogICAgICAgICA8eG1wTU06SW5zdGFuY2VJRD54bXAuaWlkOjhERjA0MTgzN0M1NzExRTU5MEVEQkREQ0FCQkU1OTIxPC94bXBNTTpJbnN0YW5jZUlEPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4Kv12OQgAAJEFJREFUeAHtnXm83dO1wM+5U27mSRBUUCGJqXhmoVJF1VBDomi9luK1ZvVMERKJmBuq9cqnaDxNaypRUyOhSEUfSmsuMYsQkch4c4dz3/e789u//M4959wbtPgj+/PZd09rr7X22mvt+XdufvTo0TW5XO6i1tbWo6qqqnoR/1SupaVlGfXvp/JPwDlLvPjmT4WsTCX4y48ZM6aaogJ4C4LEPNItJs0bPnx49SabbJJ//vnnWw3JSuEtb+uEx+duvfXWnPDnnXdeSz6fD7huueUW6eVGjBgh/iJn+6RBvZZIswiARMQl7JAhQ1ozePLkVVtOm+Sxqm3dNvBti1elPycJ2FE/69Gjxykff/xxDsWYWygUSjqrI15UKFzPbt26VS9evPg10juA9wMVA/+ZjURDkEbkA5xVeNNpHnHbog/GE2ENVWAVOZtnHFjxlMBXym9bvz0c5WCTdliU5bsc6Kq8L4kE8ueee+4CRv7u8HMA/s/4bvgSpSGvrGtsbFR7q6qrq6eBZ+O6urrcsmXLnlmwYMGOEyZMWOoonBk5y+JoLzMqK+EQ4A7G30v8SeuMHTt2MDPXvkR/Q94H5hHuTPBd/Jr4V5ubmyeOGzfuRfIrGcMwFPdQYHvRjhmEvwZ2gfDEj8A7A91IGGYsDZV0PQPJsbT3deJ34fcjPZh0E2BVxDWA+tra2t/D37vET8S/BtxthLkzzjijZ6dOnY4B/j7KO4Nzb+ILqacRE62qJWxYsmTJDZdccslC4s4yq4wKIXzergbXHSWa17lz54fPOuus+TCg/8QOQ2sGVw6DWQqur4HgLvw3NY5KI/hKElFRNdhdVltttTFz5sw5HEXbGr+oqalpl969e1/80UcfPUq5M9bJ0J6AgeZQvLeIH9TQ0HAGy5jdWc5Mi8YKXDAWeD6DWe+iRYsWtWAk7xM/eOHChUdR7gyokVxG/jJwBwOhLfLSwiDQFaW+AjoPkb4LxT4VPnadP39+M/DVKHgBmGpoP0N8MXxcjIxzo0aN2hGjnkG6P7P2JcA3gKeqS5cuo4FdRn4n2iTvtnc++X8gXAgfzoyrDARhfN6uig51aZVHSZw5cnSEyyIVqEOv4lPfzqsRh/VRljpGvqb6+vrdyZ9knssb8Rn/tA68TfPmzcuhRBuB40zxoHxNS5cuzTF7fXzBBRf0Z8SeAO0XgP0KRjEApdsG/hrxE3/+85930lg1EnhxVhjEKK5xPEHYD/i1iR+NETpTjRY/eBbQrHnGdexRgpI6a0LX2WKB+fCxBGVfAr3Vzj//fNtZ17dv3zri9xDvAU9LHDyAu0x4XCO0THcC5krq9cSAtgSvNCfCSw082c5ZAsuv4Sr3+UvADXpwKFdco7e7qY3wSahduOQo6kA6voZOb8ZIDmXknkuHn0Ao3GdZKtSgPDkU8zHwjwTXZaQXJ7NWA6P5Hn369HF5dzb03qG8Dv8k/mJmhlHMMtvA8/TZs2fb5hbqfhNjE/48Zs5gBMD+mlnlfXB+aPtsG3CpjMzLuBoML2ziydMouoBvB2aJhdSre++99zSg6cKT7sLe7C8o/U7gPwTZTOnevXsOowj1oetsVY+hm9dIldazzz57rnVXuS9WAp9pVG/DegGFiUplUZVGglIcT+efnxhSmGWy9ShzFivJz8IEZFVVrRoDSnQ68AvxV5Hv7KBSWX9dlN19whvCYywBJ3RfxKAcmfub/8orrxjIZ4AH51um4aNGXhjR/4gSzwAPICsOBoTh1Kosn+Bu1tiAv4+BZjr+QcCnnHLKKZ0JGxgonC1uZIa4DZirSa/tUoq8LL46aeBCnxxzzDHuQ1a5L1gC/zIDQaE6q4iErsFVLnHXuBzBSEahdEejgIV4dKoyJr4AbKvLtQ5kUUDxVPTXgfsJ8e9R7zCNAvweEsxPDMgDhxwzRlAw+OlrGp7c7OYGDhxoIJ8LhEdR603PnTs3LL3OOeecgfC1rjzh8rYpcfnk2Dim0xCYGlZRwu8Kfy6VtmMfMdRDCoDCDER+NTh/DJ99yBsPbCN5WQMpmoV33333onRKbFXkc5VApeXDyjKhUtDv+QJLqIdRtq8SX0xeqlUgamGE707+mSjeRPYBjYSO1uH4V2Vkpll40UUXucxRYcpuRlGmVpTODWxfNro3Qe8EDONgDKSZPHS95i/QUPGPAsd08C4i1B3pETa8/d0EdaLiuVRzRjqc7KeuuuqqZdTZns31DBT8cvJOA5/8uOTRuUQMPIOjFZ69LwnLUoVA+RLKHwmQxX+qaL85PSj/EL4vYbY5HRoauwcAqSNtPPKX5q+KfHESKGsgdKQKri/bWXGp4eZ7uQ6FJcvRKMJ4FDWP0rWiuFXEm1Ro9Od+cK2LXwv/Bvib8T3I/x+U4iCUUuXygvH37Zx41blUYaQOSxHqnYZ/hLwaaPWk7uP4yZwm/QAl3AA6j4F//169eg1mA33e+PHj30tOsYK2spSaBtwUyk8hdFr5J/4kFL8Vw7maeJgNmak2A+8j4NJY6mnvm9A7mfwajC7MVuZzKtVVHoDTWJTbakTHE97XtWtXl4FhRsM4xtOGQ/r16zeAE7mQB4yuymUae5UuJl544YXs7GLWKvcFSKDEQLjBzXvaAy9ljSPLo0qDUyHiBd3MbLlxlGY2gTD6MBqzAe2Pck9DcQajLAVG5N4o3S/ZLD9w4YUXzqWOy69A3xtl6uneQtH/hjF9ZIJZ5FEUezxKugdGGDbZKOmhwIyFL5df2+NnkT4FXFdYh3aJ05kg4Kd8OOUXEh5CnT0oU8FPovy1a665pnbWrFlTUOzNyHMvoXM55v1GEzPAVOJPmkn9GRwP9yA0qQE3E6/Fd+K0ajHpp/EaYO7MM8/8mOXmKR9++OF4DDErrwaMYwZyCTOdt/TCr3JfrATynPh4UjOfztwExZglO4RfQylGEdWAnEmyTiXrQvmvGIVvBzYqs0aSHfXMb2bk7ovyayRXQ+skjQMlfxg/kNHa5ynxNGgJPGxMnXcyOLN0243HOw6BnIXYLziih2PYpKK8pUoH/5BbvgnXGGbOnFmfXMrlVoJ+Flc2npCqHGTpVoZaVfJlkUDJDCJjGMxmHEMeiGK7uS3ilbIcF1g5bso9Dro9LreIOzJnFdC0F4e1KMXpGNME0qsTf4QReEOMowkF7US62SNPRuBJlLdrHFG5gJMpfXokncx6OU9/1lprLe9dgnFkDCfljbIajDWHYeRpY9Wxxx7rXqBJ/ODRsJ1Bde7Uc8BWM5vkgXdmEE8WV56ymoQm4GmRM4s4zMjDR5XLJumCv6p///6t0HW5l1aQfqwjDGVZ4adtJb+ss768Zgsrvekqgz9brcO4M1x8vgOuIj0irfzSdrVB5kBaxCPpsOxN4ErK29QvSiZ8hJVBUcGKxCfCt6JaiBUUaNEMknSSyr0RIC4psp0U69ugfwITlkwxs1zo6IwiNAG7Jrj/wsyxAUqpcbgEacIAa1ki3QkfPnXpyAVFi8Yg8AknnNCJS7mubpzB661zKmziNdl0e8iBizNhCmZeZskZ8slTJt28KNx6660XZXmhrCy9KNMUcQeRSvCV8hN0FWeytvXapjtgp2JxB3jK8VMuL4u/o/IsbFE8MxBm8z81voikyPLNRFEDUjo7rJkjYNtQ4ZBXUl846oYRRIXROEaOHDmAmedhlHhANA7Awjrd2Yh1+jRgdyTvCPyJxDW8ksaRH/ZHhOslsLsQrgcv3cFL0DqPspehNZkb8UknnnjisozgYrtU8BF431O1YKB5jHYyS785tgkXRj6XaeAKMwnhpsAfRvlOeI+Au7Khbmb0msd+4mVw3Ev6Zt5YLWx7yBBxMoMOhd5m1F3KgOA+ZT55NxMWuQgPza2A34HCJfgu0HiYus+RX2LIsQ7L2X7M2A40jqh62/gK9R6IMNYnXeBCczMOU3YFRvzlBkGyy7qAl5J/gmeqdz09e/YcTjrIkxBWqyZD54NIUywxTv6a1NsXGRSAq4a/xWuvvfYt6olgyTu1A4mrW8LYb+WcfLhKeIvZ+GkGKgfhVDYZen2g8UNpAUu06Gi9HF7zpNsZHh8uq+CVamXzaaSKlI7W2TLjmZljXZKPsqz6SjQOmPcpSi3pJ1lavQOuqzAeb8lfxEBtqAKNS5SAOjae8HgaeRmG1YkGeDoUyv1Dh68BnUHg2597jbNR3mMQ3EMaCcubsAREgXpiEBM1TOsjCOm+QPU5wKoo6dsxaKnIP4OX4zxhivTkTQc9n7f4NOUAcAA++mT8bfi0o1iGBJzweRyXl4dwKOFRc6ALf40YyR3ApzMP7bcjfdN1CEZ+OkvZnEtQntmcRf5zLOdCRxNPXazDgcURnMpdJg1kkIM3l8JzTzvttAGkPX7Po4ihPrzvCz8XIH/bkeLqKILswxIbfqYCOxUee0B3YnJSF3BB/0XKPojyFGfkET4GI8trlSWnnA6Oja+99trdgHwsHGX9yb+ePgryth26KPOQSP7Ii/2PTF5FhmfhU9lH2tDpD87LbGM5HFl8MS5e24MOTSgxEIioSHUA7YrydCeUQ6JVLsW8OAthRBbDJF9JC/snOuIj8KwDU4/S2K+gmI00VoVrxDjq2IO8gnJsA8y2KOt3EOqz1NsRGOmHUS7ijrMA+cfT2Kuom8MvBXdnlS0KUaG79AGPjwU3JP0gSrg3wrqPuoE2dWhGYS4nRl4gylM1cI5e4a0Vm3uV2z1MD7KmQG87Fa4SPfK9zW9FGdemE25FEU7mdO3KtjMJuBb4/gplapAWsPXweQFwvgbWysMMZ5kOlItUXuCpFt7JOdLn2Osst04Ty51r7DBKUOcg66A0DcSrkYWXq32R7+6ATlZJY31ksFh+6BZvOLPHzcF4I3LgYjSE8NxMnqd64VEr8vMFxYfgWo2403gtbQvyzFaMp5GUNyFPglZh7JMPkHHaJnj34agPUT3udpXhPjC8+VseXf7qgni4h6KMJtZuCF+30tdHIosb8Op1gcEphx4000Z1pVMGX6VZCZDgHKDsn0UlBkKxzO7Fun6y1gkjy6t08BeEYcTC6u6Hwd/h3XNoHOtqHFTPGserdNbGjBSbwMRfEa7LlF2osygaQyRHXhUKrsJuSN7P6HSVpxG8nZmBPgb1FPLfIM8XtluA02WQb6yWITiFch2XkIM9XhWnozF1fFzpfGv7vYWPjdQ44pR0J4a8HYa0DNjqhN4S0E4hPZN69dDZGnrbSw9ZNeIBq7sCHC/j77ct0tTRPumo8NKswSh9qzYYdzLpyzlcqLn22mtTxQI2wAtLVYLyyyAMrMrNMvQ8edzBPiO03fZdizSh5RJoskoa71fAWQuvyqKLMNElBgGK5Yoo7YR+AKE81KMdLlWtr+wCj5ZB0+f+UZ6hTvYPvOSRmUof6pC2TgrigAfuWmiKR8O10EHF9W/QR/INO8O/d2DCh8GXvKs5/v8zy8fXk6c6BelBK/IV8GXppYQzEeCD7GlbdTkDEfRxptAxhN3wxUOIpRmnIHGeAGnlHzErXMx9Rj/ibsjXowFuyIuMg04ahIFsggI/CxOvIovt6eD5+BqMISpopKJ2yMMIl2UYSAP16hHKU9TbjzqzIqAh6X0IJuG7IZzF8NCfEcvvSK7DB6eAs0oR8wmDJoLjdEa13ainYfuytpp2TKUdR1H2VgY+x6j1bfJvgpde4F2C74IfD8z9iWGXlTF18rTdjj6H/c8kLzLBHeiLH96zZCrGff7iSIk7EJ6dQRvhpQ68GkoeORnuaZ/AzxxntgTZJAYm73EagVEJa5DJXGB9oXAMvIUBhrIZ+JMoC0YHXIFZylliToInPBVK4p84gM909oiVYx6hfIn/2/Tji8rWND7PQCA/h1I2Cp7r8K5M6umno8k/m5nSuqmjXEPXWNy3uE97h3QncJbQp22tDIwOYrNLOk8B0lEfgGA0/lM5FHgGvG5AZzn62pBGhF4H8y+THkKnDkmM4yVgtvX+AZrpOrwN0WCgNGwLG4kHRWj72dTx015HsuY4OpK+myXOBNbX56IAXXnJm+MycF9gUgNpgz9NUtdnMH3IOA1eQ35iHI9h0N80g/Lw+axxFZMB4R6M5DA68F47UB6huSXwe8LHn3zjBWhbo7e6G9RGlLoX8fPxR3tixuBh2cq6sLySJ+ge5OyBc3ns5eSHdPQA8ptog7f6e5M3MTEo6zmwFA0upG3fK9QLu1kUyKwPGZWfMPJFOWacmSNHjnyvDP1z4XcD2nc4S13XzoL8h3/Yl5QofqI3LcA/HV9wC9ueiwZSxSgRRhYE6LfcQTIQDxTbQwBMFcpY64kR8Y2A/QhGx2J9v4Eh7zmWYSydULiXSG9N+eYYx9OMUCGNcfjMpJJxZEk7emTdaiao63o7j0LKf+Ab2pOYAS1v4NFiV0JvslfW7YMA+8FvWCMjF5XFLwKl5RN6Z5XgErouy9zjXEd6EwoWoKh9aauzb46lalFHWQcHysJiwu6ux1HIH7Es+DUj/F89tqZaOYMSXZFjCReWn2TujBINgW44/EBZfI18C3K+lnYE+ij7IcBNxNunFLc687g8I5nLDR06tMY+JBqWThoHMBYFHaF9YWAQnkE0PIUhb6X4FMlncbQrvGSQB/AEnYw6R/px5Hh4xI9c64zHvVbMj6HtRiY+EfLEU9iKOs4A1xINJNYPT7oRQvywqOwSi5E5DwMFTkhkfAmCbYDYOjD3CIJ1mTWEUdUnJDeS1jiepRHDgFkC0TtQutdJb0m6wbUiYbr2ThlJIo6qRmnYu9QJ6+Fk+eFa3wbeS74zXtpZ48aNe5n0cfjUOTParjSjQgQ635AOoSONJ22PMYI+pWBxqXEk+MTiaKyR/KgcSvhPaYLDJZWINOYJ+OOJ90GRpXch6WHQq9hh5fCbR93h4kCuzcjb9fYD+JvB9QtCZ27BduOl8lfhc6b8ki+dlDdnFoFwRfTBHaxEZaFOiGsk4lgO/u//i4EE3jjOzfPWLiwpI7/oXHgPJ2945fuuHAlbjjPhmO2dYW1D2p/lYCnPlxiISsRotgNT/R1RONnK5OVZQjSzbOkDsdsZub8PorVg9DEIr0EHrUHa8/pNMRLP70+i/s6kXfvZgIMI3iCtUdXgKxqH8JlN5VSSx0ofgfl8tx/+BmaieXSeM4Svc31L9XdwvmNdXXJOL41wqhEy2/zB4LLCHJQYYDBG+H1CcOSiQqQKpZxUlA5c3B8EMHAZOnB4iDCNeJ4TplGsdxvI2w2+R+BvCcAd/FEOuJZLL720KwPWPs50OE8H3b88jFEvQC72ydeh1cBSrp4yl5q+S7MtRYbAkiQrA4q/PI5+DgcsyV1JYEz5I6t1SPzQtiOLYCDI5S4B+GCtqD3KHh0Vrhs6czu62YC++C1SMHrrKFOyPMHqSXgn8ru0xEAEdJOiM97W2SkQ8gjsVcr+GyZd/05jGeU9h0eeeeK+63qOcg3jGmHwP8Cqf0sj/ybOZK+TjvrmlXPUC0pJeBsMP8gp1DA2ibBQWKYywEdvlGAY4TAVG2GpGC7k3VRPpJ58Bnrl8Jvn6KvjvsCTsL4K0naYR/xNw2ioxnUMIlsjp96Uh5OiDLxyq4OfV6D9miNdNCRgQt0kdHYeh3KfDGz3xCjPp47HvnZeUQeHipk/yZKyGePaDf7XdXmFHGoJn2XwekZQ2vJH8HzduPih6+DkzKtMxV+2j8n/Ujj4lT831r+iT+cR9/TRbPNdcu9KH/SlzT5X6oxeTGf1Ei5fszM3cNF5JF2Lwe2Kj3lFoX3vHQi43ragZJr0aBIBPs4Syhvjgfj1k9BvPfQDQbA+MJvjZ4PjbkanQRoHcU9PoiUvRYFDJ8PU/9JRN7z55pth3yANRwAZWAkX7kWEg86BKNTN0KhyX6Mn7lFjM7QakJNriR4ozPaUnUNjX2CkGGldFLWiMgAX+PQ7ckC7KX/4tZqhHVPiyP8Vo/4DhA/Cw4OE/qqL/kFeFfu8/xgrsVYOe7ssAvFDszPyU2ZjnVFQ4KWEG5PvjKtz4FkeK/MXBYiFI1ASIcKPZhDe67ctSZUp7nGI1yMjDWQnaG5K2g/USvreOlEWxnXUaddQl0P9a/9Gmoku5enPfTH+IwgPQ7EPJ/we6QOh2he5ufSrQaGvJ9xTTmij796ifD4TcyVmhODCV39sGJ2zw7xdhkIQGlY9BkXcE930mNe1byNLBn+0YTr7kz38oo4jzDWovx/tWEKjQr1PyjwN9qFeVXKX8V3iF0Fjf+gNA/emKIjr+NAWp1u8R5eOoHUY8zjgvZ2/pEw7QhZKHYSJkWu0nqmnoCiMm+YSB4zfuqhADmB+RRmn8CbSGlqYHeMnvm0RAB8GCMIrOcn7PvxvBn0N8ix4nQD8InGWc5SHzTlHw31R/L0S5e8EHts8KdYBzqXudOQ+FNwN9I3LLBXruezMFuENoVmkWOArSmdh/13xtjRJB1IxNKHsDegfWA4HCt7xhIGBNjuoruhEIZc7BeoHdk8RerdSImD6swXd7UX4rFVKDIQlULiwgsB6lN8E4AD8D5i6ppEX4AmbWWJsA8Pn2ik4124FRvg60vew/jsgXnpR19tQbzK7CvhpHTSDkVifuEsI/Rjiq6OQm+C3Ir0dtHZCIdbSQKQNP47EZ/GrJxM5KnyfGcgPuQBd4YTVsQ5fyjJwHji+Qp2oGGuugCyKudGVRvjEOAG3WrmOKapogo4N+OHfo+UzwXUPdR1gesLzaMrfK6m0IsOOLcD33rS1nwMUBuby6iX8YvCtR+hSYi5wT4J7qHwlSqWBjAVGA5bX2E6i/37HYFZED94qyiuRZSvtvAcwZ3L70hVKLfLZCFxbWJ14C7P2UcyqDmbfd3bknVxRY6gWVgWECynw/syDnQ5diQWp2G78QPRXar+GfwwmxieYUniYu5w+UUGWYhw1xH3acCWE9xFHcpPpuyNAgnMdkNZP8H2iANwaSdhcEkZj/QDjfQh/OX4EvA5ktDwdisL5SyjN8NYLIW+TECvhIXaafFP/ddIBFBwKdUsTPqtO6sfgRyjmzsAMRfE2p955tJVg+We4Eaij0GNd2nIvCj2Z+s6+yvTH4NnbGYV4yRINnEEGlHl066zhwYXRDeD9GfKfN6TtbxE/Fj4tq0MGfi25BfR2MMNjYsN/lYONsLwGXwnezB4uXGIKm9CV8dCetnzYLpw4j4XnI/CHsfc6nHAEfe3LgbHqoDAMfFb/DgNhf5fv8ZSrLU7qaJBRd0r4bAtfPJQmpRAbJyKYkanryX7ZIuLhWIz8b8HUUATewqzhk485+ONgOhzruAFH2UKP0TmtLMOC4ICvOFokpIsC6Hl8qlGsRmdPonPD3QK8fQzewwCe537GSnaASgysb5YuJXQm2R+4sEYlr5dwK+Gm07b9gaty6YLbHVzr4N/Bp/cgxD2ESB3pEzQseIsdn5a1F4k/IoGRjYTXfaBte/yVx71QeuVWJDMvHtljNHNkuz6kdkuWV9bxZbKnK0FG0rSq/BA6i9vXYSPPQOZm/bGM0gpe1lm9bMGKTK3PX+fsTf96QldNGPaa2WVc5pRsdWdw2uo7Md/Bzad+uMuCx0qy813cLAdd9sYtzPLVyYnW1dD6KfXcsNvPvp5Yl7izb3vKH/W+hn4ra5zUz7nPi4CmA0Iq+FGTJyvDzUQx9yB+JPknkn8A6dGEh/Jq1BemBRo6EcbOwjjCb/GyL5HRFgWLy/EU2iNNUX0W5/OJbbHFnioEBmfnDwXhXVOnTq2Kl0LZDqHMfUBw8GI7wtl3zKsUOpKjQH6GW4ewvXjrCs0rgD8YGTRqkCqWikq7Cs465Cujo6knX+VG/Erk/PUVjyvtqOfxv4DeSeDRMt3TlSgnyhX6iXBflLEL8lf5XTZWq3i2NevEQV54s4QMwg9IkLUvtNzrhFNHQbJ1ViZO3VCHcBF9/i7tHwAeT/RqkZevDu5SwRKlbiWurOQv3DNR7ubB/eI76E4zBuRRYqU9b1BijQN6DpiABqec5oOnC33lhav4sjqdgK0IoO8PboRjY/CEEXBFaWmsHLK1ALuFyrdxlr4+hLXem/B+lutpzRRovJS81foDs8k/RKsgqKOymEydvyiSLD1sZEVrTStkIuBSGM4iHt0+jPLuhwAW09HuZ65AsG9zSvV0pkqIkncckW/RUVppJ+r5rijwyaa9UMlgkyf6r0DrRvYCRzKTCuvL4IPI+yO4TuDw4o2EXpghoeVo7C26nR++L0nKP0kQ5AKdC+i879HRHjW7Cy0xtkGDBgW60BoOjDQciWtooxewzuA96KOwgyWuMTk6f5XwXPJ9MVHAkAYSOsBMo/+kEXASljjaXMl4nK01bOv+HzzvCGwe/sXxX+RPQ1Z3Eo+8+G5tOLz8UBhhYccl7GNWQEfaG/EFSR19IW2fyi+jP9sqeUUdg7b0aujbXaj/HrrtnqUivARTA2F0jgz8A6H5Iwa24XUQXULc+4FRxN2cT6QjZxO+boU4QhDVutNGsg509HPzVKVCgsN3yZWELaqyjpEnjKLQ/yV+PxroY8BGOlnjfULDoeKrlDlreau6FWU+uxCfAqznruB38DvTDOAqCoTDhcAfo/hp7AW+QTgAI4P9ZT5Y3Ie4v/H7IHTeBI/C/Rq0tpKWnY2S+L4qVQjprYxDbg4ENZz4zSEch3wnoETyWWIgLi2A2RwedkTJCVr9ItNTnQu88a9ED+U8BF43po6y81saVwjTKsEnxlepOOTHfRmymIiMToYXN5xNiMHPrO+Az4cIfX/nLDaYYBcrgttPErzhF/a35hGv2C+Wl3PUaebS2n1jWgw++6XECQMP0lBH7hIAWIN2XTSQAp0cliA0KjBqm6xJelwWA50wI5t2iZFNt42jVH6oIy7Ps1e0pC1ghTSjUDA8+JiCH4Mhn4dg6uDX49hqcA5D2MMSAYQPaGIZsPWMMn/HSMJbKklwXu43Ah4PhhHfPOLRcOMR9zxoeXz6IEbSnyVPgXgDNDpBz19it26ghaI1MSLVYoTTKPfAYh8VNmmz6KPzksq4N7VeJEaaoRzDs50eBlzFoHAUdDaFZlibAm8/pQoEvwfAlwOPv/nr8uK5l1566RnqV7FUSb/5CIgZBMmX93tQiI2RTQt4bfNe5HfB+3GGH1IFcGWBdyBxwDEvpRsAMn/cDLvfBIe0RyHnsQwsnjA5qlfRht2guZtVlAnG6ahVIK+OAcefcPop9WbibV8YWGiTH1LZPxL3JW/JgEMeReH9mF91OkrIr4YinrAHYwmc1TW/YfG00TbFfgg4OvrjKc8iELspHAqjDyUEUqHQqGpnF5WEBst0nCVSmApEhHPDvC2MuWlbgBK1nQ4rVC3Ohq/QU4SjGQnfhtdxCDgcv9LhQfiJkMKoAJ+e2PgbU7cj8KNYii1Mlk8KUb76oNTW87uD8H2IFBF2HhIqqkr1EiP6luC9lg7bD19vJ0sv0iJPerV09N3QcF1/Oje6brRVhjAlx0046LtJE3kThB+9SPdI0qZOumShjWfShrvhzefY4Qs+lNqNaA4aLnmPpVz+u/h9C8vde5OTm6JvSoRnzxRkB567wHcqcosvjgcwWBwMyI3JMktwZ8LO8gf/gV/oSq+ig27QA/gaB9+e+Y9FXzrLd5SX8tAzMIVvQOiThcjsv5HZNciSonyzhiYRZIpIq7syAChnDx2qgW071CurcBfEwDKbWWQIfR34pk2nUXYvqMLADQ/VtLnevoIn93Wh/6S1Mk6LuxYhn8rlyB+If4T3fD8IVQR2hEph+EmcDQdea+3Nhj7PXmQyv3nlEiKcTH0SXMDKj9O0gr6O75ZvIzyIBg8jfyM6tU9Cz0u6WfhnyL+NDgjrWzfWycVnjk9EF/AU4ycI1FerbhTl83W8yuSIlYPHZrxG8j7Z+4NnV5Y8I4hvBZ1+wMjLApToefJ+j4LdTai7G9zhNIn4dDPWXHPNuL6/Hhl49LoUGL/fjnundKCRrnWgdw/x/yS6OnDL2Mj7pu0By3DyfQmK4JFtK2Ua0W2hhPYkYRrQ7pDHD9U9DqzTRDfgm1BSDe7tBLDABjjWuRuDc2BbBL/SDSeYyqWCC31jGXxfzl3TLeB2iT6UOusTemVg8SLoKmflMgnYWeQrSstS2ROdjVKfgFc3w+xOf/piI4dMAiLj0YFzNPKcQnoJMtGQPJDo7e+rCYOxvUf9k/Dx/7aYvdIur+UypbvPOBor6540ZqURtAdo42mAS7eb8KfSwDCdk1/S0PbwZMvAoeJGpQtF7oP4hZEcLz0LLskiPHBhtiMsUZwIkwntqSK+lA0nY45Waf1IKzlmjNVLfm3FgkQBCIIiFOHOlkckHYW2J8tLFr4SjSxMuXi2XjbeBrZENm3KQxLe2vaNP5VU89RTT+XaLsXLwIqjPTolZe3wG/jpAF+EaTf8f87JSBcz2ukKAAAAAElFTkSuQmCC';

        array_push($docketData, $requestClientAttachment);

        $input['dockets'] = json_encode($docketData);

      }



      foreach( $docketData as $key => $docketInfo ){

        $filename_path = "docket_".md5(time().uniqid()).".jpg";
        $imageDecode = base64_decode($docketInfo['Attachment']);
        file_put_contents(public_path("image/docket/".$filename_path),$imageDecode);

        $docketData[$key]['Attachment'] = $filename_path;

      }

      $dockets = json_encode($docketData);

    }

    $requestData['Dockets'] = $dockets;

    //Calculate Travelled Kilometers
    $timesheet = json_decode($input['timesheets'],true);

    if( count($timesheet) > 0 ){

      $calendarList = array();
      $timesheetList = array();

      foreach( $timesheet as $key => $calendarInfo ){

        $traveledKilometers = $calendarInfo['TravelFinishDistance'] + $calendarInfo['TravelStartDistance'];
        $timesheet[$key]['TraveledKilometers'] = $traveledKilometers;

      }

    }

    $input['timesheets'] = json_encode($timesheet);
    $requestData['Timesheet'] = json_encode($timesheet);

    $updateRequest = $this->requestRepository->create($requestData);

    //Get Request ID
    $requestId = $updateRequest->id;



    //Check if Main Job
    if( $input['parentJob'] == "" ){

      //Check existing Site Checklist and Docket record within the given Job ID
      // Please take note this is only temporary for testing

      $jobSiteCheckListRecord = $this->jobSiteCheckListItemRepository->get($input['jobID']);

      if(count($jobSiteCheckListRecord) > 0){

        foreach( $jobSiteCheckListRecord as $jobChecklistInfo ){

          $siteCheckListRecord = $this->siteCheckListInputValueRepository->get($jobChecklistInfo['ItemValueID']);

          if(count($siteCheckListRecord) > 0){

            $deleteSiteCheckListRecord = $this->siteCheckListInputValueRepository->delete($jobChecklistInfo['ItemValueID']);

          }

          $deleteJobSiteCheckListRecord = $this->jobSiteCheckListItemRepository->deleteByJobID($input['jobID']);

        }

      }

      $docketSignatures = $this->docketSignaturesRepository->deleteByJobID($input['jobID']);

      DB::beginTransaction();

      $email = Config::get('evolution.evolutionEmail');

      if( $email ){

        $docketInfo = array(
          array(
            "JobId" => $input['jobID'],
            "SendEmail" => true,
            "SendTo" => array($email),
            "SendToDocumentManagementSystem" => true,
            "DocumentManagementSystemEmailAddress" => $email,
            "UploadReportsToFtp" => true
          )
        );

      }
      else{

         $docketInfo = array(
           array(
             "JobId" => $input['jobID'],
             "SendEmail" => false,
             "SendTo" => "",
             "SendToDocumentManagementSystem" => false,
             "DocumentManagementSystemEmailAddress" => "",
             "UploadReportsToFtp" => false
           )
         );

      }

      // $docketInfo = array(
      //   array(
      //     "JobId" => $input['jobID'],
      //     "SendEmail" => false,
      //     "SendTo" => "",
      //     "SendToDocumentManagementSystem" => false,
      //     "DocumentManagementSystemEmailAddress" => "",
      //     "UploadReportsToFtp" => false
      //   )
      // );

      $clientData = array(
        "HasData" => true,
        "OperatorID" => $input['operatorID'],
        "DocketNumber" => $input['jobID'],
        "Timesheets" => json_decode($input['timesheets']),
        "JobAssets" => json_decode($input['assets']),
        "JobRequirements" => json_decode($input['requirements']),
        "DocketAttachments" => json_decode($input['dockets']),
        "JobSiteCheckListItems" => json_decode($jobSiteCheckListItems),
        "SiteCheckListInputValues" => json_decode($siteCheckListInputValues),
        "DocketInfoes" => $docketInfo
      );


      // $clientData = array(
      //   "HasData" => true,
      //   "OperatorID" => $input['operatorID'],
      //   "DocketNumber" => $input['jobID'],
      //   "Timesheets" => json_decode($timesheetData),
      //   "JobAssets" => [],
      //   "JobRequirements" => json_decode($input['requirements']),
      //   "DocketAttachments" => [],
      //   "JobSiteCheckListItems" => [],
      //   "SiteCheckListInputValues" => [],
      //   "DocketInfoes" => $docketInfo
      // );


      // print_r(json_decode($input['dockets']));
      // die();

      // $clientData = array(
      //   "HasData" => true,
      //   "OperatorID" => $input['operatorID'],
      //   "DocketNumber" => $input['jobID'],
      //   "Timesheets" => json_decode($input['timesheets']),
      //   "JobAssets" => [],
      //   "JobRequirements" => [],
      //   "DocketAttachments" => [],
      //   "JobSiteCheckListItems" => [],
      //   "SiteCheckListInputValues" => [],
      //   "DocketInfoes" => $docketInfo
      // );

      // print_r(json_decode($input['timesheets']));
      // print_r(json_encode($clientData));
      // die();

      $uploadManstat = Config::get('evolution.uploadManstat');

      if( $uploadManstat ){

        $updateJob = $this->manstatServices->updateJob($clientData, $contactId);

      }
      else{

        $updateJob = true;

      }

      //create timesheet extension
      $timesheet = json_decode($input['timesheets']);

      if( count($timesheet) > 0 ){

        $deleteContactCalendar = $this->addContactCalendarRepository->deleteByJobID($input['jobID']);
        $deleteTimesheet = $this->addTimesheetRepository->deleteByJobID($input['jobID']);

        $calendarList = array();
        $timesheetList = array();

        foreach( $timesheet as $key => $calendarInfo ){

          $calendarRecord = [
            "JobID" => $calendarInfo->JobID,
            "ContactID" => $calendarInfo->WorkerID,
            "TSTravelTimeToEnd" => date('Y-m-d H:i:s',$calendarInfo->TravelStartDateTimeEnd / 1000),
            "TSTravelTimeFromStart" => date('Y-m-d H:i:s',$calendarInfo->TravelFinishDateTimeStart / 1000),
            "TravelTimeFromDistance" => $calendarInfo->TravelStartDistance,
            "TravelTimeToDistance" => $calendarInfo->TravelFinishDistance,
            "Odometer" => $calendarInfo->Odometer,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          array_push($calendarList, $calendarRecord);

          $timesheetRecord = [
            "JobID" => $calendarInfo->JobID,
            "ContactID" => $calendarInfo->WorkerID,
            "FatigueCompliance" => $calendarInfo->FatigueCompliance,
            "RegoNo" => $calendarInfo->RegoNo,
            "AttendDepot" => $calendarInfo->AttendDepot,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          array_push($timesheetList, $timesheetRecord);

        }

        $saveContactCalendar = $this->addContactCalendarRepository->insert($calendarList);
        $saveTimesheet = $this->addTimesheetRepository->insert($timesheetList);

      }

      //create job client notify

      if( $input['clientName'] != "" && $input['clientEmail'] ){

          $deleteJobClientNotify= $this->jobClientNotifyRepository->deleteByJobID($input['jobID']);

          $jobClientNotifyRecord = [
            "JobID" => $input['jobID'],
            "ClientName" => $input['clientName'],
            "ClientEmail" => $input['clientEmail'],
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          $saveJobClientNotify = $this->jobClientNotifyRepository->insert($jobClientNotifyRecord);

      }


      if( $input['geotag'] != "" || $input['orderNumber'] != "" ){

        $deleteAddJobHeader = $this->addJobHeaderRepository->deleteByJobID($input['jobID']);

        $addJobHeaderRecord = [
          "JobID" => $input['jobID'],
          "RevisedOrderNumber" => $input['orderNumber'],
          "GeoTag" => $input['geotag'],
          "created_at" => Carbon::now(),
          "updated_at" => Carbon::now()
        ];

        $saveAddJobHeader = $this->addJobHeaderRepository->insert($addJobHeaderRecord);

      }

      //create additional hazards
      $additionalHazards = json_decode($input['additionalHazards']);

      if( count($additionalHazards) > 0 ){

          $deleteAddHazard = $this->addHazardRepository->deleteByJobID($input['jobID']);

          $hazardList = array();

          foreach( $additionalHazards as $key => $hazardInfo ){

            $hazardRecord = [
              "JobID" => $hazardInfo->JobID,
              "AddHazard" => $hazardInfo->AddHazard,
              "InitialRisk" => $hazardInfo->InitialRisk,
              "ControlMeasures" => $hazardInfo->ControlMeasures,
              "ResidualRisk" => $hazardInfo->ResidualRisk,
              "created_at" => Carbon::now(),
              "updated_at" => Carbon::now()
            ];

            array_push($hazardList, $hazardRecord);

            array_push($additionalHazardReportData,$hazardRecord);

          }

          $saveAddHazards = $this->addHazardRepository->insert($hazardList);

      }

      //create tc signatures
      $tcData = json_decode($input['tcAttachment']);

      if( count($tcData) > 0 ){

        $deleteTCAttachment = $this->tcAttachmentRepository->deleteByJobID($input['jobID']);

        $tcList = array();

        foreach( $tcData as $key => $tcInfo ){

          $tcRecord = [
            "JobID" => $tcInfo->JobID,
            "TCName" => $tcInfo->TCName,
            "AttachedOn" => date('Y-m-d H:i:s',$tcInfo->AttachedOn / 1000),
            "Attachment" => $tcInfo->Attachment,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          array_push($tcList, $tcRecord);

        }


        $saveTCAttachment = $this->tcAttachmentRepository->insert($tcList);

      }


      //create signage audit

      $signageAudit = json_decode($input['signageAudit']);
      $sinageAuditReportData['SlowLane'] = [];
      $sinageAuditReportData['FastLane'] = [];


      if( $input['signageAudit'] != "" && count($signageAudit) > 0 ){

        //Delete current signage audit and audit signs record
        $deleteSignageAudit = $this->signageAuditRepository->deleteByJobID($input['jobID']);
        $deleteAuditSign = $this->auditSignsRepository->deleteByJobID($input['jobID']);


        foreach( $signageAudit as $key => $signage ){

          $sinageRecord = [
            "JobID" => $signage->JobID,
            "Landmark" => $signage->Landmark,
            "CarriageWay" => $signage->CarriageWay,
            "Direction" => $signage->Direction,
            "TimeErected" => date('H:i:s',$signage->TimeErected / 1000),
            "TimeCollected" => date('H:i:s',$signage->TimeCollected / 1000),
            "TimeChecked1" => date('H:i:s',$signage->TimeChecked1 / 1000),
            "TimeChecked2" => date('H:i:s',$signage->TimeChecked2 / 1000),
            "TimeChecked3" => date('H:i:s',$signage->TimeChecked3 / 1000),
            "TimeChecked4" => date('H:i:s',$signage->TimeChecked4 / 1000),
            "ErectedBy" => $signage->ErectedBy,
            "CollectedBy" => $signage->CollectedBy,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          $saveSignageAudit = $this->signageAuditRepository->create($sinageRecord);

          if( $signage->AuditSigns != "" && $saveSignageAudit ){

            $auditSignsList = explode('~',$signage->AuditSigns);

            foreach( $auditSignsList as $auditSignInfo ){



              $signID = explode(':',explode('^',$auditSignInfo)[0])[1];
              $metres = explode(':',explode('^',$auditSignInfo)[1])[1];
              $qty = explode(':',explode('^',$auditSignInfo)[2])[1];
              $afterCare = explode(':',explode('^',$auditSignInfo)[3])[1];
              $afterCareMetres = explode(':',explode('^',$auditSignInfo)[4])[1];
              $afterCareQty = explode(':',explode('^',$auditSignInfo)[5])[1];

              $auditRecord = [
                "AuditID" => $saveSignageAudit->AuditID,
                "JobID" => $signage->JobID,
                "SignID" => $signID,
                "Metres" => $metres,
                "Qty" => $qty,
                "AfterCare" => $afterCare,
                "AfterCareMetres" => $afterCareMetres,
                "AfterCareQty" => $afterCareQty,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
              ];

              $savesAuditSigns = $this->auditSignsRepository->create($auditRecord);

              switch( $signage->CarriageWay ){
                case 1:

                  array_push($sinageAuditReportData['SlowLane'],$auditRecord);

                break;
                case 2:

                  array_push($sinageAuditReportData['FastLane'],$auditRecord);

                break;
              }

            }


            switch( $signage->Direction ){
              case 1:
                $sinageAuditReportData['Direction'] = "North";
              break;
              case 2:
                $sinageAuditReportData['Direction'] = "South";
              break;
              case 3:
                $sinageAuditReportData['Direction'] = "East";
              break;
              case 4:
                $sinageAuditReportData['Direction'] = "West";
              break;
              default:
                $sinageAuditReportData['Direction'] = "";
              break;
            }

            $sinageAuditReportData['LocalLandmark'] = $signage->Landmark;

            $sinageAuditReportData['TimeErected'] = $signage->TimeErected;
            $sinageAuditReportData['TimeCollected'] = $signage->TimeCollected;
            $sinageAuditReportData['TimeChecked1'] = $signage->TimeChecked1;
            $sinageAuditReportData['TimeChecked2'] = $signage->TimeChecked2;
            $sinageAuditReportData['TimeChecked3'] = $signage->TimeChecked3;
            $sinageAuditReportData['TimeChecked4'] = $signage->TimeChecked4;


          }

        }

      }

      //create presite checklist

      $preSiteCheckList = json_decode($input['preSiteCheckList']);

      if( $input['preSiteCheckList'] != "" && count($preSiteCheckList) > 0 ){

        //Delete current signage audit and audit signs record
        $deletePreSiteCheckList = $this->preSiteCheckListRepository->deleteByJobID($input['jobID']);
        $deletePreSiteCheckListItem = $this->preSiteCheckListItemRepository->deleteByJobID($input['jobID']);

        foreach( $preSiteCheckList as $key => $checklist ){

          $checkListRecord = [
            "JobID" => $checklist->JobID,
            "ContactID" => $checklist->ContactID,
            "PhoneNumber" => $checklist->PhoneNumber,
            "VehicleRego" => $checklist->VehicleRego,
            "VehicleType" => $checklist->VehicleType,
            "PIV" => $checklist->PIV,
            "ServiceDueKms" => $checklist->ServiceDueKms,
            "RetensionWheelNuts" => $checklist->RetensionWheelNuts,
            "SpeedoStart" => $checklist->SpeedoStart,
            "SpeedoFinish" => $checklist->SpeedoFinish,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          $savePreSiteCheckList = $this->preSiteCheckListRepository->create($checkListRecord);

          if( $checklist->CheckListItems != "" && $savePreSiteCheckList ){

            foreach( $checklist->CheckListItems as $checkListInfo ){

              $checkListRecord = [
                "PreSiteCheckListID" => $savePreSiteCheckList->PreSiteCheckListID,
                "JobID" => $checklist->JobID,
                "Item" => $checkListInfo->Item,
                "Description" => $checkListInfo->Description,
                "Row" => $checkListInfo->Row,
                "Column" => $checkListInfo->Column,
                "View" => $checkListInfo->View,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
              ];

              $savesAuditSigns = $this->preSiteCheckListItemRepository->create($checkListRecord);

            }


          }

        }

      }


      if( $updateJob ){

        DB::commit();

        //Prepare email notification to Traffic Controllers

        $tcData = array();

        $jobInfo = $this->jobHeaderRepository->getById($input['jobID']);

        $timesheet = json_decode($input['timesheets']);

        if( count($timesheet) > 0 ){

          foreach( $timesheet as $key => $calendarInfo ){

            $contactInfo = $this->contactPhoneMobileEmailRepository->get($calendarInfo->WorkerID);

            $emailData = [
              'fullname' => $contactInfo['cFullname'],
              'startTravel' => date('Hi',$calendarInfo->TravelStartDateTime / 1000),
              'startOnsite' => date('Hi',$calendarInfo->JobStartDateTime / 1000),
              'startMeal' => ( ( $calendarInfo->BreakStartDateTime != 0 && strlen( $calendarInfo->BreakStartDateTime ) >= 10 )? date('Hi',$calendarInfo->BreakStartDateTime / 1000) : "" ),
              'finishMeal' => ( ( $calendarInfo->BreakFinishDateTime != 0 && strlen( $calendarInfo->BreakFinishDateTime ) >= 10 )? date('Hi',$calendarInfo->BreakFinishDateTime / 1000) : "" ),
              'finishOnsite' => date('Hi',$calendarInfo->JobFinishDateTime / 1000),
              'finishTravel' => date('Hi',$calendarInfo->TravelFinishDateTime / 1000)
            ];

            array_push($tcData, $emailData);

            $emailData = [
              'fullname' => $contactInfo['cFullname'],
              // 'email' => 'cpambrosio@straightarrow.com.ph',
              'email' => $contactInfo['cEmail'],
              'jobID' => $input['jobID'],
              'companyName' => $jobInfo['CompanyName'],
              'address' => $jobInfo['Address'],
              'dateStarted' => date('d-F-Y',strtotime($jobInfo['StartDateTime'])),
              'depot' => $jobInfo['Depot'],
              'authorized' => $jobInfo['AuthorizedPerson'],
              'startTravel' => date('Hi',$calendarInfo->TravelStartDateTime / 1000),
              'startOnsite' => date('Hi',$calendarInfo->JobStartDateTime / 1000),
              'startMeal' => ( ( $calendarInfo->BreakStartDateTime != 0 && strlen( $calendarInfo->BreakStartDateTime ) >= 10 )? date('Hi',$calendarInfo->BreakStartDateTime / 1000) : "" ),
              'finishMeal' => ( ( $calendarInfo->BreakFinishDateTime != 0 && strlen( $calendarInfo->BreakFinishDateTime ) >= 10 )? date('Hi',$calendarInfo->BreakFinishDateTime / 1000) : "" ),
              'finishOnsite' => date('Hi',$calendarInfo->JobFinishDateTime / 1000),
              'finishTravel' => date('Hi',$calendarInfo->TravelFinishDateTime / 1000)
            ];


            $this->emailServices->sendEmail(1,$emailData);

          }

        }


        //Prepare email notification for client

        if( $input['clientName'] != "" && $input['clientEmail'] ){

          $emailClientData = array(
            'fullname' => $input['clientName'],
            'email' => $input['clientEmail'],
            'jobID' => $jobInfo['JobID'],
            'companyName' => $jobInfo['CompanyName'],
            'address' => $jobInfo['Address'],
            'dateStarted' => date('d-F-Y',strtotime($jobInfo['StartDateTime'])),
            'depot' => $jobInfo['Depot'],
            'authorized' => $jobInfo['AuthorizedPerson'],
            'tcData' => $tcData
          );

          $this->emailServices->sendEmail(2,$emailClientData);

        }

        $reportInfo = [
          'siteaudit' => $siteChecklistReportData,
          'additionalHazards' => $additionalHazardReportData,
          'signageAudit' => $sinageAuditReportData
        ];

        // $pdf = PDF::loadView('forms.jobsitechecklist', $reportInfo)->setPaper('a4','portrait')->save('C:\Users\user-2\Desktop\myfile.pdf');

        //Record Job Sync
        $jobSync = Config::get('evolution.jobSyncEnable');

        if( $jobSync ){

          $countJobSync = $this->jobSyncRepository->countByJobIDIP($input['jobID'],Request::getClientIp());

          if( $countJobSync <= 0 ){

            $jobSyncData = array(
              "IP" => Request::getClientIp(),
              "JobID" => $input['jobID'],
              "RequestID" => $requestId
            );

            $createJobSync = $this->jobSyncRepository->create($jobSyncData);

          }
          else{

            $jobSyncData = array(
              "IP" => Request::getClientIp(),
              "JobID" => $input['jobID'],
              "RequestID" => $requestId
            );

            $createJobSync = $this->jobSyncRepository->updateByJobIDIP($input['jobID'],Request::getClientIp(),$jobSyncData);


          }

        }


        //Delete Docket Attachment Files
        $deleteDocketAttachment = $this->docketAttachmentServices->deleteDocketAttachments($input['jobID']);

        //Set Processed Data field
        $updateRequestData = [
          'ProcessedJob' => 1
        ];

        $updateRequest = $this->requestRepository->update($updateRequestData,$requestId);


        $response['status'] = true;
        $response['statusCode'] = 200;
        $response['message'] = "Job ".$input['jobID']." has been sent";


      }
      else{


        DB::rollback();

        $response['status'] = false;
        $response['statusCode'] = 500;
        $response['message'] = "Unable to connect to ManStat. Please try again.";

      }


    }
    else{

      DB::beginTransaction();


      try{

        $count = $this->extJobHeaderRepository->count($input['jobID']);

        //New Ext Job
        if( $count <= 0 ){

          $jobData = array(
            "ExtJobID" => $input['jobID'],
            "ParentJob" => $input['parentJob'],
            "RevisedOrderNumber" => $input['orderNumber'],
            "RevisedOrderNumber" => $input['geotag'],
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          );

          $insertExtJob = $this->extJobHeaderRepository->insert($jobData);

        }


        $timesheets = json_decode($input['timesheets']);

        if( count($timesheets) > 0 ){

          $deleteExtTimesheet = $this->extTimesheetRepository->deleteByJobID($input['jobID']);

          $timesheetList = array();

          foreach( $timesheets as $key => $timesheetInfo ){

            $timesheetRecord = [
              "ExtJobID" => $timesheetInfo->JobID,
              "WorkerID" => $timesheetInfo->WorkerID,
              "ShiftID" => $timesheetInfo->ShiftID,
              "JobStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobStartDateTime / 1000),
              "JobFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobFinishDateTime / 1000),
              "TravelStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTime / 1000),
              "TravelFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTime / 1000),
              "BreakStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakStartDateTime / 1000),
              "BreakFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakFinishDateTime / 1000),
              "TravelStartDateTimeEnd" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTimeEnd / 1000),
              "TravelFinishDateTimeStart" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTimeStart / 1000),
              "TravelStartDistance" => $timesheetInfo->TravelStartDistance,
              "TravelFinishDistance" => $timesheetInfo->TravelFinishDistance,
              "FatigueCompliance" => $timesheetInfo->FatigueCompliance,
              "RegoNo" => $timesheetInfo->RegoNo,
              "TraveledKilometers" => $timesheetInfo->TraveledKilometers,
              "Odometer" => $timesheetInfo->Odometer,
              "AttendDepot" => $timesheetInfo->AttendDepot,
              "created_at" => Carbon::now(),
              "updated_at" => Carbon::now()
            ];

            array_push($timesheetList, $timesheetRecord);

          }

          $saveTimesheet = $this->extTimesheetRepository->insert($timesheetList);

        }


        $additionalHazards = json_decode($input['additionalHazards']);

        if( count($additionalHazards) > 0 ){

          $deleteExtTimesheet = $this->addHazardRepository->deleteByJobID($input['jobID']);

          $hazardList = array();

          foreach( $additionalHazards as $key => $hazardInfo ){

            $hazardRecord = [
              "JobID" => $hazardInfo->JobID,
              "AddHazard" => $hazardInfo->AddHazard,
              "InitialRisk" => $hazardInfo->InitialRisk,
              "ControlMeasures" => $hazardInfo->ControlMeasures,
              "ResidualRisk" => $hazardInfo->ResidualRisk,
              "created_at" => Carbon::now(),
              "updated_at" => Carbon::now()
            ];

            array_push($hazardList, $hazardRecord);

          }

          $saveAddHazards = $this->addHazardRepository->insert($hazardList);

        }


        $jobRequirements = json_decode($input['requirements']);

        if( count($jobRequirements) > 0 ){

          $deleteExtJobRequirement = $this->extJobRequirementRepository->deleteByJobID($input['jobID']);

          $jobRequirementList = array();

          foreach( $jobRequirements as $key => $jobRequirementInfo ){

            $jobRequirementRecord = [
              "ExtJobID" => $jobRequirementInfo->JobID,
              "ItemID" => $jobRequirementInfo->RequirementID,
              "Qty" => $jobRequirementInfo->Quantity,
              "Notes" => $jobRequirementInfo->Description,
              "OperatorID" => $input['operatorID'],
              "created_at" => Carbon::now(),
              "updated_at" => Carbon::now()
            ];

            array_push($jobRequirementList, $jobRequirementRecord);

          }

          $saveJobRequirement = $this->extJobRequirementRepository->insert($jobRequirementList);


        }


        $docket = json_decode($input['dockets']);

        if( count($docket) > 0 ){

          $deleteExtDocket = $this->extDocketRepository->deleteByJobID($input['jobID']);

          $docketList = array();

          foreach( $docket as $key => $docketInfo ){

            $docketRecord = [
              "ExtJobID" => $docketInfo->JobID,
              "OperatorID" => $docketInfo->OperatorID,
              "AttachmentTypeID" => $docketInfo->AttachmentTypeID,
              "AttachedOn" => date('Y-m-d H:i:s', $docketInfo->AttachedOn/ 1000),
              "Attachment" => $docketInfo->Attachment,
              "created_at" => Carbon::now(),
              "updated_at" => Carbon::now()
            ];

            array_push($docketList, $docketRecord);

          }

          $saveDocket = $this->extDocketRepository->insert($docketList);

        }


        $jobSiteCheckListItems = json_decode($jobSiteCheckListItems);

        if( count($jobSiteCheckListItems) > 0 ){

          $getJobSiteCheckList = $this->extJobSiteCheckListItemRepository->get($input['jobID']);

          if(count($getJobSiteCheckList) > 0){

            foreach($getJobSiteCheckList as $getJobSiteCheckListInfo){

              $getSiteCheckList = $this->extSiteCheckListInputValueRepository->get($getJobSiteCheckListInfo['ItemValueID']);

              if(count($getSiteCheckList) > 0){

                $deleteExtSiteCheckListInputValue = $this->extSiteCheckListInputValueRepository->delete($getJobSiteCheckListInfo['ItemValueID']);

              }
            }

          }

          $deleteExtJobSiteCheckListItem = $this->extJobSiteCheckListItemRepository->deleteByJobID($input['jobID']);

          $jobSiteCheckListItemList = array();

          foreach( $jobSiteCheckListItems as $key => $jobSiteCheckListItemInfo ){


            $jobSiteCheckListItemRecord = [
              "ExtJobID" => $jobSiteCheckListItemInfo->jobID,
              "ItemID" => $jobSiteCheckListItemInfo->itemID,
              "ItemValueID" => $jobSiteCheckListItemInfo->itemValueID,
              "created_at" => Carbon::now(),
              "updated_at" => Carbon::now()
            ];

            array_push($jobSiteCheckListItemList, $jobSiteCheckListItemRecord);

          }

          $saveJobSiteCheckListItem = $this->extJobSiteCheckListItemRepository->insert($jobSiteCheckListItemList);


          $siteCheckListItnputValues = json_decode($siteCheckListInputValues);

          if( count($siteCheckListItnputValues) > 0 ){

            $siteCheckListInputValueList = array();

            foreach( $siteCheckListItnputValues as $key => $siteCheckListInputValueInfo ){

              $siteCheckListInputValueRecord = [
                "ID" => $siteCheckListInputValueInfo->Id,
                "Value" => $siteCheckListInputValueInfo->Value,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
              ];

              array_push($siteCheckListInputValueList, $siteCheckListInputValueRecord);

            }

            $saveSiteCheckListInputValue = $this->extSiteCheckListInputValueRepository->insert($siteCheckListInputValueList);

          }

        }

        //create job client notify

        if( $input['clientName'] != "" && $input['clientEmail'] ){

            $deleteJobClientNotify= $this->jobClientNotifyRepository->deleteByJobID($input['jobID']);

            $jobClientNotifyRecord = [
              "JobID" => $input['jobID'],
              "ClientName" => $input['clientName'],
              "ClientEmail" => $input['clientEmail'],
              "created_at" => Carbon::now(),
              "updated_at" => Carbon::now()
            ];

            $saveJobClientNotify = $this->jobClientNotifyRepository->insert($jobClientNotifyRecord);

        }

         //create tc signatures

        $tcData = json_decode($input['tcAttachment']);

        if( count($tcData) > 0 ){

          $deleteTCAttachment = $this->tcAttachmentRepository->deleteByJobID($input['jobID']);

          $tcList = array();

          foreach( $tcData as $key => $tcInfo ){

            $tcRecord = [
              "JobID" => $tcInfo->JobID,
              "TCName" => $tcInfo->TCName,
              "AttachedOn" => date('Y-m-d H:i:s',$tcInfo->AttachedOn / 1000),
              "Attachment" => $tcInfo->Attachment,
              "created_at" => Carbon::now(),
              "updated_at" => Carbon::now()
            ];

            array_push($tcList, $tcRecord);

          }

          $saveTCAttachment = $this->tcAttachmentRepository->insert($tcList);

        }

        //create signage audit

        $signageAudit = json_decode($input['signageAudit']);

        if( $input['signageAudit'] != "" && count($signageAudit) > 0 ){

          //Delete current signage audit and audit signs record
          $deleteSignageAudit = $this->signageAuditRepository->deleteByJobID($input['jobID']);
          $deleteAuditSign = $this->auditSignsRepository->deleteByJobID($input['jobID']);

          foreach( $signageAudit as $key => $signageInfo ){

            $sinageRecord = [
              "JobID" => $signageInfo->JobID,
              "Landmark" => $signageInfo->Landmark,
              "CarriageWay" => $signageInfo->CarriageWay,
              "Direction" => $signageInfo->Direction,
              "TimeErected" => date('H:i:s',$signageInfo->TimeErected / 1000),
              "TimeCollected" => date('H:i:s',$signageInfo->TimeCollected / 1000),
              "TimeChecked1" => date('H:i:s',$signageInfo->TimeChecked1 / 1000),
              "TimeChecked2" => date('H:i:s',$signageInfo->TimeChecked2 / 1000),
              "TimeChecked3" => date('H:i:s',$signageInfo->TimeChecked3 / 1000),
              "TimeChecked4" => date('H:i:s',$signageInfo->TimeChecked4 / 1000),
              "ErectedBy" => $signageInfo->ErectedBy,
              "CollectedBy" => $signageInfo->CollectedBy,
              "created_at" => Carbon::now(),
              "updated_at" => Carbon::now()
            ];

            $saveSignageAudit = $this->signageAuditRepository->create($sinageRecord);

            if( $signageInfo->AuditSigns != "" && $saveSignageAudit ){

              $auditSignsList = explode('~',$signageInfo->AuditSigns);

              foreach( $auditSignsList as $auditSignInfo ){

                $signID = explode(':',explode('^',$auditSignInfo)[0])[1];
                $metres = explode(':',explode('^',$auditSignInfo)[1])[1];
                $qty = explode(':',explode('^',$auditSignInfo)[2])[1];
                $afterCare = explode(':',explode('^',$auditSignInfo)[3])[1];
                $afterCareMetres = explode(':',explode('^',$auditSignInfo)[4])[1];
                $afterCareQty = explode(':',explode('^',$auditSignInfo)[5])[1];

                $auditRecord = [
                  "AuditID" => $saveSignageAudit->AuditID,
                  "JobID" => $signageInfo->JobID,
                  "SignID" => $signID,
                  "Metres" => $metres,
                  "Qty" => $qty,
                  "AfterCare" => $afterCare,
                  "AfterCareMetres" => $afterCareMetres,
                  "AfterCareQty" => $afterCareQty,
                  "created_at" => Carbon::now(),
                  "updated_at" => Carbon::now()
                ];

                $savesAuditSigns = $this->auditSignsRepository->create($auditRecord);

              }

            }

          }

        }


        DB::commit();

        //Prepare email notification to Traffic Controllers

        $tcData = array();

        $jobInfo = $this->jobHeaderRepository->getById($input['parentJob']);

        $timesheet = json_decode($input['timesheets']);

        if( count($timesheet) > 0 ){

          foreach( $timesheet as $key => $calendarInfo ){

            $contactInfo = $this->contactPhoneMobileEmailRepository->get($calendarInfo->WorkerID);

            $emailData = [
              'fullname' => $contactInfo['cFullname'],
              'startTravel' => date('Hi',$calendarInfo->TravelStartDateTime / 1000),
              'startOnsite' => date('Hi',$calendarInfo->JobStartDateTime / 1000),
              'startMeal' => ( ( $calendarInfo->BreakStartDateTime != 0 && strlen( $calendarInfo->BreakStartDateTime ) >= 10 )? date('Hi',$calendarInfo->BreakStartDateTime / 1000) : "" ),
              'finishMeal' => ( ( $calendarInfo->BreakFinishDateTime != 0 && strlen( $calendarInfo->BreakFinishDateTime ) >= 10 )? date('Hi',$calendarInfo->BreakFinishDateTime / 1000) : "" ),
              'finishOnsite' => date('Hi',$calendarInfo->JobFinishDateTime / 1000),
              'finishTravel' => date('Hi',$calendarInfo->TravelFinishDateTime / 1000)
            ];

            array_push($tcData, $emailData);

            $emailData = [
              'fullname' => $contactInfo['cFullname'],
              // 'email' => 'cpambrosio@straightarrow.com.ph',
              'email' => $contactInfo['cEmail'],
              'jobID' => $input['jobID'],
              'companyName' => $jobInfo['CompanyName'],
              'address' => $jobInfo['Address'],
              'dateStarted' => date('d-F-Y',strtotime($jobInfo['StartDateTime'])),
              'depot' => $jobInfo['Depot'],
              'authorized' => $jobInfo['AuthorizedPerson'],
              'startTravel' => date('Hi',$calendarInfo->TravelStartDateTime / 1000),
              'startOnsite' => date('Hi',$calendarInfo->JobStartDateTime / 1000),
              'startMeal' => ( ( $calendarInfo->BreakStartDateTime != 0 && strlen( $calendarInfo->BreakStartDateTime ) >= 10 )? "" : date('Hi',$calendarInfo->BreakStartDateTime / 1000) ),
              'finishMeal' => ( ( $calendarInfo->BreakFinishDateTime != 0 && strlen( $calendarInfo->BreakFinishDateTime ) >= 10 )? "" : date('Hi',$calendarInfo->BreakFinishDateTime / 1000) ),
              'finishOnsite' => date('Hi',$calendarInfo->JobFinishDateTime / 1000),
              'finishTravel' => date('Hi',$calendarInfo->TravelFinishDateTime / 1000)
            ];

            $this->emailServices->sendEmail(1,$emailData);

          }

        }

        if( $input['clientName'] != "" && $input['clientEmail'] ){

          $emailClientData = array(
            'fullname' => $input['clientName'],
            'email' => $input['clientEmail'],
            'jobID' => $input['jobID'],
            'companyName' => $jobInfo['CompanyName'],
            'address' => $jobInfo['Address'],
            'dateStarted' => date('d-F-Y',strtotime($jobInfo['StartDateTime'])),
            'depot' => $jobInfo['Depot'],
            'authorized' => $jobInfo['AuthorizedPerson'],
            'tcData' => $tcData
          );

          $this->emailServices->sendEmail(2,$emailClientData);

        }



        $response['status'] = true;
        $response['statusCode'] = 200;
        $response['message'] = "Job ".$input['jobID']." has been sent";

         //Record Job Sync

        $jobSync = Config::get('evolution.jobSyncEnable');

        if( $jobSync ){

          $countJobSync = $this->jobSyncRepository->countByJobIDIP($input['jobID'],Request::getClientIp());

          if( $countJobSync <= 0 ){

            $jobSyncData = array(
              "IP" => Request::getClientIp(),
              "JobID" => $input['jobID'],
              "RequestID" => $requestId
            );

            $createJobSync = $this->jobSyncRepository->create($jobSyncData);

          }
          else{

            $jobSyncData = array(
              "IP" => Request::getClientIp(),
              "JobID" => $input['jobID'],
              "RequestID" => $requestId
            );

            $createJobSync = $this->jobSyncRepository->updateByJobIDIP($input['jobID'],Request::getClientIp(),$jobSyncData);


          }
        }

        //Delete Docket Attachment Files
        $deleteDocketAttachment = $this->docketAttachmentServices->deleteDocketAttachments($input['jobID']);

        //Set Processed Data field
        $updateRequestData = [
          'ProcessedJob' => 1
        ];

        $updateRequest = $this->requestRepository->update($updateRequestData,$requestId);

      }
      catch ( Exception $e ){

        DB::rollback();

        $response['status'] = false;
        $response['statusCode'] = 500;
        $response['message'] = "Unable to connect to ManStat. Please try again 4.";


      }

    }

    return $response;

  }

  public function deleteExtensionJob($jobId){

      DB::beginTransaction();

      try{

        $deleteJobHeader = $this->extJobHeaderRepository->deleteByJobID($jobId);
        $deleteTimesheet = $this->extTimesheetRepository->deleteByJobID($jobId);
        $deleteJobRequirement = $this->extJobRequirementRepository->deleteByJobID($jobId);
        $deleteDocket = $this->extDocketRepository->deleteByJobID($jobId);
        $deleteJobSiteCheckListItem = $this->extJobSiteCheckListItemRepository->deleteByJobID($jobId);
        $deleteSiteCheckListInputValue = $this->extSiteCheckListInputValueRepository->deleteByJobID($jobId);
        $deleteTCAttachment = $this->tcAttachmentRepository->deleteByJobID($jobId);
        $deleteSignageAudit = $this->signageAuditRepository->deleteByJobID($jobId);
        $deleteAuditSign = $this->auditSignsRepository->deleteByJobID($jobId);

        DB::commit();

        $response['status'] = true;
        $response['statusCode'] = 200;
        $response['message'] = "Job successfully deleted";

      }
      catch ( Exception $e ){

        DB::rollback();

        $response['status'] = false;
        $response['statusCode'] = 422;
        $response['message'] = "Job delete failed";

      }

      return $response;


  }

  public function countJobSync($jobId){

    $countJobSync = $this->jobSyncRepository->countByJobID($jobId);

    if( $countJobSync > 0 ){

      $response['status'] = true;
      $response['statusCode'] = 200;
      $response['message'] = "Job already sync";

    }
    else{

      $response['status'] = false;
      $response['statusCode'] = 200;
      $response['message'] = "Job still not sync";

    }

    return $response;

  }

  public function returnAll($contactId){

    return $this->manstatServices->returnAll($contactId);

  }

  public function checkConnectivity($contactId){

    $connectivity = $this->manstatServices->checkConnectivity($contactId);

    if( $connectivity ){

      $response['status'] = true;
      $response['statusCode'] = 200;
      $response['message'] = "API successfully connected to ManStat.";

    }
    else{

      $response['status'] = false;
      $response['statusCode'] = 500;
      $response['message'] = "Unable to connect to ManStat.";


    }

    return $response;

  }

  public function checkJobExpiration($contactId, $jobId){


    $relatedJobs = $this->manstatServices->getRelatedJobs($contactId);

    $jobList = [];
    $jobExist = false;

    if ( strpos($jobId, '.') > 0 ) {

      $pos = strpos($jobId, '.') - strlen($jobId);
      $jobId = substr($jobId, 0, $pos);

    }

    //Check if given job id is still within the current user's list
    if( $relatedJobs ){

      foreach( $relatedJobs as $key => $jobInfo ){

        if( $jobInfo['JobID'] == $jobId ){

          $jobExist = true;

        }

      }

    }

    if( $jobExist ){

      $response['status'] = false;
      $response['statusCode'] = 200;
      $response['message'] = "Job is still exist";

    }
    else{

      //Get given job id's information

      $jobHeaderInfo = $this->jobHeaderRepository->getInfoById($jobId);

      if( $jobHeaderInfo ){

        //Check if current datetime is already beyond job's end datetime

        $jobStartTime = str_replace('1900-01-01 ', '', $jobHeaderInfo['JobStartTime']);
        $jobStartTime = str_replace('.000', '', $jobStartTime);

        $jobStartDateTime = date('Y-m-d',strtotime($jobHeaderInfo['JobStartDate'])).' '.$jobStartTime;

        $shiftLength = 0;

        if( $jobHeaderInfo['JobShiftLength'] > 0 ){

          $shiftLength = $jobHeaderInfo['JobShiftLength'];

        }

        $jobExpiration = Config::get('evolution.expirationDelayHours');

        //calculate end date
        $jobEndDateTime = date('Y-m-d H:i:s',strtotime('+ '.$shiftLength.' hour',strtotime( $jobStartDateTime )));

        //add delay hours before expiration
        $jobEndDateTime = date('Y-m-d H:i:s',strtotime('+ '.$jobExpiration.' hour',strtotime( $jobStartDateTime )));


        if( strtotime( $jobEndDateTime ) > strtotime( date('Y-m-d H:i:s') ) ){

          $response['status'] = false;
          $response['statusCode'] = 200;
          $response['message'] = "Job is still exist";


        }
        else{

          $response['status'] = true;
          $response['statusCode'] = 200;
          $response['message'] = "Job is already expired";

        }


      }
      else{

        $response['status'] = true;
        $response['statusCode'] = 200;
        $response['message'] = "Job is already expired";

      }

    }

    return $response;

  }

  public function clearJobSync($contactId, $jobId){

    //Get given job id's information

    $countJobSync = $this->jobSyncRepository->countByJobID($jobId);
    $jobSync = Config::get('evolution.jobSyncEnable');

    if( $jobSync || $countJobSync > 0 ){

      $jobSync = $this->jobSyncRepository->deleteByJobID($jobId);

      if( $jobSync ){

          $response['status'] = true;
          $response['statusCode'] = 200;
          $response['message'] = "Job Sync successfully removed";

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 422;
        $response['message'] = "Job Sync unsuccessfully removed";

      }

    }
    else{

      $response['status'] = true;
      $response['statusCode'] = 200;
      $response['message'] = "No Job Found";


    }


    return $response;

  }


}

?>