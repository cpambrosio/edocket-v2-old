<?php namespace App\Modules\dev\Job\Gateway;

use App\Modules\dev\Job\Services\ManstatServices;
use App\Modules\dev\Job\Services\GoogleTimezoneServices;
use App\Modules\dev\Job\Repository\Interfaces\JobSyncInterface as JobSyncRepository;
use App\Modules\dev\Job\Helper\JobHelper;

use Config;
use Carbon\Carbon;
use DB;
use Request;
use Mail;
use PDF;
use Excel;
use Input;
use File;
use DateTime;

class RequirementGateway
{

    protected $manstatServices;
    protected $googleTimezoneServices;
    protected $jobSyncRepository;
    protected $jobHelper;

    public function __construct(
        ManstatServices $manstatServices,
        GoogleTimezoneServices $googleTimezoneServices,
        JobSyncRepository $jobSyncRepository,
        JobHelper $jobHelper)
    {

        $this->manstatServices = $manstatServices;
        $this->googleTimezoneServices = $googleTimezoneServices;
        $this->jobSyncRepository = $jobSyncRepository;
        $this->jobHelper = $jobHelper;

    }


    public function getRequirements($contactId){

      $requirements = $this->manstatServices->getRequirements($contactId);

      $requirementList = [];

      if( $requirements ){

        foreach( $requirements as $key => $requirementInfo ){

          $countJobSync = $this->jobSyncRepository->countByJobID($requirementInfo['JobID']);
          $jobSync = Config::get('evolution.jobSyncEnable');

          if( $jobSync == false || $countJobSync == 0 ){

            array_push($requirementList, $requirements[$key]);

          }

        }

      }

      if( count( $requirementList ) > 0 ){

        $response['status'] = true;
        $response['statusCode'] = 200;
        $response['requirements'] = $requirementList;

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 200;
        $response['message'] = "No Job Requirements Found";

      }

      return $response;

    }

    

}

?>