<?php namespace App\Modules\dev\Job\Gateway;

use App\Modules\dev\Job\Services\ManstatServices;
use App\Modules\dev\Job\Services\GoogleTimezoneServices;
use App\Modules\dev\Job\Services\EmailServices;
use App\Modules\dev\Job\Repository\Interfaces\JobSyncInterface as JobSyncRepository;
use App\Modules\dev\Job\Repository\Interfaces\JobHeaderInterface as JobHeaderRepository;
use App\Modules\dev\Authenticate\Repository\Interfaces\ContactInterface as ContactRepository;
use App\Modules\dev\Job\Repository\Interfaces\JobImageInterface as JobImageRepository;
use App\Modules\dev\Job\Repository\Interfaces\GenerateReportInterface as GenerateReportRepository;
use App\Modules\dev\Job\Services\FileServices;
use App\Modules\dev\Job\Gateway\SiteChecklistGateway;
use App\Modules\dev\Job\Helper\JobHelper;
use App\Modules\dev\Job\Helper\InputHelper;

use Config;
use Carbon\Carbon;
use DB;
use Request;
use Mail;
use PDF;
use Excel;
use Input;
use File;
use DateTime;

class ReportGateway
{

    protected $manstatServices;
    protected $googleTimezoneServices;
    protected $emailServices;
    protected $jobSyncRepository;
    protected $jobHeaderRepository;
    protected $contactRepository;
    protected $jobImageRepository;
    protected $generateReportRepository;
    protected $fileServices;
    protected $siteChecklistGateway;
    protected $jobHelper;
    protected $inputHelper;

    public function __construct(
        ManstatServices $manstatServices,
        GoogleTimezoneServices $googleTimezoneServices,
        EmailServices $emailServices,
        JobSyncRepository $jobSyncRepository,
        JobHeaderRepository $jobHeaderRepository,
        ContactRepository $contactRepository,
        JobImageRepository $jobImageRepository,
        GenerateReportRepository $generateReportRepository,
        FileServices $fileServices,
        SiteChecklistGateway $siteChecklistGateway,
        JobHelper $jobHelper,
        InputHelper $inputHelper)
    {

        $this->manstatServices = $manstatServices;
        $this->googleTimezoneServices = $googleTimezoneServices;
        $this->emailServices = $emailServices;
        $this->jobSyncRepository = $jobSyncRepository;
        $this->jobHeaderRepository = $jobHeaderRepository;
        $this->contactRepository = $contactRepository;
        $this->jobImageRepository = $jobImageRepository;
        $this->generateReportRepository = $generateReportRepository;
        $this->fileServices = $fileServices;
        $this->siteChecklistGateway = $siteChecklistGateway;
        $this->jobHelper = $jobHelper;
        $this->inputHelper = $inputHelper;

    }

  public function generateJobDocketReport($input, $contactId, $contactCode){

    if( isset( $input['timezone'] ) && $input['timezone'] != '' ){

      date_default_timezone_set($input['timezone']);

    }
    else{

      $response['status'] = false;
      $response['statusCode'] = 422;
      $response['message'] = "Failed to set Timezone";

      return $response;

    }

    $jobSiteCheckListItems = [];
    $siteCheckListInputValues = [];

    //filter inputs for IOS
    $input = $this->inputHelper->filterInput($input);

    $siteChecklistReportData = [];
    $additionalHazardReportData = [];
    $sinageAuditReportData = [];

    //Set site checklist
    if( $input['siteCheckList'] ){

      $checkSiteChecklist = $this->siteChecklistGateway->checkSiteChecklist($input,$contactId);

      if( $checkSiteChecklist['status'] ){

        $siteChecklistReportData = $checkSiteChecklist['siteChecklistReportData'];
        $jobSiteCheckListItems = json_encode($checkSiteChecklist['jobSiteCheckListItems']);
        $siteCheckListInputValues = json_encode($checkSiteChecklist['siteCheckListInputValues']);

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 400;
        $response['message']['siteCheckList'] = $checkSiteChecklist['message'];

        return $response;

      }

    }


    //Check if Main Job
    if( $input['parentJob'] == "" ){

      //create additional hazards
      $additionalHazards = json_decode($input['additionalHazards']);
      $additionalHazardReportData = [];

      if( count($additionalHazards) > 0 ){

          $hazardList = array();

          foreach( $additionalHazards as $key => $hazardInfo ){

            $hazardRecord = [
              // "JobID" => $hazardInfo->JobID,
              "AddHazard" => $hazardInfo->AddHazard,
              "InitialRisk" => $hazardInfo->InitialRisk,
              "ControlMeasures" => $hazardInfo->ControlMeasures,
              "ResidualRisk" => $hazardInfo->ResidualRisk,
              // "created_at" => Carbon::now(),
              // "updated_at" => Carbon::now()
            ];

            array_push($hazardList, $hazardRecord);
            array_push($additionalHazardReportData,$hazardRecord);

          }

      }


      //create tc signatures
      $tcData = json_decode($input['tcAttachment']);
      $tcSignatureData = [];

      if( count($tcData) > 0 ){

        $tcList = array();

        foreach( $tcData as $key => $tcInfo ){

          // $filename_path = md5(time().uniqid()).".jpg";
          // $signatureDecode = base64_decode($tcInfo->Attachment);
          // file_put_contents(public_path("image/signatures/".$filename_path),$signatureDecode);

          $tcRecord = [
            "JobID" => $tcInfo->JobID,
            "TCName" => $tcInfo->TCName,
            "AttachedOn" => date('Y-m-d H:i:s',$tcInfo->AttachedOn / 1000),
            // "Attachment" => $tcInfo->Attachment,
            "Src" => $tcInfo->Attachment,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];


          array_push($tcList, $tcRecord);
          array_push($tcSignatureData,$tcRecord);

        }

      }


      //Job Info
      $jobInfo = $this->jobHeaderRepository->getById($input['jobID']);

      $state = "";
      $depot = $jobInfo['Depot'];

      if( $jobInfo['JobState'] == "" ){

        $state = $this->checkState($depot);

      }
      else{

        $state = $jobInfo['JobState'];

      }

      //create signage audit

      $sinageAuditReportData = [];
      $sinageAuditReportData['SlowLane'] = [];
      $sinageAuditReportData['FastLane'] = [];
      $afterCarePresent = "FALSE";
      $afterCareCount = 0;

      $signageAudit = json_decode($input['signageAudit']);

      if( $input['signageAudit'] != "" && count($signageAudit) > 0 ){

        foreach( $signageAudit as $key => $signage ){

          $sinageRecord = [
            "JobID" => $signage->JobID,
            "Landmark" => $signage->Landmark,
            "CarriageWay" => $signage->CarriageWay,
            "Direction" => $signage->Direction,
            "TimeErected" => date('H:i:s',$signage->TimeErected / 1000),
            "TimeCollected" => date('H:i:s',$signage->TimeCollected / 1000),
            "TimeChecked1" => date('H:i:s',$signage->TimeChecked1 / 1000),
            "TimeChecked2" => date('H:i:s',$signage->TimeChecked2 / 1000),
            "TimeChecked3" => date('H:i:s',$signage->TimeChecked3 / 1000),
            "TimeChecked4" => date('H:i:s',$signage->TimeChecked4 / 1000),
            "ErectedBy" => $signage->ErectedBy,
            "CollectedBy" => $signage->CollectedBy,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          if( $signage->AuditSigns != "" ){

            $auditSignsList = explode('~',$signage->AuditSigns);

            foreach( $auditSignsList as $auditSignInfo ){

              $signID = explode(':',explode('^',$auditSignInfo)[0])[1];
              $metres = explode(':',explode('^',$auditSignInfo)[1])[1];
              $qty = explode(':',explode('^',$auditSignInfo)[2])[1];
              $afterCare = explode(':',explode('^',$auditSignInfo)[3])[1];
              $afterCareMetres = explode(':',explode('^',$auditSignInfo)[4])[1];
              $afterCareQty = explode(':',explode('^',$auditSignInfo)[5])[1];

              $auditRecord = [
                "JobID" => $signage->JobID,
                "SignID" => $signID,
                "Metres" => $metres,
                "Src" => 'image\signage\\'.$state.'-Icons\sign_'.$signID.'.png',
                "Qty" => $qty,
                "AfterCare" => $afterCare,
                "AfterCareMetres" => $afterCareMetres,
                "AfterCareQty" => $afterCareQty,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
              ];

              if( $afterCare > 0 ){

                $afterCareCount++;

              }


              switch( $signage->CarriageWay ){
                case 1:

                  array_push($sinageAuditReportData['SlowLane'],$auditRecord);

                break;
                case 2:

                  array_push($sinageAuditReportData['FastLane'],$auditRecord);

                break;
              }

            }

            

          }


          switch( $signage->Direction ){
            case 1:
              $sinageAuditReportData['Direction'] = "North";
            break;
            case 2:
              $sinageAuditReportData['Direction'] = "South";
            break;
            case 3:
              $sinageAuditReportData['Direction'] = "East";
            break;
            case 4:
              $sinageAuditReportData['Direction'] = "West";
            break;
            default:
              $sinageAuditReportData['Direction'] = "";
            break;
          }

          $sinageAuditReportData['LocalLandmark'] = $signage->Landmark;

          $sinageAuditReportData['TimeErected'] = $signage->TimeErected / 1000;
          $sinageAuditReportData['TimeCollected'] = $signage->TimeCollected / 1000;
          $sinageAuditReportData['TimeChecked1'] = $signage->TimeChecked1 / 1000;
          $sinageAuditReportData['TimeChecked2'] = $signage->TimeChecked2 / 1000;
          $sinageAuditReportData['TimeChecked3'] = $signage->TimeChecked3 / 1000;
          $sinageAuditReportData['TimeChecked4'] = $signage->TimeChecked4 / 1000;

        }

      }

      $jobRequirements = json_decode($input['requirements']);
      $jobRequirementList = array();

      if( count($jobRequirements) > 0 ){

        foreach( $jobRequirements as $key => $jobRequirementInfo ){

          $jobRequirementRecord = [
            "JobID" => $jobRequirementInfo->JobID,
            "ItemID" => $jobRequirementInfo->RequirementID,
            "Qty" => $jobRequirementInfo->Quantity,
            "Notes" => $jobRequirementInfo->Description,
            "OperatorID" => $input['operatorID'],
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          array_push($jobRequirementList, $jobRequirementRecord);

        }

      }


      $timesheets = json_decode($input['timesheets']);
      $timesheetList = array();

      if( count($timesheets) > 0 ){

        foreach( $timesheets as $key => $timesheetInfo ){

          $contactInfo = $this->contactRepository->findByContactReportData($timesheetInfo->WorkerID, $timesheetInfo->JobID);

          if( !$contactInfo ){

            $contactInfo = [];
            $contactInfo['ContactName'] = "";
            $contactInfo['ContactCode'] = "";
            $contactInfo['Description'] = "";

          }

          $timesheetRecord = [
            "JobID" => $timesheetInfo->JobID,
            "WorkerID" => $timesheetInfo->WorkerID,
            "ShiftID" => $timesheetInfo->ShiftID,
            "ContactName" => $contactInfo['ContactName'],
            "ContactCode" => $contactInfo['ContactCode'],
            "ShiftType" => $contactInfo['Description'],
            "JobStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobStartDateTime / 1000),
            "JobFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobFinishDateTime / 1000),
            "TravelStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTime / 1000),
            "TravelFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTime / 1000),
            "BreakStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakStartDateTime / 1000),
            "BreakFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakFinishDateTime / 1000),
            "TravelStartDateTimeEnd" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTimeEnd / 1000),
            "TravelFinishDateTimeStart" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTimeStart / 1000),
            "TravelStartDistance" => $timesheetInfo->TravelStartDistance,
            "TravelFinishDistance" => $timesheetInfo->TravelFinishDistance,
            "FatigueCompliance" => $timesheetInfo->FatigueCompliance,
            "RegoNo" => $timesheetInfo->RegoNo,
            "TraveledKilometers" => $timesheetInfo->TraveledKilometers,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          if( $timesheetInfo->AttendDepot == 1 ){
            $timesheetRecord['AttendDepot'] = "TRUE";
          }
          else{
            $timesheetRecord['AttendDepot'] = "FALSE";
          }

          if( $timesheetInfo->FatigueCompliance == 1 ){
            $timesheetRecord['FatigueCompliance'] = "TRUE";
          }
          else{
            $timesheetRecord['FatigueCompliance'] = "FALSE";
          }

          array_push($timesheetList, $timesheetRecord);



        }

      }


      $docket = json_decode($input['dockets']);
      $clientSigned = "FALSE";

      if( count($docket) > 0 ){

        foreach( $docket as $key => $docketInfo ){

          //Check if client signed
          if( $docketInfo->AttachmentTypeID == 1 ){

            $clientSigned = "TRUE";

          }

        }

      }


      if( isset($afterCareCount) && $afterCareCount > 0 ){

        $afterCarePresent = "TRUE";

      }




      $geotag = "";

      if( isset($input['geotag'])){

        $geotag = $input['geotag'];

      }

      //Get Shift Length
      $shiftLength = 0;

      if( count($timesheets) > 0 ){

        foreach( $timesheets as $key => $timesheetInfo ){

          $travelStart = date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTime / 1000);
          $travelFinish = date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTime / 1000);

          $contactInfo = $this->contactRepository->findByContactReportData($timesheetInfo->WorkerID, $timesheetInfo->JobID);

          $strings = explode(' ', $contactInfo['Description']);

          if( in_array("Driver", $strings) ){

            $shiftLength = ( strtotime($travelFinish) - strtotime($travelStart) ) / 60;

          }

        }

      }

      //Get Job Images

      $jobImages = $this->jobImageRepository->get($input['jobID']);

      //Prepare Site Assessment eForm Report
      $tcData = array();

      $generateFilePath = Config::get('evolution.generatedFilePath');
      $enableWatchFolder = Config::get('evolution.watchFolderEnable');

      $reportInfo = [
        'jobId' => $input['jobID'],
        'location' => $jobInfo['Address'],
        'client' => $jobInfo['CompanyName'],
        'state' => $state,
        'orderNumber' => $input['orderNumber'],
        'siteaudit' => $siteChecklistReportData,
        'additionalHazards' => $additionalHazardReportData,
        'signageAudit' => $sinageAuditReportData,
        'tcSignature' => $tcSignatureData
      ];

      $pdf = PDF::loadView('forms.jobsitechecklist', $reportInfo)->setPaper('a4','portrait');

      $pdf->output();
      $dom_pdf = $pdf->getDomPDF();

      $canvas = $dom_pdf ->get_canvas();
      $canvas->page_text(275, 810, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 8, array(255, 255, 255));
      $pdf->save($generateFilePath.'JOBDOCKET_'.$input['jobID'].'.pdf');

      $uploadPDFToWatchFolder = false;

      if( $enableWatchFolder ){

        $data = [
          'username' => Config::get('evolution.watchFolderUsername'),
          'password' => Config::get('evolution.watchFolderPassword'),
          'server' => Config::get('evolution.watchFolderHost'),
          'filepath' => Config::get('evolution.generatedFilePath'),
          'foldername' => 'APPDOC',
          'old_filename' => 'JOBDOCKET_'.$input['jobID'].'.pdf',
          'new_filename' => 'JOBDOCKET_'.$input['jobID'].'.pdf'
        ];

        $uploadPDFToWatchFolder = $this->fileServices->uploadToWatchFolder($data);

      }

      //Prepare Site Assessment CSV
      $jobReportInfo = $this->jobHeaderRepository->getReportDataById($input['jobID']);

      $csvInfo = [
        'fileName' => 'JOBDOCKET_'.$jobReportInfo['JobID'].'.pdf',
        'formId' => 'DTMCD002',
        'jobNo' => $jobReportInfo['JobCode'],
        'jobID' => $jobReportInfo['JobID'],
        'jobDate' => date('d/m/Y',strtotime($jobReportInfo['JobStartdate'])),
        'jobDateMonth' => date('F',strtotime($jobReportInfo['JobStartdate'])),
        'jobDateYear' => date('Y',strtotime($jobReportInfo['JobStartdate'])),
        'clientCode' => $jobReportInfo['ContactCode'],
        'clientName' => $jobReportInfo['CompanyName'],
        'depot' => $jobReportInfo['Depot'],
        'state' => $jobReportInfo['JobState'],
        'orderNo' => $input['orderNumber'],
        'location1' => $jobReportInfo['Address'],
        'clientSign' => $clientSigned,
        'afterCare' => $afterCarePresent,
        'swmsTimeStamp' => '',
        'swmsUser' => $contactCode,
        'swmsGeotag' => $geotag,
        'rego' => [],
        'requirement' => $jobRequirementList,
        'timesheet' => $timesheetList,
        'siteaudit' => $siteChecklistReportData,
        'additionalHazards' => $additionalHazardReportData,
        'signageAudit' => $sinageAuditReportData,
        'tcSignature' => $tcSignatureData,
        'jobImages' => $jobImages,
        'shiftLength' => $shiftLength
      ];

      Excel::create($input['jobID'].'_JD', function ($excel) use ($csvInfo) {

        // Set the title
        $excel->setTitle('Job Docket eForm');

        // // Chain the setters
        $excel->setCreator('Evolution')
              ->setCompany('Evolution');

        // // Call them separately
        $excel->setDescription('Job Docket eForm');

        // Set sheets
        $excel->sheet('Site Assessment CSV', function($sheet) use ($csvInfo) {

            $sheet->loadView('forms.jobdocketcsv',$csvInfo);

            $sheet->freezeFirstRow();

        });

      })->store('csv', public_path('generated files'), true);


      $uploadCSVToWatchFolder = false;

      if( $enableWatchFolder ){

        $data = [
          'username' => Config::get('evolution.watchFolderUsername'),
          'password' => Config::get('evolution.watchFolderPassword'),
          'server' => Config::get('evolution.watchFolderHost'),
          'filepath' => Config::get('evolution.generatedFilePath'),
          'foldername' => 'APPDOC',
          'old_filename' => $input['jobID'].'_JD.csv',
          'new_filename' => $input['jobID'].'_JD.csv'
        ];

        $uploadCSVToWatchFolder = $this->fileServices->uploadToWatchFolder($data);

      }

      $siteAssessmentPath = Config::get('evolution.siteAssessmentPath');

      if( file_exists($siteAssessmentPath) ){

        $pdf = PDF::loadView('forms.jobsitechecklist', $reportInfo)->setPaper('a4','portrait');

        $pdf->output();
        $dom_pdf = $pdf->getDomPDF();

        $canvas = $dom_pdf ->get_canvas();
        $canvas->page_text(275, 810, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 8, array(255, 255, 255));
        $pdf->save($siteAssessmentPath.'JOBDOCKET_'.$input['jobID'].'.pdf');

        Excel::create(''.$input['jobID'].'_JD', function ($excel) use ($csvInfo) {

          // Set the title
          $excel->setTitle('Job Docket eForm');

          // // Chain the setters
          $excel->setCreator('Evolution')
                ->setCompany('Evolution');

          // // Call them separately
          $excel->setDescription('Job Docket eForm');

          // Set sheets
          $excel->sheet('Site Assessment CSV', function($sheet) use ($csvInfo) {

              $sheet->loadView('forms.jobdocketcsv',$csvInfo);

              $sheet->freezeFirstRow();

          });

        })->store('csv', $siteAssessmentPath, true);


      }



      $emailClientData = array(
        'fullname' => $input['clientName'],
        'email' => Config::get('evolution.evolutionEmail'),
        'jobID' => $jobInfo['JobID'],
        'companyName' => $jobInfo['CompanyName'],
        'address' => $jobInfo['Address'],
        'dateStarted' => date('d-F-Y',strtotime($jobInfo['StartDateTime'])),
        'depot' => $jobInfo['Depot'],
        'authorized' => $jobInfo['AuthorizedPerson'],
        'tcData' => $tcData,
        'attachment' => [
          public_path('generated files/JOBDOCKET_'.$input['jobID'].'.pdf'),
          public_path('generated files/'.$input['jobID'].'_JD.csv')
        ]
      );



      $this->emailServices->sendEmail(3,$emailClientData);

      if( $uploadPDFToWatchFolder && $uploadCSVToWatchFolder ){

        $countGenerateReport = $this->generateReportRepository->countByJobID($input['jobID']);

        if( $countGenerateReport > 0 ){

          $generateInfo = [
            'JobDocket' => 1,
            'updated_at' => Carbon::now()
          ];

          $updateGenerateReport = $this->generateReportRepository->updateByJobID($generateInfo, $input['jobID']);

        }
        else{

          $generateInfo = [
            'JobID' => $input['jobID'],
            'JobDocket' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
          ];

          $insertGenerateReport = $this->generateReportRepository->create($generateInfo, $input['jobID']);

        }

        $response['status'] = true;
        $response['statusCode'] = 200;
        $response['message'] = "Job Docket Report for ".$input['jobID']." has been created";

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 422;
        $response['message'] = "Job Docket Report for ".$input['jobID']." was unsuccessfully created";

      }

    }
    else{
      //create additional hazards
      $additionalHazards = json_decode($input['additionalHazards']);
      $additionalHazardReportData = [];

      if( count($additionalHazards) > 0 ){

          $hazardList = array();

          foreach( $additionalHazards as $key => $hazardInfo ){

            $hazardRecord = [
              // "JobID" => $hazardInfo->JobID,
              "AddHazard" => $hazardInfo->AddHazard,
              "InitialRisk" => $hazardInfo->InitialRisk,
              "ControlMeasures" => $hazardInfo->ControlMeasures,
              "ResidualRisk" => $hazardInfo->ResidualRisk,
              // "created_at" => Carbon::now(),
              // "updated_at" => Carbon::now()
            ];

            array_push($hazardList, $hazardRecord);
            array_push($additionalHazardReportData,$hazardRecord);

          }

      }
      $additionalHazards = json_decode($input['additionalHazards']);


      //create tc signatures
      $tcData = json_decode($input['tcAttachment']);

      if( count($tcData) > 0 ){

        $tcList = array();

        foreach( $tcData as $key => $tcInfo ){

          $filename_path = md5(time().uniqid()).".jpg";
          $signatureDecode = base64_decode($tcInfo->Attachment);
          file_put_contents(public_path("image/signatures/".$filename_path),$signatureDecode);

          $tcRecord = [
            "JobID" => $tcInfo->JobID,
            "TCName" => $tcInfo->TCName,
            "AttachedOn" => date('Y-m-d H:i:s',$tcInfo->AttachedOn / 1000),
            // "Attachment" => $tcInfo->Attachment,
            "Src" => $tcInfo->Attachment,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];


          array_push($tcList, $tcRecord);

          if( !isset($tcSignatureData)){

            $tcSignatureData = [];

          }

          array_push($tcSignatureData,$tcRecord);

        }

      }

      //Get Job Info
      $jobInfo = $this->jobHeaderRepository->getById($input['parentJob']);

      $state = "";
      $depot = $jobInfo['Depot'];

      if( $jobInfo['JobState'] == "" ){

        $state = $this->checkState($depot);

      }
      else{

        $state = $jobInfo['JobState'];

      }

      //create signage audit

      $sinageAuditReportData = [];
      $sinageAuditReportData['SlowLane'] = [];
      $sinageAuditReportData['FastLane'] = [];
      $afterCarePresent = "FALSE";
      $afterCareCount = 0;

      $signageAudit = json_decode($input['signageAudit']);



      if( $input['signageAudit'] != "" && count($signageAudit) > 0 ){

        foreach( $signageAudit as $key => $signage ){



          $sinageRecord = [
            "JobID" => $signage->JobID,
            "Landmark" => $signage->Landmark,
            "CarriageWay" => $signage->CarriageWay,
            "Direction" => $signage->Direction,
            "TimeErected" => date('H:i:s',$signage->TimeErected / 1000),
            "TimeCollected" => date('H:i:s',$signage->TimeCollected / 1000),
            "TimeChecked1" => date('H:i:s',$signage->TimeChecked1 / 1000),
            "TimeChecked2" => date('H:i:s',$signage->TimeChecked2 / 1000),
            "TimeChecked3" => date('H:i:s',$signage->TimeChecked3 / 1000),
            "TimeChecked4" => date('H:i:s',$signage->TimeChecked4 / 1000),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];



          

          if( $signage->AuditSigns != "" ){

            $auditSignsList = explode('~',$signage->AuditSigns);

            foreach( $auditSignsList as $auditSignInfo ){

              $signID = explode(':',explode('^',$auditSignInfo)[0])[1];
              $metres = explode(':',explode('^',$auditSignInfo)[1])[1];
              $qty = explode(':',explode('^',$auditSignInfo)[2])[1];
              $afterCare = explode(':',explode('^',$auditSignInfo)[3])[1];
              $afterCareMetres = explode(':',explode('^',$auditSignInfo)[4])[1];
              $afterCareQty = explode(':',explode('^',$auditSignInfo)[5])[1];

              if( $afterCare > 0 ){

                $afterCareCount++;

              }

              $auditRecord = [
                "JobID" => $signage->JobID,
                "SignID" => $signID,
                "Metres" => $metres,
                "Src" => 'image\signage\\'.$state.'-Icons\sign_'.$signID.'.png',
                "Qty" => $qty,
                "AfterCare" => $afterCare,
                "AfterCareMetres" => $afterCareMetres,
                "AfterCareQty" => $afterCareQty,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
              ];

              if( $afterCare > 0 ){

                $afterCareCount++;

              }

              switch( $signage->CarriageWay ){
                case 1:

                  array_push($sinageAuditReportData['SlowLane'],$auditRecord);

                break;
                case 2:

                  array_push($sinageAuditReportData['FastLane'],$auditRecord);

                break;
              }

            }
            

          }


          switch( $signage->Direction ){
            case 1:
              $sinageAuditReportData['Direction'] = "North";
            break;
            case 2:
              $sinageAuditReportData['Direction'] = "South";
            break;
            case 3:
              $sinageAuditReportData['Direction'] = "East";
            break;
            case 4:
              $sinageAuditReportData['Direction'] = "West";
            break;
            default:
              $sinageAuditReportData['Direction'] = "";
            break;
          }

          $sinageAuditReportData['LocalLandmark'] = $signage->Landmark;

          $sinageAuditReportData['TimeErected'] = $signage->TimeErected;
          $sinageAuditReportData['TimeCollected'] = $signage->TimeCollected;
          $sinageAuditReportData['TimeChecked1'] = $signage->TimeChecked1;
          $sinageAuditReportData['TimeChecked2'] = $signage->TimeChecked2;
          $sinageAuditReportData['TimeChecked3'] = $signage->TimeChecked3;
          $sinageAuditReportData['TimeChecked4'] = $signage->TimeChecked4;

        }

      }

      $jobRequirements = json_decode($input['requirements']);
      $jobRequirementList = array();

      if( count($jobRequirements) > 0 ){

        foreach( $jobRequirements as $key => $jobRequirementInfo ){

          $jobRequirementRecord = [
            "JobID" => $jobRequirementInfo->JobID,
            "ItemID" => $jobRequirementInfo->RequirementID,
            "Qty" => $jobRequirementInfo->Quantity,
            "Notes" => $jobRequirementInfo->Description,
            "OperatorID" => $input['operatorID'],
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          array_push($jobRequirementList, $jobRequirementRecord);

        }

      }

      $timesheets = json_decode($input['timesheets']);
      $timesheetList = array();



      if( count($timesheets) > 0 ){

        foreach( $timesheets as $key => $timesheetInfo ){

          $contactInfo = $this->contactRepository->findByContactReportData($timesheetInfo->WorkerID, $input['parentJob']);

          $timesheetRecord = [
            "JobID" => $timesheetInfo->JobID,
            "WorkerID" => $timesheetInfo->WorkerID,
            "ShiftID" => $timesheetInfo->ShiftID,
            "ContactName" => $contactInfo['ContactName'],
            "ContactCode" => $contactInfo['ContactCode'],
            "ShiftType" => $contactInfo['Description'],
            "JobStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobStartDateTime / 1000),
            "JobFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobFinishDateTime / 1000),
            "TravelStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTime / 1000),
            "TravelFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTime / 1000),
            "BreakStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakStartDateTime / 1000),
            "BreakFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakFinishDateTime / 1000),
            "TravelStartDateTimeEnd" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTimeEnd / 1000),
            "TravelFinishDateTimeStart" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTimeStart / 1000),
            "TravelStartDistance" => $timesheetInfo->TravelStartDistance,
            "TravelFinishDistance" => $timesheetInfo->TravelFinishDistance,
            "FatigueCompliance" => $timesheetInfo->FatigueCompliance,
            "RegoNo" => $timesheetInfo->RegoNo,
            "TraveledKilometers" => $timesheetInfo->TraveledKilometers,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          if( $timesheetInfo->AttendDepot == 1 ){
            $timesheetRecord['AttendDepot'] = "TRUE";
          }
          else{
            $timesheetRecord['AttendDepot'] = "FALSE";
          }

          if( $timesheetInfo->FatigueCompliance == 1 ){
            $timesheetRecord['FatigueCompliance'] = "TRUE";
          }
          else{
            $timesheetRecord['FatigueCompliance'] = "FALSE";
          }


          array_push($timesheetList, $timesheetRecord);

        }

      }





      $docket = json_decode($input['dockets']);
      $clientSigned = "FALSE";

      if( count($docket) > 0 ){

        foreach( $docket as $key => $docketInfo ){

          //Check if client signed
          if( $docketInfo->AttachmentTypeID == 1 ){

            $clientSigned = "TRUE";

          }

        }

      }


      if( $afterCareCount > 0 ){

        $afterCarePresent = "TRUE";

      }

      $geotag = "";

      if( isset($input['geotag'])){

        $geotag = $input['geotag'];

      }


      //Get Shift Length
      $shiftLength = 0;

      if( count($timesheets) > 0 ){

        foreach( $timesheets as $key => $timesheetInfo ){

          $travelStart = date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTime / 1000);
          $travelFinish = date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTime / 1000);

          $contactInfo = $this->contactRepository->findByContactReportData($timesheetInfo->WorkerID, $input['parentJob']);

          $strings = explode(' ', $contactInfo['Description']);

          if( in_array("Driver", $strings) ){

            $shiftLength = ( strtotime($travelFinish) - strtotime($travelStart) ) / 60;

          }

        }

      }

      //Get Job Images

      $jobImages = $this->jobImageRepository->get($input['jobID']);

      $generateFilePath = Config::get('evolution.generatedFilePath');
      $enableWatchFolder = Config::get('evolution.watchFolderEnable');

      //Prepare Site Assessment eForm Report
      $tcData = array();

      $reportInfo = [
        'jobId' => $input['jobID'],
        'location' => $jobInfo['Address'],
        'client' => $jobInfo['CompanyName'],
        'state' => $state,
        'orderNumber' => $input['orderNumber'],
        'siteaudit' => $siteChecklistReportData,
        'additionalHazards' => $additionalHazardReportData,
        'signageAudit' => $sinageAuditReportData,
        'tcSignature' => $tcSignatureData
      ];


      $pdf = PDF::loadView('forms.jobsitechecklist', $reportInfo)->setPaper('a4','portrait');
      $pdf->output();
      $dom_pdf = $pdf->getDomPDF();

      $canvas = $dom_pdf ->get_canvas();
      $canvas->page_text(275, 810, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 8, array(255, 255, 255));
      $pdf->save($generateFilePath.'JOBDOCKET_'.$input['jobID'].'.pdf');


      $uploadPDFToWatchFolder = false;

      if( $enableWatchFolder ){

        $data = [
          'username' => Config::get('evolution.watchFolderUsername'),
          'password' => Config::get('evolution.watchFolderPassword'),
          'server' => Config::get('evolution.watchFolderHost'),
          'filepath' => Config::get('evolution.generatedFilePath'),
          'foldername' => 'APPDOC',
          'old_filename' => 'JOBDOCKET_'.$input['jobID'].'.pdf',
          'new_filename' => 'JOBDOCKET_'.$input['jobID'].'.pdf'
        ];

        $uploadPDFToWatchFolder = $this->fileServices->uploadToWatchFolder($data);

      }

      //Prepare Site Assessment CSV
      $jobReportInfo = $this->jobHeaderRepository->getReportDataById($input['parentJob']);

      $csvInfo = [
        'fileName' => 'JOBDOCKET_'.$input['jobID'].'.pdf',
        'formId' => 'DTMCD002',
        'jobNo' => $jobReportInfo['JobCode'],
        'jobID' => $jobReportInfo['JobID'],
        'jobDate' => date('d/m/Y',strtotime($jobReportInfo['JobStartdate'])),
        'jobDateMonth' => date('F',strtotime($jobReportInfo['JobStartdate'])),
        'jobDateYear' => date('Y',strtotime($jobReportInfo['JobStartdate'])),
        'clientCode' => $jobReportInfo['ContactCode'],
        'clientName' => $jobReportInfo['CompanyName'],
        'depot' => $jobReportInfo['Depot'],
        'state' => $jobReportInfo['JobState'],
        'orderNo' => $input['orderNumber'],
        'location1' => $jobReportInfo['Address'],
        'clientSign' => $clientSigned,
        'afterCare' => $afterCarePresent,
        'swmsTimeStamp' => '',
        'swmsUser' => $contactCode,
        'swmsGeotag' => $geotag,
        'rego' => [],
        'requirement' => $jobRequirementList,
        'timesheet' => $timesheetList,
        'siteaudit' => $siteChecklistReportData,
        'additionalHazards' => $additionalHazardReportData,
        'signageAudit' => $sinageAuditReportData,
        'tcSignature' => $tcSignatureData,
        'jobImages' => $jobImages,
        'shiftLength' => $shiftLength
      ];


      Excel::create($input['jobID'].'_JD', function ($excel) use ($csvInfo) {

        // Set the title
        $excel->setTitle('Job Docket eForm');

        // Chain the setters
        $excel->setCreator('Evolution')
              ->setCompany('Evolution');

        // Call them separately
        $excel->setDescription('Job Docket eForm');

        // Set sheets
        $excel->sheet('Site Assessment CSV', function($sheet) use ($csvInfo) {

            $sheet->loadView('forms.jobdocketcsv',$csvInfo);

            $sheet->freezeFirstRow();

        });


      })->store('csv', public_path('generated files'), true);

      $uploadCSVToWatchFolder = false;

      if( $enableWatchFolder ){

        $data = [
          'username' => Config::get('evolution.watchFolderUsername'),
          'password' => Config::get('evolution.watchFolderPassword'),
          'server' => Config::get('evolution.watchFolderHost'),
          'filepath' => Config::get('evolution.generatedFilePath'),
          'foldername' => 'APPDOC',
          'old_filename' => $input['jobID'].'_JD.csv',
          'new_filename' => $input['jobID'].'_JD.csv'
        ];

        $uploadCSVToWatchFolder = $this->fileServices->uploadToWatchFolder($data);

      }



      $siteAssessmentPath = Config::get('evolution.siteAssessmentPath');

      if( file_exists($siteAssessmentPath) ){

        $pdf = PDF::loadView('forms.jobsitechecklist', $reportInfo)->setPaper('a4','portrait');

        $pdf->output();
        $dom_pdf = $pdf->getDomPDF();

        $canvas = $dom_pdf ->get_canvas();
        $canvas->page_text(275, 810, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 8, array(255, 255, 255));
        $pdf->save($siteAssessmentPath.'JOBDOCKET_'.$input['jobID'].'.pdf');

        Excel::create($input['jobID'].'_JD', function ($excel) use ($csvInfo) {

          // Set the title
          $excel->setTitle('Job Docket eForm');

          // // Chain the setters
          $excel->setCreator('Evolution')
                ->setCompany('Evolution');

          // // Call them separately
          $excel->setDescription('Job Docket eForm');

          // Set sheets
          $excel->sheet('Site Assessment CSV', function($sheet) use ($csvInfo) {

              $sheet->loadView('forms.jobdocketcsv',$csvInfo);

              $sheet->freezeFirstRow();

          });

        })->store('csv', $siteAssessmentPath, true);

      }

      $emailClientData = array(
        'fullname' => $input['clientName'],
        'email' => 'appdata@evolutiontraffic.com.au',
        'jobID' => $jobInfo['JobID'],
        'companyName' => $jobInfo['CompanyName'],
        'address' => $jobInfo['Address'],
        'dateStarted' => date('d-F-Y',strtotime($jobInfo['StartDateTime'])),
        'depot' => $jobInfo['Depot'],
        'authorized' => $jobInfo['AuthorizedPerson'],
        'tcData' => $tcData,
        'attachment' => [
          public_path('generated files/JOBDOCKET_'.$input['jobID'].'.pdf'),
          public_path('generated files/'.$input['jobID'].'_JD.csv')
        ]
      );

      $this->emailServices->sendEmail(3,$emailClientData);


      if( $uploadPDFToWatchFolder && $uploadCSVToWatchFolder ){

        $countGenerateReport = $this->generateReportRepository->countByJobID($input['jobID']);

        if( $countGenerateReport > 0 ){

          $generateInfo = [
            'JobDocket' => 1,
            'updated_at' => Carbon::now()
          ];

          $updateGenerateReport = $this->generateReportRepository->updateByJobID($generateInfo, $input['jobID']);

        }
        else{

          $generateInfo = [
            'JobID' => $input['jobID'],
            'JobDocket' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
          ];

          $insertGenerateReport = $this->generateReportRepository->create($generateInfo, $input['jobID']);

        }

        $response['status'] = true;
        $response['statusCode'] = 200;
        $response['message'] = "Job Docket Report for ".$input['jobID']." has been created";

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 422;
        $response['message'] = "Job Docket Report for ".$input['jobID']." was unsuccessfully created";

      }


    }

    return $response;

  }


  public function generateAppSafetyReport($input, $contactId, $contactCode){

    $timezone = $this->googleTimezoneServices->setTimezone($input['geotag']);

    if( !$timezone ){

      $response['status'] = false;
      $response['statusCode'] = 422;
      $response['message'] = "Failed to set Timezone";

      return $response;

    }

    //filter inputs for IOS
    $input = $this->inputHelper->filterInput($input);

    $siteChecklistSections = [];
    $siteChecklist = [];
    $siteChecklistReportData = [];
    $additionalHazardReportData = [];
    $sinageAuditReportData = [];

    //Set site checklist
    if( $input['siteCheckList'] ){

      $checkSiteChecklist = $this->siteChecklistGateway->checkSiteChecklist($input,$contactId);

      if( $checkSiteChecklist['status'] ){

        $siteChecklistSections = $checkSiteChecklist['siteChecklistSections'];
        $siteChecklist = $checkSiteChecklist['siteChecklist'];
        $siteChecklistReportData = $checkSiteChecklist['siteChecklistReportData'];
        $jobSiteCheckListItems = json_encode($checkSiteChecklist['jobSiteCheckListItems']);
        $siteCheckListInputValues = json_encode($checkSiteChecklist['siteCheckListInputValues']);

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 400;
        $response['message']['siteCheckList'] = $checkSiteChecklist['message'];

        return $response;

      }

    }

    //Check if Main Job
    if( $input['parentJob'] == "" ){

      //create additional hazards
      $additionalHazards = json_decode($input['additionalHazards']);
      $additionalHazardReportData = [];

      if( count($additionalHazards) > 0 ){

          $hazardList = array();

          foreach( $additionalHazards as $key => $hazardInfo ){

            $hazardRecord = [
              // "JobID" => $hazardInfo->JobID,
              "AddHazard" => $hazardInfo->AddHazard,
              "InitialRisk" => $hazardInfo->InitialRisk,
              "ControlMeasures" => $hazardInfo->ControlMeasures,
              "ResidualRisk" => $hazardInfo->ResidualRisk,
              // "created_at" => Carbon::now(),
              // "updated_at" => Carbon::now()
            ];

            array_push($hazardList, $hazardRecord);
            array_push($additionalHazardReportData,$hazardRecord);

          }

      }


      //create tc signatures
      $tcData = json_decode($input['tcAttachment']);
      $tcSignatureData = [];

      if( count($tcData) > 0 ){

        $tcList = array();

        foreach( $tcData as $key => $tcInfo ){

          // $filename_path = md5(time().uniqid()).".jpg";
          // $signatureDecode = base64_decode($tcInfo->Attachment);
          // file_put_contents(public_path("image/signatures/".$filename_path),$signatureDecode);

          $tcRecord = [
            "JobID" => $tcInfo->JobID,
            "TCName" => $tcInfo->TCName,
            "AttachedOn" => date('Y-m-d H:i:s',$tcInfo->AttachedOn / 1000),
            // "Attachment" => $tcInfo->Attachment,
            "Src" => $tcInfo->Attachment,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];


          array_push($tcList, $tcRecord);
          array_push($tcSignatureData,$tcRecord);

        }

      }

      //Job Info
      $jobInfo = $this->jobHeaderRepository->getById($input['jobID']);

      $state = "";
      $depot = $jobInfo['Depot'];

      if( $jobInfo['JobState'] == "" ){

        $state = $this->checkState($depot);

      }
      else{

        $state = $jobInfo['JobState'];

      }



      //create signage audit

      $afterCarePresent = "FALSE";
      $sinageAuditReportData = [];
      $afterCareCount = 0;

      $signageAudit = json_decode($input['signageAudit']);

      if( $input['signageAudit'] != "" && count($signageAudit) > 0 ){

        foreach( $signageAudit as $key => $signage ){

          $sinageAuditReportData = [
            "JobID" => $signage->JobID,
            "Landmark" => $signage->Landmark,
            "CarriageWay" => $signage->CarriageWay,
            "Direction" => $signage->Direction,
            "TimeErected" => date('H:i',$signage->TimeErected / 1000),
            "TimeCollected" => date('H:i',$signage->TimeCollected / 1000),
            "TimeChecked1" => date('H:i',$signage->TimeChecked1 / 1000),
            "TimeChecked2" => date('H:i',$signage->TimeChecked2 / 1000),
            "TimeChecked3" => date('H:i',$signage->TimeChecked3 / 1000),
            "TimeChecked4" => date('H:i',$signage->TimeChecked4 / 1000),
            "ErectedBy" => $signage->ErectedBy,
            "CollectedBy" => $signage->CollectedBy,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
            'SlowLane' => [],
            'FastLane' => []
          ];

          
          
          if( $signage->AuditSigns != "" ){

            $auditSignsList = explode('~',$signage->AuditSigns);

            foreach( $auditSignsList as $auditSignInfo ){

              $signID = explode(':',explode('^',$auditSignInfo)[0])[1];
              $metres = explode(':',explode('^',$auditSignInfo)[1])[1];
              $qty = explode(':',explode('^',$auditSignInfo)[2])[1];
              $afterCare = explode(':',explode('^',$auditSignInfo)[3])[1];
              $afterCareMetres = explode(':',explode('^',$auditSignInfo)[4])[1];
              $afterCareQty = explode(':',explode('^',$auditSignInfo)[5])[1];

              $auditRecord = [
                "JobID" => $signage->JobID,
                "SignID" => $signID,
                "Metres" => $metres,
                "Src" => 'image\signage\\'.$state.'-Icons\sign_'.$signID.'.png',
                "Qty" => $qty,
                "AfterCare" => $afterCare,
                "AfterCareMetres" => $afterCareMetres,
                "AfterCareQty" => $afterCareQty,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
              ];

              if( $afterCare > 0 ){

                $afterCareCount++;

              }


              switch( $signage->CarriageWay ){
                case 1:

                  array_push($sinageAuditReportData['SlowLane'],$auditRecord);

                break;
                case 2:

                  array_push($sinageAuditReportData['FastLane'],$auditRecord);

                break;
              }

            }

          }


          switch( $signage->Direction ){
            case 1:
              $sinageAuditReportData['Direction'] = "North";
            break;
            case 2:
              $sinageAuditReportData['Direction'] = "South";
            break;
            case 3:
              $sinageAuditReportData['Direction'] = "East";
            break;
            case 4:
              $sinageAuditReportData['Direction'] = "West";
            break;
            default:
              $sinageAuditReportData['Direction'] = "";
            break;
          }

          $sinageAuditReportData['LocalLandmark'] = $signage->Landmark;


        }

      }


      $jobRequirements = json_decode($input['requirements']);
      $jobRequirementList = array();

      if( count($jobRequirements) > 0 ){

        foreach( $jobRequirements as $key => $jobRequirementInfo ){

          $jobRequirementRecord = [
            "JobID" => $jobRequirementInfo->JobID,
            "ItemID" => $jobRequirementInfo->RequirementID,
            "Qty" => $jobRequirementInfo->Quantity,
            "Notes" => $jobRequirementInfo->Description,
            "OperatorID" => $input['operatorID'],
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          array_push($jobRequirementList, $jobRequirementRecord);

        }

      }



      $timesheets = json_decode($input['timesheets']);
      $timesheetList = array();

      if( count($timesheets) > 0 ){

        foreach( $timesheets as $key => $timesheetInfo ){

          $contactInfo = $this->contactRepository->findByContactReportData($timesheetInfo->WorkerID, $timesheetInfo->JobID);

          $timesheetRecord = [
            "JobID" => $timesheetInfo->JobID,
            "WorkerID" => $timesheetInfo->WorkerID,
            "ShiftID" => $timesheetInfo->ShiftID,
            "ContactName" => $contactInfo['ContactName'],
            "ContactCode" => $contactInfo['ContactCode'],
            "ShiftType" => $contactInfo['Description'],
            "AttendDepot" => "FALSE",
            "JobStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobStartDateTime / 1000),
            "JobFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobFinishDateTime / 1000),
            "TravelStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTime / 1000),
            "TravelFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTime / 1000),
            "BreakStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakStartDateTime / 1000),
            "BreakFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakFinishDateTime / 1000),
            "TravelStartDateTimeEnd" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTimeEnd / 1000),
            "TravelFinishDateTimeStart" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTimeStart / 1000),
            "TravelStartDistance" => $timesheetInfo->TravelStartDistance,
            "TravelFinishDistance" => $timesheetInfo->TravelFinishDistance,
            "FatigueCompliance" => $timesheetInfo->FatigueCompliance,
            "RegoNo" => $timesheetInfo->RegoNo,
            "TraveledKilometers" => $timesheetInfo->TraveledKilometers,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          array_push($timesheetList, $timesheetRecord);

        }

      }

      $docket = json_decode($input['dockets']);
      $clientSigned = "FALSE";

      if( count($docket) > 0 ){

        foreach( $docket as $key => $docketInfo ){

          //Check if client signed
          if( $docketInfo->AttachmentTypeID == 1 ){

            $clientSigned = "TRUE";

          }

        }

      }


      if( isset($afterCareCount) && $afterCareCount > 0 ){

        $afterCarePresent = "TRUE";

      }


      $geotag = "";

      if( isset($input['geotag'])){

        $geotag = $input['geotag'];

      }


      //Get Shift Length
      $shiftLength = 0;

      if( count($timesheets) > 0 ){

        foreach( $timesheets as $key => $timesheetInfo ){

          $travelStart = date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTime / 1000);
          $travelFinish = date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTime / 1000);

          $contactInfo = $this->contactRepository->findByContactReportData($timesheetInfo->WorkerID, $timesheetInfo->JobID);

          $strings = explode(' ', $contactInfo['Description']);

          if( in_array("Driver", $strings) ){

            $shiftLength = ( strtotime($travelFinish) - strtotime($travelStart) ) / 60;

          }

        }

      }



      //Get Job Images

      $jobImages = $this->jobImageRepository->get($input['jobID']);

      //Prepare Site Assessment eForm Report
      $tcData = array();

      $generateFilePath = Config::get('evolution.generatedFilePath');
      $enableWatchFolder = Config::get('evolution.watchFolderEnable');

      $reportInfo = [
        'jobId' => $input['jobID'],
        'location' => $jobInfo['Address'],
        'client' => $jobInfo['CompanyName'],
        'state' => $state,
        'orderNumber' => $input['orderNumber'],
        'siteaudit' => $siteChecklistReportData,
        'additionalHazards' => $additionalHazardReportData
      ];

      $pdf = PDF::loadView('forms.appsafety', $reportInfo)->setPaper('a4','portrait');


      $pdf->output();
      $dom_pdf = $pdf->getDomPDF();

      $canvas = $dom_pdf ->get_canvas();
      $canvas->page_text(275, 810, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 8, array(255, 255, 255));
      $pdf->save($generateFilePath.'SAFETYDOCKET_'.$input['jobID'].'.pdf');

      $uploadPDFToWatchFolder = false;

      if( $enableWatchFolder ){

        $data = [
          'username' => Config::get('evolution.watchFolderUsername'),
          'password' => Config::get('evolution.watchFolderPassword'),
          'server' => Config::get('evolution.watchFolderHost'),
          'filepath' => Config::get('evolution.generatedFilePath'),
          'foldername' => 'APPSAFETY',
          'old_filename' => 'SAFETYDOCKET_'.$input['jobID'].'.pdf',
          'new_filename' => 'SAFETYDOCKET_'.$input['jobID'].'.pdf'
        ];

        $uploadPDFToWatchFolder = $this->fileServices->uploadToWatchFolder($data);

      }


      //Prepare Site Assessment CSV
      $jobReportInfo = $this->jobHeaderRepository->getReportDataById($input['jobID']);

      $csvInfo = [
        'fileName' => 'SAFETYDOCKET_'.$input['jobID'].'.pdf',
        'formId' => 'DTMCD001',
        'jobNo' => $jobReportInfo['JobCode'],
        'jobID' => $jobReportInfo['JobID'],
        'jobDate' => date('d/m/Y',strtotime($jobReportInfo['JobStartdate'])),
        'jobDateMonth' => date('F',strtotime($jobReportInfo['JobStartdate'])),
        'jobDateYear' => date('Y',strtotime($jobReportInfo['JobStartdate'])),
        'clientCode' => $jobReportInfo['ContactCode'],
        'clientName' => $jobReportInfo['CompanyName'],
        'depot' => $jobReportInfo['Depot'],
        'state' => $jobReportInfo['JobState'],
        'orderNo' => $input['orderNumber'],
        'location1' => $jobReportInfo['Address'],
        'clientSign' => $clientSigned,
        'afterCare' => $afterCarePresent,
        'swmsTimeStamp' => '',
        'swmsUser' => $contactCode,
        'swmsGeotag' => $geotag,
        'rego' => [],
        'requirement' => $jobRequirementList,
        'timesheet' => $timesheetList,
        'siteaudit' => $siteChecklistReportData,
        'additionalHazards' => $additionalHazardReportData,
        'signageAudit' => $sinageAuditReportData,
        'tcSignature' => $tcSignatureData,
        'jobImages' => $jobImages,
        'shiftLength' => $shiftLength
      ];

      Excel::create($input['jobID'].'_SD', function ($excel) use ($csvInfo, $siteChecklist, $siteChecklistSections) {

        // Set the title
        $excel->setTitle('Safety Docket eForm');

        // // Chain the setters
        $excel->setCreator('Evolution')
              ->setCompany('Evolution');

        // // Call them separately
        $excel->setDescription('Safety Docket eForm');

        // Set sheets
        $excel->sheet('Site Assessment CSV', function($sheet) use ($csvInfo, $siteChecklist, $siteChecklistSections) {

            if( $siteChecklist && $siteChecklistSections ){

              $sheet->loadView('forms.appsafetycsv',$csvInfo);

            }
            else{

              $sheet->loadView('forms.appsafetycsvfail',$csvInfo);

            }

            $sheet->freezeFirstRow();

        });

      })->store('csv', public_path('generated files'), true);

      $uploadToWatchFolder = false;

      if( $enableWatchFolder ){

        $data = [
          'username' => Config::get('evolution.watchFolderUsername'),
          'password' => Config::get('evolution.watchFolderPassword'),
          'server' => Config::get('evolution.watchFolderHost'),
          'filepath' => Config::get('evolution.generatedFilePath'),
          'foldername' => 'APPSAFETY',
          'old_filename' => $input['jobID'].'_SD.csv',
          'new_filename' => $input['jobID'].'_SD.csv'
        ];

        $uploadToWatchFolder = $this->fileServices->uploadToWatchFolder($data);

      }
      else{

        $uploadToWatchFolder = true;

      }

      $siteAssessmentPath = Config::get('evolution.siteAssessmentPath');

      if( file_exists($siteAssessmentPath) ){

        Excel::create('SAFETYDOCKET_'.$input['jobID'], function ($excel) use ($csvInfo, $siteChecklist, $siteChecklistSections) {

          // Set the title
          $excel->setTitle('Safety Docket eForm');

          // // Chain the setters
          $excel->setCreator('Evolution')
                ->setCompany('Evolution');

          // // Call them separately
          $excel->setDescription('Safety Docket eForm');

          // Set sheets
          $excel->sheet('Site Assessment CSV', function($sheet) use ($csvInfo, $siteChecklist, $siteChecklistSections) {

              if( $siteChecklist && $siteChecklistSections ){

                $sheet->loadView('forms.appsafetycsv',$csvInfo);

              }
              else{

                $sheet->loadView('forms.appsafetycsvfail',$csvInfo);

              }

              $sheet->freezeFirstRow();

          });

        })->store('csv', $siteAssessmentPath, true);


      }

      if( $uploadToWatchFolder ){

        $countGenerateReport = $this->generateReportRepository->countByJobID($input['jobID']);

        if( $countGenerateReport > 0 ){

          $generateInfo = [
            'SafetyDocket' => 1,
            'updated_at' => Carbon::now()
          ];

          $updateGenerateReport = $this->generateReportRepository->updateByJobID($generateInfo, $input['jobID']);

        }
        else{

          $generateInfo = [
            'JobID' => $input['jobID'],
            'SafetyDocket' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
          ];

          $insertGenerateReport = $this->generateReportRepository->create($generateInfo, $input['jobID']);

        }

        $response['status'] = true;
        $response['statusCode'] = 200;
        $response['message'] = "Safety Docket Report for ".$input['jobID']." has been created";

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 422;
        $response['message'] = "Safety Docket Report for ".$input['jobID']." was unsuccessfully created";

      }

    }
    else{

      //create additional hazards
      $additionalHazards = json_decode($input['additionalHazards']);
      $additionalHazardReportData = [];

      if( count($additionalHazards) > 0 ){

          $hazardList = array();

          foreach( $additionalHazards as $key => $hazardInfo ){

            $hazardRecord = [
              // "JobID" => $hazardInfo->JobID,
              "AddHazard" => $hazardInfo->AddHazard,
              "InitialRisk" => $hazardInfo->InitialRisk,
              "ControlMeasures" => $hazardInfo->ControlMeasures,
              "ResidualRisk" => $hazardInfo->ResidualRisk,
              // "created_at" => Carbon::now(),
              // "updated_at" => Carbon::now()
            ];

            array_push($hazardList, $hazardRecord);
            array_push($additionalHazardReportData,$hazardRecord);

          }

      }


      //create tc signatures
      $tcData = json_decode($input['tcAttachment']);
      $tcSignatureData = [];

      if( count($tcData) > 0 ){

        $tcList = array();

        foreach( $tcData as $key => $tcInfo ){

          $filename_path = md5(time().uniqid()).".jpg";
          $signatureDecode = base64_decode($tcInfo->Attachment);
          file_put_contents(public_path("image/signatures/".$filename_path),$signatureDecode);

          $tcRecord = [
            "JobID" => $tcInfo->JobID,
            "TCName" => $tcInfo->TCName,
            "AttachedOn" => date('Y-m-d H:i:s',$tcInfo->AttachedOn / 1000),
            // "Attachment" => $tcInfo->Attachment,
            "Src" => $tcInfo->Attachment,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];


          array_push($tcList, $tcRecord);
          array_push($tcSignatureData,$tcRecord);


        }

      }

      //Get Job Info
      $jobInfo = $this->jobHeaderRepository->getById($input['parentJob']);



      $state = "";
      $depot = $jobInfo['Depot'];

      if( $jobInfo['JobState'] == "" ){

        $state = $this->checkState($depot);

      }
      else{

        $state = $jobInfo['JobState'];

      }

      //create signage audit

      $sinageAuditReportData = [];
      $afterCarePresent = "FALSE";
      $sinageAuditReportData['SlowLane'] = [];
      $sinageAuditReportData['FastLane'] = [];
      $afterCareCount = 0;

      $signageAudit = json_decode($input['signageAudit']);

      if( $input['signageAudit'] != "" && count($signageAudit) > 0 ){

        foreach( $signageAudit as $key => $signage ){

          $sinageRecord = [
            "JobID" => $signage->JobID,
            "Landmark" => $signage->Landmark,
            "CarriageWay" => $signage->CarriageWay,
            "Direction" => $signage->Direction,
            "TimeErected" => date('H:i:s',$signage->TimeErected / 1000),
            "TimeCollected" => date('H:i:s',$signage->TimeCollected / 1000),
            "TimeChecked1" => date('H:i:s',$signage->TimeChecked1 / 1000),
            "TimeChecked2" => date('H:i:s',$signage->TimeChecked2 / 1000),
            "TimeChecked3" => date('H:i:s',$signage->TimeChecked3 / 1000),
            "TimeChecked4" => date('H:i:s',$signage->TimeChecked4 / 1000),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          

          if( $signage->AuditSigns != "" ){

            $auditSignsList = explode('~',$signage->AuditSigns);

            foreach( $auditSignsList as $auditSignInfo ){

              $signID = explode(':',explode('^',$auditSignInfo)[0])[1];
              $metres = explode(':',explode('^',$auditSignInfo)[1])[1];
              $qty = explode(':',explode('^',$auditSignInfo)[2])[1];
              $afterCare = explode(':',explode('^',$auditSignInfo)[3])[1];
              $afterCareMetres = explode(':',explode('^',$auditSignInfo)[4])[1];
              $afterCareQty = explode(':',explode('^',$auditSignInfo)[5])[1];

              if( $afterCare > 0 ){

                $afterCareCount++;

              }

              $auditRecord = [
                "JobID" => $signage->JobID,
                "SignID" => $signID,
                "Metres" => $metres,
                "Src" => 'image\signage\\'.$state.'-Icons\sign_'.$signID.'.png',
                "Qty" => $qty,
                "AfterCare" => $afterCare,
                "AfterCareMetres" => $afterCareMetres,
                "AfterCareQty" => $afterCareQty,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
              ];

              if( $afterCare > 0 ){

                $afterCareCount++;

              }

              switch( $signage->CarriageWay ){
                case 1:

                  array_push($sinageAuditReportData['SlowLane'],$auditRecord);

                break;
                case 2:

                  array_push($sinageAuditReportData['FastLane'],$auditRecord);

                break;
              }

            }

          }


          switch( $signage->Direction ){
            case 1:
              $sinageAuditReportData['Direction'] = "North";
            break;
            case 2:
              $sinageAuditReportData['Direction'] = "South";
            break;
            case 3:
              $sinageAuditReportData['Direction'] = "East";
            break;
            case 4:
              $sinageAuditReportData['Direction'] = "West";
            break;
            default:
              $sinageAuditReportData['Direction'] = "";
            break;
          }

          $sinageAuditReportData['LocalLandmark'] = $signage->Landmark;

          $sinageAuditReportData['TimeErected'] = $signage->TimeErected;
          $sinageAuditReportData['TimeCollected'] = $signage->TimeCollected;
          $sinageAuditReportData['TimeChecked1'] = $signage->TimeChecked1;
          $sinageAuditReportData['TimeChecked2'] = $signage->TimeChecked2;
          $sinageAuditReportData['TimeChecked3'] = $signage->TimeChecked3;
          $sinageAuditReportData['TimeChecked4'] = $signage->TimeChecked4;

        }

      }



      $jobRequirements = json_decode($input['requirements']);
      $jobRequirementList = array();

      if( count($jobRequirements) > 0 ){

        foreach( $jobRequirements as $key => $jobRequirementInfo ){

          $jobRequirementRecord = [
            "JobID" => $jobRequirementInfo->JobID,
            "ItemID" => $jobRequirementInfo->RequirementID,
            "Qty" => $jobRequirementInfo->Quantity,
            "Notes" => $jobRequirementInfo->Description,
            "OperatorID" => $input['operatorID'],
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          array_push($jobRequirementList, $jobRequirementRecord);

        }

      }


      $timesheets = json_decode($input['timesheets']);
      $timesheetList = array();

      if( count($timesheets) > 0 ){

        foreach( $timesheets as $key => $timesheetInfo ){

          $contactInfo = $this->contactRepository->findByContactReportData($timesheetInfo->WorkerID, $input['parentJob']);



          $timesheetRecord = [
            "JobID" => $timesheetInfo->JobID,
            "WorkerID" => $timesheetInfo->WorkerID,
            "ShiftID" => $timesheetInfo->ShiftID,
            "ContactName" => $contactInfo['ContactName'],
            "ContactCode" => $contactInfo['ContactCode'],
            "ShiftType" => $contactInfo['Description'],
            "AttendDepot" => "FALSE",
            "JobStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobStartDateTime / 1000),
            "JobFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobFinishDateTime / 1000),
            "TravelStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTime / 1000),
            "TravelFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTime / 1000),
            "BreakStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakStartDateTime / 1000),
            "BreakFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakFinishDateTime / 1000),
            "TravelStartDateTimeEnd" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTimeEnd / 1000),
            "TravelFinishDateTimeStart" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTimeStart / 1000),
            "TravelStartDistance" => $timesheetInfo->TravelStartDistance,
            "TravelFinishDistance" => $timesheetInfo->TravelFinishDistance,
            "FatigueCompliance" => $timesheetInfo->FatigueCompliance,
            "RegoNo" => $timesheetInfo->RegoNo,
            "TraveledKilometers" => $timesheetInfo->TraveledKilometers,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];


          array_push($timesheetList, $timesheetRecord);

        }

      }




      $docket = json_decode($input['dockets']);
      $clientSigned = "FALSE";

      if( count($docket) > 0 ){

        foreach( $docket as $key => $docketInfo ){

          //Check if client signed
          if( $docketInfo->AttachmentTypeID == 1 ){

            $clientSigned = "TRUE";

          }

        }

      }


      if( isset($afterCareCount) && $afterCareCount > 0 ){

        $afterCarePresent = "TRUE";

      }

      $geotag = "";

      if( isset($input['geotag'])){

        $geotag = $input['geotag'];

      }

      //Get Shift Length
      $shiftLength = 0;

      if( count($timesheets) > 0 ){

        foreach( $timesheets as $key => $timesheetInfo ){

          $travelStart = date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTime / 1000);
          $travelFinish = date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTime / 1000);

          $contactInfo = $this->contactRepository->findByContactReportData($timesheetInfo->WorkerID, $input['parentJob']);

          $strings = explode(' ', $contactInfo['Description']);

          if( in_array("Driver", $strings) ){

            $shiftLength = ( strtotime($travelFinish) - strtotime($travelStart) ) / 60;

          }

        }

      }



      //Get Job Images

      $jobImages = $this->jobImageRepository->get($input['jobID']);

      $generateFilePath = Config::get('evolution.generatedFilePath');
      $enableWatchFolder = Config::get('evolution.watchFolderEnable');


      $reportInfo = [
        'jobId' => $input['jobID'],
        'location' => $jobInfo['Address'],
        'client' => $jobInfo['CompanyName'],
        'state' => $state,
        'orderNumber' => $input['orderNumber'],
        'siteaudit' => $siteChecklistReportData,
        'additionalHazards' => $additionalHazardReportData,
        'signageAudit' => $sinageAuditReportData,
        'tcSignature' => $tcSignatureData
      ];


      $pdf = PDF::loadView('forms.appsafety', $reportInfo)->setPaper('a4','portrait');
      $pdf->output();
      $dom_pdf = $pdf->getDomPDF();

      $canvas = $dom_pdf ->get_canvas();
      $canvas->page_text(275, 810, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 8, array(255, 255, 255));
      $pdf->save($generateFilePath.'SAFETYDOCKET_'.$input['jobID'].'.pdf');


      $uploadPDFToWatchFolder = false;

      if( $enableWatchFolder ){

        $data = [
          'username' => Config::get('evolution.watchFolderUsername'),
          'password' => Config::get('evolution.watchFolderPassword'),
          'server' => Config::get('evolution.watchFolderHost'),
          'filepath' => Config::get('evolution.generatedFilePath'),
          'foldername' => 'APPSAFETY',
          'old_filename' => 'SAFETYDOCKET_'.$input['jobID'].'.pdf',
          'new_filename' => 'SAFETYDOCKET_'.$input['jobID'].'.pdf'
        ];

        $uploadPDFToWatchFolder = $this->fileServices->uploadToWatchFolder($data);

      }


      //Prepare Site Assessment CSV
      $jobReportInfo = $this->jobHeaderRepository->getReportDataById($input['parentJob']);

      $csvInfo = [
        'fileName' => 'SAFETYDOCKET_'.$input['jobID'].'.pdf',
        'formId' => 'DTMCD001',
        'jobNo' => $jobReportInfo['JobCode'],
        'jobID' => $jobReportInfo['JobID'],
        'jobDate' => date('d/m/Y',strtotime($jobReportInfo['JobStartdate'])),
        'jobDateMonth' => date('m',strtotime($jobReportInfo['JobStartdate'])),
        'jobDateYear' => date('Y',strtotime($jobReportInfo['JobStartdate'])),
        'clientCode' => $jobReportInfo['ContactCode'],
        'clientName' => $jobReportInfo['CompanyName'],
        'depot' => $jobReportInfo['Depot'],
        'state' => $jobReportInfo['JobState'],
        'orderNo' => $input['orderNumber'],
        'location1' => $jobReportInfo['Address'],
        'clientSign' => $clientSigned,
        'afterCare' => $afterCarePresent,
        'swmsTimeStamp' => '',
        'swmsUser' => $contactCode,
        'swmsGeotag' => $geotag,
        'rego' => [],
        'requirement' => $jobRequirementList,
        'timesheet' => $timesheetList,
        'siteaudit' => $siteChecklistReportData,
        'additionalHazards' => $additionalHazardReportData,
        'signageAudit' => $sinageAuditReportData,
        'tcSignature' => $tcSignatureData,
        'jobImages' => $jobImages,
        'shiftLength' => $shiftLength
      ];




      Excel::create($input['jobID'].'_SD', function ($excel) use ($csvInfo, $siteChecklist, $siteChecklistSections) {

        // Set the title
        $excel->setTitle('Safety Docket eForm');

        // Chain the setters
        $excel->setCreator('Evolution')
              ->setCompany('Evolution');

        // Call them separately
        $excel->setDescription('Safety Docket eForm');

        // Set sheets
        $excel->sheet('Site Assessment CSV', function($sheet) use ($csvInfo, $siteChecklist, $siteChecklistSections) {

            if( $siteChecklist && $siteChecklistSections ){

              $sheet->loadView('forms.appsafetycsv',$csvInfo);

            }
            else{

              $sheet->loadView('forms.appsafetycsvfail',$csvInfo);

            }

            $sheet->freezeFirstRow();

        });


      })->store('csv', public_path('generated files'), true);


      $uploadToWatchFolder = false;

      if( $enableWatchFolder ){

        $data = [
          'username' => Config::get('evolution.watchFolderUsername'),
          'password' => Config::get('evolution.watchFolderPassword'),
          'server' => Config::get('evolution.watchFolderHost'),
          'filepath' => Config::get('evolution.generatedFilePath'),
          'foldername' => 'APPSAFETY',
          'old_filename' => $input['jobID'].'_SD.csv',
          'new_filename' => $input['jobID'].'_SD.csv'
        ];

        $uploadToWatchFolder = $this->fileServices->uploadToWatchFolder($data);

      }
      else{

        $uploadToWatchFolder = true;


      }



      $siteAssessmentPath = Config::get('evolution.siteAssessmentPath');

      if( file_exists($siteAssessmentPath) ){

        Excel::create($input['jobID'].'_SD', function ($excel) use ($csvInfo, $siteChecklist, $siteChecklistSections) {

          // Set the title
          $excel->setTitle('Safety Docket eForm');

          // // Chain the setters
          $excel->setCreator('Evolution')
                ->setCompany('Evolution');

          // // Call them separately
          $excel->setDescription('Safety Docket eForm');

          // Set sheets
          $excel->sheet('Site Assessment CSV', function($sheet) use ($csvInfo, $siteChecklist, $siteChecklistSections) {

              if( $siteChecklist && $siteChecklistSections ){

                $sheet->loadView('forms.appsafetycsv',$csvInfo);

              }
              else{

                $sheet->loadView('forms.appsafetycsvfail',$csvInfo);

              }

              $sheet->freezeFirstRow();

          });

        })->store('csv', $siteAssessmentPath, true);

      }

      if( $uploadToWatchFolder ){

        $countGenerateReport = $this->generateReportRepository->countByJobID($input['jobID']);

        if( $countGenerateReport > 0 ){

          $generateInfo = [
            'SafetyDocket' => 1,
            'updated_at' => Carbon::now()
          ];

          $updateGenerateReport = $this->generateReportRepository->updateByJobID($generateInfo, $input['jobID']);

        }
        else{

          $generateInfo = [
            'JobID' => $input['jobID'],
            'SafetyDocket' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
          ];

          $insertGenerateReport = $this->generateReportRepository->create($generateInfo, $input['jobID']);

        }

        $response['status'] = true;
        $response['statusCode'] = 200;
        $response['message'] = "Safety Docket Report for ".$input['jobID']." has been created";

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 422;
        $response['message'] = "Safety Docket Report for ".$input['jobID']." was unsuccessfully created";

      }

    }

    return $response;

  }


  public function generateJobImageReport($input, $contactId, $contactCode){

    $jobID = $input['jobID'];

    //Check if uploading in watch folder is enabled
    $enableWatchFolder = Config::get('evolution.watchFolderEnable');

    $countPendingJobImages = $this->jobImageRepository->countPendingJobImage($jobID);

    //Check if there are pending images in database
    if( $countPendingJobImages <= 0 ){

      $response['status'] = false;
      $response['statusCode'] = 422;
      $response['message'] = "No Pending Image Found";

    }
    else{

      $pendingJobImagesList = $this->jobImageRepository->getPendingJobImage($jobID);

      $jobImageData = [];
      $currenctJobID = "";



      if ( strpos($input['jobID'], '.') > 0 ) {

        $pos = strpos($input['jobID'], '.') - strlen($input['jobID']);
        $parentJob = substr($input['jobID'], 0, $pos);

        $jobReportInfo = $this->jobHeaderRepository->getReportDataById($parentJob);

      }
      else{

        $jobReportInfo = $this->jobHeaderRepository->getReportDataById($input['jobID']);

      }




      $imageCount = 1;

      foreach( $pendingJobImagesList as $pendingImagesRecord ){

        $imageName = pathinfo($pendingImagesRecord['JobImage']);

        //Check if current job image exist
        if( File::exists(public_path("image/job/".$imageName['basename'])) ){

          $newFilename = 'IMAGEDOCKET_'.$input['jobID'].'.'.$imageName['extension'];

          //If multiple image
          if( $countPendingJobImages > 1 ){

            $newFilename = 'IMAGEDOCKET_'.$input['jobID'].'-'.$imageCount.'.'.$imageName['extension'];

          }


          $uploadToWatchFolder = false;

          if( $enableWatchFolder ){

            $data = [
              'username' => Config::get('evolution.watchFolderUsername'),
              'password' => Config::get('evolution.watchFolderPassword'),
              'server' => Config::get('evolution.watchFolderHost'),
              'filepath' => public_path("image/job/"),
              'foldername' => 'APPIMAGE',
              'old_filename' => $imageName['basename'],
              'new_filename' => $newFilename
            ];

            $uploadToWatchFolder = $this->fileServices->uploadToWatchFolder($data);

          }

          //Check if upload to FTP is successful
          if( $uploadToWatchFolder ){

            $jobImageData[] = [
                'fileName' => $newFilename,
                'oldFilePath' => public_path("image/job/".$imageName['basename']),
                'formId' => 'DTMCD003',
                'jobNo' => $jobReportInfo['JobCode'],
                'jobID' => $jobReportInfo['JobID'],
                'jobDate' => date('d/m/Y',strtotime($jobReportInfo['JobStartdate'])),
                'jobDateMonth' => date('F',strtotime($jobReportInfo['JobStartdate'])),
                'jobDateYear' => date('Y',strtotime($jobReportInfo['JobStartdate'])),
                'clientCode' => $jobReportInfo['ContactCode'],
                'clientName' => $jobReportInfo['CompanyName'],
                'depot' => $jobReportInfo['Depot'],
                'state' => $jobReportInfo['JobState'],
                'orderNo' => $jobReportInfo['OrderNumber'],
                'location1' => $jobReportInfo['Address'],
                'jobImageTime' => date('H:i:s',strtotime($pendingImagesRecord['created_at'])),
                'jobImageGeo' => $pendingImagesRecord['GeoTag']
              ];

              $imageCount++;

          }

        }

      }



      //Check if there are job image record included
      if( count( $jobImageData ) > 0 ){

        $csvInfo['jobImageData'] = $jobImageData;

        Excel::create($input['jobID'].'_IM', function ($excel) use ($csvInfo) {

          // Set the title
          $excel->setTitle('Job Image CSV');

          // // Chain the setters
          $excel->setCreator('Evolution')
                ->setCompany('Evolution');

          // // Call them separately
          $excel->setDescription('Job Image CSV');

          // Set sheets
          $excel->sheet('Job Image CSV', function($sheet) use ($csvInfo) {

              $sheet->loadView('forms.appimagecsv',$csvInfo);

              $sheet->freezeFirstRow();

          });

        })->store('csv', public_path('generated files'), true);

        $uploadToWatchFolder = false;

        if( $enableWatchFolder ){

          $data = [
            'username' => Config::get('evolution.watchFolderUsername'),
            'password' => Config::get('evolution.watchFolderPassword'),
            'server' => Config::get('evolution.watchFolderHost'),
            'filepath' => Config::get('evolution.generatedFilePath'),
            'foldername' => 'APPIMAGE',
            'old_filename' => $input['jobID'].'_IM.csv',
            'new_filename' => $input['jobID'].'_IM.csv'
          ];

          $uploadToWatchFolder = $this->fileServices->uploadToWatchFolder($data);


        }



        if( $uploadToWatchFolder ){

          foreach( $jobImageData as $jobImageInfo ){

            // File::delete($jobImageInfo['oldFilePath']);

          }


          $countGenerateReport = $this->generateReportRepository->countByJobID($input['jobID']);

          if( $countGenerateReport > 0 ){

            $generateInfo = [
              'JobImage' => 1,
              'updated_at' => Carbon::now()
            ];

            $updateGenerateReport = $this->generateReportRepository->updateByJobID($generateInfo, $input['jobID']);

          }
          else{

            $generateInfo = [
              'JobID' => $input['jobID'],
              'JobImage' => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
            ];

            $insertGenerateReport = $this->generateReportRepository->create($generateInfo, $input['jobID']);

          }


          $jobImageInfo = [
            'Processed' => 1,
            'updated_at' => Carbon::now()
          ];

          $updateJobImage = $this->jobImageRepository->updateByJobID($jobImageInfo, $input['jobID']);

          $response['status'] = true;
          $response['statusCode'] = 200;
          $response['message'] = "Job Image Report for ".$input['jobID']." has been created";

        }
        else{

          $response['status'] = false;
          $response['statusCode'] = 422;
          $response['message'] = "Job Image Report for ".$input['jobID']." was unsuccessfully created";

        }

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 200;
        $response['message'] = "No Pending Image Found";


      }

    }

    return $response;

  }

  public function extract_unit($string, $start, $end){

    $pos = stripos($string, $start);
     
    $str = substr($string, $pos);
     
    $str_two = substr($str, strlen($start));
     
    $second_pos = stripos($str_two, $end);
     
    $str_three = substr($str_two, 0, $second_pos);
     
    $unit = trim($str_three); // remove whitespaces
     
    return $unit;
  }



  public function generateFailSafeReport($input, $contactId, $contactCode){

    $timezone = $this->googleTimezoneServices->setTimezone($input['geotag']);

    if( !$timezone ){

      $response['status'] = false;
      $response['statusCode'] = 422;
      $response['message'] = "Failed to set Timezone";

      return $response;

    }


    //filter inputs for IOS
    $input = $this->inputHelper->filterInput($input);

    $siteChecklistSections = [];
    $siteChecklist = [];
    $siteChecklistReportData = [];
    $additionalHazardReportData = [];
    $sinageAuditReportData = [];

    //Set site checklist
  if( $input['siteCheckList'] ){

      // $siteChecklist = $this->manstatRepository->getSiteCheckList($activation, $contactId);
      // $siteChecklistSections = $this->manstatRepository->getSiteCheckListSections($activation, $contactId);

      $siteChecklistInput = $input['siteCheckList'];

      $questionList = explode('~', $siteChecklistInput);

      $error_value_type_count = 0;
      $error_question_count = 0;
      $error_answer_count = 0;

      foreach( $questionList as $questionInfo ){

        $itemID = "";
        $itemValue = "";
        $valueType = substr($questionInfo,0,strrpos($questionInfo,':'));
        $question = $this->extract_unit($questionInfo,':','^');
        $answerlist = substr($questionInfo, strrpos($questionInfo, '^') + 1);



        if( $question == "" ){

          $error_question_count++;
        }

        // foreach( $siteChecklist as $siteCheckListInfo ){

          // if( $siteCheckListInfo['Description'] == $question ){

            // $itemID = $siteCheckListInfo['Id'];

            if( in_array($valueType, array('Multi_Select','Multiple_Select','Single_Select','Simple_Boolean')) ){

              foreach( explode('|', $answerlist) as $answerInfo ){


                // foreach( $siteCheckListInfo['CheckListItemOptions'] as $itemOption ){

                //   if( $itemOption['Value'] == $answerInfo ){

                //     $itemValue = $itemOption['Id'];

                //   }

                // }


                // if( $itemValue == "" ){

                //   $error_answer_count++;
                // }

                // $checklistData = array(
                //   "jobID" => $input['jobID'],
                //   "itemID" => $itemID,
                //   "itemValueID" => $itemValue
                // );

                // array_push($jobSiteCheckListItems,$checklistData);


                // Create report data for site checklist
                // $reportSection = "";
                // $reportSectionID = "";

                // foreach( $siteChecklistSections as $sectionKey => $sectionValue ){

                  // if( $sectionValue['Id'] == $siteCheckListInfo['SectionID'] ){

                    // $reportSection = $sectionValue['Name'];
                    // $reportSectionID = $sectionValue['Id'];

                  // }

                // }



                // $section_in = str_replace(" ", "_", $reportSection);
                $question_in = str_replace(" ", "_", $question);

                // $siteChecklistReportData[$section_in][$question_in] = $answerlist;

                $siteChecklistReportData[$question_in] = $answerlist;

              }

            }
            elseif( in_array($valueType, array('Numeric','Text')) ){

              $itemValue = $answerlist;

              //Generate UUID for Site Checklist Input Value
              // $uuid = $this->uuidv4();

              // $checklistData = array(
              //   "jobID" => $input['jobID'],
              //   "itemID" => $itemID,
              //   "itemValueID" => $uuid
              // );

              // array_push($jobSiteCheckListItems,$checklistData);

              // $checklistData = array(
              //   "Id" => $uuid,
              //   "Value" => $itemValue
              // );

              // array_push($siteCheckListInputValues,$checklistData);

                // Create report data for site checklist
                // $reportSection = "";
                // $reportSectionID = "";

                // foreach( $siteChecklistSections as $sectionKey => $sectionValue ){

                //   if( $sectionValue['Id'] == $siteCheckListInfo['SectionID'] ){

                //     $reportSection = $sectionValue['Name'];
                //     $reportSectionID = $sectionValue['Id'];

                //   }

                // }

                // $section_in = str_replace(" ", "_", $reportSection);
                $question_in = str_replace(" ", "_", $question);

                // $siteChecklistReportData[$section_in][$question_in] = $itemValue;
                $siteChecklistReportData[$question_in] = $itemValue;


            }
            else{

              $error_value_type_count++;

            }

          // }


        // }

      }


      if( $error_answer_count > 0 ){

        $response['status'] = false;
        $response['statusCode'] = 400;
        $response['message']['siteCheckList'] = "Some answers in site checklist is either missing or not within the available selection";

        return $response;

      }

      if( $error_question_count > 0 ){

        $response['status'] = false;
        $response['statusCode'] = 400;
        $response['message']['siteCheckList'] = "Some questions in site checklist is either missing or not within the available selection";

        return $response;

      }

      if( $error_value_type_count > 0 ){

        $response['status'] = false;
        $response['statusCode'] = 400;
        $response['message']['siteCheckList'] = "Some value type in site checklist is either missing or not within the available selection";

        return $response;

      }

    }


    //convert base64 images for tc attachment
    $tcData = json_decode($input['tcAttachment'],true);
    $tcAttachment = "";


    if( count($tcData) > 0 ){

      foreach( $tcData as $key => $tcInfo ){

        $filename_path = "tc_attachment_".md5(time().uniqid()).".jpg";
        $imageDecode = base64_decode($tcInfo['Attachment']);
        file_put_contents(public_path("image/tc_attachment/".$filename_path),$imageDecode);

        $tcData[$key]['Attachment'] = $filename_path;

      }

      $input['tcAttachment'] = json_encode($tcData);

    }

    //convert base64 images for docket
    $docketData = json_decode($input['dockets'],true);
    $dockets = "";

    if( count($docketData) > 0 ){

      $clientSignature = false;

      foreach( $docketData as $key => $docketInfo ){

        if( $docketInfo['AttachmentTypeID'] == 1 ){

          $clientSignature = true;

        }

      }

      //If no client signature found

      if( $clientSignature == false ){

        $attachedOn = ( strtotime( date('Y-m-d H:i:s') ) * 1000 );

        $requestClientAttachment = [
          'JobID' => $input['jobID'],
          'OperatorID' => $input['operatorID'],
          'Attachment' => '',
          'AttachmentTypeID' => 1,
          'AttachedOn' => $attachedOn
        ];

        $requestClientAttachment['Attachment'] = 'iVBORw0KGgoAAAANSUhEUgAAAMgAAAAkCAYAAADM3nVnAAAAAXNSR0IArs4c6QAAAAlwSFlzAAALEwAACxMBAJqcGAAAA/BpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIgogICAgICAgICAgICB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIKICAgICAgICAgICAgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiCiAgICAgICAgICAgIHhtbG5zOnRpZmY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vdGlmZi8xLjAvIj4KICAgICAgICAgPHhtcDpDcmVhdG9yVG9vbD5QaG90b3MgMS4wLjE8L3htcDpDcmVhdG9yVG9vbD4KICAgICAgICAgPHhtcE1NOkRvY3VtZW50SUQ+eG1wLmRpZDo4REYwNDE4NDdDNTcxMUU1OTBFREJERENBQkJFNTkyMTwveG1wTU06RG9jdW1lbnRJRD4KICAgICAgICAgPHhtcE1NOkRlcml2ZWRGcm9tIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgPHN0UmVmOmluc3RhbmNlSUQ+MUVFMUJGMjVFMzQxNEJBMDcwOTE1M0NFNzE1RUUxNDQ8L3N0UmVmOmluc3RhbmNlSUQ+CiAgICAgICAgICAgIDxzdFJlZjpkb2N1bWVudElEPjFFRTFCRjI1RTM0MTRCQTA3MDkxNTNDRTcxNUVFMTQ0PC9zdFJlZjpkb2N1bWVudElEPgogICAgICAgICA8L3htcE1NOkRlcml2ZWRGcm9tPgogICAgICAgICA8eG1wTU06SW5zdGFuY2VJRD54bXAuaWlkOjhERjA0MTgzN0M1NzExRTU5MEVEQkREQ0FCQkU1OTIxPC94bXBNTTpJbnN0YW5jZUlEPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4Kv12OQgAAJEFJREFUeAHtnXm83dO1wM+5U27mSRBUUCGJqXhmoVJF1VBDomi9luK1ZvVMERKJmBuq9cqnaDxNaypRUyOhSEUfSmsuMYsQkch4c4dz3/e789u//M4959wbtPgj+/PZd09rr7X22mvt+XdufvTo0TW5XO6i1tbWo6qqqnoR/1SupaVlGfXvp/JPwDlLvPjmT4WsTCX4y48ZM6aaogJ4C4LEPNItJs0bPnx49SabbJJ//vnnWw3JSuEtb+uEx+duvfXWnPDnnXdeSz6fD7huueUW6eVGjBgh/iJn+6RBvZZIswiARMQl7JAhQ1ozePLkVVtOm+Sxqm3dNvBti1elPycJ2FE/69Gjxykff/xxDsWYWygUSjqrI15UKFzPbt26VS9evPg10juA9wMVA/+ZjURDkEbkA5xVeNNpHnHbog/GE2ENVWAVOZtnHFjxlMBXym9bvz0c5WCTdliU5bsc6Kq8L4kE8ueee+4CRv7u8HMA/s/4bvgSpSGvrGtsbFR7q6qrq6eBZ+O6urrcsmXLnlmwYMGOEyZMWOoonBk5y+JoLzMqK+EQ4A7G30v8SeuMHTt2MDPXvkR/Q94H5hHuTPBd/Jr4V5ubmyeOGzfuRfIrGcMwFPdQYHvRjhmEvwZ2gfDEj8A7A91IGGYsDZV0PQPJsbT3deJ34fcjPZh0E2BVxDWA+tra2t/D37vET8S/BtxthLkzzjijZ6dOnY4B/j7KO4Nzb+ILqacRE62qJWxYsmTJDZdccslC4s4yq4wKIXzergbXHSWa17lz54fPOuus+TCg/8QOQ2sGVw6DWQqur4HgLvw3NY5KI/hKElFRNdhdVltttTFz5sw5HEXbGr+oqalpl969e1/80UcfPUq5M9bJ0J6AgeZQvLeIH9TQ0HAGy5jdWc5Mi8YKXDAWeD6DWe+iRYsWtWAk7xM/eOHChUdR7gyokVxG/jJwBwOhLfLSwiDQFaW+AjoPkb4LxT4VPnadP39+M/DVKHgBmGpoP0N8MXxcjIxzo0aN2hGjnkG6P7P2JcA3gKeqS5cuo4FdRn4n2iTvtnc++X8gXAgfzoyrDARhfN6uig51aZVHSZw5cnSEyyIVqEOv4lPfzqsRh/VRljpGvqb6+vrdyZ9knssb8Rn/tA68TfPmzcuhRBuB40zxoHxNS5cuzTF7fXzBBRf0Z8SeAO0XgP0KRjEApdsG/hrxE3/+85930lg1EnhxVhjEKK5xPEHYD/i1iR+NETpTjRY/eBbQrHnGdexRgpI6a0LX2WKB+fCxBGVfAr3Vzj//fNtZ17dv3zri9xDvAU9LHDyAu0x4XCO0THcC5krq9cSAtgSvNCfCSw082c5ZAsuv4Sr3+UvADXpwKFdco7e7qY3wSahduOQo6kA6voZOb8ZIDmXknkuHn0Ao3GdZKtSgPDkU8zHwjwTXZaQXJ7NWA6P5Hn369HF5dzb03qG8Dv8k/mJmhlHMMtvA8/TZs2fb5hbqfhNjE/48Zs5gBMD+mlnlfXB+aPtsG3CpjMzLuBoML2ziydMouoBvB2aJhdSre++99zSg6cKT7sLe7C8o/U7gPwTZTOnevXsOowj1oetsVY+hm9dIldazzz57rnVXuS9WAp9pVG/DegGFiUplUZVGglIcT+efnxhSmGWy9ShzFivJz8IEZFVVrRoDSnQ68AvxV5Hv7KBSWX9dlN19whvCYywBJ3RfxKAcmfub/8orrxjIZ4AH51um4aNGXhjR/4gSzwAPICsOBoTh1Kosn+Bu1tiAv4+BZjr+QcCnnHLKKZ0JGxgonC1uZIa4DZirSa/tUoq8LL46aeBCnxxzzDHuQ1a5L1gC/zIDQaE6q4iErsFVLnHXuBzBSEahdEejgIV4dKoyJr4AbKvLtQ5kUUDxVPTXgfsJ8e9R7zCNAvweEsxPDMgDhxwzRlAw+OlrGp7c7OYGDhxoIJ8LhEdR603PnTs3LL3OOeecgfC1rjzh8rYpcfnk2Dim0xCYGlZRwu8Kfy6VtmMfMdRDCoDCDER+NTh/DJ99yBsPbCN5WQMpmoV33333onRKbFXkc5VApeXDyjKhUtDv+QJLqIdRtq8SX0xeqlUgamGE707+mSjeRPYBjYSO1uH4V2Vkpll40UUXucxRYcpuRlGmVpTODWxfNro3Qe8EDONgDKSZPHS95i/QUPGPAsd08C4i1B3pETa8/d0EdaLiuVRzRjqc7KeuuuqqZdTZns31DBT8cvJOA5/8uOTRuUQMPIOjFZ69LwnLUoVA+RLKHwmQxX+qaL85PSj/EL4vYbY5HRoauwcAqSNtPPKX5q+KfHESKGsgdKQKri/bWXGp4eZ7uQ6FJcvRKMJ4FDWP0rWiuFXEm1Ro9Od+cK2LXwv/Bvib8T3I/x+U4iCUUuXygvH37Zx41blUYaQOSxHqnYZ/hLwaaPWk7uP4yZwm/QAl3AA6j4F//169eg1mA33e+PHj30tOsYK2spSaBtwUyk8hdFr5J/4kFL8Vw7maeJgNmak2A+8j4NJY6mnvm9A7mfwajC7MVuZzKtVVHoDTWJTbakTHE97XtWtXl4FhRsM4xtOGQ/r16zeAE7mQB4yuymUae5UuJl544YXs7GLWKvcFSKDEQLjBzXvaAy9ljSPLo0qDUyHiBd3MbLlxlGY2gTD6MBqzAe2Pck9DcQajLAVG5N4o3S/ZLD9w4YUXzqWOy69A3xtl6uneQtH/hjF9ZIJZ5FEUezxKugdGGDbZKOmhwIyFL5df2+NnkT4FXFdYh3aJ05kg4Kd8OOUXEh5CnT0oU8FPovy1a665pnbWrFlTUOzNyHMvoXM55v1GEzPAVOJPmkn9GRwP9yA0qQE3E6/Fd+K0ajHpp/EaYO7MM8/8mOXmKR9++OF4DDErrwaMYwZyCTOdt/TCr3JfrATynPh4UjOfztwExZglO4RfQylGEdWAnEmyTiXrQvmvGIVvBzYqs0aSHfXMb2bk7ovyayRXQ+skjQMlfxg/kNHa5ynxNGgJPGxMnXcyOLN0243HOw6BnIXYLziih2PYpKK8pUoH/5BbvgnXGGbOnFmfXMrlVoJ+Flc2npCqHGTpVoZaVfJlkUDJDCJjGMxmHEMeiGK7uS3ilbIcF1g5bso9Dro9LreIOzJnFdC0F4e1KMXpGNME0qsTf4QReEOMowkF7US62SNPRuBJlLdrHFG5gJMpfXokncx6OU9/1lprLe9dgnFkDCfljbIajDWHYeRpY9Wxxx7rXqBJ/ODRsJ1Bde7Uc8BWM5vkgXdmEE8WV56ymoQm4GmRM4s4zMjDR5XLJumCv6p///6t0HW5l1aQfqwjDGVZ4adtJb+ss768Zgsrvekqgz9brcO4M1x8vgOuIj0irfzSdrVB5kBaxCPpsOxN4ErK29QvSiZ8hJVBUcGKxCfCt6JaiBUUaNEMknSSyr0RIC4psp0U69ugfwITlkwxs1zo6IwiNAG7Jrj/wsyxAUqpcbgEacIAa1ki3QkfPnXpyAVFi8Yg8AknnNCJS7mubpzB661zKmziNdl0e8iBizNhCmZeZskZ8slTJt28KNx6660XZXmhrCy9KNMUcQeRSvCV8hN0FWeytvXapjtgp2JxB3jK8VMuL4u/o/IsbFE8MxBm8z81voikyPLNRFEDUjo7rJkjYNtQ4ZBXUl846oYRRIXROEaOHDmAmedhlHhANA7Awjrd2Yh1+jRgdyTvCPyJxDW8ksaRH/ZHhOslsLsQrgcv3cFL0DqPspehNZkb8UknnnjisozgYrtU8BF431O1YKB5jHYyS785tgkXRj6XaeAKMwnhpsAfRvlOeI+Au7Khbmb0msd+4mVw3Ev6Zt5YLWx7yBBxMoMOhd5m1F3KgOA+ZT55NxMWuQgPza2A34HCJfgu0HiYus+RX2LIsQ7L2X7M2A40jqh62/gK9R6IMNYnXeBCczMOU3YFRvzlBkGyy7qAl5J/gmeqdz09e/YcTjrIkxBWqyZD54NIUywxTv6a1NsXGRSAq4a/xWuvvfYt6olgyTu1A4mrW8LYb+WcfLhKeIvZ+GkGKgfhVDYZen2g8UNpAUu06Gi9HF7zpNsZHh8uq+CVamXzaaSKlI7W2TLjmZljXZKPsqz6SjQOmPcpSi3pJ1lavQOuqzAeb8lfxEBtqAKNS5SAOjae8HgaeRmG1YkGeDoUyv1Dh68BnUHg2597jbNR3mMQ3EMaCcubsAREgXpiEBM1TOsjCOm+QPU5wKoo6dsxaKnIP4OX4zxhivTkTQc9n7f4NOUAcAA++mT8bfi0o1iGBJzweRyXl4dwKOFRc6ALf40YyR3ApzMP7bcjfdN1CEZ+OkvZnEtQntmcRf5zLOdCRxNPXazDgcURnMpdJg1kkIM3l8JzTzvttAGkPX7Po4ihPrzvCz8XIH/bkeLqKILswxIbfqYCOxUee0B3YnJSF3BB/0XKPojyFGfkET4GI8trlSWnnA6Oja+99trdgHwsHGX9yb+ePgryth26KPOQSP7Ii/2PTF5FhmfhU9lH2tDpD87LbGM5HFl8MS5e24MOTSgxEIioSHUA7YrydCeUQ6JVLsW8OAthRBbDJF9JC/snOuIj8KwDU4/S2K+gmI00VoVrxDjq2IO8gnJsA8y2KOt3EOqz1NsRGOmHUS7ijrMA+cfT2Kuom8MvBXdnlS0KUaG79AGPjwU3JP0gSrg3wrqPuoE2dWhGYS4nRl4gylM1cI5e4a0Vm3uV2z1MD7KmQG87Fa4SPfK9zW9FGdemE25FEU7mdO3KtjMJuBb4/gplapAWsPXweQFwvgbWysMMZ5kOlItUXuCpFt7JOdLn2Osst04Ty51r7DBKUOcg66A0DcSrkYWXq32R7+6ATlZJY31ksFh+6BZvOLPHzcF4I3LgYjSE8NxMnqd64VEr8vMFxYfgWo2403gtbQvyzFaMp5GUNyFPglZh7JMPkHHaJnj34agPUT3udpXhPjC8+VseXf7qgni4h6KMJtZuCF+30tdHIosb8Op1gcEphx4000Z1pVMGX6VZCZDgHKDsn0UlBkKxzO7Fun6y1gkjy6t08BeEYcTC6u6Hwd/h3XNoHOtqHFTPGserdNbGjBSbwMRfEa7LlF2osygaQyRHXhUKrsJuSN7P6HSVpxG8nZmBPgb1FPLfIM8XtluA02WQb6yWITiFch2XkIM9XhWnozF1fFzpfGv7vYWPjdQ44pR0J4a8HYa0DNjqhN4S0E4hPZN69dDZGnrbSw9ZNeIBq7sCHC/j77ct0tTRPumo8NKswSh9qzYYdzLpyzlcqLn22mtTxQI2wAtLVYLyyyAMrMrNMvQ8edzBPiO03fZdizSh5RJoskoa71fAWQuvyqKLMNElBgGK5Yoo7YR+AKE81KMdLlWtr+wCj5ZB0+f+UZ6hTvYPvOSRmUof6pC2TgrigAfuWmiKR8O10EHF9W/QR/INO8O/d2DCh8GXvKs5/v8zy8fXk6c6BelBK/IV8GXppYQzEeCD7GlbdTkDEfRxptAxhN3wxUOIpRmnIHGeAGnlHzErXMx9Rj/ibsjXowFuyIuMg04ahIFsggI/CxOvIovt6eD5+BqMISpopKJ2yMMIl2UYSAP16hHKU9TbjzqzIqAh6X0IJuG7IZzF8NCfEcvvSK7DB6eAs0oR8wmDJoLjdEa13ainYfuytpp2TKUdR1H2VgY+x6j1bfJvgpde4F2C74IfD8z9iWGXlTF18rTdjj6H/c8kLzLBHeiLH96zZCrGff7iSIk7EJ6dQRvhpQ68GkoeORnuaZ/AzxxntgTZJAYm73EagVEJa5DJXGB9oXAMvIUBhrIZ+JMoC0YHXIFZylliToInPBVK4p84gM909oiVYx6hfIn/2/Tji8rWND7PQCA/h1I2Cp7r8K5M6umno8k/m5nSuqmjXEPXWNy3uE97h3QncJbQp22tDIwOYrNLOk8B0lEfgGA0/lM5FHgGvG5AZzn62pBGhF4H8y+THkKnDkmM4yVgtvX+AZrpOrwN0WCgNGwLG4kHRWj72dTx015HsuY4OpK+myXOBNbX56IAXXnJm+MycF9gUgNpgz9NUtdnMH3IOA1eQ35iHI9h0N80g/Lw+axxFZMB4R6M5DA68F47UB6huSXwe8LHn3zjBWhbo7e6G9RGlLoX8fPxR3tixuBh2cq6sLySJ+ge5OyBc3ns5eSHdPQA8ptog7f6e5M3MTEo6zmwFA0upG3fK9QLu1kUyKwPGZWfMPJFOWacmSNHjnyvDP1z4XcD2nc4S13XzoL8h3/Yl5QofqI3LcA/HV9wC9ueiwZSxSgRRhYE6LfcQTIQDxTbQwBMFcpY64kR8Y2A/QhGx2J9v4Eh7zmWYSydULiXSG9N+eYYx9OMUCGNcfjMpJJxZEk7emTdaiao63o7j0LKf+Ab2pOYAS1v4NFiV0JvslfW7YMA+8FvWCMjF5XFLwKl5RN6Z5XgErouy9zjXEd6EwoWoKh9aauzb46lalFHWQcHysJiwu6ux1HIH7Es+DUj/F89tqZaOYMSXZFjCReWn2TujBINgW44/EBZfI18C3K+lnYE+ij7IcBNxNunFLc687g8I5nLDR06tMY+JBqWThoHMBYFHaF9YWAQnkE0PIUhb6X4FMlncbQrvGSQB/AEnYw6R/px5Hh4xI9c64zHvVbMj6HtRiY+EfLEU9iKOs4A1xINJNYPT7oRQvywqOwSi5E5DwMFTkhkfAmCbYDYOjD3CIJ1mTWEUdUnJDeS1jiepRHDgFkC0TtQutdJb0m6wbUiYbr2ThlJIo6qRmnYu9QJ6+Fk+eFa3wbeS74zXtpZ48aNe5n0cfjUOTParjSjQgQ635AOoSONJ22PMYI+pWBxqXEk+MTiaKyR/KgcSvhPaYLDJZWINOYJ+OOJ90GRpXch6WHQq9hh5fCbR93h4kCuzcjb9fYD+JvB9QtCZ27BduOl8lfhc6b8ki+dlDdnFoFwRfTBHaxEZaFOiGsk4lgO/u//i4EE3jjOzfPWLiwpI7/oXHgPJ2945fuuHAlbjjPhmO2dYW1D2p/lYCnPlxiISsRotgNT/R1RONnK5OVZQjSzbOkDsdsZub8PorVg9DEIr0EHrUHa8/pNMRLP70+i/s6kXfvZgIMI3iCtUdXgKxqH8JlN5VSSx0ofgfl8tx/+BmaieXSeM4Svc31L9XdwvmNdXXJOL41wqhEy2/zB4LLCHJQYYDBG+H1CcOSiQqQKpZxUlA5c3B8EMHAZOnB4iDCNeJ4TplGsdxvI2w2+R+BvCcAd/FEOuJZLL720KwPWPs50OE8H3b88jFEvQC72ydeh1cBSrp4yl5q+S7MtRYbAkiQrA4q/PI5+DgcsyV1JYEz5I6t1SPzQtiOLYCDI5S4B+GCtqD3KHh0Vrhs6czu62YC++C1SMHrrKFOyPMHqSXgn8ru0xEAEdJOiM97W2SkQ8gjsVcr+GyZd/05jGeU9h0eeeeK+63qOcg3jGmHwP8Cqf0sj/ybOZK+TjvrmlXPUC0pJeBsMP8gp1DA2ibBQWKYywEdvlGAY4TAVG2GpGC7k3VRPpJ58Bnrl8Jvn6KvjvsCTsL4K0naYR/xNw2ioxnUMIlsjp96Uh5OiDLxyq4OfV6D9miNdNCRgQt0kdHYeh3KfDGz3xCjPp47HvnZeUQeHipk/yZKyGePaDf7XdXmFHGoJn2XwekZQ2vJH8HzduPih6+DkzKtMxV+2j8n/Ujj4lT831r+iT+cR9/TRbPNdcu9KH/SlzT5X6oxeTGf1Ei5fszM3cNF5JF2Lwe2Kj3lFoX3vHQi43ragZJr0aBIBPs4Syhvjgfj1k9BvPfQDQbA+MJvjZ4PjbkanQRoHcU9PoiUvRYFDJ8PU/9JRN7z55pth3yANRwAZWAkX7kWEg86BKNTN0KhyX6Mn7lFjM7QakJNriR4ozPaUnUNjX2CkGGldFLWiMgAX+PQ7ckC7KX/4tZqhHVPiyP8Vo/4DhA/Cw4OE/qqL/kFeFfu8/xgrsVYOe7ssAvFDszPyU2ZjnVFQ4KWEG5PvjKtz4FkeK/MXBYiFI1ASIcKPZhDe67ctSZUp7nGI1yMjDWQnaG5K2g/USvreOlEWxnXUaddQl0P9a/9Gmoku5enPfTH+IwgPQ7EPJ/we6QOh2he5ufSrQaGvJ9xTTmij796ifD4TcyVmhODCV39sGJ2zw7xdhkIQGlY9BkXcE930mNe1byNLBn+0YTr7kz38oo4jzDWovx/tWEKjQr1PyjwN9qFeVXKX8V3iF0Fjf+gNA/emKIjr+NAWp1u8R5eOoHUY8zjgvZ2/pEw7QhZKHYSJkWu0nqmnoCiMm+YSB4zfuqhADmB+RRmn8CbSGlqYHeMnvm0RAB8GCMIrOcn7PvxvBn0N8ix4nQD8InGWc5SHzTlHw31R/L0S5e8EHts8KdYBzqXudOQ+FNwN9I3LLBXruezMFuENoVmkWOArSmdh/13xtjRJB1IxNKHsDegfWA4HCt7xhIGBNjuoruhEIZc7BeoHdk8RerdSImD6swXd7UX4rFVKDIQlULiwgsB6lN8E4AD8D5i6ppEX4AmbWWJsA8Pn2ik4124FRvg60vew/jsgXnpR19tQbzK7CvhpHTSDkVifuEsI/Rjiq6OQm+C3Ir0dtHZCIdbSQKQNP47EZ/GrJxM5KnyfGcgPuQBd4YTVsQ5fyjJwHji+Qp2oGGuugCyKudGVRvjEOAG3WrmOKapogo4N+OHfo+UzwXUPdR1gesLzaMrfK6m0IsOOLcD33rS1nwMUBuby6iX8YvCtR+hSYi5wT4J7qHwlSqWBjAVGA5bX2E6i/37HYFZED94qyiuRZSvtvAcwZ3L70hVKLfLZCFxbWJ14C7P2UcyqDmbfd3bknVxRY6gWVgWECynw/syDnQ5diQWp2G78QPRXar+GfwwmxieYUniYu5w+UUGWYhw1xH3acCWE9xFHcpPpuyNAgnMdkNZP8H2iANwaSdhcEkZj/QDjfQh/OX4EvA5ktDwdisL5SyjN8NYLIW+TECvhIXaafFP/ddIBFBwKdUsTPqtO6sfgRyjmzsAMRfE2p955tJVg+We4Eaij0GNd2nIvCj2Z+s6+yvTH4NnbGYV4yRINnEEGlHl066zhwYXRDeD9GfKfN6TtbxE/Fj4tq0MGfi25BfR2MMNjYsN/lYONsLwGXwnezB4uXGIKm9CV8dCetnzYLpw4j4XnI/CHsfc6nHAEfe3LgbHqoDAMfFb/DgNhf5fv8ZSrLU7qaJBRd0r4bAtfPJQmpRAbJyKYkanryX7ZIuLhWIz8b8HUUATewqzhk485+ONgOhzruAFH2UKP0TmtLMOC4ICvOFokpIsC6Hl8qlGsRmdPonPD3QK8fQzewwCe537GSnaASgysb5YuJXQm2R+4sEYlr5dwK+Gm07b9gaty6YLbHVzr4N/Bp/cgxD2ESB3pEzQseIsdn5a1F4k/IoGRjYTXfaBte/yVx71QeuVWJDMvHtljNHNkuz6kdkuWV9bxZbKnK0FG0rSq/BA6i9vXYSPPQOZm/bGM0gpe1lm9bMGKTK3PX+fsTf96QldNGPaa2WVc5pRsdWdw2uo7Md/Bzad+uMuCx0qy813cLAdd9sYtzPLVyYnW1dD6KfXcsNvPvp5Yl7izb3vKH/W+hn4ra5zUz7nPi4CmA0Iq+FGTJyvDzUQx9yB+JPknkn8A6dGEh/Jq1BemBRo6EcbOwjjCb/GyL5HRFgWLy/EU2iNNUX0W5/OJbbHFnioEBmfnDwXhXVOnTq2Kl0LZDqHMfUBw8GI7wtl3zKsUOpKjQH6GW4ewvXjrCs0rgD8YGTRqkCqWikq7Cs465Cujo6knX+VG/Erk/PUVjyvtqOfxv4DeSeDRMt3TlSgnyhX6iXBflLEL8lf5XTZWq3i2NevEQV54s4QMwg9IkLUvtNzrhFNHQbJ1ViZO3VCHcBF9/i7tHwAeT/RqkZevDu5SwRKlbiWurOQv3DNR7ubB/eI76E4zBuRRYqU9b1BijQN6DpiABqec5oOnC33lhav4sjqdgK0IoO8PboRjY/CEEXBFaWmsHLK1ALuFyrdxlr4+hLXem/B+lutpzRRovJS81foDs8k/RKsgqKOymEydvyiSLD1sZEVrTStkIuBSGM4iHt0+jPLuhwAW09HuZ65AsG9zSvV0pkqIkncckW/RUVppJ+r5rijwyaa9UMlgkyf6r0DrRvYCRzKTCuvL4IPI+yO4TuDw4o2EXpghoeVo7C26nR++L0nKP0kQ5AKdC+i879HRHjW7Cy0xtkGDBgW60BoOjDQciWtooxewzuA96KOwgyWuMTk6f5XwXPJ9MVHAkAYSOsBMo/+kEXASljjaXMl4nK01bOv+HzzvCGwe/sXxX+RPQ1Z3Eo+8+G5tOLz8UBhhYccl7GNWQEfaG/EFSR19IW2fyi+jP9sqeUUdg7b0aujbXaj/HrrtnqUivARTA2F0jgz8A6H5Iwa24XUQXULc+4FRxN2cT6QjZxO+boU4QhDVutNGsg509HPzVKVCgsN3yZWELaqyjpEnjKLQ/yV+PxroY8BGOlnjfULDoeKrlDlreau6FWU+uxCfAqznruB38DvTDOAqCoTDhcAfo/hp7AW+QTgAI4P9ZT5Y3Ie4v/H7IHTeBI/C/Rq0tpKWnY2S+L4qVQjprYxDbg4ENZz4zSEch3wnoETyWWIgLi2A2RwedkTJCVr9ItNTnQu88a9ED+U8BF43po6y81saVwjTKsEnxlepOOTHfRmymIiMToYXN5xNiMHPrO+Az4cIfX/nLDaYYBcrgttPErzhF/a35hGv2C+Wl3PUaebS2n1jWgw++6XECQMP0lBH7hIAWIN2XTSQAp0cliA0KjBqm6xJelwWA50wI5t2iZFNt42jVH6oIy7Ps1e0pC1ghTSjUDA8+JiCH4Mhn4dg6uDX49hqcA5D2MMSAYQPaGIZsPWMMn/HSMJbKklwXu43Ah4PhhHfPOLRcOMR9zxoeXz6IEbSnyVPgXgDNDpBz19it26ghaI1MSLVYoTTKPfAYh8VNmmz6KPzksq4N7VeJEaaoRzDs50eBlzFoHAUdDaFZlibAm8/pQoEvwfAlwOPv/nr8uK5l1566RnqV7FUSb/5CIgZBMmX93tQiI2RTQt4bfNe5HfB+3GGH1IFcGWBdyBxwDEvpRsAMn/cDLvfBIe0RyHnsQwsnjA5qlfRht2guZtVlAnG6ahVIK+OAcefcPop9WbibV8YWGiTH1LZPxL3JW/JgEMeReH9mF91OkrIr4YinrAHYwmc1TW/YfG00TbFfgg4OvrjKc8iELspHAqjDyUEUqHQqGpnF5WEBst0nCVSmApEhHPDvC2MuWlbgBK1nQ4rVC3Ohq/QU4SjGQnfhtdxCDgcv9LhQfiJkMKoAJ+e2PgbU7cj8KNYii1Mlk8KUb76oNTW87uD8H2IFBF2HhIqqkr1EiP6luC9lg7bD19vJ0sv0iJPerV09N3QcF1/Oje6brRVhjAlx0046LtJE3kThB+9SPdI0qZOumShjWfShrvhzefY4Qs+lNqNaA4aLnmPpVz+u/h9C8vde5OTm6JvSoRnzxRkB567wHcqcosvjgcwWBwMyI3JMktwZ8LO8gf/gV/oSq+ig27QA/gaB9+e+Y9FXzrLd5SX8tAzMIVvQOiThcjsv5HZNciSonyzhiYRZIpIq7syAChnDx2qgW071CurcBfEwDKbWWQIfR34pk2nUXYvqMLADQ/VtLnevoIn93Wh/6S1Mk6LuxYhn8rlyB+If4T3fD8IVQR2hEph+EmcDQdea+3Nhj7PXmQyv3nlEiKcTH0SXMDKj9O0gr6O75ZvIzyIBg8jfyM6tU9Cz0u6WfhnyL+NDgjrWzfWycVnjk9EF/AU4ycI1FerbhTl83W8yuSIlYPHZrxG8j7Z+4NnV5Y8I4hvBZ1+wMjLApToefJ+j4LdTai7G9zhNIn4dDPWXHPNuL6/Hhl49LoUGL/fjnundKCRrnWgdw/x/yS6OnDL2Mj7pu0By3DyfQmK4JFtK2Ua0W2hhPYkYRrQ7pDHD9U9DqzTRDfgm1BSDe7tBLDABjjWuRuDc2BbBL/SDSeYyqWCC31jGXxfzl3TLeB2iT6UOusTemVg8SLoKmflMgnYWeQrSstS2ROdjVKfgFc3w+xOf/piI4dMAiLj0YFzNPKcQnoJMtGQPJDo7e+rCYOxvUf9k/Dx/7aYvdIur+UypbvPOBor6540ZqURtAdo42mAS7eb8KfSwDCdk1/S0PbwZMvAoeJGpQtF7oP4hZEcLz0LLskiPHBhtiMsUZwIkwntqSK+lA0nY45Waf1IKzlmjNVLfm3FgkQBCIIiFOHOlkckHYW2J8tLFr4SjSxMuXi2XjbeBrZENm3KQxLe2vaNP5VU89RTT+XaLsXLwIqjPTolZe3wG/jpAF+EaTf8f87JSBcz2ukKAAAAAElFTkSuQmCC';

        array_push($docketData, $requestClientAttachment);

        $input['dockets'] = json_encode($docketData);

      }



      foreach( $docketData as $key => $docketInfo ){

        $filename_path = "docket_".md5(time().uniqid()).".jpg";
        $imageDecode = base64_decode($docketInfo['Attachment']);
        file_put_contents(public_path("image/docket/".$filename_path),$imageDecode);

        $docketData[$key]['Attachment'] = $filename_path;

      }

      $input['dockets'] = json_encode($docketData);

    }


    if( $input['parentJob'] == "" ){


      //create additional hazards
      $additionalHazards = json_decode($input['additionalHazards']);
      $additionalHazardReportData = [];

      if( count($additionalHazards) > 0 ){

          $hazardList = array();

          foreach( $additionalHazards as $key => $hazardInfo ){

            $hazardRecord = [
              // "JobID" => $hazardInfo->JobID,
              "AddHazard" => $hazardInfo->AddHazard,
              "InitialRisk" => $hazardInfo->InitialRisk,
              "ControlMeasures" => $hazardInfo->ControlMeasures,
              "ResidualRisk" => $hazardInfo->ResidualRisk,
              // "created_at" => Carbon::now(),
              // "updated_at" => Carbon::now()
            ];

            array_push($hazardList, $hazardRecord);
            array_push($additionalHazardReportData,$hazardRecord);

          }

      }


      //create tc signatures
      $tcData = json_decode($input['tcAttachment']);
      $tcSignatureData = [];

      if( count($tcData) > 0 ){

        $tcList = array();

        foreach( $tcData as $key => $tcInfo ){

          // $filename_path = md5(time().uniqid()).".jpg";
          // $signatureDecode = base64_decode($tcInfo->Attachment);
          // file_put_contents(public_path("image/signatures/".$filename_path),$signatureDecode);

          $tcRecord = [
            "JobID" => $tcInfo->JobID,
            "TCName" => $tcInfo->TCName,
            "AttachedOn" => date('Y-m-d H:i:s',$tcInfo->AttachedOn / 1000),
            // "Attachment" => $tcInfo->Attachment,
            "Src" => $tcInfo->Attachment,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];


          array_push($tcList, $tcRecord);
          array_push($tcSignatureData,$tcRecord);

        }

      }

      //Job Info
      $jobInfo = $this->jobHeaderRepository->getById($input['jobID']);

      $state = "";
      $depot = $jobInfo['Depot'];

      if( $jobInfo['JobState'] == "" ){

        $state = $this->checkState($depot);

      }
      else{

        $state = $jobInfo['JobState'];

      }




      //create signage audit

      $sinageAuditReportData = [];
      $sinageAuditReportData['SlowLane'] = [];
      $sinageAuditReportData['FastLane'] = [];
      $afterCarePresent = "FALSE";
      $afterCareCount = 0;

      $signageAudit = json_decode($input['signageAudit']);

      if( $input['signageAudit'] != "" && count($signageAudit) > 0 ){

        foreach( $signageAudit as $key => $signage ){

          $sinageRecord = [
            "JobID" => $signage->JobID,
            "Landmark" => $signage->Landmark,
            "CarriageWay" => $signage->CarriageWay,
            "Direction" => $signage->Direction,
            "TimeErected" => date('H:i:s',$signage->TimeErected / 1000),
            "TimeCollected" => date('H:i:s',$signage->TimeCollected / 1000),
            "TimeChecked1" => date('H:i:s',$signage->TimeChecked1 / 1000),
            "TimeChecked2" => date('H:i:s',$signage->TimeChecked2 / 1000),
            "TimeChecked3" => date('H:i:s',$signage->TimeChecked3 / 1000),
            "TimeChecked4" => date('H:i:s',$signage->TimeChecked4 / 1000),
            "ErectedBy" => $signage->ErectedBy,
            "CollectedBy" => $signage->CollectedBy,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];



          

          if( $signage->AuditSigns != "" ){

            $auditSignsList = explode('~',$signage->AuditSigns);

            foreach( $auditSignsList as $auditSignInfo ){

              $signID = explode(':',explode('^',$auditSignInfo)[0])[1];
              $metres = explode(':',explode('^',$auditSignInfo)[1])[1];
              $qty = explode(':',explode('^',$auditSignInfo)[2])[1];
              $afterCare = explode(':',explode('^',$auditSignInfo)[3])[1];
              $afterCareMetres = explode(':',explode('^',$auditSignInfo)[4])[1];
              $afterCareQty = explode(':',explode('^',$auditSignInfo)[5])[1];

              $auditRecord = [
                "JobID" => $signage->JobID,
                "SignID" => $signID,
                "Metres" => $metres,
                "Src" => 'image\signage\\'.$state.'-Icons\sign_'.$signID.'.png',
                "Qty" => $qty,
                "AfterCare" => $afterCare,
                "AfterCareMetres" => $afterCareMetres,
                "AfterCareQty" => $afterCareQty,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
              ];

              if( $afterCare > 0 ){

                $afterCareCount++;

              }


              switch( $signage->CarriageWay ){
                case 1:

                  array_push($sinageAuditReportData['SlowLane'],$auditRecord);

                break;
                case 2:

                  array_push($sinageAuditReportData['FastLane'],$auditRecord);

                break;
              }

            }

          }


          switch( $signage->Direction ){
            case 1:
              $sinageAuditReportData['Direction'] = "North";
            break;
            case 2:
              $sinageAuditReportData['Direction'] = "South";
            break;
            case 3:
              $sinageAuditReportData['Direction'] = "East";
            break;
            case 4:
              $sinageAuditReportData['Direction'] = "West";
            break;
            default:
              $sinageAuditReportData['Direction'] = "";
            break;
          }

          $sinageAuditReportData['LocalLandmark'] = $signage->Landmark;

          $sinageAuditReportData['TimeErected'] = $signage->TimeErected / 1000;
          $sinageAuditReportData['TimeCollected'] = $signage->TimeCollected / 1000;
          $sinageAuditReportData['TimeChecked1'] = $signage->TimeChecked1 / 1000;
          $sinageAuditReportData['TimeChecked2'] = $signage->TimeChecked2 / 1000;
          $sinageAuditReportData['TimeChecked3'] = $signage->TimeChecked3 / 1000;
          $sinageAuditReportData['TimeChecked4'] = $signage->TimeChecked4 / 1000;

        }

      }

      $jobRequirements = json_decode($input['requirements']);
      $jobRequirementList = array();

      if( count($jobRequirements) > 0 ){

        foreach( $jobRequirements as $key => $jobRequirementInfo ){

          $jobRequirementRecord = [
            "JobID" => $jobRequirementInfo->JobID,
            "ItemID" => $jobRequirementInfo->RequirementID,
            "Qty" => $jobRequirementInfo->Quantity,
            "Notes" => $jobRequirementInfo->Description,
            "OperatorID" => $input['operatorID'],
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          array_push($jobRequirementList, $jobRequirementRecord);

        }

      }



      $timesheets = json_decode($input['timesheets']);
      $timesheetList = array();

      if( count($timesheets) > 0 ){

        foreach( $timesheets as $key => $timesheetInfo ){

          $contactInfo = $this->contactRepository->findByContactReportData($timesheetInfo->WorkerID, $timesheetInfo->JobID);

          $timesheetRecord = [
            "JobID" => $timesheetInfo->JobID,
            "WorkerID" => $timesheetInfo->WorkerID,
            "ShiftID" => $timesheetInfo->ShiftID,
            "ContactName" => $contactInfo['ContactName'],
            "ContactCode" => $contactInfo['ContactCode'],
            "ShiftType" => $contactInfo['Description'],
            "JobStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobStartDateTime / 1000),
            "JobFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobFinishDateTime / 1000),
            "TravelStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTime / 1000),
            "TravelFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTime / 1000),
            "BreakStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakStartDateTime / 1000),
            "BreakFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakFinishDateTime / 1000),
            "TravelStartDateTimeEnd" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTimeEnd / 1000),
            "TravelFinishDateTimeStart" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTimeStart / 1000),
            "TravelStartDistance" => $timesheetInfo->TravelStartDistance,
            "TravelFinishDistance" => $timesheetInfo->TravelFinishDistance,
            "FatigueCompliance" => $timesheetInfo->FatigueCompliance,
            "RegoNo" => $timesheetInfo->RegoNo,
            "TraveledKilometers" => $timesheetInfo->Odometer,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          if( $timesheetInfo->AttendDepot == 1 ){
            $timesheetRecord['AttendDepot'] = "TRUE";
          }
          else{
            $timesheetRecord['AttendDepot'] = "FALSE";
          }

          array_push($timesheetList, $timesheetRecord);

        }

      }


      $docket = json_decode($input['dockets']);
      $clientSigned = "FALSE";

      if( count($docket) > 0 ){

        foreach( $docket as $key => $docketInfo ){

          //Check if client signed
          if( $docketInfo->AttachmentTypeID == 1 ){

            $clientSigned = "TRUE";

          }

        }

      }


      if( isset($afterCareCount) && $afterCareCount > 0 ){

        $afterCarePresent = "TRUE";

      }




      $geotag = "";

      if( isset($input['geotag'])){

        $geotag = $input['geotag'];

      }

      //Get Shift Length
      $shiftLength = 0;

      if( count($timesheets) > 0 ){

        foreach( $timesheets as $key => $timesheetInfo ){

          $travelStart = date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTime / 1000);
          $travelFinish = date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTime / 1000);

          $contactInfo = $this->contactRepository->findByContactReportData($timesheetInfo->WorkerID, $timesheetInfo->JobID);

          $strings = explode(' ', $contactInfo['Description']);

          if( in_array("Driver", $strings) ){

            $shiftLength = ( strtotime($travelFinish) - strtotime($travelStart) ) / 60;

          }

        }

      }

      //Get Job Images

      $jobImages = $this->jobImageRepository->get($input['jobID']);


      //Prepare Site Assessment eForm Report
      $tcData = array();

      $generateFilePath = Config::get('evolution.generatedFilePath');
      $enableWatchFolder = Config::get('evolution.watchFolderEnable');


      $reportInfo = [
        'jobId' => $input['jobID'],
        'location' => $jobInfo['Address'],
        'client' => $jobInfo['CompanyName'],
        'state' => $state,
        'orderNumber' => $input['orderNumber'],
        'siteaudit' => $siteChecklistReportData,
        'additionalHazards' => $additionalHazardReportData,
        'signageAudit' => $sinageAuditReportData,
        'tcSignature' => $tcSignatureData
      ];

      // print_r($siteChecklistReportData);
      // die();

      $pdf = PDF::loadView('forms.jobsitechecklistfail', $reportInfo)->setPaper('a4','portrait');


      $pdf->output();
      $dom_pdf = $pdf->getDomPDF();

      $canvas = $dom_pdf ->get_canvas();
      $canvas->page_text(275, 810, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 8, array(255, 255, 255));
      $pdf->save($generateFilePath.'JOBDOCKET_'.$input['jobID'].'.pdf');

      $uploadPDFToWatchFolder = false;

      if( $enableWatchFolder ){

        $data = [
          'username' => Config::get('evolution.watchFolderUsername'),
          'password' => Config::get('evolution.watchFolderPassword'),
          'server' => Config::get('evolution.watchFolderHost'),
          'filepath' => Config::get('evolution.generatedFilePath'),
          'foldername' => 'APPDOC',
          'old_filename' => 'JOBDOCKET_'.$input['jobID'].'.pdf',
          'new_filename' => 'JOBDOCKET_'.$input['jobID'].'.pdf'
        ];

        $uploadPDFToWatchFolder = $this->fileServices->uploadToWatchFolder($data);

      }
      else{

        $uploadPDFToWatchFolder = true;

      }

       //Generate Timesheet CSV

      $jobReportInfo = $this->jobHeaderRepository->getReportDataById($input['jobID']);

      $timesheets = json_decode($input['timesheets']);
      $jobTimesheet = array();

      if( count($timesheets) > 0 ){

        foreach( $timesheets as $key => $timesheetInfo ){

          $contactInfo = $this->contactRepository->findByContactReportData($timesheetInfo->WorkerID, $timesheetInfo->JobID);

          $timesheetRecord = [
            "JobID" => $timesheetInfo->JobID,
            "WorkerID" => $timesheetInfo->WorkerID,
            "ShiftID" => $timesheetInfo->ShiftID,
            "ContactName" => $contactInfo['ContactName'],
            "ContactCode" => $contactInfo['ContactCode'],
            "ShiftType" => $contactInfo['Description'],
            "JobStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobStartDateTime / 1000),
            "JobFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobFinishDateTime / 1000),
            "TravelStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTime / 1000),
            "TravelFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTime / 1000),
            "BreakStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakStartDateTime / 1000),
            "BreakFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakFinishDateTime / 1000),
            "TravelStartDateTimeEnd" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTimeEnd / 1000),
            "TravelFinishDateTimeStart" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTimeStart / 1000),
            "TravelStartDistance" => $timesheetInfo->TravelStartDistance,
            "TravelFinishDistance" => $timesheetInfo->TravelFinishDistance,
            "FatigueCompliance" => $timesheetInfo->FatigueCompliance,
            "RegoNo" => $timesheetInfo->RegoNo,
            "TraveledKilometers" => $timesheetInfo->Odometer,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          if( $timesheetInfo->AttendDepot == 1 ){
            $timesheetRecord['AttendDepot'] = "TRUE";
          }
          else{
            $timesheetRecord['AttendDepot'] = "FALSE";
          }

          if( $timesheetInfo->FatigueCompliance == 1 ){
            $timesheetRecord['FatigueCompliance'] = "TRUE";
          }
          else{
            $timesheetRecord['FatigueCompliance'] = "FALSE";
          }

          array_push($jobTimesheet, $timesheetRecord);

        }

      }

      $csvInfo = [
        'jobNo' => $jobReportInfo['ContactCode'],
        'jobID' => $jobReportInfo['JobID'],
        'timesheet' => $jobTimesheet,
      ];

      Excel::create($input['jobID'].'_JD', function ($excel) use ($csvInfo) {

        // Set the title
        $excel->setTitle('Job Docket Info');

        // // Chain the setters
        $excel->setCreator('Evolution')
              ->setCompany('Evolution');

        // // Call them separately
        $excel->setDescription('Job Docket Info');

        // Set sheets
        $excel->sheet('Site Assessment CSV', function($sheet) use ($csvInfo) {

            $sheet->loadView('forms.jobdocketlogcsv',$csvInfo);

            $sheet->freezeFirstRow();

        });

      })->store('csv', public_path('generated files'), true);

      $uploadCSVToWatchFolder = false;

      if( $enableWatchFolder ){

        $data = [
          'username' => Config::get('evolution.watchFolderUsername'),
          'password' => Config::get('evolution.watchFolderPassword'),
          'server' => Config::get('evolution.watchFolderHost'),
          'filepath' => Config::get('evolution.generatedFilePath'),
          'foldername' => 'APPDOC',
          'old_filename' => $input['jobID'].'_JD.csv',
          'new_filename' => $input['jobID'].'_JD.csv'
        ];

        $uploadCSVToWatchFolder = $this->fileServices->uploadToWatchFolder($data);


      }
      else{

        $uploadCSVToWatchFolder = true;

      }

    }
    else{

        //create additional hazards
      $additionalHazards = json_decode($input['additionalHazards']);
      $additionalHazardReportData = [];

      if( count($additionalHazards) > 0 ){

          $hazardList = array();

          foreach( $additionalHazards as $key => $hazardInfo ){

            $hazardRecord = [
              // "JobID" => $hazardInfo->JobID,
              "AddHazard" => $hazardInfo->AddHazard,
              "InitialRisk" => $hazardInfo->InitialRisk,
              "ControlMeasures" => $hazardInfo->ControlMeasures,
              "ResidualRisk" => $hazardInfo->ResidualRisk,
              // "created_at" => Carbon::now(),
              // "updated_at" => Carbon::now()
            ];

            array_push($hazardList, $hazardRecord);
            array_push($additionalHazardReportData,$hazardRecord);

          }

      }
      $additionalHazards = json_decode($input['additionalHazards']);


      //create tc signatures
      $tcData = json_decode($input['tcAttachment']);
      $tcSignatureData = [];

      if( count($tcData) > 0 ){

        $tcList = array();

        foreach( $tcData as $key => $tcInfo ){

          $filename_path = md5(time().uniqid()).".jpg";
          $signatureDecode = base64_decode($tcInfo->Attachment);
          file_put_contents(public_path("image/signatures/".$filename_path),$signatureDecode);

          $tcRecord = [
            "JobID" => $tcInfo->JobID,
            "TCName" => $tcInfo->TCName,
            "AttachedOn" => date('Y-m-d H:i:s',$tcInfo->AttachedOn / 1000),
            // "Attachment" => $tcInfo->Attachment,
            "Src" => $tcInfo->Attachment,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];


          array_push($tcList, $tcRecord);
          array_push($tcSignatureData,$tcRecord);

        }

      }

      //Get Job Info
      $jobInfo = $this->jobHeaderRepository->getById($input['parentJob']);

      $state = "";
      $depot = $jobInfo['Depot'];

      if( $jobInfo['JobState'] == "" ){

        $state = $this->checkState($depot);

      }
      else{

        $state = $jobInfo['JobState'];

      }

      //create signage audit

      $sinageAuditReportData = [];
      $sinageAuditReportData['SlowLane'] = [];
      $sinageAuditReportData['FastLane'] = [];
      $afterCarePresent = "FALSE";
      $afterCareCount = 0;

      $signageAudit = json_decode($input['signageAudit']);

      if( $input['signageAudit'] != "" && count($signageAudit) > 0 ){

        foreach( $signageAudit as $key => $signage ){

          $sinageRecord = [
            "JobID" => $signage->JobID,
            "Landmark" => $signage->Landmark,
            "CarriageWay" => $signage->CarriageWay,
            "Direction" => $signage->Direction,
            "TimeErected" => date('H:i:s',$signage->TimeErected / 1000),
            "TimeCollected" => date('H:i:s',$signage->TimeCollected / 1000),
            "TimeChecked1" => date('H:i:s',$signage->TimeChecked1 / 1000),
            "TimeChecked2" => date('H:i:s',$signage->TimeChecked2 / 1000),
            "TimeChecked3" => date('H:i:s',$signage->TimeChecked3 / 1000),
            "TimeChecked4" => date('H:i:s',$signage->TimeChecked4 / 1000),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];



          

          if( $signage->AuditSigns != "" ){

            $auditSignsList = explode('~',$signage->AuditSigns);

            foreach( $auditSignsList as $auditSignInfo ){

              $signID = explode(':',explode('^',$auditSignInfo)[0])[1];
              $metres = explode(':',explode('^',$auditSignInfo)[1])[1];
              $qty = explode(':',explode('^',$auditSignInfo)[2])[1];
              $afterCare = explode(':',explode('^',$auditSignInfo)[3])[1];
              $afterCareMetres = explode(':',explode('^',$auditSignInfo)[4])[1];
              $afterCareQty = explode(':',explode('^',$auditSignInfo)[5])[1];

              if( $afterCare > 0 ){

                $afterCareCount++;

              }

              $auditRecord = [
                "JobID" => $signage->JobID,
                "SignID" => $signID,
                "Metres" => $metres,
                "Src" => 'image\signage\\'.$state.'-Icons\sign_'.$signID.'.png',
                "Qty" => $qty,
                "AfterCare" => $afterCare,
                "AfterCareMetres" => $afterCareMetres,
                "AfterCareQty" => $afterCareQty,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
              ];

              if( $afterCare > 0 ){

                $afterCareCount++;

              }

              switch( $signage->CarriageWay ){
                case 1:

                  array_push($sinageAuditReportData['SlowLane'],$auditRecord);

                break;
                case 2:

                  array_push($sinageAuditReportData['FastLane'],$auditRecord);

                break;
              }

            }

          }


          switch( $signage->Direction ){
            case 1:
              $sinageAuditReportData['Direction'] = "North";
            break;
            case 2:
              $sinageAuditReportData['Direction'] = "South";
            break;
            case 3:
              $sinageAuditReportData['Direction'] = "East";
            break;
            case 4:
              $sinageAuditReportData['Direction'] = "West";
            break;
            default:
              $sinageAuditReportData['Direction'] = "";
            break;
          }

          $sinageAuditReportData['LocalLandmark'] = $signage->Landmark;

          $sinageAuditReportData['TimeErected'] = $signage->TimeErected;
          $sinageAuditReportData['TimeCollected'] = $signage->TimeCollected;
          $sinageAuditReportData['TimeChecked1'] = $signage->TimeChecked1;
          $sinageAuditReportData['TimeChecked2'] = $signage->TimeChecked2;
          $sinageAuditReportData['TimeChecked3'] = $signage->TimeChecked3;
          $sinageAuditReportData['TimeChecked4'] = $signage->TimeChecked4;

        }

      }

      $jobRequirements = json_decode($input['requirements']);
      $jobRequirementList = array();

      if( count($jobRequirements) > 0 ){

        foreach( $jobRequirements as $key => $jobRequirementInfo ){

          $jobRequirementRecord = [
            "JobID" => $jobRequirementInfo->JobID,
            "ItemID" => $jobRequirementInfo->RequirementID,
            "Qty" => $jobRequirementInfo->Quantity,
            "Notes" => $jobRequirementInfo->Description,
            "OperatorID" => $input['operatorID'],
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          array_push($jobRequirementList, $jobRequirementRecord);

        }

      }

      $timesheets = json_decode($input['timesheets']);
      $timesheetList = array();



      if( count($timesheets) > 0 ){

        foreach( $timesheets as $key => $timesheetInfo ){

          $contactInfo = $this->contactRepository->findByContactReportData($timesheetInfo->WorkerID, $input['parentJob']);

          $timesheetRecord = [
            "JobID" => $timesheetInfo->JobID,
            "WorkerID" => $timesheetInfo->WorkerID,
            "ShiftID" => $timesheetInfo->ShiftID,
            "ContactName" => $contactInfo['ContactName'],
            "ContactCode" => $contactInfo['ContactCode'],
            "ShiftType" => $contactInfo['Description'],
            "JobStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobStartDateTime / 1000),
            "JobFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobFinishDateTime / 1000),
            "TravelStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTime / 1000),
            "TravelFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTime / 1000),
            "BreakStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakStartDateTime / 1000),
            "BreakFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakFinishDateTime / 1000),
            "TravelStartDateTimeEnd" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTimeEnd / 1000),
            "TravelFinishDateTimeStart" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTimeStart / 1000),
            "TravelStartDistance" => $timesheetInfo->TravelStartDistance,
            "TravelFinishDistance" => $timesheetInfo->TravelFinishDistance,
            "FatigueCompliance" => $timesheetInfo->FatigueCompliance,
            "RegoNo" => $timesheetInfo->RegoNo,
            "TraveledKilometers" => $timesheetInfo->Odometer,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          if( $timesheetInfo->AttendDepot == 1 ){
            $timesheetRecord['AttendDepot'] = "TRUE";
          }
          else{
            $timesheetRecord['AttendDepot'] = "FALSE";
          }


          array_push($timesheetList, $timesheetRecord);

        }

      }





      $docket = json_decode($input['dockets']);
      $clientSigned = "FALSE";

      if( count($docket) > 0 ){

        foreach( $docket as $key => $docketInfo ){

          //Check if client signed
          if( $docketInfo->AttachmentTypeID == 1 ){

            $clientSigned = "TRUE";

          }

        }

      }


      if( $afterCareCount > 0 ){

        $afterCarePresent = "TRUE";

      }

      $geotag = "";

      if( isset($input['geotag'])){

        $geotag = $input['geotag'];

      }


      //Get Shift Length
      $shiftLength = 0;

      if( count($timesheets) > 0 ){

        foreach( $timesheets as $key => $timesheetInfo ){

          $travelStart = date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTime / 1000);
          $travelFinish = date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTime / 1000);

          $contactInfo = $this->contactRepository->findByContactReportData($timesheetInfo->WorkerID, $input['parentJob']);

          $strings = explode(' ', $contactInfo['Description']);

          if( in_array("Driver", $strings) ){

            $shiftLength = ( strtotime($travelFinish) - strtotime($travelStart) ) / 60;

          }

        }

      }

      //Get Job Images

      $jobImages = $this->jobImageRepository->get($input['jobID']);

      $generateFilePath = Config::get('evolution.generatedFilePath');
      $enableWatchFolder = Config::get('evolution.watchFolderEnable');

      //Prepare Site Assessment eForm Report
      $tcData = array();

      $reportInfo = [
        'jobId' => $input['jobID'],
        'location' => $jobInfo['Address'],
        'client' => $jobInfo['CompanyName'],
        'state' => $state,
        'orderNumber' => $input['orderNumber'],
        'siteaudit' => $siteChecklistReportData,
        'additionalHazards' => $additionalHazardReportData,
        'signageAudit' => $sinageAuditReportData,
        'tcSignature' => $tcSignatureData
      ];


      $pdf = PDF::loadView('forms.jobsitechecklistfail', $reportInfo)->setPaper('a4','portrait');
      $pdf->output();
      $dom_pdf = $pdf->getDomPDF();

      $canvas = $dom_pdf ->get_canvas();
      $canvas->page_text(275, 810, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 8, array(255, 255, 255));
      $pdf->save($generateFilePath.'JOBDOCKET_'.$input['jobID'].'.pdf');


      $uploadPDFToWatchFolder = false;

      if( $enableWatchFolder ){

        $data = [
          'username' => Config::get('evolution.watchFolderUsername'),
          'password' => Config::get('evolution.watchFolderPassword'),
          'server' => Config::get('evolution.watchFolderHost'),
          'filepath' => Config::get('evolution.generatedFilePath'),
          'foldername' => 'APPDOC',
          'old_filename' => 'JOBDOCKET_'.$input['jobID'].'.pdf',
          'new_filename' => 'JOBDOCKET_'.$input['jobID'].'.pdf'
        ];

        $uploadPDFToWatchFolder = $this->fileServices->uploadToWatchFolder($data);


      }
      else{

        $uploadPDFToWatchFolder = true;


      }




       //Generate Timesheet CSV

      $jobReportInfo = $this->jobHeaderRepository->getReportDataById($input['parentJob']);

      $timesheets = json_decode($input['timesheets']);
      $jobTimesheet = array();

      if( count($timesheets) > 0 ){

        foreach( $timesheets as $key => $timesheetInfo ){

          $contactInfo = $this->contactRepository->findByContactReportData($timesheetInfo->WorkerID, $input['parentJob']);

          $timesheetRecord = [
            "JobID" => $timesheetInfo->JobID,
            "WorkerID" => $timesheetInfo->WorkerID,
            "ShiftID" => $timesheetInfo->ShiftID,
            "ContactName" => $contactInfo['ContactName'],
            "ContactCode" => $contactInfo['ContactCode'],
            "ShiftType" => $contactInfo['Description'],
            "JobStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobStartDateTime / 1000),
            "JobFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->JobFinishDateTime / 1000),
            "TravelStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTime / 1000),
            "TravelFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTime / 1000),
            "BreakStartDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakStartDateTime / 1000),
            "BreakFinishDateTime" => date('Y-m-d H:i:s',$timesheetInfo->BreakFinishDateTime / 1000),
            "TravelStartDateTimeEnd" => date('Y-m-d H:i:s',$timesheetInfo->TravelStartDateTimeEnd / 1000),
            "TravelFinishDateTimeStart" => date('Y-m-d H:i:s',$timesheetInfo->TravelFinishDateTimeStart / 1000),
            "TravelStartDistance" => $timesheetInfo->TravelStartDistance,
            "TravelFinishDistance" => $timesheetInfo->TravelFinishDistance,
            "FatigueCompliance" => $timesheetInfo->FatigueCompliance,
            "RegoNo" => $timesheetInfo->RegoNo,
            "TraveledKilometers" => $timesheetInfo->Odometer,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
          ];

          if( $timesheetInfo->AttendDepot == 1 ){
            $timesheetRecord['AttendDepot'] = "TRUE";
          }
          else{
            $timesheetRecord['AttendDepot'] = "FALSE";
          }

          if( $timesheetInfo->FatigueCompliance == 1 ){
            $timesheetRecord['FatigueCompliance'] = "TRUE";
          }
          else{
            $timesheetRecord['FatigueCompliance'] = "FALSE";
          }

          array_push($jobTimesheet, $timesheetRecord);

        }

      }

      $csvInfo = [
        'jobNo' => $jobReportInfo['ContactCode'],
        'jobID' => $jobReportInfo['JobID'],
        'timesheet' => $jobTimesheet,
      ];

      Excel::create($input['jobID'].'_JD', function ($excel) use ($csvInfo) {

        // Set the title
        $excel->setTitle('Job Docket Info');

        // // Chain the setters
        $excel->setCreator('Evolution')
              ->setCompany('Evolution');

        // // Call them separately
        $excel->setDescription('Job Docket Info');

        // Set sheets
        $excel->sheet('Site Assessment CSV', function($sheet) use ($csvInfo) {

            $sheet->loadView('forms.jobdocketlogcsv',$csvInfo);

            $sheet->freezeFirstRow();

        });

      })->store('csv', public_path('generated files'), true);

      $uploadCSVToWatchFolder = false;

      if( $enableWatchFolder ){

        $data = [
          'username' => Config::get('evolution.watchFolderUsername'),
          'password' => Config::get('evolution.watchFolderPassword'),
          'server' => Config::get('evolution.watchFolderHost'),
          'filepath' => Config::get('evolution.generatedFilePath'),
          'foldername' => 'APPDOC',
          'old_filename' => $input['jobID'].'_JD.csv',
          'new_filename' => $input['jobID'].'_JD.csv'
        ];

        $uploadCSVToWatchFolder = $this->fileServices->uploadToWatchFolder($data);

      }
      else{

        $uploadCSVToWatchFolder = true;

      }


    }


    //Check if uploading in watch folder is enabled
    $uploadPhotoToWatchFolder = false;

    $enableWatchFolder = Config::get('evolution.watchFolderEnable');
    $countPendingJobImages = $this->jobImageRepository->countPendingJobImage($input['jobID']);

    // Check if there are pending images in database
    if( $countPendingJobImages > 0 ){


      $pendingJobImagesList = $this->jobImageRepository->getPendingJobImage($input['jobID']);

      $jobImageData = [];
      $currenctJobID = "";

      if ( strpos($input['jobID'], '.') > 0 ) {

        $pos = strpos($input['jobID'], '.') - strlen($input['jobID']);
        $parentJob = substr($input['jobID'], 0, $pos);

        $jobReportInfo = $this->jobHeaderRepository->getReportDataById($parentJob);

      }
      else{

        $jobReportInfo = $this->jobHeaderRepository->getReportDataById($input['jobID']);

      }

      $imageCount = 1;

      foreach( $pendingJobImagesList as $pendingImagesRecord ){

        $imageName = pathinfo($pendingImagesRecord['JobImage']);

        //Check if current job image exist
        if( File::exists(public_path("image/job/".$imageName['basename'])) ){

          $newFilename = 'IMAGEDOCKET_'.$input['jobID'].'.'.$imageName['extension'];

          //If multiple image
          if( $countPendingJobImages > 1 ){

            $newFilename = 'IMAGEDOCKET_'.$input['jobID'].'-'.$imageCount.'.'.$imageName['extension'];

          }


          $uploadToWatchFolder = false;

          if( $enableWatchFolder ){

            $data = [
              'username' => Config::get('evolution.watchFolderUsername'),
              'password' => Config::get('evolution.watchFolderPassword'),
              'server' => Config::get('evolution.watchFolderHost'),
              'filepath' => public_path("image/job/"),
              'foldername' => 'APPIMAGE',
              'old_filename' => $imageName['basename'],
              'new_filename' => $newFilename
            ];

            $uploadToWatchFolder = $this->fileServices->uploadToWatchFolder($data);

          }

          //Check if upload to FTP is successful
          if( $uploadToWatchFolder ){

            $jobImageData[] = [
                'fileName' => $newFilename,
                'oldFilePath' => public_path("image/job/".$imageName['basename']),
                'formId' => 'DTMCD003',
                'jobNo' => $jobReportInfo['JobCode'],
                'jobID' => $jobReportInfo['JobID'],
                'jobDate' => date('d/m/Y',strtotime($jobReportInfo['JobStartdate'])),
                'jobDateMonth' => date('F',strtotime($jobReportInfo['JobStartdate'])),
                'jobDateYear' => date('Y',strtotime($jobReportInfo['JobStartdate'])),
                'clientCode' => $jobReportInfo['ContactCode'],
                'clientName' => $jobReportInfo['CompanyName'],
                'depot' => $jobReportInfo['Depot'],
                'state' => $jobReportInfo['JobState'],
                'orderNo' => $jobReportInfo['OrderNumber'],
                'location1' => $jobReportInfo['Address'],
                'jobImageTime' => date('H:i:s',strtotime($pendingImagesRecord['created_at'])),
                'jobImageGeo' => $pendingImagesRecord['GeoTag']
              ];

              $imageCount++;

          }

        }

      }

      //Check if there are job image record included
      if( count( $jobImageData ) > 0 ){

        $csvInfo['jobImageData'] = $jobImageData;

        Excel::create($input['jobID'].'_IM', function ($excel) use ($csvInfo) {

          // Set the title
          $excel->setTitle('Job Image CSV');

          // // Chain the setters
          $excel->setCreator('Evolution')
                ->setCompany('Evolution');

          // // Call them separately
          $excel->setDescription('Job Image CSV');

          // Set sheets
          $excel->sheet('Job Image CSV', function($sheet) use ($csvInfo) {

              $sheet->loadView('forms.appimagecsv',$csvInfo);

              $sheet->freezeFirstRow();

          });

        })->store('csv', public_path('generated files'), true);


        if( $enableWatchFolder ){

          $data = [
            'username' => Config::get('evolution.watchFolderUsername'),
            'password' => Config::get('evolution.watchFolderPassword'),
            'server' => Config::get('evolution.watchFolderHost'),
            'filepath' => Config::get('evolution.generatedFilePath'),
            'foldername' => 'APPIMAGE',
            'old_filename' => $input['jobID'].'_IM.csv',
            'new_filename' => $input['jobID'].'_IM.csv'
          ];

          $uploadPhotoToWatchFolder = $this->fileServices->uploadToWatchFolder($data);


          foreach( $jobImageData as $jobImageInfo ){

            File::delete($jobImageInfo['oldFilePath']);

          }


          $countGenerateReport = $this->generateReportRepository->countByJobID($input['jobID']);

          if( $countGenerateReport > 0 ){

            $generateInfo = [
              'JobImage' => 1,
              'updated_at' => Carbon::now()
            ];

            $updateGenerateReport = $this->generateReportRepository->updateByJobID($generateInfo, $input['jobID']);

          }
          else{

            $generateInfo = [
              'JobID' => $input['jobID'],
              'JobImage' => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
            ];

            $insertGenerateReport = $this->generateReportRepository->create($generateInfo, $input['jobID']);

          }


          $jobImageInfo = [
            'Processed' => 1,
            'updated_at' => Carbon::now()
          ];

          $updateJobImage = $this->jobImageRepository->updateByJobID($jobImageInfo, $input['jobID']);


        }



      }



    }




    $email = Config::get('evolution.evolutionEmail');

    if( $email ){

      if( $input['parentJob'] == "" ){

        $emailJobData = array(
          'email' => $email,
          'jobID' => $jobReportInfo['JobID']
        );

      }
      else{

        $emailJobData = array(
          'email' => $email,
          'jobID' => $input['jobID']
        );
        
      }


      $emailJobData['attachment'][] = public_path('generated files/'.$input['jobID'].'_JD.csv');

      $this->emailServices->sendEmail(4,$emailJobData);

    }


    if( $uploadCSVToWatchFolder && $uploadPDFToWatchFolder ){

      //Record Job Sync
      $jobSync = Config::get('evolution.jobSyncEnable');

      if( $jobSync ){

        $countJobSync = $this->jobSyncRepository->countByJobIDIP($input['jobID'],Request::getClientIp());

        if( $countJobSync <= 0 ){

          $jobSyncData = array(
            "IP" => Request::getClientIp(),
            "JobID" => $input['jobID'],
            "RequestID" => 0
          );

          $createJobSync = $this->jobSyncRepository->create($jobSyncData);

        }
        else{

          $jobSyncData = array(
            "IP" => Request::getClientIp(),
            "JobID" => $input['jobID'],
            "RequestID" => 0
          );

          $createJobSync = $this->jobSyncRepository->updateByJobIDIP($input['jobID'],Request::getClientIp(),$jobSyncData);


        }

      }

      $response['status'] = true;
      $response['statusCode'] = 200;
      $response['message'] = "Fail safe report for ".$input['jobID']." has been successfully sent";

    }
    else{

      $response['status'] = false;
      $response['statusCode'] = 422;
      $response['message'] = "Fail safe report for ".$input['jobID']." has been unsuccessfully sent";


    }


    return $response;


  }


  public function checkJobReport($type, $jobId = false){

    if( $type ){

      if( $type == 'jobdocket' ){

        $countReport = $this->generateReportRepository->countJobDocket($jobId);

        if( $countReport > 0 ){

          $response['status'] = true;
          $response['statusCode'] = 200;
          $response['message'] = "Job Docket Report was already created";

        }
        else{

          $response['status'] = false;
          $response['statusCode'] = 200;
          $response['message'] = "Job Docket Report is not yet created";

        }

      }
      elseif( $type == 'safety' ){

        $countReport = $this->generateReportRepository->countSafetyDocket($jobId);

        if( $countReport > 0 ){

          $response['status'] = true;
          $response['statusCode'] = 200;
          $response['message'] = "Safety Docket Report was already created";

        }
        else{

          $response['status'] = false;
          $response['statusCode'] = 200;
          $response['message'] = "Safety Docket Report is not yet created";

        }


      }
      elseif( $type == 'jobimage' ){

        $countReport = $this->generateReportRepository->countJobImage($jobId);

        if( $countReport > 0 ){

          $response['status'] = true;
          $response['statusCode'] = 200;
          $response['message'] = "Job Image Report was already created";

        }
        else{

          $response['status'] = false;
          $response['statusCode'] = 200;
          $response['message'] = "Job Image Report is not yet created";

        }

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 400;
        $response['message'] = "Invalid Report Type";

      }

    }
    else{

      $response['status'] = false;
      $response['statusCode'] = 400;
      $response['message'] = "No specified Report Type";

    }

    return $response;

  }
   

}

?>