<?php namespace App\Modules\dev\Job\Gateway;

use App\Modules\dev\Job\Services\ManstatServices;
use App\Modules\dev\Job\Services\DocushareServices;
use App\Modules\dev\Job\Services\GoogleTimezoneServices;
use App\Modules\dev\Job\Services\DocketAttachmentServices;
use App\Modules\dev\Job\Repository\Interfaces\JobSyncInterface as JobSyncRepository;
use App\Modules\dev\Job\Repository\Interfaces\JobShiftDetailInterface as JobShiftDetailRepository;
use App\Modules\dev\Job\Repository\Interfaces\JobHeaderInterface as JobHeaderRepository;
use App\Modules\dev\Job\Repository\Interfaces\ContactPhoneMobileEmailInterface as ContactPhoneMobileEmailRepository;
use App\Modules\dev\Job\Helper\JobHelper;

use Config;
use Carbon\Carbon;
use DB;
use Request;
use Mail;
use PDF;
use Excel;
use Input;
use File;
use DateTime;

class DocketGateway
{

  protected $manstatServices;
  protected $docushareServices;
  protected $googleTimezoneServices;
  protected $jobSyncRepository;
  protected $jobShiftDetailRepository;
  protected $jobHeaderRepository;
  protected $contactPhoneMobileEmailRepository;
  protected $docketAttachmentServices;
  protected $jobHelper;

  public function __construct(
      ManstatServices $manstatServices,
      DocushareServices $docushareServices,
      GoogleTimezoneServices $googleTimezoneServices,
      JobSyncRepository $jobSyncRepository,
      JobShiftDetailRepository $jobShiftDetailRepository,
      JobHeaderRepository $jobHeaderRepository,
      ContactPhoneMobileEmailRepository $contactPhoneMobileEmailRepository,
      DocketAttachmentServices $docketAttachmentServices,
      JobHelper $jobHelper)
  {

      $this->manstatServices = $manstatServices;
      $this->docushareServices = $docushareServices;
      $this->googleTimezoneServices = $googleTimezoneServices;
      $this->jobSyncRepository = $jobSyncRepository;
      $this->jobShiftDetailRepository = $jobShiftDetailRepository;
      $this->jobHeaderRepository = $jobHeaderRepository;
      $this->contactPhoneMobileEmailRepository = $contactPhoneMobileEmailRepository;
      $this->docketAttachmentServices = $docketAttachmentServices;
      $this->jobHelper = $jobHelper;
  }

  public function getDocketAttachments($contactId){

    $attachments = $this->manstatServices->getDocketAttachments($contactId);

    $attachmentList = [];

    if( $attachments ){

      foreach( $attachments as $key => $attachmentInfo ){

        $countJobSync = $this->jobSyncRepository->countByJobID($attachmentInfo['JobId']);
        $jobSync = Config::get('evolution.jobSyncEnable');

        if( $jobSync == false || $countJobSync == 0 ){

          $docpath = $attachments[$key]['ExternalLink'];
          $docpath = str_replace('https://', 'http://', $docpath);

          //Create App TMP Path
          $data = [
            'url' => $docpath
          ];

          $docketDocument = $this->docushareServices->get($data);

          if( !file_exists(public_path('tmp')) ){

            mkdir(public_path('tmp'));

          }

          if( !file_exists(public_path('tmp/'.$attachmentInfo['JobId'])) ){

            mkdir(public_path('tmp/'.$attachmentInfo['JobId']));

          }

          $filename = str_random(10);
          $pathInfo = pathinfo($attachments[$key]['ExternalLink']);

          $path = public_path('tmp/'.$attachmentInfo['JobId']."/".$filename.'.'.$pathInfo['extension']);

          if( file_exists($path) ){

            unlink($path);

          }

          $sitepath = url('/tmp/'.$attachmentInfo['JobId']."/".$filename.'.'.$pathInfo['extension']);

          if( file_put_contents($path, $docketDocument) ){

            $attachments[$key]['AppLink'] = $sitepath;

          }
          else{

            $attachments[$key]['AppLink'] = "";

          }

          array_push($attachmentList, $attachments[$key]);

        }

      }

    }

    if( count( $attachments ) > 0 ){

      $response['status'] = true;
      $response['statusCode'] = 200;
      $response['attachments'] = $attachmentList;

    }
    else{

      $response['status'] = false;
      $response['statusCode'] = 200;
      $response['message'] = "No Docket Attachments Found";

    }

    return $response;

  }

  public function viewDocketLink($input){

    $data = [
      'url' => $input['url']
    ];

    $docketDocument = $this->docushareServices->get($data);

    if( !file_exists(public_path('tmp')) ){

      mkdir(public_path('tmp'));

    }

    if( !file_exists(public_path('tmp/'.$input['jobID'])) ){

      mkdir(public_path('tmp/'.$input['jobID']));

    }

    $path = public_path('tmp/'.$input['jobID']."/".basename($input['url']));

    if( file_exists($path) ){

      unlink($path);

    }

    $sitepath = url('/tmp/'.$input['jobID']."/".basename($input['url']));

    if( file_put_contents($path, $docketDocument) ){

      $response['status'] = true;
      $response['statusCode'] = 200;
      $response['path'] = $sitepath;

    }
    else{

      $response['status'] = false;
      $response['statusCode'] = 422;
      $response['message'] = "Failed to open file";


    }

    return $response;

  }

  public function truncateDocketAttachments(){

    $target = public_path('tmp/');

    if( is_dir($target) ){

      $files = glob( $target . '*', GLOB_MARK );
      $errorCount = 0;

      foreach( $files as $path )
      {

        $deleteFile = $this->docketAttachmentServices->deleteFile( $path );

      }

      $response['status'] = true;
      $response['statusCode'] = 200;
      $response['message'] = "Folder successfully truncated";

      return $response;

    } else {

      $response['status'] = false;
      $response['statusCode'] = 200;
      $response['message'] = "No Folder Found";

      return $response;

    }

  }


}

?>