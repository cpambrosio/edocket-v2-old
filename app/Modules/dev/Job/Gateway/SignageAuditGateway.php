<?php namespace App\Modules\dev\Job\Gateway;

use App\Modules\dev\Job\Services\ManstatServices;
use App\Modules\dev\Job\Services\GoogleTimezoneServices;
use App\Modules\dev\Job\Repository\Interfaces\JobSyncInterface as JobSyncRepository;
use App\Modules\dev\Job\Repository\Interfaces\SignageAuditInterface as SignageAuditRepository;
use App\Modules\dev\Job\Repository\Interfaces\AuditSignsInterface as AuditSignsRepository;
use App\Modules\dev\Job\Repository\Interfaces\SignInterface as SignRepository;
use App\Modules\dev\Job\Repository\Interfaces\CarriageWayInterface as CarriageWayRepository;
use App\Modules\dev\Job\Repository\Interfaces\DirectionInterface as DirectionRepository;
use App\Modules\dev\Job\Helper\JobHelper;

use Config;
use Carbon\Carbon;
use DB;
use Request;
use Mail;
use PDF;
use Excel;
use Input;
use File;
use DateTime;

class SignageAuditGateway
{

  protected $manstatServices;
  protected $googleTimezoneServices;
  protected $jobSyncRepository;
  protected $signageAuditRepository;
  protected $auditSignsRepository;
  protected $signRepository;
  protected $carriageWayRepository;
  protected $directionRepository;
  protected $jobHelper;

  public function __construct(
      ManstatServices $manstatServices,
      GoogleTimezoneServices $googleTimezoneServices,
      JobSyncRepository $jobSyncRepository,
      SignageAuditRepository $signageAuditRepository,
      AuditSignsRepository $auditSignsRepository,
      SignRepository $signRepository,
      CarriageWayRepository $carriageWayRepository,
      DirectionRepository $directionRepository,
      JobHelper $jobHelper)
  {

      $this->manstatServices = $manstatServices;
      $this->googleTimezoneServices = $googleTimezoneServices;
      $this->jobSyncRepository = $jobSyncRepository;
      $this->signageAuditRepository = $signageAuditRepository;
      $this->auditSignsRepository = $auditSignsRepository;
      $this->signRepository = $signRepository;
      $this->carriageWayRepository = $carriageWayRepository;
      $this->directionRepository = $directionRepository;
      $this->jobHelper = $jobHelper;

  }

  public function getSignageAudit($contactId){

    $relatedJobs = $this->manstatServices->getRelatedJobs($contactId);

    if( $relatedJobs ){

      $relatedSignage = [];

      foreach( $relatedJobs as $key => $jobInfo ){

        $countJobSync = $this->jobSyncRepository->countByJobID($jobInfo['JobID']);
        $jobSync = Config::get('evolution.jobSyncEnable');

        if( $jobSync == false || $countJobSync == 0 ){

          $signageAudit = $this->signageAuditRepository->get($jobInfo['Id']);

          if( $signageAudit ){

            foreach($signageAudit as $key => $signageInfo){

              $signageInfo['AuditSigns'] = [];

              $auditSigns = $this->auditSignsRepository->get($signageInfo['AuditID']);

              if( $auditSigns ){

                array_push($signageInfo['AuditSigns'],$auditSigns);

              }

              array_push($relatedSignage,$signageInfo);

            }

          }

        }

      }

      if( count($relatedSignage) > 0 ){

        $response['status'] = true;
        $response['statusCode'] = 200;
        $response['signageAudit'] = $relatedSignage;

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 200;
        $response['message'] = "No Signage Audit Found";

      }

    }
    else{

      $response['status'] = false;
      $response['statusCode'] = 200;
      $response['message'] = "No Signage Audit Found";

    }


    return $response;

  }

  public function getSign(){

      $sign = $this->signRepository->all();

      if( $sign ){

        $response['status'] = true;
        $response['statusCode'] = 200;
        $response['signs'] = $sign;

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 200;
        $response['message'] = "No Signs Found";

      }

      return $response;

  }

  public function getCarriageWay(){

      $carriageWay = $this->carriageWayRepository->all();

      if( $carriageWay ){

        $response['status'] = true;
        $response['statusCode'] = 200;
        $response['carriageWay'] = $carriageWay;

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 200;
        $response['message'] = "No Carriage Way Found";

      }

      return $response;

  }

  public function getDirection(){

      $direction = $this->directionRepository->all();

      if( $direction ){

        $response['status'] = true;
        $response['statusCode'] = 200;
        $response['directions'] = $direction;

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 200;
        $response['message'] = "No Directions Found";

      }

      return $response;

  }

}

?>