<?php namespace App\Modules\dev\Job\Gateway;

use App\Modules\dev\Job\Services\ManstatServices;
use App\Modules\dev\Job\Services\GoogleTimezoneServices;
use App\Modules\dev\Job\Repository\Interfaces\VideoInterface as VideoRepository;
use App\Modules\dev\Job\Services\EmailServices;
use App\Modules\dev\Job\Services\FileServices;
use App\Modules\dev\Job\Helper\JobHelper;

use Config;
use Carbon\Carbon;
use DB;
use Request;
use Mail;
use PDF;
use Excel;
use File;
use DateTime;

class VideoGateway
{

  protected $manstatServices;
  protected $googleTimezoneServices;
  protected $videoRepository;
  protected $emailServices;
  protected $fileServices;
  protected $jobHelper;

  public function __construct(
      ManstatServices $manstatServices,
      GoogleTimezoneServices $googleTimezoneServices,
      VideoRepository $videoRepository,
      EmailServices $emailServices,
      FileServices $fileServices,
      JobHelper $jobHelper)
  {

      $this->manstatServices = $manstatServices;
      $this->googleTimezoneServices = $googleTimezoneServices;
      $this->videoRepository = $videoRepository;
      $this->emailServices = $emailServices;
      $this->fileServices = $fileServices;
      $this->jobHelper = $jobHelper;

  }

  public function uploadVideo($input){

    $timezone = $this->googleTimezoneServices->setTimezone($input['startLocation']);

    if( !$timezone ){

      $timezone = Config::get('evolution.defaultTimezone');

      if( isset($timezone) && $timezone != '' ){

        date_default_timezone_set($timezone);

      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 422;
        $response['message'] = "Failed to set Timezone";

        return $response;

      }

    }

    //Check if uploading in watch folder is enabled
    $enableWatchFolder = Config::get('evolution.watchFolderEnable');

    if( Request::hasFile('video') ){

      $extension = strtolower( Request::file('video')->getClientOriginalExtension() );

      $videoExtensions = Config::get('evolution.videoExtension');

      $filename = "vid_".md5(time().uniqid());
      $filename_path = $filename.".".$extension;
      $videoFileSize = $input['video']->getSize();

      $video = [
        'Video' => $filename_path,
        'Subject' => $input['subject'],
        'Note' => $input['note'],
        'StartLocation' => $input['startLocation'],
        'EndLocation' => $input['endLocation'],
        'StartCapture' => date('Y-m-d H:i:s',$input['startCapture'] / 1000),
        'EndCapture' => date('Y-m-d H:i:s',$input['endCapture'] / 1000),
        'Processed' => 0,
        'FileSize' => round($videoFileSize,-3),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ];

      $saveVideo = $this->videoRepository->insert($video);

      if( $saveVideo ){ 

        $videoId = $saveVideo->id;
        $originalName = $input['video']->getClientOriginalName();
        $mime = $input['video']->getMimeType();

        Request::file('video')->move(public_path("video/"), $filename_path);

        $startLocation = str_replace(' ', '', $input['startLocation']);
        $endLocation = str_replace(' ','',$input['endLocation']);

        $saveVideo = false;

        $uploadFileSize = $videoFileSize / 1000000;

        $attachment = [];
        $link = "";

        //Get video attachment size limit in config
        $videoAttachmentLimit = ( Config::get('evolution.videoAttachmentLimit') > 0 )? Config::get('evolution.videoAttachmentLimit') : 0 ;

        // Check video size limit for attachment
        if( $uploadFileSize <= $videoAttachmentLimit ){

          $attachment = [
            'path' => public_path('video/'.$filename_path),
            'name' => $filename_path,
            'mime' => $mime
          ];

        }
        else{ 

          //If attachment is greater than set upload attachment limit, send as a link

          $uploadToWatchFolder = false;

          if( $enableWatchFolder ){

            $data = [
              'username' => Config::get('evolution.watchFolderUsername'),
              'password' => Config::get('evolution.watchFolderPassword'),
              'server' => Config::get('evolution.watchFolderHost'),
              'filepath' => public_path("video/"),
              'foldername' => 'APPVIDEO',
              'old_filename' => $filename_path,
              'new_filename' => $filename_path
            ];

            $uploadToWatchFolder = $this->fileServices->uploadToWatchFolder($data, true);

          }

          if( $uploadToWatchFolder ){

            $videoData = array(
              'subject' => $input['subject'],
              'formId' => 'Video',
              'note' => $input['note'],
              'fileName' => $filename_path,
              'startLocation' => $input['startLocation'],
              'endLocation' => $input['endLocation'],
              'startCapture' => date('Y-m-d H:i:s',$input['startCapture'] / 1000),
              'endCapture' => date('Y-m-d H:i:s',$input['endCapture'] / 1000),
            );


            $csvInfo['videoData'][] = $videoData;

            Excel::create($filename.'_VID', function ($excel) use ($csvInfo) {

              // Set the title
              $excel->setTitle('Video CSV');

              // // Chain the setters
              $excel->setCreator('Evolution')
                    ->setCompany('Evolution');

              // // Call them separately
              $excel->setDescription('Video CSV');

              // Set sheets
              $excel->sheet('Video CSV', function($sheet) use ($csvInfo) {

                  $sheet->loadView('forms.v2.appvideocsv',$csvInfo);

                  $sheet->freezeFirstRow();

              });

            })->store('csv', public_path('generated files'), true);

            $uploadToWatchFolder = false;

            if( $enableWatchFolder ){

              $data = [
                'username' => Config::get('evolution.watchFolderUsername'),
                'password' => Config::get('evolution.watchFolderPassword'),
                'server' => Config::get('evolution.watchFolderHost'),
                'filepath' => Config::get('evolution.generatedFilePath'),
                'foldername' => 'APPVIDEO',
                'old_filename' => $filename.'_VID.csv',
                'new_filename' => $filename.'_VID.csv'
              ];

              $uploadToWatchFolder = $this->fileServices->uploadToWatchFolder($data);

              if( !$uploadToWatchFolder ){

                $response['status'] = false;
                $response['statusCode'] = 422;
                $response['message'] = "Video unsuccessfully uploaded";

                return $response;

              }

            }


          }
          else{

            $response['status'] = false;
            $response['statusCode'] = 422;
            $response['message'] = "Video unsuccessfully uploaded";

            return $response;

          }


        }

        $startData = [
          'geotag' => $input['startLocation']
        ];

        $startAddress = $this->googleTimezoneServices->getAddress($startData);

        $email = Config::get('evolution.evolutionEmail');

        if( $email ){

          $emailJobData = array(
            'email' => $email,
            'subject' => $input['subject'],
            'note' => $input['note'],
            'video' => $filename_path,
            'startLocation' => $input['startLocation'],
            'endLocation' => $input['endLocation'],
            'startCapture' => date('Y-m-d H:i:s',$input['startCapture'] / 1000),
            'endCapture' => date('Y-m-d H:i:s',$input['endCapture'] / 1000),
          );

          $emailJobData['attachment'] = [];

          if( count( $attachment ) > 0 ){

            $emailJobData['attachment'][] = $attachment;

          }

          $emailJobData['link'] = "";

          if( $link != "" ){

            $emailJobData['link'] = $link;

          }

          $sendEmail = $this->emailServices->send(5,$emailJobData);

          if( !$sendEmail ){

            $response['status'] = false;
            $response['statusCode'] = 422;
            $response['message'] = "Video unsuccessfully uploaded";

            return $response;

          }

        }

        $videoData = [
          'Processed' => 1
        ];

        $updateVideo = $this->videoRepository->updateByVideoID($videoData,$videoId);

        if( $updateVideo ){

          $response['status'] = true;
          $response['statusCode'] = 200;
          $response['message'] = "Video was successfully uploaded";

        }
        else{

          $response['status'] = false;
          $response['statusCode'] = 422;
          $response['message'] = "Video unsuccessfully uploaded";

        }

        return $response;


      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 422;
        $response['message'] = "Video unsuccessfully uploaded";

      }

    }
    else{

      $response['status'] = false;
      $response['statusCode'] = 400;
      $response['message'] = "Video is required";

    }

    return $response;

  }

}

?>