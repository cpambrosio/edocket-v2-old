<?php namespace App\Modules\dev\Job\Gateway;

use App\Modules\dev\Job\Services\ManstatServices;
use App\Modules\dev\Job\Services\GoogleTimezoneServices;
use App\Modules\dev\Job\Repository\Interfaces\JobSyncInterface as JobSyncRepository;
use App\Modules\dev\Job\Repository\Interfaces\TCAttachmentInterface as TCAttachmentRepository;
use App\Modules\dev\Job\Helper\JobHelper;

use Config;
use Carbon\Carbon;
use DB;
use Request;
use Mail;
use PDF;
use Excel;
use Input;
use File;
use DateTime;

class TcAttachmentGateway
{

    protected $manstatServices;
    protected $googleTimezoneServices;
    protected $jobSyncRepository;
    protected $tcAttachmentRepository;
    protected $jobHelper;

    public function __construct(
        ManstatServices $manstatServices,
        GoogleTimezoneServices $googleTimezoneServices,
        JobSyncRepository $jobSyncRepository,
        TCAttachmentRepository $tcAttachmentRepository,
        JobHelper $jobHelper)
    {

        $this->manstatServices = $manstatServices;
        $this->googleTimezoneServices = $googleTimezoneServices;
        $this->jobSyncRepository = $jobSyncRepository;
        $this->tcAttachmentRepository = $tcAttachmentRepository;
        $this->jobHelper = $jobHelper;

    }

  public function getTCAttachment($contactId){

    $relatedJobs = $this->manstatServices->getRelatedJobs($contactId);

    if( $relatedJobs ){

      $relatedAttachment = [];

      foreach( $relatedJobs as $key => $jobInfo ){

        $countJobSync = $this->jobSyncRepository->countByJobID($jobInfo['JobID']);
        $jobSync = Config::get('evolution.jobSyncEnable');

        if( $jobSync == false || $countJobSync == 0 ){

          $tcAttachment = $this->tcAttachmentRepository->get($jobInfo['Id']);

          if( $tcAttachment ){

            foreach($tcAttachment as $key => $tcInfo){

              array_push($relatedAttachment,$tcInfo);

            }

          }

        }

      }

      if( count($relatedAttachment) > 0 ){

        $response['status'] = true;
        $response['statusCode'] = 200;
        $response['tcAttachment'] = $relatedAttachment;


      }
      else{

        $response['status'] = false;
        $response['statusCode'] = 200;
        $response['message'] = "No TC Attachment Found";

      }

    }
    else{

      $response['status'] = false;
      $response['statusCode'] = 200;
      $response['message'] = "No TC Attachment Found";

    }

    return $response;

  }

  public function saveTCAttachment($input){

    $data = [
      "JobID" => $input['jobID'],
      "TCName" => $input['tcName'],
      "AttachedOn" => Carbon::now(),
      "Attachment" => $input['attachment'],
      "created_at" => Carbon::now(),
      "updated_at" => Carbon::now()
    ];

    $saveTCAttachment = $this->tcAttachmentRepository->insert($data);

    if( $saveTCAttachment ){

      $response['status'] = true;
      $response['statusCode'] = 200;
      $response['message'] = "Signature successfully save";

    }
    else{

      $response['status'] = false;
      $response['statusCode'] = 422;
      $response['message'] = "Signature unsuccessfully save";

    }

    return $response;

  }

}

?>