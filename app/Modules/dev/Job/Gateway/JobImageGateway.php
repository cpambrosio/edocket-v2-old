<?php namespace App\Modules\dev\Job\Gateway;

use App\Modules\dev\Job\Services\ManstatServices;
use App\Modules\dev\Job\Services\GoogleTimezoneServices;
use App\Modules\dev\Job\Repository\Interfaces\JobSyncInterface as JobSyncRepository;
use App\Modules\dev\Job\Repository\Interfaces\JobShiftDetailInterface as JobShiftDetailRepository;
use App\Modules\dev\Job\Repository\Interfaces\JobHeaderInterface as JobHeaderRepository;
use App\Modules\dev\Job\Repository\Interfaces\ContactPhoneMobileEmailInterface as ContactPhoneMobileEmailRepository;
use App\Modules\dev\Job\Repository\Interfaces\JobImageInterface as JobImageRepository;
use App\Modules\dev\Job\Helper\JobHelper;

use Config;
use Carbon\Carbon;
use DB;
use Request;
use Mail;
use PDF;
use Excel;
use File;
use DateTime;

class JobImageGateway
{

    protected $manstatServices;
    protected $googleTimezoneServices;
    protected $jobSyncRepository;
    protected $jobShiftDetailRepository;
    protected $jobHeaderRepository;
    protected $contactPhoneMobileEmailRepository;
    protected $jobImageRepository;
    protected $jobHelper;

    public function __construct(
        ManstatServices $manstatServices,
        GoogleTimezoneServices $googleTimezoneServices,
        JobSyncRepository $jobSyncRepository,
        JobShiftDetailRepository $jobShiftDetailRepository,
        JobHeaderRepository $jobHeaderRepository,
        ContactPhoneMobileEmailRepository $contactPhoneMobileEmailRepository,
        JobImageRepository $jobImageRepository,
        JobHelper $jobHelper)
    {

        $this->manstatServices = $manstatServices;
        $this->googleTimezoneServices = $googleTimezoneServices;
        $this->jobSyncRepository = $jobSyncRepository;
        $this->jobShiftDetailRepository = $jobShiftDetailRepository;
        $this->jobHeaderRepository = $jobHeaderRepository;
        $this->contactPhoneMobileEmailRepository = $contactPhoneMobileEmailRepository;
        $this->jobImageRepository = $jobImageRepository;
        $this->jobHelper = $jobHelper;
    }

    public function uploadJobImage($input){

        if( Request::hasFile('image') ){

          $extension = strtolower( Request::file('image')->getClientOriginalExtension() );

          $imageExtensions = Config::get('evolution.imageExtension');

          //Set default list of image extension
          if( count( $imageExtensions ) == 0 ){

            $imageExtensions = ['png','jpg'];

          }

          if( in_array($extension, $imageExtensions) ){

            $filename_path = $input['jobId']."_".md5(time().uniqid()).".".$extension;
            Request::file('image')->move(public_path("image/job/"), $filename_path);

            $jobImage = [
              'jobID' => $input['jobId'],
              'JobImage' => $filename_path,
              'GeoTag' => $input['geotag'],
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
            ];

            $saveJobImage = $this->jobImageRepository->insert($jobImage);

            if( $saveJobImage ){

              $response['status'] = true;
              $response['statusCode'] = 200;
              $response['message'] = "Image was successfully uploaded";

            }
            else{

              $response['status'] = false;
              $response['statusCode'] = 422;
              $response['message'] = "Image unsuccessfully save";

            }

          }
          else{

            $response['status'] = false;
            $response['statusCode'] = 400;
            $response['message'] = "Invalid image file";

          }

        }
        else{

          $response['status'] = false;
          $response['statusCode'] = 400;
          $response['message'] = "Image is required";

        }

        return $response;

    }



}