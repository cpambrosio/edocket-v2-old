<?php namespace App\Modules\dev\Job\Gateway;

use App\Modules\dev\Job\Services\ManstatServices;
use App\Modules\dev\Job\Services\GoogleTimezoneServices;
use App\Modules\dev\Job\Repository\Interfaces\JobSyncInterface as JobSyncRepository;
use App\Modules\dev\Job\Helper\JobHelper;

use Config;
use Carbon\Carbon;
use DB;
use Request;
use Mail;
use PDF;
use Excel;
use Input;
use File;
use DateTime;

class SiteChecklistGateway
{

    protected $manstatServices;
    protected $googleTimezoneServices;
    protected $jobSyncRepository;
    protected $jobHelper;

    public function __construct(
        ManstatServices $manstatServices,
        GoogleTimezoneServices $googleTimezoneServices,
        JobSyncRepository $jobSyncRepository,
        JobHelper $jobHelper)
    {

        $this->manstatServices = $manstatServices;
        $this->googleTimezoneServices = $googleTimezoneServices;
        $this->jobSyncRepository = $jobSyncRepository;
        $this->jobHelper = $jobHelper;

    }

    public function getSiteCheckList($contactId){

      $siteChecklist = $this->manstatServices->getSiteCheckList($contactId);

      if( $siteChecklist ){

      $response['status'] = true;
      $response['statusCode'] = 200;
      $response['siteChecklist'] = $siteChecklist;

      }
      else{

      $response['status'] = false;
      $response['statusCode'] = 200;
      $response['message'] = "No Site Checklist Found";

      }

      return $response;

    }

    public function checkSiteChecklist($input,$contactId){

      $jobSiteCheckListItems = [];
      $siteCheckListInputValues = [];

      //Add PO Number
      $input['siteCheckList'] .= '~Text:PO Number^'.$input['orderNumber'];

      $siteChecklist = $this->manstatServices->getSiteCheckList($contactId);

      if( !$siteChecklist ){

      $response['status'] = false;
      $response['statusCode'] = 500;
      $response['message'] = "Unable to connect to ManStat. Please try again.";

      return $response;

      }

      $siteChecklistSections = $this->manstatServices->getSiteCheckListSections($contactId);

      if( !$siteChecklistSections ){

      $response['status'] = false;
      $response['statusCode'] = 500;
      $response['message'] = "Unable to connect to ManStat. Please try again.";

      return $response;

      }

      $siteChecklistInput = $input['siteCheckList'];

      $questionList = explode('~', $siteChecklistInput);

      //Check Question
      // $sample = $questionList;

      $error_value_type_count = 0;
      $error_question_count = 0;
      $error_answer_count = 0;

      $questionRemaining = count($siteChecklist);

      foreach( $questionList as $q => $questionInfo ){

        $itemID = "";
        $itemValue = "";
        $valueType = substr($questionInfo,0,strrpos($questionInfo,':'));
        $question = $this->extract_unit($questionInfo,':','^');
        $answerlist = substr($questionInfo, strrpos($questionInfo, '^') + 1);

        if( $question == "" ){

          $error_question_count++;
        }

        foreach( $siteChecklist as $siteCheckListInfo ){

          if( $siteCheckListInfo['Description'] == $question ){

            //Check Question
            // unset($sample[$q]);

            $questionRemaining--;

            $itemID = $siteCheckListInfo['Id'];

            if( in_array($valueType, array('Multi_Select','Multiple_Select','Single_Select','Simple_Boolean')) ){

              foreach( explode('|', $answerlist) as $answerInfo ){

                foreach( $siteCheckListInfo['CheckListItemOptions'] as $itemOption ){

                  if( $itemOption['Value'] == $answerInfo ){

                    $itemValue = $itemOption['Id'];

                  }

                }


                if( $itemValue == "" ){

                  $error_answer_count++;
                }

                $checklistData = array(
                  "jobID" => $input['jobID'],
                  "itemID" => $itemID,
                  "itemValueID" => $itemValue
                );

                array_push($jobSiteCheckListItems,$checklistData);


                // Create report data for site checklist
                $reportSection = "";
                $reportSectionID = "";

                foreach( $siteChecklistSections as $sectionKey => $sectionValue ){

                  if( $sectionValue['Id'] == $siteCheckListInfo['SectionID'] ){

                    $reportSection = $sectionValue['Name'];
                    $reportSectionID = $sectionValue['Id'];

                  }

                }

                if(!isset($siteChecklistReportData[$reportSection])){

                  $siteChecklistReportData[$reportSection] = [];

                }

                $section_in = str_replace(" ", "_", $reportSection);
                $question_in = str_replace(" ", "_", $question);

                $siteChecklistReportData[$section_in][$question_in] = $answerInfo;

              }

            }
            elseif( in_array($valueType, array('Numeric','Text')) ){

              $itemValue = $answerlist;

              //Generate UUID for Site Checklist Input Value
              $uuid = $this->uuidv4();

              $checklistData = array(
                "jobID" => $input['jobID'],
                "itemID" => $itemID,
                "itemValueID" => $uuid
              );

              array_push($jobSiteCheckListItems,$checklistData);

              $checklistData = array(
                "Id" => $uuid,
                "Value" => $itemValue
              );

              array_push($siteCheckListInputValues,$checklistData);

                // Create report data for site checklist
                $reportSection = "";
                $reportSectionID = "";

                foreach( $siteChecklistSections as $sectionKey => $sectionValue ){

                  if( $sectionValue['Id'] == $siteCheckListInfo['SectionID'] ){

                    $reportSection = $sectionValue['Name'];
                    $reportSectionID = $sectionValue['Id'];

                  }

                }

                if(!isset($siteChecklistReportData[$reportSection])){

                  $siteChecklistReportData[$reportSection] = [];

                }

                $section_in = str_replace(" ", "_", $reportSection);
                $question_in = str_replace(" ", "_", $question);

                $siteChecklistReportData[$section_in][$question_in] = $answerInfo;


            }
            else{

              $error_value_type_count++;

            }

          }


        }

      }


      if( $error_answer_count > 0 ){

        $response['status'] = false;
        $response['message'] = "Some answers in site checklist is either missing or not within the available selection";

        return $response;

      }

      if( $error_question_count > 0 || $questionRemaining > 0 ){

        $response['status'] = false;
        $response['message'] = "Some questions in site checklist is either missing or not within the available selection";

        return $response;

      }

      if( $error_value_type_count > 0 ){

        $response['status'] = false;
        $response['message'] = "Some value type in site checklist is either missing or not within the available selection";

        return $response;

      }

      $response['siteChecklistSections'] = $siteChecklistSections;
      $response['siteChecklist'] = $siteChecklist;
      $response['siteChecklistReportData'] = $siteChecklistReportData;
      $response['jobSiteCheckListItems'] = $jobSiteCheckListItems;
      $response['siteCheckListInputValues'] = $siteCheckListInputValues;
      $response['status'] = true;

      return $response;


    }

    public function extract_unit($string, $start, $end){

      $pos = stripos($string, $start);
       
      $str = substr($string, $pos);
       
      $str_two = substr($str, strlen($start));
       
      $second_pos = stripos($str_two, $end);
       
      $str_three = substr($str_two, 0, $second_pos);
       
      $unit = trim($str_three); // remove whitespaces
       
      return $unit;
    }

    public function uuidv4()
    {

      $data = openssl_random_pseudo_bytes(16);

        assert(strlen($data) == 16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return strtoupper(vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4)));
    }

}

?>