<?php namespace App\Modules\dev\Job\Services;

use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;

use Config;
use Carbon\Carbon;
use Hash;

class DocushareServices
{

	protected $client;

	function __construct(){

		$this->client = new \GuzzleHttp\Client(['cookies' => true]);
		$this->client->setDefaultOption('verify', storage_path('certificates\cacert.pem'));

	}

	function login(){

		$username = Config::get('evolution.docushareUser');
		$password = Config::get('evolution.docusharePassword');
		$domain = Config::get('evolution.docushareDomain');

		$data = [
		  'username' => $username,
		  'password' => $password,
		  'domain' => $domain
		];

		$loginToken = $this->getDocushareApi(1,$data);

		return $loginToken;

	}

	function upload($uploadType, $data){

		$loginToken = $this->login();

		$request = [
		  'file' => $data['file'],
		  'title' => $data['title'],
		  'cookie' => $loginToken
		];

		switch($uploadType){
		  case 1: // Job Docket
		    
		    $request['collection'] = 'Collection-10115';

		  break;
		  case 2:


		  break;
		  default:

		  break;
		}

		$upload = $this->getDocushareApi(2,$request);

		return $upload;

	}

	function search($searchType, $title = ""){

		$loginToken = $this->login();

		switch($searchType){
		  case 1: // Search All Job

		    $request = [
		      'title' => $title,
		      'cookie' => $loginToken,
		      'collection' => 'Collection-10115',
		      'condition' => []
		    ];

		  break;
		  case 2: // Search Specific Job

		    $request = [
		      'title' => $tite,
		      'cookie' => $loginToken,
		      'collection' => 'Collection-10115',
		      'condition' => [
		        'displayname' => $title
		      ]
		    ];

		  break;
		  default:


		  break;
		}

		$search = $this->getDocushareApi(3,$request);

	}

	function get($data){

		$loginToken = $this->login();

		$request = [
		  'url' => $data['url'],
		  'cookie' => $loginToken
		];

		$search = $this->getDocushareApi(4,$request);

		return $search;

	}

	function getDocushareApi($serviceType, $data){

		switch($serviceType){
		  case 1: // Login

		    $url = 'http://docushare.ermg.com.au/docushare/dsweb/login';

		    $headers = [
		      'User-Agent' => 'DsAxess/4.0',
		      'Host' => 'docushare.ermg.com.au',
		      'Accept-Language' => 'en',
		      'Content-Type' => 'text/xml',
		      'Accept' => 'text/xml',
		      'DocuShare-Version' => '5.0',
		      'Content-Length' => '182'
		    ];

		    $body = '<?xml version="1.0" ?>
		    <authorization>
		     <username><![CDATA['.$data['username'].']]></username>
		     <password><![CDATA['.$data['password'].']]></password>
		    <domain><![CDATA['.$data['domain'].']]></domain>
		    </authorization>';


		  break;
		  case 2: // Upload to Docushare

		    $url = 'http://docushare.ermg.com.au/docushare/dsweb/APPLYUPLOAD/'.$data['collection'];

		    $headers = [
		      'User-Agent' => 'DsAxess/4.0',
		      'Host' => 'docushare.ermg.com.au',
		      'Accept-Language' => 'en',
		      'Content-Type' => 'multipart/form-data',
		      'Accept' => '*/*, text/xml',
		      'DocuShare-Version' => '5.0',
		      'Cookie' => $data['cookie']
		    ];

		    $body = [
		      'parent' => $data['collection'],
		      'title' => $data['title'],
		      'file1' => fopen($data['file'], 'r')
		    ];

		  break;
		  case 3: // Search

		    $url = 'http://docushare.ermg.com.au/docushare/dsweb/SEARCH';

		    $headers = [
		      'User-Agent' => 'DsAxess/4.0',
		      'Host' => 'docushare.ermg.com.au',
		      'Accept-Language' => 'en',
		      'Content-Type' => 'text/xml',
		      'Accept' => '*/*, text/html',
		      'DocuShare-Version' => '5.0',
		      'Cookie' => $data['cookie']
		    ];

		    $body = '<?xml version="1.0" ?>
		      <searchrequest>
		       <simplesearch>
		         <select><prop>
		            <entityowner/>
		            <creationdate/>
		            <displayname/>
		            <summary/>
		            <parents/>
		            <getcontenttype/>
		         </prop></select>
		         <from><scope><href>http://docushare.ermg.com.au/docushare/dsweb/'.$data['collection'].'</href></scope></from>
		         <where>';

		    if(!empty($data['conditions'])){

		      $body .= '<and>';

		      foreach($data['conditions'] as $field => $value ){

		        $body .= '
		         <contains>
		             <prop><'.$field.'/></prop>
		             <literal>'.$value.'</literal>
		         </contains>';

		      }

		      $body .= '</and>';

		    }

		    $body .= '</where>
		       </simplesearch>
		      </searchrequest>';

		  break;
		  case 4: // Get Document

		    $url = $data['url'];

		    $headers = [
		      'User-Agent' => 'DsAxess/4.0',
		      'Host' => 'docushare.ermg.com.au',
		      'Accept-Language' => 'en',
		      'Content-Type' => 'text/xml',
		      'Accept' => '*/*, text/html',
		      'DocuShare-Version' => '5.0',
		      'Authorization' => 'Basic TWFuc3RhdElTOlBhc3M0TUlTIQ==',
		      'Cookie' => $data['cookie']
		    ];

		    $body = '';


		  break;
		  default:


		  break;
		}


		try{

		    switch($serviceType){
		      case 1: // Login

		        $res = $this->client->post($url, [
		          'headers' => $headers,
		          'body' => $body
		        ]);

		        $return = $res->getHeader('Set-Cookie');

		      break;
		      case 2: // Upload

		        $return = true;

		      break;
		      case 3: // Search

		        $res = $this->client->post($url, [
		          'headers' => $headers,
		          'body' => $body
		        ]);

		        $return = $res->xml();

		      break;
		      case 4: // Get

		        $res = $this->client->get($url, [
		          'headers' => $headers,
		          'body' => $body
		        ]);

		        $return = $res->getBody()->getContents();


		      break;
		      default:


		      break;
		    }

		    return $return;

		}
		catch(ResponseInterface $e){


		  if ($e->getResponse()->getStatusCode() == '400') {
		    return false;
		  }

		  if ($e->getResponse()->getStatusCode() == '404') {
		    return false;

		  }

		  if ($e->getResponse()->getStatusCode() == '401') {
		    return false;

		  }

		  if ($e->getResponse()->getStatusCode() == '500') {
		    return false;
		  }

		}



	}


}


?>