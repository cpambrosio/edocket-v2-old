<?php namespace EGM\Modules\dev\Job\Services;

use EGM\Modules\dev\Job\Repository\Interfaces\VideoInterface as VideoRepository;

use Config;
use Carbon\Carbon;
use Hash;

class VideoServices
{

	protected $videoRepository;

	function __construct(
		VideoRepository $videoRepository){

		$this->videoRepository = $videoRepository;

	}


	public function insert($data){

		return $this->videoRepository->insert($data);

	}

	public function updateByVideoID($data,$videoId){

		return $this->videoRepository->updateByVideoID($data,$videoId);

	}


}


?>