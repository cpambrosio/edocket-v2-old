<?php namespace App\Modules\dev\Job\Services;

use Config;
use Carbon\Carbon;
use Hash;

class DocketAttachmentServices
{

	function __construct(){

	}


	public function deleteDocketAttachments($jobId){

		$path = public_path('tmp/'.$jobId."/");

		$deleteFile = $this->deleteFile( $path );

		return $deleteFile;

	}

	public function deleteFile($target) {

		if( is_dir($target) ){

		  $files = glob( $target . '*', GLOB_MARK );

		  foreach( $files as $file )
		  {

		      $deleteFile = $this->deleteFile( $file );

		      if( !$deleteFile ){

		        return false;

		      }

		  }

		  if( rmdir( $target ) ){

		    return true;

		  }
		  else{

		    return false;

		  }

		} elseif( is_file($target) ) {

		  if( unlink( $target ) ){

		    return true;

		  }
		  else{

		    return false;

		  }

		}

	}


}


?>