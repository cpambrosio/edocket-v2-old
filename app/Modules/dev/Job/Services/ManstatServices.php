<?php namespace App\Modules\dev\Job\Services;

use App\Modules\dev\Job\Helper\JobHelper;

use Config;
use Carbon\Carbon;
use Hash;

class ManstatServices
{

	protected $client;
	protected $activationCode;
	protected $jobHelper;

	function __construct(JobHelper $jobHelper){

		$this->client = new \GuzzleHttp\Client();
		$this->client->setDefaultOption('verify', storage_path('certificates\cacert.pem'));
		
		$this->activationCode = $this->setActivation();

		$this->jobHelper = $jobHelper;

	}

	/**
     * Get the user id of the provided contact code.
     *
     * @param etccode The contact code of the employee
     *
     * @return user id
     */
	public function getUserID($etccode){

		$data['code'] = $etccode;

		$response = $this->getManstatApi(1, $data);

		if( $response['GetUserIDResult'] >= 1 ){
		  return $response['GetUserIDResult'];
		}
		else{
		  return false;
		}

	}

	/**
     * Count users base on contact code.
     *
     * @param etccode The contact code of the employee
     *
     * @return total users
     */
	public function countUserByCode($etccode){

	    $data['code'] = $etccode;

	    $response = $this->getManstatApi(1, $data);

	    if( !$response ){

	      return false;

	    }
	    else{

	      if( $response['GetUserIDResult'] >= 1 ){
	        return $response['GetUserIDResult'];
	      }
	      else{
	        return 0;
	      }

	    }

	}

	/**
     * Count users base on contact code.
     *
     * @param etccode The contact code of the employee
     *
     * @return total users
     */
	public function setActivation(){

		$data['activationCode'] = Config::get('evolution.activationCode');

		$response = $this->getManstatApi(4, $data);

		$activation['communicationInfo'] = [
			'DataConnectionInfo' => $response['CheckActivationResult']['DataConnectionInfo'],
			'ClientDeviceInfo' => [
					'TimeZoneOffset' => '1',
					'Model' => '',
					'Version' => '',
					'OperatingSystem' => '',
					'RegistrationCode' => ''
			]
		];

		$activation['ServiceAccessUser'] = Config::get('evolution.activation_user');
		$activation['ServiceAccessPassword'] = $response['CheckActivationResult']['ServiceAccessPassword'];
		$activation['ServiceAddress'] = $response['CheckActivationResult']['ServiceAddress'];

		return $activation;

	}


	public function getRelatedJobs($contactId){

    $data['code'] = $contactId;

    $response = $this->getManstatApi(2, $data);

    if(count($response['Jobs']) == 0){

      return false;

    }


    $jobs = $response['Jobs'];
    $contacts = $response['Contacts'];
    $depots = $response['Depots'];
    $locations = $response['Locations'];
    $relatedJob = [];

    $jobIds = [];

    foreach( $jobs as $key => $jobInfo ){

      if( in_array($jobInfo['Id'], $jobIds) ){
        unset($jobs[$key]);
        continue;
      }
      else{
        array_push($jobIds, $jobInfo['Id']);
      }

      $jobs[$key]['JobID'] = $jobInfo['Id'];
      $jobs[$key]['JobCode'] = $jobInfo['Code'];
      $jobs[$key]['ContactPerson'] = '';
      $jobs[$key]['AuthorizedPerson'] = '';
      $jobs[$key]['Address'] = '';
      $jobs[$key]['Depot'] = '';
      $jobs[$key]['CompanyName'] = '';
      $jobs[$key]['SplitParentJob'] = '';

      $jobs[$key]['StartDateTime'] = str_replace('/Date(', '', $jobs[$key]['StartDateTime']);
      $jobs[$key]['StartDateTime'] = str_replace(')/', '', $jobs[$key]['StartDateTime']);
      $jobs[$key]['StartDateTimeFormat'] = date('d-F-Y H:i:s',round( $jobs[$key]['StartDateTime'] / 1000));

      $jobs[$key]['EndDateTime'] = str_replace('/Date(', '', $jobs[$key]['EndDateTime']);
      $jobs[$key]['EndDateTime'] = str_replace(')/', '', $jobs[$key]['EndDateTime']);
      $jobs[$key]['EndDateTimeFormat'] = date('d-F-Y H:i:s',round( $jobs[$key]['EndDateTime'] / 1000));


      foreach( $contacts as $contactInfo ){

        if( $jobInfo['ForemanID'] == $contactInfo['Id'] ){

          $jobs[$key]['ContactPerson'] = $contactInfo['Name'];

        }

        if( $jobInfo['AuthorizerID'] == $contactInfo['Id'] ){

          $jobs[$key]['AuthorizedPerson'] = $contactInfo['Name'];

        }

        if( $jobInfo['ContactID'] == $contactInfo['Id'] ){

          $jobs[$key]['CompanyName'] = $contactInfo['Name'];

        }

      }

      foreach( $depots as $depotInfo ){

        if( $jobInfo['DepotID'] == $depotInfo['Id'] ){

          $jobs[$key]['Depot'] = $depotInfo['Description'];

        }

      }

      foreach( $locations as $locationInfo ){

        if( $jobInfo['Id'] == $locationInfo['EntityID'] ){

          $jobs[$key]['Address'] = $locationInfo['AddressLine'].' '.$locationInfo['Suburb'].' '.$locationInfo['State'];
          $jobs[$key]['State'] = $locationInfo['State'];

        }

      }

      array_push($relatedJob,$jobs[$key]);

    }

    return $relatedJob;

  }

   public function getDocketAttachments($contactId){

    $activation['databaseParams'] = [
      'SyncLevel' => 2,
      'UserID' => $contactId
    ];

    $data['code'] = $contactId;
    $response = $this->getManstatApi(2, $data);

    // print_r($response);
    // die();

    if(count($response['Jobs']) == 0){

      return false;

    }

    $docketAttachments = $response['DocketExternalLinks'];
    $attachments = [];

    $count = 1;

    foreach( $docketAttachments as $key => $attachmentInfo ){

        $docketAttachments[$key]["RowID"] = $count;

        array_push($attachments, $docketAttachments[$key]);

        $count++;

    }


    return $attachments;

  }

  public function getTimesheetEmployees($contactId){

    $activation['databaseParams'] = [
      'SyncLevel' => 2,
      'UserID' => $contactId
    ];

    $data['code'] = $contactId;
    $response = $this->getManstatApi(2, $data);

    // print_r($response);
    // die();

    if(count($response['Jobs']) == 0){

      return false;

    }

    $shifts = $response['Shifts'];
    $contacts = $response['Contacts'];
    $locations = $response['Locations'];
    $startTypes = $response['StartTypes'];
    $timesheet = [];

    $count = 1;

    foreach( $shifts as $key => $shiftInfo ){



      $shifts[$key]['UTCStartDateTime'] = str_replace('/Date(', '', $shifts[$key]['StartDateTime']);
      $shifts[$key]['UTCStartDateTime'] = str_replace(')/', '', $shifts[$key]['UTCStartDateTime']);
      $shifts[$key]['UTCStartDateTimeFormat'] = gmdate('d-F-Y H:i:s',round($shifts[$key]['UTCStartDateTime']  / 1000));

      $shifts[$key]['UTCEndDateTime'] = str_replace('/Date(', '', $shifts[$key]['EndDateTime']);
      $shifts[$key]['UTCEndDateTime'] = str_replace(')/', '', $shifts[$key]['UTCEndDateTime']);
      $shifts[$key]['UTCEndDateTimeFormat'] = gmdate('d-F-Y H:i:s',round($shifts[$key]['UTCEndDateTime'] / 1000));

      $shifts[$key]['StartDateTime'] = str_replace('/Date(', '', $shifts[$key]['StartDateTime']);
      $shifts[$key]['StartDateTime'] = str_replace(')/', '', $shifts[$key]['StartDateTime']);
      $shifts[$key]['StartDateTime'] = strtotime(date('Y-m-d H:i:s', round( $shifts[$key]['StartDateTime'] / 1000)));
      $shifts[$key]['StartDateTimeFormat'] = date('d-F-Y H:i:s',$shifts[$key]['StartDateTime']);

      $shifts[$key]['EndDateTime'] = str_replace('/Date(', '', $shifts[$key]['EndDateTime']);
      $shifts[$key]['EndDateTime'] = str_replace(')/', '', $shifts[$key]['EndDateTime']);
      $shifts[$key]['EndDateTime'] = strtotime(date('Y-m-d H:i:s', round( $shifts[$key]['EndDateTime'] / 1000)));
      $shifts[$key]['EndDateTimeFormat'] = date('d-F-Y H:i:s',$shifts[$key]['EndDateTime']);

      foreach( $contacts as $contactInfo ){

        if( $shiftInfo['ContactID'] == $contactInfo['Id'] ){

          $shifts[$key]['Name'] = $contactInfo['Name'];

        }

      }

      foreach( $startTypes as $startTypeInfo ){

        if( $shiftInfo['StartType'] == $startTypeInfo['Id'] ){

          $shifts[$key]['Description'] = $startTypeInfo['Description'];

        }

      }

      foreach( $locations as $locationInfo ){

        if( $shiftInfo['Id'] == $locationInfo['EntityID'] ){

          $shifts[$key]['Address'] = $locationInfo['AddressLine'].' '.$locationInfo['Suburb'].' '.$locationInfo['State'];

        }

      }

      $shifts[$key]["RowID"] = $count;

      array_push($timesheet, $shifts[$key]);

      $count++;

    }


    return $timesheet;

  }

	public function getRequirements($contactId){

		$activation['databaseParams'] = [
		  'SyncLevel' => 2,
		  'UserID' => $contactId
		];

		$data['code'] = $contactId;
		$response = $this->getManstatApi(2, $data);

		if(count($response['Jobs']) == 0){

		  return false;

		}

		$jobRequirements = $response['JobRequirements'];
		$requirements = [];

		$count = 1;

		foreach( $jobRequirements as $key => $requirementInfo ){

		    $jobRequirements[$key]["RowID"] = $count;

		    array_push($requirements, $jobRequirements[$key]);

		    $count++;

		}

		return $requirements;

	}

	public function getSiteCheckList($contactId){

	    $activation['databaseParams'] = [
	      'SyncLevel' => 2,
	      'UserID' => $contactId
	    ];

	    $data['code'] = $contactId;

	    $response = $this->getManstatApi(2, $data);

	    if( !$response ){

	      return $response;

	    }

	    $siteCheckListSections = $response['SiteCheckListSections'];
	    $siteCheckListItems = $response['SiteCheckListItems'];
	    $siteCheckListItemOptions = $response['SiteCheckListItemOptions'];
	    $siteCheckListSelectableValues = $response['SiteCheckListSelectableValues'];
	    $siteCheckList = [];

	    $count = 1;


	    if( $siteCheckListSections ){

	      $siteCheckListSections = $this->jobHelper->arraySorter($siteCheckListSections,'SequenceNo');

	      foreach( $siteCheckListSections as $sectionKey => $checklistSectionInfo ){

	        $checklistItem = [];

	        foreach( $siteCheckListItems as $itemKey => $checklistItemInfo ){

	          if( $checklistSectionInfo['Id'] == $checklistItemInfo['SectionID'] ){

	            $checklistItemInfo['CheckListItemOptions'] = array();

	            foreach( $siteCheckListItemOptions as $itemOptionKey => $checklistItemOptionInfo ){

	              if( $checklistItemInfo['Id'] == $checklistItemOptionInfo['itemID'] ){

	                foreach( $siteCheckListSelectableValues as $selectableKey => $checklistSelectableValueInfo ){

	                  if( $checklistItemOptionInfo['selectableValueID'] == $checklistSelectableValueInfo['Id'] ){

	                    $checklistItemInfo['CheckListItemOptions'][] = $checklistSelectableValueInfo;

	                  }

	                }

	              }

	            }

	            $checklistItem[] = $checklistItemInfo;

	          }

	        }

	        $checklistItem = $this->arraySorter($checklistItem,'SequenceNo');

	        foreach( $checklistItem as $checklistItemInfo ){

	          array_push($siteCheckList, $checklistItemInfo);

	        }

	      }

	    }

	    if( count( $siteCheckList ) > 0 ){

	      return $siteCheckList;

	    }
	    else{

	      return false;

	    }

	}

	public function getSiteCheckListSections($contactId){

		$activation['databaseParams'] = [
			'SyncLevel' => 2,
			'UserID' => $contactId
		];

		$data['code'] = $contactId;

		$response = $this->getManstatApi(2, $data);

		if( $response ){

			$siteCheckListSections = $response['SiteCheckListSections'];

			return $siteCheckListSections;

		}
		else{

			return false;

		}

	}


	public function getAllJobData($contactId){

		$data['code'] = $contactId;

		$response = $this->getManstatApi(2, $data);

		$jobs = $response['Jobs'];
		$contacts = $response['Contacts'];
		$depots = $response['Depots'];
		$locations = $response['Locations'];
		$shifts = $response['Shifts'];
		$startTypes = $response['StartTypes'];
		$jobRequirements = $response['JobRequirements'];
		$docketAttachments = $response['DocketExternalLinks'];
		$jobAssets = $response['JobAssets'];
		$assets = $response['Assets'];
		$relatedJob = [];

		$jobIds = [];


		foreach( $jobs as $key => $jobInfo ){

		  $timesheet = [];
		  $requirements = [];
		  $attachments = [];
		  $relatedAsset = [];

		  if( in_array($jobInfo['Id'], $jobIds) ){
		    unset($jobs[$key]);
		    continue;
		  }
		  else{
		    array_push($jobIds, $jobInfo['Id']);
		  }

		  $jobs[$key]['JobID'] = $jobInfo['Id'];
		  $jobs[$key]['JobCode'] = $jobInfo['Code'];
		  $jobs[$key]['ContactPerson'] = '';
		  $jobs[$key]['AuthorizedPerson'] = '';
		  $jobs[$key]['Address'] = '';
		  $jobs[$key]['Depot'] = '';
		  $jobs[$key]['CompanyName'] = '';
		  $jobs[$key]['SplitParentJob'] = '';

		  $jobs[$key]['StartDateTime'] = str_replace('/Date(', '', $jobs[$key]['StartDateTime']);
		  $jobs[$key]['StartDateTime'] = str_replace(')/', '', $jobs[$key]['StartDateTime']);
		  $jobs[$key]['StartDateTimeFormat'] = date('d-F-Y H:i:s',round( $jobs[$key]['StartDateTime'] / 1000));

		  $jobs[$key]['EndDateTime'] = str_replace('/Date(', '', $jobs[$key]['EndDateTime']);
		  $jobs[$key]['EndDateTime'] = str_replace(')/', '', $jobs[$key]['EndDateTime']);
		  $jobs[$key]['EndDateTimeFormat'] = date('d-F-Y H:i:s',round( $jobs[$key]['EndDateTime'] / 1000));


		  foreach( $contacts as $contactInfo ){

		    if( $jobInfo['ForemanID'] == $contactInfo['Id'] ){

		      $jobs[$key]['ContactPerson'] = $contactInfo['Name'];

		    }

		    if( $jobInfo['AuthorizerID'] == $contactInfo['Id'] ){

		      $jobs[$key]['AuthorizedPerson'] = $contactInfo['Name'];

		    }

		    if( $jobInfo['ContactID'] == $contactInfo['Id'] ){

		      $jobs[$key]['CompanyName'] = $contactInfo['Name'];

		    }

		  }

		  foreach( $depots as $depotInfo ){

		    if( $jobInfo['DepotID'] == $depotInfo['Id'] ){

		      $jobs[$key]['Depot'] = $depotInfo['Description'];

		    }

		  }

		  foreach( $locations as $locationInfo ){

		    if( $jobInfo['Id'] == $locationInfo['EntityID'] ){

		      $jobs[$key]['Address'] = $locationInfo['AddressLine'].' '.$locationInfo['Suburb'].' '.$locationInfo['State'];
		      $jobs[$key]['State'] = $locationInfo['State'];

		    }

		  }

		  //Get Timesheet

		  $count = 1;

		  foreach( $shifts as $shiftsKey => $shiftInfo ){

		    if( $shiftInfo['JobID'] == $jobInfo['Id'] ){

		      $shifts[$key]['UTCStartDateTime'] = str_replace('/Date(', '', $shifts[$key]['StartDateTime']);
		      $shifts[$key]['UTCStartDateTime'] = str_replace(')/', '', $shifts[$key]['UTCStartDateTime']);
		      $shifts[$key]['UTCStartDateTimeFormat'] = gmdate('d-F-Y H:i:s',round($shifts[$key]['UTCStartDateTime']  / 1000));

		      $shifts[$key]['UTCEndDateTime'] = str_replace('/Date(', '', $shifts[$key]['EndDateTime']);
		      $shifts[$key]['UTCEndDateTime'] = str_replace(')/', '', $shifts[$key]['UTCEndDateTime']);
		      $shifts[$key]['UTCEndDateTimeFormat'] = gmdate('d-F-Y H:i:s',round($shifts[$key]['UTCEndDateTime'] / 1000));

		      $shifts[$key]['StartDateTime'] = str_replace('/Date(', '', $shifts[$key]['StartDateTime']);
		      $shifts[$key]['StartDateTime'] = str_replace(')/', '', $shifts[$key]['StartDateTime']);
		      $shifts[$key]['StartDateTime'] = strtotime(date('Y-m-d H:i:s', round( $shifts[$key]['StartDateTime'] / 1000)));
		      $shifts[$key]['StartDateTimeFormat'] = date('d-F-Y H:i:s',$shifts[$key]['StartDateTime']);

		      $shifts[$key]['EndDateTime'] = str_replace('/Date(', '', $shifts[$key]['EndDateTime']);
		      $shifts[$key]['EndDateTime'] = str_replace(')/', '', $shifts[$key]['EndDateTime']);
		      $shifts[$key]['EndDateTime'] = strtotime(date('Y-m-d H:i:s', round( $shifts[$key]['EndDateTime'] / 1000)));
		      $shifts[$key]['EndDateTimeFormat'] = date('d-F-Y H:i:s',$shifts[$key]['EndDateTime']);


		      foreach( $contacts as $contactInfo ){

		        if( $shiftInfo['ContactID'] == $contactInfo['Id'] ){

		          $shifts[$shiftsKey]['Name'] = $contactInfo['Name'];

		        }

		      }

		      foreach( $startTypes as $startTypeInfo ){

		        if( $shiftInfo['StartType'] == $startTypeInfo['Id'] ){

		          $shifts[$shiftsKey]['Description'] = $startTypeInfo['Description'];

		        }

		      }

		      foreach( $locations as $locationInfo ){

		        if( $shiftInfo['Id'] == $locationInfo['EntityID'] ){

		          $shifts[$shiftsKey]['Address'] = $locationInfo['AddressLine'].' '.$locationInfo['Suburb'].' '.$locationInfo['State'];

		        }

		      }

		      $shifts[$shiftsKey]["RowID"] = $count;

		      array_push($timesheet, $shifts[$shiftsKey]);

		      $count++;

		    }


		  }

		  //Get Job Requirements

		  $count = 1;

		  foreach( $jobRequirements as $requirementKey => $requirementInfo ){

		    if( $requirementInfo['JobID'] == $jobInfo['Id'] ){

		      $jobRequirements[$requirementKey]["RowID"] = $count;

		      array_push($requirements, $jobRequirements[$requirementKey]);

		      $count++;

		    }

		  }

		  // Get Docket Attachments

		  $count = 1;

		  foreach( $docketAttachments as $docketAttachmentKey => $attachmentInfo ){

		    if( $attachmentInfo['JobId'] == $jobInfo['Id'] ){

		      $docketAttachments[$key]["RowID"] = $count;

		      array_push($attachments, $docketAttachments[$docketAttachmentKey]);

		      $count++;

		    }

		  }



		  $count = 1;

		   foreach( $jobAssets as $jobAssetKey => $jobAssetInfo ){

		    if( $jobAssetInfo['JobID'] == $jobInfo['Id'] ){

		      foreach( $assets as $assetInfo ){

		        if( $assetInfo['Id'] == $jobAssetInfo['AssetID'] ){

		          $jobAssets[$jobAssetKey]['Code'] = $assetInfo['Code'];
		          $jobAssets[$jobAssetKey]['Description'] = $assetInfo['Description'];
		          $jobAssets[$jobAssetKey]['Status'] = $assetInfo['Status'];
		          $jobAssets[$jobAssetKey]['Type'] = $assetInfo['Type'];

		        }

		      }

		      $jobAssets[$jobAssetKey]["RowID"] = $count;

		      array_push($relatedAsset, $jobAssets[$jobAssetKey]);

		      $count++;

		    }

		  }

		  $jobs[$key]['Assets'] = $relatedAsset;
		  $jobs[$key]['Requirements'] = $requirements;
		  $jobs[$key]['Timesheets'] = $timesheet;
		  $jobs[$key]['DocketAttachments'] = $attachments;

		  array_push($relatedJob,$jobs[$key]);

		}

		return $relatedJob;

	}

	public function checkConnectivity($contactId){

		$activation['databaseParams'] = [
		'SyncLevel' => 2,
		'UserID' => $contactId
		];

		$data['code'] = $contactId;
		$response = $this->getManstatApi(2, $data);

		if( $response ){

		return true;

		}
		else{

		return false;

		}

	}

	public function returnAll($contactId){

		$activation['databaseParams'] = [
			'SyncLevel' => 2,
			'UserID' => $contactId
		];

		$data['code'] = $contactId;
		$response = $this->getManstatApi(2, $data);

		return $response;

	}

	public function updateJob($clientData, $contactId){


		$data['clientData'] = $clientData;
		$response = $this->getManstatApi(3, $data);

		return $response;

	}


	/**
     * Execute manstat api base on the service type.
     *
     * @param $serviceType The manstat api to be run
     * @param $data The data to be sent in manstat api
     *
     * @return the api's response
     */
	function getManstatApi($serviceType = 1, $data){

		$activation = $this->activationCode;

		$requestBody['communicationInfo'] = $activation['communicationInfo'];

	    $url = "";
	    $host = $activation['ServiceAddress'];
	    $host = str_replace('http://', '', $host);
	    $host = str_replace('/DocketService/DocketService.svc','',$host);

		switch($serviceType){

			//Get User ID
			case 1:

				$url = $activation['ServiceAddress'].'/GetUserID/'.$data['code'];
				$method = 'post';

				$accessUsername = $activation['ServiceAccessUser'];
				$accessPassword = $activation['ServiceAccessPassword'];

			break;

			//Get Database
			case 2:

				$url = $activation['ServiceAddress'].'/GetDatabase';
				$method = 'post';

				$accessUsername = $activation['ServiceAccessUser'];
				$accessPassword = $activation['ServiceAccessPassword'];

				$requestBody['databaseParams'] = [
				  'SyncLevel' => 2,
				  'UserID' => $data['code']
				];

			break;

			//Update Database
			case 3:

				$url = $activation['ServiceAddress'].'/UpdateDatabase/ClientData';
				$method = 'post';

				$accessUsername = $activation['ServiceAccessUser'];
				$accessPassword = $activation['ServiceAccessPassword'];

				$requestBody['clientData'] = $data['clientData'];

			break;

			//Check Activation
			case 4:

				$host = Config::get('evolution.manstatAddress');

				$url = 'http://'.$host.'/DocketService/DocketService.svc/CheckActivation/'.$data['activationCode'];
				$method = 'get';

				$accessUsername = Config::get('evolution.manstatUsername');
				$accessPassword = Config::get('evolution.manstatPassword');

			break;
			default:


			break;
		}

	    try{

	      $res = $this->client->{$method}($url, [
	        'auth' =>  [$accessUsername, $accessPassword],
	        'headers' => [
	        'Content-Type' => 'application/json',
	        'Accept' => 'application/json',
	        'Host' => $host
	       ],
	       'body' => json_encode($requestBody)
	      ]);

	    }
	    catch(RequestException $e){


	    }

	    return json_decode($res->getBody(), true);;

	}

	/**
     * Sorts array.
     *
     * @param &$array The array to be sorted
     * @param $field The key to be sorted
     * @param $desc activation of decrement sorting
     *
     * @return the sorted array
     */
	function arraySorter(&$array, $field, $desc = 0){

		usort($array,function($a, $b) use($field,$desc)
		{

		  $t1 = $a[$field];
		  $t2 = $b[$field];

		  if( $desc == 0 )
		  {
		    return ($t1 < $t2) ? -1 : 1;
		  }
		  else
		  {
		    return ($t1 > $t2) ? -1 : 1;
		  }

		});

		return $array;

	}


}


?>