<?php namespace App\Modules\dev\Job\Services;

use Mail;
use Config;
use Carbon\Carbon;
use Hash;

class FileServices
{

	function __construct(){

	}


	public function uploadToWatchFolder($data){

		return true;

		$username = $data['username'];
		$password = $data['password'];
		$server = $data['server'];
		$foldername = $data['foldername'];

		//Disable saving within folder temporarily
		$foldername = "";

		$port = 990;
		$initial_path = '';
		$passive_mode = false;

		$path = $data['filepath'].$data['old_filename'];
		$filename = $data['new_filename'];

		$fp = fopen($path, 'r+');

		if( $foldername != "" ){

		  $ftp_server = 'ftps://'.$server.'/'.$foldername.'/'.$filename;

		}
		else{

		  $ftp_server = 'ftps://'.$server.'/'.$filename;

		}


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $ftp_server);
		curl_setopt($ch, CURLOPT_USERPWD,$username.':'.$password);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_FTP_SSL, CURLFTPSSL_TRY);
		curl_setopt($ch, CURLOPT_FTPSSLAUTH, CURLFTPAUTH_TLS);
		curl_setopt($ch, CURLOPT_UPLOAD, 1);
		curl_setopt($ch, CURLOPT_INFILE, $fp);

		$output = curl_exec($ch);

		$error_no = curl_errno($ch);

		// print_r($path.' ');
		// print_r($filename.' ');
		// print_r($error_no.' ');
		// die();


		//var_dump(curl_error($ch));
		curl_close($ch);


		fclose($fp);

		if( $output ){

		  return true;

		}
		else{

		  return false;

		}

	}


}


?>