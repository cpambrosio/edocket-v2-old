<?php namespace App\Modules\dev\Job\Services;

use Config;
use Carbon\Carbon;
use Hash;

class GoogleTimezoneServices
{

	protected $client;

	function __construct(){

		$this->client = new \GuzzleHttp\Client();

	}

	function convertTimeByGeotag($data){

		$timestamp = $data['timestamp'];
		$geotag = $data['geotag'];

		$gmt = $this->setLocalToGmt($timestamp);

		$request = [
		  'geotag' => $geotag,
		  'timestamp' => $gmt
		];

		$timezoneResult = json_decode($this->getTimezone($request));

		if( $timezoneResult->status = 'OK' ){

		  $convertTimestamp = $gmt + $timezoneResult->rawOffset;

		}
		else{

		  return false;

		}

	}

	function setTimezone($geotag, $timestamp = NULL){

		if( $timestamp === NULL ){
			$timestamp = strtotime(date('Y-m-d H:i:s'));
		}

		$data = [
			'geotag' => $geotag,
			'timestamp' => $timestamp
		];

		$timezone = $this->getTimezone($data);

	    if( ( isset( $timezone->status ) && isset($timezone->timeZoneId) ) && $timezone->status == 'OK' ){

	      date_default_timezone_set($timezone->timeZoneId);

	      return $timezone->timeZoneId;

	    }
	    else{

	      return false;

	    }


	}

	function getTimezone($data, $json = true){

		$request = [
		  'geotag' => $data['geotag'],
		  'timestamp' => $data['timestamp']
		];

		$getTimezone = $this->getGoogleApi(1,$request);

		if( $getTimezone ){

		  if( $json ){

		    return json_decode( $getTimezone );

		  }
		  else{

		    return $getTimezone;

		  }

		}
		else{

		  return false;

		}

	}

	function setLocalToGmt($timestamp, $milliseconds = false){

		if( strlen($timestamp) > 10 ){

		  $timestamp = $timestamp / 1000;

		}

		$gmt = gmdate('Y-m-d H:i:s', $timestamp);

		// print_r($timestamp);
		//     print_r('<br />');
		// print_r(strtotime( $gmt ) );
		//     print_r('<br />');

		//     die();

		if( $milliseconds ){

		  return strtotime($gmt) * 1000;

		}
		else{

		  return strtotime($gmt);

		}


	}

	function getGeocode($data, $json = true){

		$request = [
		  'address' => $data['address']
		];

		$getGeocode = $this->getGoogleApi(2,$request);

		if( $getGeocode ){

		  if( $json ){

		    return json_decode( $getGeocode );

		  }
		  else{

		    return $getGeocode;

		  }

		}
		else{

		  return false;

		}

	}

	function getAddress($data, $json = true){

		$request = [
		  'geotag' => $data['geotag']
		];

		$getAddress = $this->getGoogleApi(3,$request);

		if( $getAddress ){

		  if( $json ){

		    return json_decode( $getAddress );

		  }
		  else{

		    return $getAddress;

		  }

		}
		else{

		  return false;

		}

	}
	  
	function getGoogleApi($serviceType, $data){


		$client = new \GuzzleHttp\Client();
		$client->setDefaultOption('verify', storage_path('certificates\cacert.pem'));

		switch($serviceType){
		  case 1: // Get Timezone API

		    $geolocation = str_replace(' ', '', $data['geotag']);
		    $timestamp = $data['timestamp'];
		    $googleApiKey = Config::get('evolution.googleApiKey');

		    $url = 'https://maps.googleapis.com/maps/api/timezone/json';

		    if( $geolocation != '' && $timestamp != '' && $googleApiKey != '' ){

		      $url .= '?location='.$geolocation.'&timestamp='.$timestamp.'&key='.$googleApiKey;

		    }


		    $headers = [
		      'Host' => 'maps.googleapis.com'
		    ];

		    $body = '';




		  break;
		  case 2:

		    $address = str_replace(' ', '+', $data['address']);
		    $googleApiKey = Config::get('evolution.googleApiKey');

		    $url = 'https://maps.googleapis.com/maps/api/geocode/json';

		    if( $address != '' && $googleApiKey != '' ){

		      $url .= '?address='.$address.'&key='.$googleApiKey;

		    }

		    $headers = [
		      'Host' => 'maps.googleapis.com'
		    ];

		    $body = '';

		  break;
		    case 3:

		      $geotag = str_replace(' ', '', $data['geotag']);
		      $googleApiKey = Config::get('evolution.googleApiKey');

		      $url = 'https://maps.googleapis.com/maps/api/geocode/json';

		      if( $geotag != '' && $googleApiKey != '' ){

		        $url .= '?latlng='.$geotag.'&key='.$googleApiKey;

		      }

		      $headers = [
		        'Host' => 'maps.googleapis.com'
		      ];

		      $body = '';


		  break;
		  default:


		  break;
		}

		try{

		    switch($serviceType){
		      case 1: 

		        $res = $client->get($url);
		        $return = $res->getBody()->getContents();

		      break;
		      case 2: 

		        $res = $client->get($url);
		        $return = $res->getBody()->getContents();

		      break;
		  case 3: 

		        $res = $client->get($url);
		        $return = $res->getBody()->getContents();

		      break;
		      default:


		      break;
		    }

		    return $return;

		}
		catch(RequestException $e){

		  if ($e->getResponse()->getStatusCode() == '400') {
		    return false;
		  }

		  if ($e->getResponse()->getStatusCode() == '500') {
		    return false;
		  }

		}


	}



}


?>