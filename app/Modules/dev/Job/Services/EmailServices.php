<?php namespace App\Modules\dev\Job\Services;

use App\Modules\dev\Job\Mail\SendVideoMail;
use App\Modules\dev\Job\Mail\SendJobUpdateMail;
use App\Modules\dev\Job\Mail\SendGenerateReportMail;
use App\Modules\dev\Job\Mail\SendGenerateFailReportMail;

use Mail;
use Config;
use Carbon\Carbon;
use Hash;

class EmailServices
{

	function __construct(){

	}


	public function queue(){

		// $job = (new ProcessTest())
  //                   ->onConnection('database')
  //                   ->delay(Carbon::now()->addMinutes(1));

  //       dispatch($job);


		

		// Mail::to('cpambrosio@straightarrow.com.ph')
		// ->queue(new SendVideoMail());

	}

	public function sendEmail($type,$data){

		ini_set('SMTP', 'reanimotion.com');
	    ini_set('smtp_port', '587');

	    switch($type){
	      //Update Traffic Controller Job
	      case 1:

	        // Mail::send('emails.updatetc', $data, function ($message) use($data) {
	        //     $message->from('notification@manstat.com', 'Evolution (eDocket)');
	        //     $message->to($data['email']);
	        //     $message->subject('Job Docket no.'.$data['jobID'].' has been sent');
	        // });

	      break;
	      //Update Client Data
	      case 2:

	      	Mail::to($data['email'])
	      		->send(new SendJobUpdateMail($data));

	      break;
	      //Generate Report
	      case 3:

	      	Mail::to($data['email'])
	      		->send(new SendGenerateReportMail($data));

	        // Mail::send('emails.siteassessment', $data, function ($message) use($data) {
	        //     $message->to($data['email']);
	        //     $message->subject('Evo Docket eForm for Job Docket no.'.$data['jobID']);

	        //     foreach($data['attachment'] as $attachInfo){
	        //       $message->attach($attachInfo);
	        //     }

	        // });

	      break;
	      case 4:

	      	Mail::to($data['email'])
	      		->send(new SendGenerateFailReportMail($data));

	        // Mail::send('emails.jobdocketlog', $data, function ($message) use($data) {
	        //     $message->to($data['email']);
	        //     $message->subject('Job Docket no.'.$data['jobID'].' was failed to submit');

	        //     if( isset( $data['attachment'] ) && count( $data['attachment'] ) > 0 ){

	        //       foreach ($data['attachment'] as $attachmentInfo) {

	        //         $message->attach($attachmentInfo);

	        //       }

	        //     }

	        // });

	      break;
	      case 5:

	      	Mail::to($data['email'])
	      		->send(new SendVideoMail($data));


	        // Mail::send('emails.videoupload', $data, function ($message) use($data) {
	        //     $message->to($data['email']);
	        //     $message->subject('A video was successfully uploaded');

	        //     if( isset( $data['attachment'] ) && count( $data['attachment'] ) > 0 ){

	        //       foreach ($data['attachment'] as $attachmentInfo) {

	        //         $message->attach($attachmentInfo['path'],['as'=>$attachmentInfo['name'],'mime'=>$attachmentInfo['mime']]);

	        //       }

	        //     }

	        // });

	      break;
	    }

	    if ( Mail::failures() ){

	        return false;

	    }

	    return true;

	}


}


?>