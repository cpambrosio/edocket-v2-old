<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocklistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_Stocklist', function (Blueprint $table) {
            $table->increments('StocklistID');
            $table->string('JobID', 225);
            $table->text('Type')->nullable();
            $table->text('Brand')->nullable();
            $table->text('Category')->nullable();
            $table->text('Length')->nullable();
            $table->text('Size')->nullable();
            $table->text('Description')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_Stocklist');
    }
}
