<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtJobSiteCheckListItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_ExtJobSiteCheckListItem', function (Blueprint $table) {
            $table->increments('ExtJobSiteCheckListItemID');
            $table->string('ExtJobID', 225);
            $table->text('ItemID');
            $table->text('ItemValueID')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_ExtJobSiteCheckListItem');
    }
}
