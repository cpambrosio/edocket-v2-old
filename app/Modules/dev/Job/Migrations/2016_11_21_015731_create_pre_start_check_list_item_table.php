<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreStartCheckListItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_PreSiteCheckListItem', function (Blueprint $table) {
            $table->increments('PreSiteCheckListItemID');
            $table->integer('PreSiteCheckListID'); 
            $table->string('JobID', 225);
            $table->string('Item', 255);
            $table->text('Description')->nullable();
            $table->integer('Row')->nullable();
            $table->integer('Column')->nullable();
            $table->integer('View')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_PreSiteCheckListItem');
    }
}
