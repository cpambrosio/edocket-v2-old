<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTcAttachments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('App_TCAttachment', function (Blueprint $table) {
            $table->increments('TCAttachmentID');
            $table->string('JobID', 225);
            $table->string('TCName', 225);
            $table->dateTime('AttachedOn')->nullable();
            $table->text('Attachment')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('App_TCAttachment');
    }
}
