<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreStartCheckListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_PreSiteCheckList', function (Blueprint $table) {
            $table->increments('PreSiteCheckListID');
            $table->string('JobID', 225);
            $table->integer('ContactID'); 
            $table->string('PhoneNumber', 120)->nullable();
            $table->char('VehicleRego', 10)->nullable();
            $table->integer('VehicleType')->nullable();
            $table->text('PIV')->nullable();
            $table->decimal('ServiceDueKms', 18, 0)->nullable();
            $table->integer('RetensionWheelNuts')->nullable();
            $table->decimal('SpeedoStart', 18, 0)->nullable();
            $table->decimal('SpeedoFinish', 18, 0)->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_PreSiteCheckList');
    }
}
