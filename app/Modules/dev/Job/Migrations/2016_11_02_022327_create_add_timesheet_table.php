<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddTimesheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_AddTimesheet', function (Blueprint $table) {
            $table->increments('AddTimesheetID');
            $table->string('JobID', 225);
            $table->integer('ContactID');  
            $table->integer('FatigueCompliance')->nullable(); 
            $table->string('RegoNo', 225)->nullable();
            $table->integer('AttendDepot')->nullable(); 
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_AddTimesheet');
    }
}
