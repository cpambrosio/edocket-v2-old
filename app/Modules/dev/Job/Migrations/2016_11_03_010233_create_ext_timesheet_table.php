<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtTimesheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_ExtTimesheet', function (Blueprint $table) {
            $table->increments('ExtTimesheetID');
            $table->string('ExtJobID', 225);
            $table->integer('WorkerID');
            $table->integer('ShiftID')->nullable();    
            $table->dateTime('JobStartDateTime');
            $table->dateTime('JobFinishDateTime');
            $table->dateTime('TravelStartDateTime')->nullable();
            $table->dateTime('TravelStartDateTimeEnd')->nullable();
            $table->text('TravelStartDistance')->nullable();
            $table->dateTime('TravelFinishDateTime')->nullable();
            $table->dateTime('TravelFinishDateTimeStart')->nullable();
            $table->text('TravelFinishDistance')->nullable();
            $table->dateTime('BreakStartDateTime')->nullable();
            $table->dateTime('BreakFinishDateTime')->nullable();
            $table->integer('FatigueCompliance')->nullable();  
            $table->string('RegoNo', 225)->nullable();
            $table->decimal('TraveledKilometers', 18, 0);
            $table->decimal('Odometer', 18, 0);
            $table->integer('AttendDepot')->nullable(); 
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_ExtTimesheet');
    }
}
