<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtJobRequirementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_ExtJobRequirement', function (Blueprint $table) {
            $table->increments('ExtRequirementID');
            $table->string('ExtJobID', 225);
            $table->integer('ItemID');
            $table->integer('Qty')->nullable();  
            $table->text('Notes')->nullable();
            $table->integer('OperatorID')->nullable(); 
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_ExtJobRequirement');
    }
}
