<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddContactCalendarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_AddContactCalendar', function (Blueprint $table) {
            $table->increments('AddContactCalendarID');
            $table->integer('ContactID');  
            $table->string('JobID', 225);
            $table->dateTime('TSTravelTimeToEnd')->nullable();
            $table->dateTime('TSTravelTimeFromStart')->nullable();
            $table->text('TravelTimeFromDistance')->nullable();
            $table->text('TravelTimeToDistance')->nullable();
            $table->decimal('Odometer', 18, 0);
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_AddContactCalendar');
    }
}
