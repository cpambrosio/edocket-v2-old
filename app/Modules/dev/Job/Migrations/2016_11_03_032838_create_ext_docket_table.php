<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtDocketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_ExtDocket', function (Blueprint $table) {
            $table->increments('ExtDocketID');
            $table->string('ExtJobID', 225);
            $table->integer('OperatorID');
            $table->integer('AttachmentTypeID'); 
            $table->dateTime('AttachedOn')->nullable(); 
            $table->text('Attachment')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_ExtDocket');
    }
}
