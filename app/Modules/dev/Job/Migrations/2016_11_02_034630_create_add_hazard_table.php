<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddHazardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_AddHazard', function (Blueprint $table) {
            $table->increments('AddHazardID');
            $table->string('JobID', 225);
            $table->text('AddHazard')->nullable();
            $table->string('InitialRisk', 225)->nullable();
            $table->text('ControlMeasures')->nullable();
            $table->string('ResidualRisk', 225)->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_AddHazard');
    }
}
