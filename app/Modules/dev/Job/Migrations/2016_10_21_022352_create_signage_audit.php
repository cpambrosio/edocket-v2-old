<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignageAudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('App_SignageAudit', function (Blueprint $table) {
            $table->increments('AuditID');
            $table->string('JobID', 225);
            $table->text('Landmark')->nullable();
            $table->integer('CarriageWay');  
            $table->integer('Direction')->nullable();    
            $table->time('TimeErected')->nullable();
            $table->time('TimeCollected')->nullable();
            $table->time('TimeChecked1')->nullable();
            $table->time('TimeChecked2')->nullable();
            $table->time('TimeChecked3')->nullable();
            $table->time('TimeChecked4')->nullable();
            $table->integer('ErectedBy')->nullable();  
            $table->integer('CollectedBy')->nullable();  
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('App_SignageAudit');
    }
}
