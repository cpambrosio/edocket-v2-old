<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_JobImage', function (Blueprint $table) {
            $table->increments('JobImageID');
            $table->string('JobID', 225)->nullable();
            $table->text('JobImage')->nullable();
            $table->string('GeoTag', 225)->nullable();
            $table->integer('Processed')->nullable();  
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_JobImage');
    }
}
