<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtJobHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_ExtJobHeader', function (Blueprint $table) {
            $table->increments('ExtJobHeaderID');
            $table->string('ExtJobID', 225);
            $table->integer('ParentJob')->nullable();  
            $table->string('RevisedOrderNumber', 225)->nullable();
            $table->string('GeoTag', 225)->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_ExtJobHeader');
    }
}
