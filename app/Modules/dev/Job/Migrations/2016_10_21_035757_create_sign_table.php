<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_Sign', function (Blueprint $table) {
            $table->increments('SignID');  
            $table->text('Sign')->nullable();
            $table->text('Attachment')->nullable();
            $table->string('State', 225)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_Sign');
    }
}
