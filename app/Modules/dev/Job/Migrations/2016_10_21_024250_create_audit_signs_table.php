<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditSignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_AuditSigns', function (Blueprint $table) {
            $table->increments('AuditSignsID');
            $table->integer('AuditID');  
            $table->string('JobID', 225);
            $table->integer('SignID');  
            $table->text('Metres')->nullable();
            $table->integer('Qty');  
            $table->integer('AfterCare'); 
            $table->text('AfterCareMetres');
            $table->integer('AfterCareQty'); 
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_AuditSigns');
    }
}
