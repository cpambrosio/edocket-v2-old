<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_Video', function (Blueprint $table) {
            $table->increments('VideoID');
            $table->text('Video')->nullable();
            $table->integer('FileSize')->nullable();  
            $table->text('Subject')->nullable();
            $table->text('Note')->nullable();
            $table->string('StartLocation', 225)->nullable();
            $table->string('EndLocation', 225)->nullable();
            $table->dateTime('StartCapture')->nullable();
            $table->dateTime('EndCapture')->nullable();
            $table->integer('Processed')->nullable();  
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_Video');
    }
}
