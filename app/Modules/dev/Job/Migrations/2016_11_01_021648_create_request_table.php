<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_Request', function (Blueprint $table) {
            $table->increments('RequestID');
            $table->text('IP')->nullable();
            $table->integer('OperatorID');  
            $table->string('JobID', 225);
            $table->string('ParentJobID', 225)->nullable();
            $table->text('Timesheet')->nullable();
            $table->text('Assets')->nullable();
            $table->text('Requirements')->nullable();
            $table->text('Dockets')->nullable();
            $table->text('SiteCheckList')->nullable();
            $table->text('SignageAudit')->nullable();
            $table->text('TCAttachment')->nullable();
            $table->text('AdditionalHazard')->nullable();
            $table->text('ClientName')->nullable();
            $table->text('ClientEmail')->nullable();
            $table->text('OrderNumber')->nullable();
            $table->string('GeoTag', 225)->nullable();
            $table->string('Timezone', 225)->nullable();
            $table->integer('ProcessedJob')->nullable();
            $table->integer('SiteAssessmentForm')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_Request');
    }
}
