<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenerateReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('App_GenerateReport', function (Blueprint $table) {
            $table->increments('GenerateReportID');
            $table->string('JobID', 225);
            $table->tinyInteger('JobDocket')->default(0);
            $table->tinyInteger('SafetyDocket')->default(0);
            $table->tinyInteger('JobImage')->default(0);
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('App_GenerateReport');
    }
}
