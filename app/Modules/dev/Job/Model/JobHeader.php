<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class JobHeader extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'JobHeader';

		/**
     * The connection name for the model.
     *
     * @var string
     */
	protected $connection = 'manstatsql';


}