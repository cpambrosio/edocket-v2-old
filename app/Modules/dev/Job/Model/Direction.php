<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class Direction extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'App_Direction';

	protected $fillable = ['Direction','Description'];

	protected $timestamp = false;

	//all field that will be treated as dates
	// protected $dates = ['published_at'];


}