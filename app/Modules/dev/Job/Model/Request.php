<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class Request extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'App_Request';

	protected $fillable = [
		'IP',
		'OperatorID',
		'JobID',
		'ParentJobID',
		'Timesheet',
		'Assets',
		'Requirements'
		,'Dockets',
		'SiteCheckList',
		'SignageAudit',
		'TCAttachment',
		'AdditionalHazard',
		'ClientName',
		'ClientEmail',
		'AfterCare',
		'OrderNumber',
		'GeoTag',
		'Timezone',
		'ProcessedJob',
		'SiteAssessmentForm',
		'created_at',
		'updated_at',
		'deleted_at'
	];

	protected $timestamp = true;

	/**
     * The connection name for the model.
     *
     * @var string
     */
	protected $connection = 'sqlsrv';

}