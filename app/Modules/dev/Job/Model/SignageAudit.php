<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class SignageAudit extends Model{

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'App_SignageAudit';

	protected $primaryKey = "AuditID";

	protected $fillable = ['JobID','Landmark','CarriageWay','Direction','TimeErected','TimeCollected','TimeChecked1','TimeChecked2','TimeChecked3','TimeChecked4','ErectedBy','CollectedBy','created_at','updated_at','deleted_at'];

	/**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	/**
     * The connection name for the model.
     *
     * @var string
     */
	protected $connection = 'sqlsrv';


}