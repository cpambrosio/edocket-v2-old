<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class Sign extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'App_Sign';

	protected $fillable = ['Description','Attachment','State'];

	protected $timestamp = false;

	//all field that will be treated as dates
	// protected $dates = ['published_at'];


}