<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class CarriageWay extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'App_CarriageWay';

	protected $fillable = ['CarriageWay','Description'];

	protected $timestamp = false;

	//all field that will be treated as dates
	// protected $dates = ['published_at'];


}