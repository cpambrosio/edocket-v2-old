<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class AddHazard extends Model{

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'App_AddHazard';

	protected $fillable = ['JobID','AddHazard','InitialRisk','ControlMeasures','ResidualRisk','created_at','updated_at','deleted_at'];

	protected $timestamp = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	//all field that will be treated as dates
	// protected $dates = ['published_at'];


	/**
	* create additional query for published at
	*
	* @format set{name of field - uppercased}Attribute
	*/
	// function scopePublished($query){

	// 	$query->where('published_at','<=',Carbon::now());

	// }

	/**
	* Automatically format publish date
	*
	* @format set{name of field - uppercased}Attribute
	*/
	// function setPublishedAtAttribute($date){

	// 	$this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d h:i:s',$date);

	// }


}