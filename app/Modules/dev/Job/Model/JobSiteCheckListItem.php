<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class JobSiteCheckListItem extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'JobSiteCheckListItem';

	protected $fillable = ['JobID','ItemID','ItemValueID'];

	protected $timestamp = false;

	/**
     * The connection name for the model.
     *
     * @var string
     */
	protected $connection = 'manstatsql';

}