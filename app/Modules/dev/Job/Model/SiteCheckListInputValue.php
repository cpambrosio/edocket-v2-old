<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class SiteCheckListInputValue extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'SiteCheckListInputValue';

	protected $fillable = ['ID','Value'];

	protected $timestamp = false;

	/**
     * The connection name for the model.
     *
     * @var string
     */
	protected $connection = 'manstatsql';

}