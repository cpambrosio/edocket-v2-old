<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class ExtSiteCheckListInputValue extends Model{

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'App_ExtSiteCheckListInputValue';

	protected $fillable = ['ExtID','ID','Value'];

	protected $primaryKey = 'ExtSiteCheckListInputValueID';

	protected $timestamp = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	//all field that will be treated as dates
	// protected $dates = ['published_at'];


}