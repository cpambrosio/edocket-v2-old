<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class AccountInfo extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'App_AccountInfo';

	protected $fillable = ['AccountID','ContactID','Password'];

	protected $timestamp = false;

	/**
     * The connection name for the model.
     *
     * @var string
     */
	protected $connection = 'sqlsrv';

}