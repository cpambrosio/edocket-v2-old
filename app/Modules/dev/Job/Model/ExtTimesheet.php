<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class ExtTimesheet extends Model{

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'App_ExtTimesheet';

	protected $fillable = ['ExtJobID','WorkerID','ShiftID','StartDateTime','EndDateTime','TravelStartDateTime','TravelEndDateTime','MealStartDateTime','MealEndDateTime','FatigueCompliance','RegNo','TraveledKilometers','AttendDepot','Odometer'];

	protected $primaryKey = 'ExtTimesheetID';

	protected $timestamp = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	//all field that will be treated as dates
	// protected $dates = ['published_at'];


}