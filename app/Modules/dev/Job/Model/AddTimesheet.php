<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class AddTimesheet extends Model{

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'App_AddTimesheet';

	protected $fillable = ['ContactID','JobID','ContactID','FatigueCompliance','RegoNo','AttendDepot','created_at','updated_at','deleted_at'];

	protected $timestamp = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The connection name for the model.
     *
     * @var string
     */
	protected $connection = 'sqlsrv';


}