<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class ExtJobSiteCheckListItem extends Model{

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'App_ExtJobSiteCheckListItem';

	protected $fillable = ['ExtJobID','ItemID','ItemValueID'];

	protected $primaryKey = 'ExtJobSiteCheckListItemID';

	protected $timestamp = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	//all field that will be treated as dates
	// protected $dates = ['published_at'];


}