<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class JobShiftDetail extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'JobShiftDetail';

		/**
     * The connection name for the model.
     *
     * @var string
     */
	protected $connection = 'manstatsql';



}