<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class Notes extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'HandoverLog';

	protected $timestamp = false;

	/**
     * The connection name for the model.
     *
     * @var string
     */
	protected $connection = 'manstatsql';


}