<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class GenerateReport extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'App_GenerateReport';

	protected $fillable = [
		'JobID',
		'JobDocket',
		'SafetyDocket',
		'JobImage',
		'created_at',
		'updated_at',
		'deleted_at'
	];

	protected $timestamp = true;

    /**
     * The connection name for the model.
     *
     * @var string
     */
	protected $connection = 'sqlsrv';

}