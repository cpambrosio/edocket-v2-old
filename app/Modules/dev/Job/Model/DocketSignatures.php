<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class DocketSignatures extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'DocketSignatures';

	protected $fillable = ['JobID','DocketID','SignatureType','SignedOn','SignatureImage'];

	protected $timestamp = false;

	/**
     * The connection name for the model.
     *
     * @var string
     */
	protected $connection = 'manstatsql';

}