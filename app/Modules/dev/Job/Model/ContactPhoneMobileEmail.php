<?php namespace App\Modules\dev\Job\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class ContactPhoneMobileEmail extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ContactPhoneMobileEmail';

	protected $fillable = ['ContactID','cFullname','cCompanyName','cMobilePhone','cPhone','cEmail'];

	protected $timestamp = false;

			/**
     * The connection name for the model.
     *
     * @var string
     */
	protected $connection = 'manstatsql';



}