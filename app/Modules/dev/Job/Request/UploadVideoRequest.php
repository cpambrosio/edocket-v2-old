<?php namespace App\Modules\dev\Job\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Input;

class UploadVideoRequest extends FormRequest {


	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{


		$rules = [
			'subject' => 'required',
			'note' => 'required',
			'video' => 'required',
			'startCapture' => 'required|microtime',
			'endCapture' => 'required|microtime',
			'startLocation' => 'required|geotag',
			'endLocation' => 'required|geotag',
		];
		
	
		return $rules;
	}

	/**
	 * Set custom messages for validator errors.
	 *
	 * @return array
	 */
	public function messages()
	{


		$messages = [
			'startLocation.geotag' => 'The start location field must be a valid coordinates.',
			'startLocation.required' => 'The start location field is required.',

			'endLocation.geotag' => 'The end location field must be a valid coordinates.',
			'endLocation.required' => 'The end location field is required.',

			'startCapture.required' => 'The start capture field is required.',
			'startCapture.microtime' => 'The start capture field must be a valid microtime.',

			'endCapture.required' => 'The start capture field is required.',
			'endCapture.microtime' => 'The start capture field must be a valid microtime.',

			'subject.required' => 'The subject field is required.',

			'note.required' => 'The note field is required.',

			'image.required' => 'The image field is required.',
			'image.max' => 'The image filesize must not be more than 10mb.'
		];

		return $messages;

	}

	/**
	 * Get the proper failed validation response for the request. This is optional.
	 *
	 * @param  array  $errors
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function response(array $errors)
	{

		$fields = [
			'startCapture',
			'endCapture',
			'startLocation',
			'endLocation',
			'subject',
			'note',
			'video'
		];

		//Make all error messages inline
		$errorMessage = [];

		foreach($fields as $fieldInfo){

			$list = [];

			foreach( $errors as $field => $errorInfo ){

				if( strpos($field, $fieldInfo) !== false  ){

					foreach( $errorInfo as $error ){

						$list[] = $error;

					}

				}


			}

			if( count($list) > 0 ){

				$errorMessage[] = $fieldInfo.':'.implode('|', $list);

			}

		}

		$errorList = implode('~', $errorMessage);

		$message = [
	        'status' => false,
	        'message' => $errorList
      	];

		return new JsonResponse($message, 200);

	}

}
