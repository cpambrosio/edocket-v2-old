<?php namespace App\Modules\dev\Job\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Input;

class GenerateAppSafetyReportRequest extends FormRequest {


	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{

		$timesheets = json_decode($this->request->get('timesheets'),TRUE);
		$requirements = json_decode($this->request->get('requirements'));
		$dockets = json_decode($this->request->get('dockets'));
		$signageAudit = json_decode($this->request->get('signageAudit'));
		$tcAttachment = json_decode($this->request->get('tcAttachment'));

		$rules = [
			'operatorID' => 'required|integer',
			'jobID' => 'required', 
			'siteCheckList' => 'required',
			'geotag' => 'required|geotag'
		];


		return $rules;
	}

	/**
	 * Set custom messages for validator errors.
	 *
	 * @return array
	 */
	public function messages()
	{

		$messages = [
			'operatorID.required' => 'The operator id field is required.',
			'operatorID.integer' => 'The operator id must be an integer.',
			'jobID.required' => 'The job id field is required.',
			'siteCheckList.required' => 'The site checklist field is required.',
			'geotag.required' => 'The geotag field is required.',
			'geotag.geotag' => 'The geotag field must be a valid coordinates.'
		];

		return $messages;

	}

	/**
	 * Get the proper failed validation response for the request. This is optional.
	 *
	 * @param  array  $errors
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function response(array $errors)
	{

		$fields = [
			'operatorID',
			'jobID', 
			'siteCheckList',
			'geotag'
		];

		//Make all error messages inline
		$errorMessage = [];

		foreach($fields as $fieldInfo){

			$list = [];

			foreach( $errors as $field => $errorInfo ){

				if( strpos($field, $fieldInfo) !== false  ){

					foreach( $errorInfo as $error ){

						$list[] = $error;

					}

				}


			}

			if( count($list) > 0 ){

				$errorMessage[] = $fieldInfo.':'.implode('|', $list);

			}

		}

		$errorList = implode('~', $errorMessage);

		$message = [
	        'status' => false,
	        'message' => $errorList
      	];

		return new JsonResponse($message, 200);

	}

}
