<?php namespace App\Modules\dev\Job\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Input;

class GenerateJobDocketReportRequest extends FormRequest {


	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{

		$timesheets = json_decode($this->request->get('timesheets'),TRUE);
		$requirements = json_decode($this->request->get('requirements'));
		$dockets = json_decode($this->request->get('dockets'));
		$signageAudit = json_decode($this->request->get('signageAudit'));
		$tcAttachment = json_decode($this->request->get('tcAttachment'));

		$rules = [
			'operatorID' => 'required|integer',
			'jobID' => 'required', 
			'timesheets' => 'required',
			'requirements' => 'required',
			'dockets' => 'required',
			'siteCheckList' => 'required',
			'signageAudit' => 'required',
			'tcAttachment' => 'required',
			'clientEmail' => 'email',
			'clientName' => '',
			'geotag' => 'required|geotag'
		];

		if( count($timesheets) > 0 ){

			foreach($timesheets as $key => $timeInfo){

				$ruleKey = 'timesheets.'.$key;
				$rules[$ruleKey . '.TravelFinishDateTime'] = 'required|microtime';
				$rules[$ruleKey . '.TravelStartDateTime'] = 'required|microtime';
				$rules[$ruleKey . '.JobFinishDateTime'] = 'required|microtime';
				$rules[$ruleKey . '.JobStartDateTime'] = 'required|microtime';
				$rules[$ruleKey . '.BreakFinishDateTime'] = 'numeric';
				$rules[$ruleKey . '.BreakStartDateTime'] = 'numeric';
				$rules[$ruleKey . '.ShiftID'] = 'required|integer';
				$rules[$ruleKey . '.WorkerID'] = 'required|integer';
				$rules[$ruleKey . '.JobID'] = 'required';
				$rules[$ruleKey . '.FatigueCompliance'] = 'required|boolean';
				$rules[$ruleKey . '.RegoNo'] = '';
				$rules[$ruleKey . '.Odometer'] = 'numeric';
				$rules[$ruleKey . '.AttendDepot'] = 'required|boolean';
				$rules[$ruleKey . '.TravelFinishDistance'] = 'required|numeric';
				$rules[$ruleKey . '.TravelStartDistance'] = 'required|numeric';
				
			}

		}

		if( count($requirements) > 0 ){

			foreach($requirements as $key => $requirementInfo){

				$ruleKey = 'requirements.'.$key;
				$rules[$ruleKey . '.Quantity'] = 'required|integer';
				$rules[$ruleKey . '.Description'] = 'required';
				$rules[$ruleKey . '.RequirementID'] = 'required|integer';
				$rules[$ruleKey . '.JobID'] = 'required';

			}

		}
		
		if( count($dockets) > 0 ){

			foreach($dockets as $key => $docketInfo){

				$ruleKey = 'dockets.'.$key;
				$rules[$ruleKey . '.Attachment'] = '';
				$rules[$ruleKey . '.AttachedOn'] = 'microtime';
				$rules[$ruleKey . '.AttachmentTypeID'] = 'integer';
				$rules[$ruleKey . '.OperatorID'] = 'integer';
				$rules[$ruleKey . '.JobID'] = 'required';
				
			}

		}

		if( count($signageAudit) > 0 ){

			foreach($signageAudit as $key => $signageAuditInfo){

				$ruleKey = 'signageAudit.'.$key;
				$rules[$ruleKey . '.AuditSigns'] = '';
				$rules[$ruleKey . '.TimeChecked4'] = 'microtime';
				$rules[$ruleKey . '.TimeChecked3'] = 'microtime';
				$rules[$ruleKey . '.TimeChecked2'] = 'microtime';
				$rules[$ruleKey . '.TimeChecked1'] = 'microtime';
				$rules[$ruleKey . '.TimeCollected'] = 'microtime';
				$rules[$ruleKey . '.TimeErected'] = 'microtime';
				$rules[$ruleKey . '.ErectedBy'] = 'integer';
				$rules[$ruleKey . '.CollectedBy'] = 'integer';
				$rules[$ruleKey . '.Direction'] = 'integer';
				$rules[$ruleKey . '.CarriageWay'] = 'integer';
				$rules[$ruleKey . '.Landmark'] = 'required';
				$rules[$ruleKey . '.RegoNo'] = '';
				$rules[$ruleKey . '.JobID'] = 'required';
			}

		}

		if( count($tcAttachment) > 0 ){

			foreach($tcAttachment as $key => $tcAttachmentInfo){

				$ruleKey = 'tcAttachment.'.$key;
				$rules[$ruleKey . '.Attachment'] = 'required';
				$rules[$ruleKey . '.AttachedOn'] = 'required|microtime';
				$rules[$ruleKey . '.TCName'] = 'required';
				$rules[$ruleKey . '.JobID'] = 'required';
				
			}

		}
		
	
		return $rules;
	}

	/**
	 * Set custom messages for validator errors.
	 *
	 * @return array
	 */
	public function messages()
	{

		$timesheets = json_decode($this->request->get('timesheets'),true);
		$requirements = json_decode($this->request->get('requirements'),true);
		$dockets = json_decode($this->request->get('dockets'),true);
		$signageAudit = json_decode($this->request->get('signageAudit'),true);
		$tcAttachment = json_decode($this->request->get('tcAttachment'),true);

		$messages = [
			'operatorID.required' => 'The operator id field is required.',
			'operatorID.integer' => 'The operator id must be an integer.',
			'jobID.required' => 'The job id field is required.',
			'timesheets.json' => 'The timesheet must be in json format.',
			'timesheets.required_multi' => 'all timesheet fields are required.',
			'requirements.required_multi' => 'all job requirement fields are required.',
			'dockets.required_multi' => 'all docket fields are required.',
			'signageAudit.required_multi' => 'all signage audit fields are required.',
			'tcAttachment.required_multi' => 'all tc attachment fields are required.',
			'clientEmail.required' => 'The client email field is required.',
			'clientEmail.email' => 'The email field must be a valid email address.',
			'clientName.required' => 'The client name field is required.',
			'geotag.required' => 'The geotag field is required.',
			'geotag.geotag' => 'The geotag field must be a valid coordinates.'
		];

		if( count($timesheets) > 0 ){

			foreach($timesheets as $key => $timeInfo){

				$messagesKey = 'timesheets.'.$key;
				$messages[$messagesKey . '.TravelFinishDateTime.required'] = 'The travel finish date time within timesheets field is required.';
				$messages[$messagesKey . '.TravelFinishDateTime.microtime'] = 'The travel finish date time within timesheets field must be a valid microtime.';
				$messages[$messagesKey . '.TravelStartDateTime.required'] = 'The travel start date time within timesheets field is required.';
				$messages[$messagesKey . '.TravelStartDateTime.microtime'] = 'The travel start date time within timesheets field must be a valid microtime.';
				
				$messages[$messagesKey . '.JobFinishDateTime.required'] = 'The job finish date time within timesheets field is required.';
				$messages[$messagesKey . '.JobFinishDateTime.microtime'] = 'The job finish date time within timesheets field must be a valid microtime.';
				$messages[$messagesKey . '.JobStartDateTime.required'] = 'The job start date time within timesheets field is required.';
				$messages[$messagesKey . '.JobStartDateTime.microtime'] = 'The job start date time within timesheets field must be a valid microtime.';
				
				$messages[$messagesKey . '.BreakFinishDateTime.required'] = 'The break finish date time within timesheets field is required.';
				$messages[$messagesKey . '.BreakFinishDateTime.microtime'] = 'The break finish date time within timesheets field must be a valid microtime.';
				$messages[$messagesKey . '.BreakStartDateTime.required'] = 'The break start date time within timesheets field is required.';
				$messages[$messagesKey . '.BreakStartDateTime.microtime'] = 'The break start date time within timesheets field must be a valid microtime.';

				$messages[$messagesKey . '.ShiftID.required'] = 'The shift id within timesheets field is required.';
				$messages[$messagesKey . '.ShiftID.integer'] = 'The shift id within timesheets field must be an integer.';

				$messages[$messagesKey . '.WorkerID.required'] = 'The worker id within timesheets field is required.';
				$messages[$messagesKey . '.WorkerID.integer'] = 'The worker id within timesheets field must be an integer.';

				$messages[$messagesKey . '.JobID.required'] = 'The job id within timesheets field is required.';
				$messages[$messagesKey . '.JobID.integer'] = 'The job id within timesheets field must be an integer.';

				$messages[$messagesKey . '.FatigueCompliance.required'] = 'The fatigue compliance within timesheets field is required.';
				$messages[$messagesKey . '.FatigueCompliance.boolean'] = 'The fatigue compliance within timesheets field must be a boolean.';

				$messages[$messagesKey . '.TravelFinishDistance.required'] = 'The travel finish distance within timesheets field is required.';
				$messages[$messagesKey . '.TravelStartDistance.required'] = 'The travel start distance within timesheets field is required.';

				$messages[$messagesKey . '.Odometer.numeric'] = 'The odometer within timesheets field must be numeric.';

			}

		}

		if( count($requirements) > 0 ){

			foreach($requirements as $key => $requirementInfo){

				$messagesKey = 'requirements.'.$key;

				$messages[$messagesKey . '.Quantity.required'] = 'The quantity within requirements field is required.';
				$messages[$messagesKey . '.Quantity.integer'] = 'The quantity within requirements field must be an integer.';

				$messages[$messagesKey . '.Description.required'] = 'The description within requirements field is required.';

				$messages[$messagesKey . '.RequirementID.required'] = 'The requirement id within requirements field is required.';
				$messages[$messagesKey . '.RequirementID.integer'] = 'The requirement id within requirements field must be an integer.';

				$messages[$messagesKey . '.JobID.required'] = 'The job id within requirements field is required.';
				$messages[$messagesKey . '.JobID.integer'] = 'The job id within requirements field must be an integer.';


			}

		}

		if( count($dockets) > 0 ){

			foreach($dockets as $key => $docketInfo){

				$messagesKey = 'dockets.'.$key;
 
				$messages[$messagesKey . '.Attachment.required'] = 'The attachment within dockets field is required.';

				$messages[$messagesKey . '.AttachedOn.required'] = 'The attached on within dockets field is required.';
				$messages[$messagesKey . '.AttachedOn.microtime'] = 'The attached on within dockets field must be a valid microtime.';

				$messages[$messagesKey . '.AttachmentTypeID.required'] = 'The attachment type id within dockets field is required.';
				$messages[$messagesKey . '.AttachmentTypeID.integer'] = 'The attachment type id within dockets field must be an integer.';

				$messages[$messagesKey . '.OperatorID.required'] = 'The operator id within dockets field is required.';
				$messages[$messagesKey . '.OperatorID.integer'] = 'The operator id within dockets field must be an integer.';

				$messages[$messagesKey . '.JobID.required'] = 'The job id within dockets field is required.';
				$messages[$messagesKey . '.JobID.integer'] = 'The job id within dockets field must be an integer.';

			}

		}

		if( count($signageAudit) > 0 ){

			foreach($signageAudit as $key => $signageAuditInfo){

				$messagesKey = 'signageAudit.'.$key;

				$messages[$messagesKey . '.AuditSigns.required'] = 'The audit signs within signage audit field is required.';

				$messages[$messagesKey . '.TimeChecked4.required'] = 'The time checked 4 within signage audit field is required.';
				$messages[$messagesKey . '.TimeChecked4.regex'] = 'The time checked 4 within signage audit field format is invalid.';

				$messages[$messagesKey . '.TimeChecked3.required'] = 'The time checked 3 within signage audit field is required.';
				$messages[$messagesKey . '.TimeChecked3.regex'] = 'The time checked 3 within signage audit field format is invalid.';

				$messages[$messagesKey . '.TimeChecked2.required'] = 'The time checked 2 within signage audit field is required.';
				$messages[$messagesKey . '.TimeChecked2.regex'] = 'The time checked 2 within signage audit field format is invalid.';

				$messages[$messagesKey . '.TimeChecked1.required'] = 'The time checked 1 within signage audit field is required.';
				$messages[$messagesKey . '.TimeChecked1.regex'] = 'The time checked 1 within signage audit field format is invalid.';

				$messages[$messagesKey . '.TimeCollected.required'] = 'The time collected within signage audit field is required.';
				$messages[$messagesKey . '.TimeCollected.regex'] = 'The time collected within signage audit field format is invalid.';

				$messages[$messagesKey . '.TimeErected.required'] = 'The time erected within signage audit field is required.';
				$messages[$messagesKey . '.TimeErected.regex'] = 'The time erected within signage audit field format is invalid.';

				$messages[$messagesKey . '.ErectedBy.integer'] = 'The erected by within signage audit field must be an integer.';

				$messages[$messagesKey . '.CollectedBy.integer'] = 'The collected by within signage audit field must be an integer.';

				$messages[$messagesKey . '.Direction.required'] = 'The direction within signage audit field is required.';
				$messages[$messagesKey . '.Direction.integer'] = 'The direction within signage audit field must be an integer.';

				$messages[$messagesKey . '.Landmark.required'] = 'The landmark within signage audit field is required.';

				$messages[$messagesKey . '.RegoNo.required'] = 'The reg no. within signage audit field is required.';
				$messages[$messagesKey . '.RegoNo.integer'] = 'The reg no. within signage audit field must be an integer.';

				$messages[$messagesKey . '.JobID.required'] = 'The job id within signage audit field is required.';
				$messages[$messagesKey . '.JobID.integer'] = 'The job id within signage audit field must be an integer.';


			}

		}

		if( count($tcAttachment) > 0 ){

			foreach($tcAttachment as $key => $tcAttachmentInfo){

				$messagesKey = 'tcAttachment.'.$key;

				$messages[$messagesKey . '.Attachment.required'] = 'The attachment within tc attachment field is required.';

				$messages[$messagesKey . '.AttachedOn.required'] = 'The attached on within tc attachment field is required.';
				$messages[$messagesKey . '.AttachedOn.microtime'] = 'The attached on within tc attachment field must be a valid microtime.';

				$messages[$messagesKey . '.TCName.required'] = 'The tc name within tc attachment field is required.';

				$messages[$messagesKey . '.JobID.required'] = 'The job id within tc attachment field is required.';
				$messages[$messagesKey . '.JobID.integer'] = 'The job id within tc attachment field must be an integer.';

			}

		}


		return $messages;

	}

	/**
	 * Get the proper failed validation response for the request. This is optional.
	 *
	 * @param  array  $errors
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function response(array $errors)
	{

		$fields = [
			'operatorID',
			'jobID', 
			'timesheets',
			'requirements',
			'dockets',
			'siteCheckList',
			'signageAudit',
			'tcAttachment',
			'clientEmail',
			'clientName',
			'orderNumber',
			'geotag'
		];

		//Make all error messages inline
		$errorMessage = [];

		foreach($fields as $fieldInfo){

			$list = [];

			foreach( $errors as $field => $errorInfo ){

				if( strpos($field, $fieldInfo) !== false  ){

					foreach( $errorInfo as $error ){

						$list[] = $error;

					}

				}


			}

			if( count($list) > 0 ){

				$errorMessage[] = $fieldInfo.':'.implode('|', $list);

			}

		}

		$errorList = implode('~', $errorMessage);

		$message = [
	        'status' => false,
	        'message' => $errorList
      	];

		return new JsonResponse($message, 200);

	}

}
