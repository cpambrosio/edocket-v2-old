<?php namespace App\Modules\dev\Job\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Input;

class GetTimesheetRequest extends FormRequest {


	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{


		$rules = [
			'geotag' => 'required|geotag'
		];
		
	
		return $rules;
	}

	/**
	 * Set custom messages for validator errors.
	 *
	 * @return array
	 */
	public function messages()
	{


		$messages = [
			'geotag.geotag' => 'The geotag field must be a valid coordinates.'
		];

		return $messages;

	}

	/**
	 * Get the proper failed validation response for the request. This is optional.
	 *
	 * @param  array  $errors
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function response(array $errors)
	{

		$fields = [
			'geotag'
		];

		//Make all error messages inline
		$errorMessage = [];

		foreach($fields as $fieldInfo){

			$list = [];

			foreach( $errors as $field => $errorInfo ){

				if( strpos($field, $fieldInfo) !== false  ){

					foreach( $errorInfo as $error ){

						$list[] = $error;

					}

				}


			}

			if( count($list) > 0 ){

				$errorMessage[] = $fieldInfo.':'.implode('|', $list);

			}

		}

		$errorList = implode('~', $errorMessage);

		$message = [
	        'status' => false,
	        'message' => $errorList
      	];

		return new JsonResponse($message, 200);

	}

}
