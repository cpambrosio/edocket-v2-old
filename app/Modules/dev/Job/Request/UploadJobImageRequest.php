<?php namespace App\Modules\dev\Job\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Input;

class UploadJobImageRequest extends FormRequest {


	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{


		$rules = [
			'geotag' => 'required|geotag',
			'jobId' => 'required',
			'image' => 'required|max:10000'
		];
		
	
		return $rules;
	}

	/**
	 * Set custom messages for validator errors.
	 *
	 * @return array
	 */
	public function messages()
	{


		$messages = [
			'geotag.geotag' => 'The geotag field must be a valid coordinates.',
			'geotag.required' => 'The geotag field is required.',

			'jobId.required' => 'The jobId field is required.',

			'image.required' => 'The image field is required.',
			'image.max' => 'The image filesize must not be more than 10mb.'
		];

		return $messages;

	}

	/**
	 * Get the proper failed validation response for the request. This is optional.
	 *
	 * @param  array  $errors
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function response(array $errors)
	{

		$fields = [
			'geotag',
			'jobId',
			'image'
		];

		//Make all error messages inline
		$errorMessage = [];

		foreach($fields as $fieldInfo){

			$list = [];

			foreach( $errors as $field => $errorInfo ){

				if( strpos($field, $fieldInfo) !== false  ){

					foreach( $errorInfo as $error ){

						$list[] = $error;

					}

				}


			}

			if( count($list) > 0 ){

				$errorMessage[] = $fieldInfo.':'.implode('|', $list);

			}

		}

		$errorList = implode('~', $errorMessage);

		$message = [
	        'status' => false,
	        'message' => $errorList
      	];

		return new JsonResponse($message, 200);

	}

}
