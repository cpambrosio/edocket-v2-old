<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface ExtSiteCheckListInputValueInterface
{

	function get($Id);

	function all();

	function insert($data);

	function deleteByJobID($Id);

	function count($jobId);



}