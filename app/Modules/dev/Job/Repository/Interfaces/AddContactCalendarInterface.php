<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface AddContactCalendarInterface
{

	function get($jobId);

	function insert($data);

	function deleteByJobID($jobId);

}