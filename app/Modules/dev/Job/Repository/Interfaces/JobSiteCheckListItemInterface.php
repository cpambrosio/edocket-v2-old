<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface JobSiteCheckListItemInterface
{

	function get($jobId);

	function deleteByJobID($jobId);

}