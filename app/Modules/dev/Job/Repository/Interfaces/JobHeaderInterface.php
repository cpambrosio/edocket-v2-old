<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface JobHeaderInterface
{

	function all();

	function getByCurrentUser($operator);

	function getById($id);

	function getReportDataById($id);

	function getInfoById($id);

}