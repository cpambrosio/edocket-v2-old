<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface AddJobHeaderInterface
{

	function get($jobId);

	function insert($data);

	function deleteByJobID($jobId);

}