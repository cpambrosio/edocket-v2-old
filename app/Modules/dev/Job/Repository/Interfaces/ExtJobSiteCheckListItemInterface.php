<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface ExtJobSiteCheckListItemInterface
{

	function get($Id);

	function all();

	function insert($data);

	function deleteByJobID($jobID);

	function count($jobId);

}