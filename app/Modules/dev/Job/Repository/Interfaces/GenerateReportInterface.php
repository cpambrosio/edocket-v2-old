<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface GenerateReportInterface
{

	function get($requestId);

	function getByJobID($jobId);

	function create($data);

	function update($data, $generateReportId);

	function delete($generateReportId);

	function deleteByJobID($jobId);

	function countByJobID($jobId);

	function countJobDocket($jobId);

	function countSafetyDocket($jobId);

	function countJobImage($jobId);

}