<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface DocketSignaturesInterface
{

	function get($jobId);

	function deleteByJobID($jobId);

}