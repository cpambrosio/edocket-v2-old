<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface ExtTimesheetInterface
{

	function all();

	function insert($data);

	function deleteByJobID($jobID);

	function count($jobId);

	function getByJobID($jobId);

}