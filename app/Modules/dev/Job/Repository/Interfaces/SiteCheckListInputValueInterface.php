<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface SiteCheckListInputValueInterface
{

	function get($Id);

	function delete($Id);

}