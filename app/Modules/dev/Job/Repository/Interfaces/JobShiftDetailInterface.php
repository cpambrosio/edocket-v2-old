<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface JobShiftDetailInterface
{

	function all();

	function getUniqueById($id);


}