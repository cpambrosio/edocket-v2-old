<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface JobSyncInterface
{

	function get($requestId);

	function getByJobID($jobId);

	function updateByJobID($jobId,$data);

	function updateByJobIDIP($jobID,$ip,$data);

	function countByJobID($jobId);

	function countByJobIDIP($jobID,$ip);

	function create($data);

	function delete($requestId);

	function deleteByJobID($jobId);

}