<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface AddHazardInterface
{

	function get($jobId);

	function insert($data);

	function deleteByJobID($jobId);

}