<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface PreSiteCheckListInterface
{

	function get($jobId);

	function create($data);

	function deleteByJobID($jobId);

}