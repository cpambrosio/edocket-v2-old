<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface RequestInterface
{

	function get($requestId);

	function create($data);

	function update($data, $requestId);

	function delete($requestId);

	function deleteByJobID($jobId);

}