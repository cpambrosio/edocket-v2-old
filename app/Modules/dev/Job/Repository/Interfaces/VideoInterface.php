<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface VideoInterface
{

	function get($videoId);

	function getPendingVideo();

	function countPendingVideo();

	function insert($data);

	function updateByVideoID($data, $videoId);

	function deleteByVideoID($videoId);

}