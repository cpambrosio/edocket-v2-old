<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface ExtDocketInterface
{

	function all();

	function insert($data);

	function deleteByJobID($jobID);

	function count($jobId);

}