<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface ContactPhoneMobileEmailInterface
{

	function get($contactId);

}