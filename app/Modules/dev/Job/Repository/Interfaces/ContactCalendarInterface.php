<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface ContactCalendarInterface
{

	function all();

	function getRelatedJobs($contactID);

	function getRoosteredContacts($id);

	function getTimesheet($contactId, $jobId);

}