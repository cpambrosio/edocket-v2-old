<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface JobClientNotifyInterface
{

	function get($jobId);

	function insert($data);

	function deleteByJobID($jobId);

}