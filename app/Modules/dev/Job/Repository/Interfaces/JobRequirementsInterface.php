<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface JobRequirementsInterface
{

	public function getByJobId($JobId);

}