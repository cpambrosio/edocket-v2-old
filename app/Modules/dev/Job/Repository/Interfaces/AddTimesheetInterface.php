<?php namespace App\Modules\dev\Job\Repository\Interfaces;


interface AddTimesheetInterface
{

	function get($jobId);

	function insert($data);

	function deleteByJobID($jobId);

}