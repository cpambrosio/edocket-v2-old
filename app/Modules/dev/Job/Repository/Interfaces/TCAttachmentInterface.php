<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface TCAttachmentInterface
{

	function get($jobId);

	function insert($data);

	function deleteByJobID($jobId);

}