<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface ExtJobHeaderInterface
{

	function all();

	function insert($data);

	function deleteByJobID($jobID);

	function deleteByJobIDAndParentJob($jobID, $parentId);

	function count($jobId);

	function getByContact($contactId);

}