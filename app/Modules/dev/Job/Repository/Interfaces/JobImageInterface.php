<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface JobImageInterface
{

	function get($jobId);

	function getPendingJobImage($jobId);

	function countPendingJobImage($jobId);

	function insert($data);

	function updateByJobID($data, $jobId);

	function deleteByJobID($jobId);

}