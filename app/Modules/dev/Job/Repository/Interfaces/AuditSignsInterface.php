<?php namespace App\Modules\dev\Job\Repository\Interfaces;

interface AuditSignsInterface
{

	function get($auditId);

	function create($data);

	function delete($auditId);

	function deleteByJobID($jobId);

}