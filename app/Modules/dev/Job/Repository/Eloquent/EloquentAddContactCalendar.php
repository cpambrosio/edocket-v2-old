<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\AddContactCalendarInterface;

use DB;

class EloquentAddContactCalendar implements AddContactCalendarInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $addContactCalendar;


  public function __construct( Model $addContactCalendar )
  {

    $this->addContactCalendar   = $addContactCalendar;

  }

  public function get($jobId){

    return $this->addContactCalendar->where('JobID','=',$jobId)->get()->toArray();

  }

  public function insert($data){

  	return $this->addContactCalendar->insert($data);
  }

  public function deleteByJobID($jobId){

    return $this->addContactCalendar->where('JobID','=',$jobId)->delete();


  }



}