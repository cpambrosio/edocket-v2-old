<?php namespace App\Modules\dev\Job\Repository\Eloquent;


use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\PreSiteCheckListItemInterface;

use DB;


class EloquentPreSiteCheckListItem implements PreSiteCheckListItemInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $preSiteCheckListItem;


  public function __construct( Model $preSiteCheckListItem )
  {

    $this->preSiteCheckListItem   = $preSiteCheckListItem;

  }

  public function get($checkListId){

    return $this->preSiteCheckListItem->where('PreSiteCheckListID','=',$checkListId)->get()->toArray();

  }

  public function create($data){

  	return $this->preSiteCheckListItem->create($data);


  }

  public function delete($checkListId){

    return $this->preSiteCheckListItem->where('PreSiteCheckListID','=',$checkListId)->delete();

  }

  public function deleteByJobID($jobId){

    return $this->preSiteCheckListItem->where('JobID','=',$jobId)->delete();

  }



}