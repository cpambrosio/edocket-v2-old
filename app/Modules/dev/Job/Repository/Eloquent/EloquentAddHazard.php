<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\AddHazardInterface;

use DB;

class EloquentAddHazard implements AddHazardInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $addHazard;


  public function __construct( Model $addHazard )
  {

    $this->addHazard   = $addHazard;

  }

  public function get($jobId){

    return $this->addHazard->where('JobID','=',$jobId)->get()->toArray();

  }

  public function insert($data){

  	return $this->addHazard->insert($data);
  }

  public function deleteByJobID($jobId){

    return $this->addHazard->where('JobID','=',$jobId)->delete();


  }



}