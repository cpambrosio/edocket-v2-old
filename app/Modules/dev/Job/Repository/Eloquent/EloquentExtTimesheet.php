<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\ExtTimesheetInterface;

use DB;

class EloquentExtTimesheet implements ExtTimesheetInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $extTimesheet;
  protected $extJobHeader;


  public function __construct( Model $extTimesheet, Model $extJobHeader )
  {

    $this->extTimesheet   = $extTimesheet;
    $this->extJobHeader   = $extJobHeader;

  }

  public function all(){

  	return $this->extTimesheet->get()->toArray();

  }

  public function insert($data){

    return $this->extTimesheet->insert($data);

  }

  public function deleteByJobID($jobID){

    return $this->extTimesheet->where('ExtJobID','=',$jobID)->delete();

  }

  public function count($jobId){

    return $this->extTimesheet
                ->where('ExtJobID','=',$jobId)
                ->count();

  }

  public function getByJobID($jobId){

    $fields = [
      'App_ExtTimesheet.WorkerID as ContactID',
      'App_ExtTimesheet.JobFinishDateTime as EndDateTime',
      'App_ExtTimesheet.ShiftID as Id',
      'App_ExtTimesheet.ExtJobID as JobID',
      'App_ExtTimesheet.JobStartDateTime as StartDateTime',
      // 'App_ExtTimesheet.TravelStartDateTime',
      // 'App_ExtTimesheet.TravelFinishDateTime',
      // 'App_ExtTimesheet.BreakStartDateTime',
      // 'App_ExtTimesheet.BreakFinishDateTime',
      'JobShiftDetail.JobStartType as StartType',
      DB::raw("(RTRIM(LTRIM(Worker.FirstName))+' '+RTRIM(LTRIM(Worker.LastName))) as Name"),
      DB::raw("(RTRIM(LTRIM(StartType.Description))) as Description")
    ];

    return $this->extJobHeader
            ->join("JobHeader","JobHeader.JobID","=","App_ExtJobHeader.ParentJob")
            ->join("App_ExtTimesheet", function($join)
              {
                  $join->on("App_ExtJobHeader.ExtJobID", "=", "App_ExtTimesheet.ExtJobID")
                       ->whereNull('App_ExtTimesheet.deleted_at');
              })
            ->leftJoin("Contact as Worker","Worker.ContactID","=","App_ExtTimesheet.WorkerID")
            ->leftJoin("Lookup as Depot","Depot.LookupID","=","JobHeader.JobDepot")
            ->join("JobShiftDetail","JobShiftDetail.JobID","=","JobHeader.JobID")
            ->leftJoin("Lookup as StartType","StartType.LookupID","=","JobShiftDetail.JobStartType")
            ->where('App_ExtJobHeader.ExtJobID','=',$jobId)
            ->whereNull('App_ExtJobHeader.deleted_at')
            ->get($fields)->toArray();


  }


}