<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\SignInterface;

use DB;

class EloquentSign implements SignInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $sign;


  public function __construct( Model $sign )
  {

    $this->sign = $sign;

  }

  public function all(){

  	return $this->sign->get()->toArray();


  }

}