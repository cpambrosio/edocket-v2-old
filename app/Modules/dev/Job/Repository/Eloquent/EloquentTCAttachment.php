<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\TCAttachmentInterface;

use DB;

class EloquentTCAttachment implements TCAttachmentInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $tcAttachment;


  public function __construct( Model $tcAttachment )
  {

    $this->tcAttachment   = $tcAttachment;

  }

  public function get($jobId){

    return $this->tcAttachment->where('JobID','=',$jobId)->get()->toArray();

  }

  public function insert($data){

  	return $this->tcAttachment->insert($data);

  }

  public function deleteByJobID($jobId){

    return $this->tcAttachment->where('JobID','=',$jobId)->delete();


  }

}