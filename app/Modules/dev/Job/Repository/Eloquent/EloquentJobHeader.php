<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\JobHeaderInterface;

use DB;

class EloquentJobHeader implements JobHeaderInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $jobHeader;


  public function __construct( Model $jobHeader )
  {

    $this->jobHeader   = $jobHeader;

  }


  public function all(){

    return $this->jobHeader->all();

  }

  public function getByCurrentUser($operator){

    $fields = array(
      "JobHeader.JobID",
      "JobHeader.JobCode",
      DB::raw("(RTRIM(LTRIM(Contact.CompanyName))) as CompanyName"),
      "JobHeader.CreateTime",
      DB::raw("(RTRIM(LTRIM(JobHeader.JobAddressLine1))+' '+RTRIM(LTRIM(JobHeader.JobSuburb))+' '+RTRIM(LTRIM(JobHeader.JobState))) as Address")
    );

    $result = $this->jobHeader
                   ->join("Contact","Contact.ContactID","=","JobHeader.ContactID")
                   ->where("JobHeader.Operator","=",$operator)
                   ->where("JobHeader.JobStatus","!=","2")
                   ->orderBy("JobHeader.CreateTime","desc")
                   ->get($fields)
                   ->toArray();

    return $result;

  }

  public function getById($id){

    $fields = array(
      "JobHeader.JobID",
      "JobHeader.JobCode",
      DB::raw("(RTRIM(LTRIM(Contact.FirstName))+' '+RTRIM(LTRIM(Contact.LastName))) as ContactPerson"),
      DB::raw("(RTRIM(LTRIM(Contact.CompanyName))) as CompanyName"),
      DB::raw("(RTRIM(LTRIM(Authorized.FirstName))+' '+RTRIM(LTRIM(Authorized.LastName))) as AuthorizedPerson"),
      "JobHeader.CreateTime",
      DB::raw("(RTRIM(LTRIM(JobHeader.JobAddressLine1))+' '+RTRIM(LTRIM(JobHeader.JobSuburb))+' '+RTRIM(LTRIM(JobHeader.JobState))) as Address"),
      DB::raw("(RTRIM(LTRIM(Depot.Description))) as Depot"),
      "JobShiftDetail.StartDateTime",
      "JobHeader.OrderNumber",
      DB::raw("(RTRIM(LTRIM(JobHeader.JobState))) as JobState"),
      DB::raw("JobHeader.CreateTime as CreateDateTime")
    );

    $count = $this->jobHeader
                   ->select($fields)
                   ->leftJoin("Contact","Contact.ContactID","=","JobHeader.ContactID")
                   ->leftJoin("Contact as Authorized","Authorized.ContactID","=","JobHeader.ContactID")
                   ->leftJoin("Lookup as Depot","Depot.LookupID","=","JobHeader.JobDepot")
                   ->join("JobShiftDetail","JobShiftDetail.JobID","=","JobHeader.JobID")
                   ->where("JobHeader.JobID","=",$id)
                   // ->where("JobHeader.JobStatus","!=","2")
                   ->count();

    if( $count <= 0 ){
      return false;
    }

    $result = $this->jobHeader
                   ->select($fields)
                   ->leftJoin("Contact","Contact.ContactID","=","JobHeader.ContactID")
                   ->leftJoin("Contact as Authorized","Authorized.ContactID","=","JobHeader.AuthorisedByID")
                   ->leftJoin("Lookup as Depot","Depot.LookupID","=","JobHeader.JobDepot")
                   ->join("JobShiftDetail","JobShiftDetail.JobID","=","JobHeader.JobID")
                   ->where("JobHeader.JobID","=",$id)
                   // ->where("JobHeader.JobStatus","!=","2")
                   ->first()
                   ->toArray();

    return $result;

  }

  public function getReportDataById($id){

    $fields = array(
      "JobHeader.JobID",
      "JobHeader.JobCode",
      DB::raw("(RTRIM(LTRIM(Contact.FirstName))+' '+RTRIM(LTRIM(Contact.LastName))) as ContactPerson"),
      DB::raw("(RTRIM(LTRIM(Contact.CompanyName))) as CompanyName"),
      DB::raw("(RTRIM(LTRIM(Authorized.FirstName))+' '+RTRIM(LTRIM(Authorized.LastName))) as AuthorizedPerson"),
      "JobHeader.CreateTime",
      DB::raw("(RTRIM(LTRIM(JobHeader.JobAddressLine1))+' '+RTRIM(LTRIM(JobHeader.JobSuburb))+' '+RTRIM(LTRIM(JobHeader.JobState))) as Address"),
      DB::raw("(RTRIM(LTRIM(Depot.Description))) as Depot"),
      "JobShiftDetail.StartDateTime",
      "JobHeader.JobStartdate",
      "JobHeader.OrderNumber",
      DB::raw("(RTRIM(LTRIM(JobHeader.JobState))) as JobState"),
      "Contact.ContactCode",
      "Contact.AttacheCode"
    );

    $count = $this->jobHeader
                   ->select($fields)
                   ->leftJoin("Contact","Contact.ContactID","=","JobHeader.ContactID")
                   ->leftJoin("Contact as Authorized","Authorized.ContactID","=","JobHeader.ContactID")
                   ->leftJoin("Lookup as Depot","Depot.LookupID","=","JobHeader.JobDepot")
                   ->join("JobShiftDetail","JobShiftDetail.JobID","=","JobHeader.JobID")
                   ->where("JobHeader.JobID","=",$id)
                   // ->where("JobHeader.JobStatus","!=","2")
                   ->count();

    if( $count <= 0 ){
      return false;
    }

    $result = $this->jobHeader
                   ->select($fields)
                   ->leftJoin("Contact","Contact.ContactID","=","JobHeader.ContactID")
                   ->leftJoin("Contact as Authorized","Authorized.ContactID","=","JobHeader.AuthorisedByID")
                   ->leftJoin("Lookup as Depot","Depot.LookupID","=","JobHeader.JobDepot")
                   ->join("JobShiftDetail","JobShiftDetail.JobID","=","JobHeader.JobID")
                   ->where("JobHeader.JobID","=",$id)
                   // ->where("JobHeader.JobStatus","!=","2")
                   ->first()
                   ->toArray();

    return $result;

  }

  public function getInfoById($id){

    $fields = array(
      "JobHeader.JobID",
      "JobHeader.JobCode",
      "JobHeader.CreateTime",
      DB::raw("(RTRIM(LTRIM(JobHeader.JobAddressLine1))+' '+RTRIM(LTRIM(JobHeader.JobSuburb))+' '+RTRIM(LTRIM(JobHeader.JobState))) as Address"),
      "JobHeader.OrderNumber",
      DB::raw("(RTRIM(LTRIM(JobHeader.JobState))) as JobState"),
      DB::raw("JobHeader.CreateTime as CreateDateTime"),
      DB::raw("JobHeader.JobStartdate as JobStartDate"),
      DB::raw("JobHeader.JobStartTime as JobStartTime"),
      DB::raw("JobHeader.JobShiftLength as JobShiftLength"),
      DB::raw("JobHeader.JobEndDate as JobEndDate")
    );

    $count = $this->jobHeader
                   ->select($fields)
                   ->where("JobHeader.JobID","=",$id)
                   // ->where("JobHeader.JobStatus","!=","2")
                   ->count();

    if( $count <= 0 ){
      return false;
    }

    $result = $this->jobHeader
                   ->select($fields)
                   ->where("JobHeader.JobID","=",$id)
                   // ->where("JobHeader.JobStatus","!=","2")
                   ->first()
                   ->toArray();

    return $result;

  }

  public function getCreateDate($jobId){

    $data = $this->getById($jobId);

    $createDate = [
      'CreateDateTime' => strtotime($data['CreateDateTime']) * 1000,
      'CreateDateTimeFormat' => date('d-F-Y H:i:s',strtotime($data['CreateDateTime']))
    ];

    return $createDate;

  }



}