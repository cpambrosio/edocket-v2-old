<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\RequestInterface;

use DB;

class EloquentRequest implements RequestInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $request;


  public function __construct( Model $request )
  {

    $this->request   = $request;

  }

  public function get($requestId){

    return $this->request->where('RequestID','=',$requestId)->get()->toArray();

  }

  public function create($data){

  	return $this->request->create($data);

  }

  public function update($data, $requestId){

    return $this->request->where('RequestId','=',$requestId)->update($data);

  }

  public function delete($requestId){

    return $this->request->where('RequestID','=',$requestId)->delete();

  }

  public function deleteByJobID($jobId){

    return $this->request->where('JobID','=',$jobId)->delete();

  }



}