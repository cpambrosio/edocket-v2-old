<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\PreSiteCheckListInterface;

use DB;

class EloquentPreSiteCheckList implements PreSiteCheckListInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $preSiteCheckList;


  public function __construct( Model $preSiteCheckList )
  {

    $this->preSiteCheckList   = $preSiteCheckList;

  }

  public function get($jobId){

    return $this->preSiteCheckList->where('JobID','=',$jobId)->get()->toArray();

  }

  public function create($data){

  	return $this->preSiteCheckList->create($data);
  }

  public function deleteByJobID($jobId){

    return $this->preSiteCheckList->where('JobID','=',$jobId)->delete();


  }



}