<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\DirectionInterface;

use DB;

class EloquentDirection implements DirectionInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $direction;


  public function __construct( Model $direction )
  {

    $this->direction   = $direction;

  }

  public function all(){

  	return $this->direction->get()->toArray();


  }

}