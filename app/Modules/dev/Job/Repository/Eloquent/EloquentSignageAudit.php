<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\SignageAuditInterface;

use DB;

class EloquentSignageAudit implements SignageAuditInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $signageAudit;


  public function __construct( Model $signageAudit )
  {

    $this->signageAudit   = $signageAudit;

  }

  public function get($jobId){

    return $this->signageAudit->where('JobID','=',$jobId)->get()->toArray();

  }

  public function create($data){

  	return $this->signageAudit->create($data);
  }

  public function deleteByJobID($jobId){

    return $this->signageAudit->where('JobID','=',$jobId)->delete();


  }



}