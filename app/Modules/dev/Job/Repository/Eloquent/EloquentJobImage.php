<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\JobImageInterface;

use DB;

class EloquentJobImage implements JobImageInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $jobImage;


  public function __construct( Model $jobImage )
  {

    $this->jobImage   = $jobImage;

  }

  public function get($jobId){

    return $this->jobImage->where('JobID','=',$jobId)->get()->toArray();

  }

  public function getPendingJobImage($jobId){

    return $this->jobImage
                ->where('JobID','=',$jobId)
                ->whereNull('Processed')
                ->orderBy('JobID', 'desc')
                ->get()
                ->toArray();

  }

  public function countPendingJobImage($jobId){

    return $this->jobImage
                ->where('JobID','=',$jobId)
                ->whereNull('Processed')
                ->count();

  }

  public function insert($data){

  	return $this->jobImage->insert($data);
  }

  public function updateByJobID($data, $jobId){

    return $this->jobImage->where('JobID','=',$jobId)->update($data);

  }

  public function deleteByJobID($jobId){

    return $this->jobImage->where('JobID','=',$jobId)->delete();


  }



}