<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\CarriageWayInterface;

use DB;

class EloquentCarriageWay implements CarriageWayInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $carriageWay;


  public function __construct( Model $carriageWay )
  {

    $this->carriageWay   = $carriageWay;

  }

  public function all(){

  	return $this->carriageWay->get()->toArray();


  }

}