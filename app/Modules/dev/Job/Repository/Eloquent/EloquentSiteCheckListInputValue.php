<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\SiteCheckListInputValueInterface;

use DB;

class EloquentSiteCheckListInputValue implements SiteCheckListInputValueInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $siteCheckListInputValue;


  public function __construct( Model $siteCheckListInputValue )
  {

    $this->siteCheckListInputValue   = $siteCheckListInputValue;

  }

  public function get($Id){

    return $this->siteCheckListInputValue->where('ID','=',$Id)->get()->toArray();

  }

  public function delete($Id){

    return $this->siteCheckListInputValue->where('ID','=',$Id)->delete();

  }



}