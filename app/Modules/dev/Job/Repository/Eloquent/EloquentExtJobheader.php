<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\ExtJobheaderInterface;

use DB;

class EloquentExtJobheader implements ExtJobheaderInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $extJobHeader;


  public function __construct( Model $extJobHeader)
  {

    $this->extJobHeader   = $extJobHeader;

  }

  public function all(){

  	return $this->extJobHeader->get()->toArray();

  }

  public function insert($data){

    return $this->extJobHeader->insert($data);

  }

  public function deleteByJobID($jobID){

    return $this->extJobHeader->where('ExtJobID','=',$jobID)->delete();

  }

  public function deleteByJobIDAndParentJob($jobID, $parentId){

    return $this->extJobHeader->where('ParentJob','=',$parentId)->delete();

  }

  public function count($jobId){

    return $this->extJobHeader
                ->where('ExtJobID','=',$jobId)
                ->count();

  }

  public function getByContact($contactId){

    $fields = array(
      "JobHeader.JobWorkNotes as AdditionalNotes",
      DB::raw("CAST(JobHeader.AuthorisedByID AS INT) as AuthorizerID"),
      "JobHeader.JobCode as Code",
      "JobHeader.ContactID",
      "JobHeader.JobDepot as DepotID",
      "JobShiftDetail.EndDateTime",
      "JobHeader.ForemanID",
      "App_ExtJobHeader.ExtJobID as Id",
      "App_ExtJobHeader.ParentJob as SplitParentJob",
      "JobHeader.Operator as OperatorID",
      "JobHeader.OrderNumber",
      "JobHeader.ParentJobID",
      "JobShiftDetail.StartDateTime",
      "JobHeader.StartTimeType as StartType",
      "App_ExtJobHeader.ExtJobID as JobID",
      "JobHeader.JobCode",
      DB::raw("(RTRIM(LTRIM(Contact.FirstName))+' '+RTRIM(LTRIM(Contact.LastName))) as ContactPerson"),
      DB::raw("(RTRIM(LTRIM(Authorized.FirstName))+' '+RTRIM(LTRIM(Authorized.LastName))) as AuthorizedPerson"),
      DB::raw("(RTRIM(LTRIM(JobHeader.JobAddressLine1))+' '+RTRIM(LTRIM(JobHeader.JobSuburb))+' '+RTRIM(LTRIM(JobHeader.JobState))) as Address"),
      DB::raw("(RTRIM(LTRIM(Contact.CompanyName))) as CompanyName"),
      DB::raw("Depot.Description as Depot"),
      DB::raw("JobShiftDetail.StartDateTime as StartDateTimeFormat"),
      DB::raw("JobShiftDetail.EndDateTime as EndDateTimeFormat"),
      "JobHeader.JobState as State"
    );

    return $this->extJobHeader
                ->join("JobHeader","JobHeader.JobID","=","App_ExtJobHeader.ParentJob")
                ->join("App_ExtTimesheet", function($join)
                {
                    $join->on("App_ExtJobHeader.ExtJobID", "=", "App_ExtTimesheet.ExtJobID")
                         ->whereNull('App_ExtTimesheet.deleted_at');
                })
                ->leftJoin("Contact","Contact.ContactID","=","JobHeader.ContactID")
                ->leftJoin("Contact as Authorized","Authorized.ContactID","=","JobHeader.ContactID")
                ->leftJoin("Lookup as Depot","Depot.LookupID","=","JobHeader.JobDepot")
                ->join("JobShiftDetail","JobShiftDetail.JobID","=","JobHeader.JobID")
                ->where('App_ExtTimesheet.WorkerID','=',$contactId)
                ->whereNull('App_ExtJobHeader.deleted_at')
                ->get($fields)->toArray();

  }


}