<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\ExtJobRequirementInterface;

use DB;

class EloquentExtJobRequirement implements ExtJobRequirementInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $extJobRequirement;
  protected $extJobHeader;


  public function __construct( Model $extJobRequirement, Model $extJobHeader )
  {

    $this->extJobRequirement   = $extJobRequirement;
    $this->extJobHeader   = $extJobHeader;

  }

  public function all(){

  	return $this->extJobRequirement->get()->toArray();

  }

  public function insert($data){

    return $this->extJobRequirement->insert($data);

  }

  public function deleteByJobID($jobID){

    return $this->extJobRequirement->where('ExtJobID','=',$jobID)->delete();

  }

  public function count($jobId){

    return $this->extJobRequirement
                ->where('ExtJobID','=',$jobId)
                ->count();

  }

  public function getByJobID($jobId){

    $fields = [
      'App_ExtJobRequirement.Notes as Description',
      'App_ExtJobRequirement.ExtJobID as JobID',
      'App_ExtJobRequirement.Qty as Quantity',
      'App_ExtJobRequirement.ItemID as RequirementID'
    ];

    return $this->extJobHeader
            ->join("JobHeader","JobHeader.JobID","=","App_ExtJobHeader.ParentJob")
            ->join('App_ExtJobRequirement','App_ExtJobHeader.ExtJobID','=','App_ExtJobRequirement.ExtJobID')
            ->where('App_ExtJobHeader.ExtJobID','=',$jobId)
            ->get($fields)->toArray();


  }


}