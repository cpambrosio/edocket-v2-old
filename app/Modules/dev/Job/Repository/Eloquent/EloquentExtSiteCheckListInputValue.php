<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\ExtSiteCheckListInputValueInterface;

use DB;

class EloquentExtSiteCheckListInputValue implements ExtSiteCheckListInputValueInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $extSiteCheckListInputValue;


  public function __construct( Model $extJobSiteCheckListInputValue )
  {

    $this->extSiteCheckListInputValue = $extJobSiteCheckListInputValue;

  }

  public function get($Id){

    return $this->extSiteCheckListInputValue->where('ID','LIKE',$Id)->get()->toArray();

  }

  public function all(){

  	return $this->extSiteCheckListInputValue->get()->toArray();

  }

  public function insert($data){

    return $this->extSiteCheckListInputValue->insert($data);

  }

  public function deleteByJobID($Id){

    return $this->extSiteCheckListInputValue->where('ID','=',$Id)->delete();

  }

  public function count($jobId){

    return $this->extSiteCheckListInputValue
                ->where('ExtJobID','=',$jobId)
                ->count();

  }


}