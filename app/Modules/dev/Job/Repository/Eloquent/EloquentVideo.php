<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\VideoInterface;

use DB;

class EloquentVideo implements VideoInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $video;


  public function __construct( Model $video )
  {

    $this->video   = $video;

  }

  public function get($videoId){

    return $this->video->where('VideoID','=',$videoId)->get()->toArray();

  }

  public function getPendingVideo(){

    return $this->video
                ->whereNull('Processed')
                ->orderBy('VideoID', 'desc')
                ->get()
                ->toArray();

  }

  public function countPendingVideo(){

    return $this->video
                ->whereNull('Processed')
                ->count();

  }

  public function insert($data){

  	return $this->video->create($data);
  }

  public function updateByVideoID($data, $videoId){

    return $this->video->where('VideoID','=',$videoId)->update($data);

  }

  public function deleteByVideoID($videoId){

    return $this->video->where('VideoID','=',$videoId)->delete();


  }



}