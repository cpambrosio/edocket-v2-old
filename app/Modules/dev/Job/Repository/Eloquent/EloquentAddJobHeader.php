<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\AddJobHeaderInterface;

use DB;

class EloquentAddJobHeader implements AddJobHeaderInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $addJobHeader;


  public function __construct( Model $addJobHeader )
  {

    $this->addJobHeader   = $addJobHeader;

  }

  public function get($jobId){

    return $this->addJobHeader->where('JobID','=',$jobId)->get()->toArray();

  }

  public function insert($data){

  	return $this->addJobHeader->insert($data);
  }

  public function deleteByJobID($jobId){

    return $this->addJobHeader->where('JobID','=',$jobId)->delete();


  }



}