<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\AddTimesheetInterface;

use DB;

class EloquentAddTimesheet implements AddTimesheetInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $addTimesheet;


  public function __construct( Model $addTimesheet )
  {

    $this->addTimesheet   = $addTimesheet;

  }

  public function get($jobId){

    return $this->addTimesheet->where('JobID','=',$jobId)->get()->toArray();

  }

  public function insert($data){

  	return $this->addTimesheet->insert($data);
  }

  public function deleteByJobID($jobId){

    return $this->addTimesheet->where('JobID','=',$jobId)->delete();


  }



}