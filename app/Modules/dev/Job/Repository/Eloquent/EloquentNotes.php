<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\NotesInterface;

use DB;

use Carbon\Carbon;

class EloquentNotes implements NotesInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $notes;


  public function __construct( Model $notes )
  {

    $this->notes   = $notes;

  }

  public function getCurrentNotes(){

    $fields = array(
      "HandoverLog.Notes",
      "HandoverLog.StartDate",
      "HandoverLog.EndDate"
    );


    $count = $this->notes
                   ->select($fields)
                   ->where("HandoverLog.StartDate","<=",Carbon::now()->format('Y-m-d H:i:s'))
                   ->where("HandoverLog.EndDate",">=",Carbon::now()->format('Y-m-d H:i:s'))
                   ->count();

    if( $count > 0 ){

      $result = $this->notes
                     ->select($fields)
                     ->where("HandoverLog.StartDate","<=",Carbon::now()->format('Y-m-d H:i:s'))
                     ->where("HandoverLog.EndDate",">=",Carbon::now()->format('Y-m-d H:i:s'))
                     ->first()
                     ->toArray();


      return $result;

    }
    else{

      return false;

    }


  }

}