<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\ContactPhoneMobileEmailInterface;

use DB;

class EloquentContactPhoneMobileEmail implements ContactPhoneMobileEmailInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $contactPhoneMobileEmail;


  public function __construct( Model $contactPhoneMobileEmail )
  {

    $this->contactPhoneMobileEmail   = $contactPhoneMobileEmail;

  }

  public function get($contactId){

    return $this->contactPhoneMobileEmail->where('ContactID','=',$contactId)->first()->toArray();

  }

  public function getContactInfo($contactId){

    $data = $this->get($contactId);

    $contactInfo = [
      'ContactMobilePhone' => $data['cMobilePhone'],
      'ContactPhone' => $data['cPhone'],
      'ContactEmail' => $data['cEmail']
    ];

    return $contactInfo;

  }
 

}