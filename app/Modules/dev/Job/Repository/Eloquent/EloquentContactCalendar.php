<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\ContactCalendarInterface;

use DB;

class EloquentContactCalendar implements ContactCalendarInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $contactCalendar;


  public function __construct( Model $contactCalendar )
  {

    $this->contactCalendar   = $contactCalendar;

  }

  public function all(){

    return $this->contactCalendar->all();

  }

  public function getRelatedJobs($contactID){

    $fields = array(
      "JobHeader.JobID",
      "JobHeader.JobCode",
      DB::raw("(RTRIM(LTRIM(Contact.FirstName))+' '+RTRIM(LTRIM(Contact.LastName))) as ContactPerson"),
      DB::raw("(RTRIM(LTRIM(Contact.CompanyName))) as CompanyName"),
      DB::raw("(RTRIM(LTRIM(Authorized.FirstName))+' '+RTRIM(LTRIM(Authorized.LastName))) as AuthorizedPerson"),
      "JobHeader.CreateTime",
      DB::raw("(RTRIM(LTRIM(JobHeader.JobAddressLine1))+' '+RTRIM(LTRIM(JobHeader.JobSuburb))+' '+RTRIM(LTRIM(JobHeader.JobState))) as Address"),
      "ContactCalendar.Status",
      DB::raw("Depot.Description as Depot"),
      "JobShiftDetail.StartDateTime"
    );

    $result = $this->contactCalendar
                   ->select($fields)
                   ->join("JobHeader","JobHeader.JobID","=","ContactCalendar.JobID")
                   ->join("Contact","Contact.ContactID","=","jobHeader.ContactID")
                   ->leftJoin("Contact as Authorized","Authorized.ContactID","=","JobHeader.ContactID")
                   ->leftJoin("Lookup as Depot","Depot.LookupID","=","JobHeader.JobDepot")
                   ->join("JobShiftDetail","JobShiftDetail.JobID","=","JobHeader.JobID")
                   ->where("ContactCalendar.ContactID","=",$contactID)
                   ->where("ContactCalendar.EntryType","=","1108")
                   // ->whereIn("ContactCalendar.Status",array('null',0,6))
                   ->get()
                   ->toArray();


    return $result;

  }

  public function getRoosteredContacts($id){

    $field = array(
      "Contact.ContactID",
      DB::raw("(RTRIM(LTRIM(Contact.FirstName))+' '+RTRIM(LTRIM(Contact.LastName))) as Name"),
      "JobShiftDetail.StartDateTime",
      "JobShiftDetail.EndDateTime",
      "Lookup.Description",
      "ContactCalendar.tsTravelTimeTo",
      "ContactCalendar.tsTravelTimeFrom",
      "ContactCalendar.tsStartDateTime",
      "ContactCalendar.tsEndDateTime",
      "ContactCalendar.tsMealStart",
      "ContactCalendar.tsMealFinish"
    );

    $result = $this->contactCalendar
                   ->select($field)
                   ->join("Contact","Contact.ContactID","=","ContactCalendar.ContactID")
                   ->leftJoin("JobShiftDetail", function($join)
                    {
                        $join->on("JobShiftDetail.JobID", "=", "ContactCalendar.JobID")
                             ->on("JobShiftDetail.ContactID","=","ContactCalendar.ContactID")
                             ->on("JobShiftDetail.ShiftID","=","ContactCalendar.ShiftInfoID");
                    })
                   ->join("Lookup","Lookup.LookupID","=","JobShiftDetail.JobStartType")
                   ->where("ContactCalendar.JobID","=",$id)
                   ->whereNotNull("DocketNo")
                   ->get($field)
                   ->toArray();

    return $result;

  }

  public function getTimesheet($contactId, $jobId){

    $field = array(
      DB::raw("(RTRIM(LTRIM(Contact.FirstName))+' '+RTRIM(LTRIM(Contact.LastName))) as Name"),
      "JobShiftDetail.StartDateTime",
      "JobShiftDetail.EndDateTime",
      "Lookup.Description",
      "ContactCalendar.tsTravelTimeTo",
      "ContactCalendar.tsTravelTimeFrom",
      "ContactCalendar.tsStartDateTime",
      "ContactCalendar.tsEndDateTime",
      "ContactCalendar.tsMealStart",
      "ContactCalendar.tsMealFinish"
    );

    $result = $this->contactCalendar
                   ->select($field)
                   ->join("Contact","Contact.ContactID","=","ContactCalendar.ContactID")
                   ->join("JobShiftDetail", function($join)
                    {
                        $join->on("JobShiftDetail.JobID", "=", "ContactCalendar.JobID")
                             ->on("JobShiftDetail.ContactID","=","ContactCalendar.ContactID")
                             ->on("JobShiftDetail.ShiftID","=","ContactCalendar.ShiftInfoID");
                    })
                   ->join("Lookup","Lookup.LookupID","=","JobShiftDetail.JobStartType")
                   ->where("Contact.ContactID","=",$contactId)
                   ->where("ContactCalendar.JobID","=",$jobId)
                   ->first();

    return $result;

  }

}