<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\JobSyncInterface;

use DB;

class EloquentJobSync implements JobSyncInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $jobSync;


  public function __construct( Model $jobSync )
  {

    $this->jobSync   = $jobSync;

  }

  public function get($jobSyncId){

    return $this->jobSync->where('JobSyncID','=',$jobSyncId)->get()->toArray();

  }

  public function getByJobID($jobId){

    return $this->jobSync->where('JobID','LIKE',$jobId)->get()->toArray();

  }

  public function updateByJobID($jobId,$data){

    return $this->jobSync->where('JobID','LIKE',$jobId)->update($data);

  }

  public function updateByJobIDIP($jobID,$ip,$data){

    return $this->jobSync->where('IP','=',$ip)->where('JobID','LIKE',$jobID)->update($data);

  }

  public function countByJobID($jobId){

    return $this->jobSync
                ->where('JobID','LIKE',$jobId)
                ->count();

  }

  public function countByJobIDIP($jobID,$ip){

    return $this->jobSync
                ->where('JobID','LIKE',$jobID)
                ->where('IP','=',$ip)
                ->count();

  }



  public function create($data){

  	return $this->jobSync->create($data);

  }

  public function delete($jobSyncId){

    return $this->jobSync->where('JobSyncID','=',$jobSyncId)->delete();

  }

  public function deleteByJobID($jobId){

    return $this->jobSync->where('JobID','LIKE',$jobId)->delete();

  }



}