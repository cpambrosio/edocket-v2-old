<?php namespace App\Modules\dev\Job\Repository\Eloquent;


use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\AuditSignsInterface;

use DB;

class EloquentAuditSigns implements AuditSignsInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $auditSigns;


  public function __construct( Model $auditSigns )
  {

    $this->auditSigns   = $auditSigns;

  }

  public function get($auditId){

    return $this->auditSigns->where('AuditID','=',$auditId)->get()->toArray();

  }

  public function create($data){

  	return $this->auditSigns->create($data);


  }

  public function delete($auditId){

    return $this->auditSigns->where('AuditID','=',$auditId)->delete();

  }

  public function deleteByJobID($jobId){

    return $this->auditSigns->where('JobID','=',$jobId)->delete();

  }



}