<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\JobShiftDetailInterface;

use DB;

class EloquentJobShiftDetail implements JobShiftDetailInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $jobShiftDetail;


  public function __construct( Model $jobShiftDetail )
  {

    $this->jobShiftDetail   = $jobShiftDetail;

  }


  public function all(){

    return $this->jobShiftDetail->all();

  }

  public function getUniqueById($id){

    $fields = array(
      "JobShiftDetail.JobID",
      "JobShiftDetail.StartDateTime",
      "JobShiftDetail.EndDateTime"
    );

    $result = $this->jobShiftDetail
                   ->where("JobShiftDetail.JobID","=",$id)
                   ->GroupBy("JobShiftDetail.JobID")
                   ->GroupBy("JobShiftDetail.StartDateTime")
                   ->GroupBy("JobShiftDetail.EndDateTime")
                   ->first($fields)
                   ->toArray();

    return $result;

  }

  public function getJobShiftDate($jobId){

    $data = $this->getUniqueById($jobId);
    
    $shiftDates = [
      'JobShiftStartDateTime' => strtotime($data['StartDateTime']) * 1000,
      'JobShiftStartDateTimeFormat' => date('d-F-Y H:i:s',strtotime($data['StartDateTime'])),
      'JobShiftEndDateTime' => strtotime($data['EndDateTime']) * 1000,
      'JobShiftEndDateTimeFormat' => date('d-F-Y H:i:s',strtotime($data['EndDateTime']))
    ];

    return  $shiftDates;

  }


}