<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\ExtJobSiteCheckListItemInterface;

use DB;

class EloquentExtJobSiteCheckListItem implements ExtJobSiteCheckListItemInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $extJobSiteCheckListItem;


  public function __construct( Model $extJobSiteCheckListItem )
  {

    $this->extJobSiteCheckListItem   = $extJobSiteCheckListItem;

  }

  public function get($Id){

    return $this->extJobSiteCheckListItem->where('ExtJobID','=',$Id)->get()->toArray();

  }

  public function all(){

  	return $this->extJobSiteCheckListItem->get()->toArray();

  }

  public function insert($data){

    return $this->extJobSiteCheckListItem->insert($data);

  }

  public function deleteByJobID($jobID){

    return $this->extJobSiteCheckListItem->where('ExtJobID','=',$jobID)->delete();

  }

  public function count($jobId){

    return $this->extJobSiteCheckListItem
                ->where('ExtJobID','=',$jobId)
                ->count();

  }


}