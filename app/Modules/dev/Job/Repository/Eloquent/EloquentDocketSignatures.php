<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\DocketSignaturesInterface;

use DB;

class EloquentDocketSignatures implements DocketSignaturesInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $docketSignatures;


  public function __construct( Model $docketSignatures )
  {

    $this->docketSignatures   = $docketSignatures;

  }

  public function get($jobId){

    return $this->docketSignatures->where('JobID','=',$jobId)->get()->toArray();

  }

  public function deleteByJobID($jobId){

    return $this->docketSignatures->where('JobID','=',$jobId)->delete();

  }



}