<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\JobSiteCheckListItemInterface;

use DB;

class EloquentJobSiteCheckListItem implements JobSiteCheckListItemInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $jobSiteCheckListItem;


  public function __construct( Model $jobSiteCheckListItem )
  {

    $this->jobSiteCheckListItem   = $jobSiteCheckListItem;

  }

  public function get($jobId){

    return $this->jobSiteCheckListItem->where('JobID','=',$jobId)->get()->toArray();

  }

  public function deleteByJobID($jobId){

    return $this->jobSiteCheckListItem->where('JobID','=',$jobId)->delete();

  }



}