<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\GenerateReportInterface;

use DB;

class EloquentGenerateReport implements GenerateReportInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $generateReport;


  public function __construct( Model $generateReport )
  {

    $this->generateReport   = $generateReport;

  }

  public function get($generateReportId){

    return $this->generateReport->where('GenerateReportID','=',$generateReportId)->get()->toArray();

  }

  public function getByJobID($jobId){

    return $this->generateReport->where('JobID','=',$jobId)->get()->toArray();

  }

  public function getInfoByJobID($jobId){

    return $this->generateReport->where('JobID','=',$jobId)->first()->toArray();

  }

  public function create($data){

  	return $this->generateReport->create($data);

  }

  public function update($data, $generateReportId){

    return $this->generateReport->where('GenerateReportId','=',$generateReportId)->update($data);

  }

  public function updateByJobID($data, $jobId){

    return $this->generateReport->where('JobID','=',$jobId)->update($data);

  }

  public function delete($generateReportId){

    return $this->generateReport->where('GenerateReportID','=',$generateReportId)->delete();

  }

  public function deleteByJobID($jobId){

    return $this->generateReport->where('JobID','=',$jobId)->delete();

  }

  public function countByJobID($jobId){

    return $this->generateReport
                ->where('JobID','=',$jobId)
                ->count();

  }

  public function countJobDocket($jobId){

    return $this->generateReport
                ->where('JobID','=',$jobId)
                ->where('JobDocket','=',1)
                ->count();

  }

  public function countSafetyDocket($jobId){

    return $this->generateReport
                ->where('JobID','=',$jobId)
                ->where('SafetyDocket','=',1)
                ->count();

  }

  public function countJobImage($jobId){

    return $this->generateReport
                ->where('JobID','=',$jobId)
                ->where('JobImage','=',1)
                ->count();

  }



}