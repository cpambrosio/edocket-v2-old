<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\JobClientNotifyInterface;

use DB;

class EloquentJobClientNotify implements JobClientNotifyInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $jobClientNotify;


  public function __construct( Model $jobClientNotify )
  {

    $this->jobClientNotify   = $jobClientNotify;

  }

  public function get($jobId){

    return $this->jobClientNotify->where('JobID','=',$jobId)->get()->toArray();

  }

  public function insert($data){

  	return $this->jobClientNotify->insert($data);
  }

  public function deleteByJobID($jobId){

    return $this->jobClientNotify->where('JobID','=',$jobId)->delete();


  }



}