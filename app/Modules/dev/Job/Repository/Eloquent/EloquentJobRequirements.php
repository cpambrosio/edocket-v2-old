<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\JobRequirementsInterface;

use DB;

class EloquentJobRequirements implements JobRequirementsInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $jobRequirements;

  public function __construct( Model $jobRequirements )
  {

    $this->jobRequirements   = $jobRequirements;

  }

  function getByJobId($JobId){

    $fields = array(
      "Lookup.Description",
      "JobRequirements.Qty"
    );

    $result = $this->jobRequirements
                   ->select($fields)
                   ->join("Lookup","Lookup.LookupID","=","JobRequirements.ItemID")
                   ->where("JobRequirements.JobID","=",$JobId)
                   ->get()
                   ->toArray();

    return $result;

  }

}