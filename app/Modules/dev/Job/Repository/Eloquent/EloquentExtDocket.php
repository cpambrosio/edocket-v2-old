<?php namespace App\Modules\dev\Job\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Job\Repository\Interfaces\ExtDocketInterface;

use DB;

class EloquentExtDocket implements ExtDocketInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $extDocket;


  public function __construct( Model $extDocket )
  {

    $this->extDocket   = $extDocket;

  }

  public function all(){

  	return $this->extDocket->get()->toArray();

  }

  public function insert($data){

    return $this->extDocket->insert($data);

  }

  public function deleteByJobID($jobID){

    return $this->extDocket->where('ExtJobID','=',$jobID)->delete();

  }

  public function count($jobId){

    return $this->extDocket
                ->where('ExtJobID','=',$jobId)
                ->count();

  }


}