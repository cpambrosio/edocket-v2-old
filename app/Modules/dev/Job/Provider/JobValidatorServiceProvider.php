<?php namespace App\Modules\dev\Job\Provider;

use App\Modules\dev\Job\Request\JobValidator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class JobValidatorServiceProvider extends ServiceProvider{

    public function boot()
    {

        //Custom Job Validation

        Validator::resolver(function($translator, $data, $rules, $messages)
        {

            return new JobValidator($translator, $data, $rules, $messages);
            
        });

    }

    public function register()
    {
    }
}