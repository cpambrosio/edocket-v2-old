<?php namespace App\Modules\dev\Job\Provider;

use Illuminate\Support\ServiceProvider;

use App\Modules\dev\Job\Repository\Eloquent\EloquentJobHeader;

use App\Modules\dev\Job\Repository\Eloquent\EloquentContactCalendar;

use App\Modules\dev\Job\Repository\Eloquent\EloquentJobRequirements;

use App\Modules\dev\Job\Repository\Eloquent\EloquentTCAttachment;

use App\Modules\dev\Job\Repository\Eloquent\EloquentSignageAudit;

use App\Modules\dev\Job\Repository\Eloquent\EloquentAuditSigns;

use App\Modules\dev\Job\Repository\Eloquent\EloquentSign;

use App\Modules\dev\Job\Repository\Eloquent\EloquentCarriageWay;

use App\Modules\dev\Job\Repository\Eloquent\EloquentDirection;

use App\Modules\dev\Job\Repository\Eloquent\EloquentExtJobheader;

use App\Modules\dev\Job\Repository\Eloquent\EloquentExtTimesheet;

use App\Modules\dev\Job\Repository\Eloquent\EloquentExtJobRequirement;

use App\Modules\dev\Job\Repository\Eloquent\EloquentExtDocket;

use App\Modules\dev\Job\Repository\Eloquent\EloquentExtJobSiteCheckListItem;

use App\Modules\dev\Job\Repository\Eloquent\EloquentExtSiteCheckListInputValue;

use App\Modules\dev\Job\Repository\Eloquent\EloquentAddContactCalendar;

use App\Modules\dev\Job\Repository\Eloquent\EloquentAddTimesheet;

use App\Modules\dev\Job\Repository\Eloquent\EloquentAddJobHeader;

use App\Modules\dev\Job\Repository\Eloquent\EloquentJobClientNotify;

use App\Modules\dev\Job\Repository\Eloquent\EloquentAddHazard;

use App\Modules\dev\Job\Repository\Eloquent\EloquentDocketSignatures;

use App\Modules\dev\Job\Repository\Eloquent\EloquentRequest;

use App\Modules\dev\Job\Repository\Eloquent\EloquentJobSync;

use App\Modules\dev\Job\Repository\Eloquent\EloquentJobSiteCheckListItem;

use App\Modules\dev\Job\Repository\Eloquent\EloquentSiteCheckListInputValue;

use App\Modules\dev\Job\Repository\Eloquent\EloquentContactPhoneMobileEmail;

use App\Modules\dev\Job\Repository\Eloquent\EloquentJobShiftDetail;

use App\Modules\dev\Job\Repository\Eloquent\EloquentJobImage;

use App\Modules\dev\Job\Repository\Eloquent\EloquentNotes;

use App\Modules\dev\Job\Repository\Eloquent\EloquentGenerateReport;

use App\Modules\dev\Job\Repository\Eloquent\EloquentVideo;

use App\Modules\dev\Job\Repository\Eloquent\EloquentPreSiteCheckList;

use App\Modules\dev\Job\Repository\Eloquent\EloquentPreSiteCheckListItem;

use App\Modules\dev\Job\Model\JobHeader;

use App\Modules\dev\Job\Model\ContactCalendar;

use App\Modules\dev\Job\Model\JobRequirements;

use App\Modules\dev\Job\Model\TCAttachment;

use App\Modules\dev\Job\Model\SignageAudit;

use App\Modules\dev\Job\Model\AuditSigns;

use App\Modules\dev\Job\Model\Sign;

use App\Modules\dev\Job\Model\CarriageWay;

use App\Modules\dev\Job\Model\Direction;

use App\Modules\dev\Job\Model\Notes;

use App\Modules\dev\Job\Model\GenerateReport;

use App\Modules\dev\Job\Model\ExtJobheader;

use App\Modules\dev\Job\Model\ExtTimesheet;

use App\Modules\dev\Job\Model\ExtJobRequirement;

use App\Modules\dev\Job\Model\ExtDocket;

use App\Modules\dev\Job\Model\ExtJobSiteCheckListItem;

use App\Modules\dev\Job\Model\ExtSiteCheckListInputValue;

use App\Modules\dev\Job\Model\AddContactCalendar;

use App\Modules\dev\Job\Model\AddTimesheet;

use App\Modules\dev\Job\Model\AddJobHeader;

use App\Modules\dev\Job\Model\JobClientNotify;

use App\Modules\dev\Job\Model\AddHazard;

use App\Modules\dev\Job\Model\DocketSignatures;

use App\Modules\dev\Job\Model\JobSiteCheckListItem;

use App\Modules\dev\Job\Model\SiteCheckListInputValue;

use App\Modules\dev\Job\Model\ContactPhoneMobileEmail;

use App\Modules\dev\Job\Model\JobShiftDetail;

use App\Modules\dev\Job\Model\Contact;

use App\Modules\dev\Job\Model\Request;

use App\Modules\dev\Job\Model\JobSync;

use App\Modules\dev\Job\Model\JobImage;

use App\Modules\dev\Job\Model\Video;

use App\Modules\dev\Job\Model\PreSiteCheckList;

use App\Modules\dev\Job\Model\PreSiteCheckListItem;



class JobServiceProvider extends ServiceProvider

{

  public function register()

  {

    $app = $this->app;


    // Job Header Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\JobHeaderInterface',function(){

      return new EloquentJobHeader(

        new JobHeader

        );

    });

    // TC Attachment Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\TCAttachmentInterface',function(){

      return new EloquentTCAttachment(

        new TCAttachment

        );

    });

    // Signage Audit Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\SignageAuditInterface',function(){

      return new EloquentSignageAudit(

        new SignageAudit

        );

    });

    // Sign Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\SignInterface',function(){

      return new EloquentSign(

        new Sign

        );

    });

    // CarriageWay Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\CarriageWayInterface',function(){

      return new EloquentCarriageWay(

        new CarriageWay

        );

    });

    // Direction Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\DirectionInterface',function(){

      return new EloquentDirection(

        new Direction

        );

    });

    // Notes Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\NotesInterface',function(){

      return new EloquentNotes(

        new Notes

        );

    });

    // GenerateReport Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\GenerateReportInterface',function(){

      return new EloquentGenerateReport(

        new GenerateReport

        );

    });

    // ExtJobHeader Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\ExtJobHeaderInterface',function(){

      return new EloquentExtJobHeader(

        new ExtJobHeader

        );

    });

    // ExtTimesheet Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\ExtTimesheetInterface',function(){

      return new EloquentExtTimesheet(

        new ExtTimesheet,
        new ExtJobHeader

        );

    });

    // ExtJobRequirement Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\ExtJobRequirementInterface',function(){

      return new EloquentExtJobRequirement(

        new ExtJobRequirement,
        new ExtJobHeader

        );

    });

    // ExtDocket Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\ExtDocketInterface',function(){

      return new EloquentExtDocket(

        new ExtDocket

        );

    });

    // ExtJobSiteCheckListItem Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\ExtJobSiteCheckListItemInterface',function(){

      return new EloquentExtJobSiteCheckListItem(

        new ExtJobSiteCheckListItem

        );

    });

    // ExtSiteCheckListInputValue Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\ExtSiteCheckListInputValueInterface',function(){

      return new EloquentExtSiteCheckListInputValue(

        new ExtSiteCheckListInputValue

        );

    });

    // AppContactCalendar Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\AddContactCalendarInterface',function(){

      return new EloquentAddContactCalendar(

        new AddContactCalendar

        );

    });

    // AppAddTimesheet Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\AddTimesheetInterface',function(){

      return new EloquentAddTimesheet(

        new AddTimesheet

        );

    });

    // AppAddJobHeader Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\AddJobHeaderInterface',function(){

      return new EloquentAddJobHeader(

        new AddJobHeader

        );

    });

    // AppJobClientNotify Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\JobClientNotifyInterface',function(){

      return new EloquentJobCLientNotify(

        new JobCLientNotify

        );

    });

    // AppHazard Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\AddHazardInterface',function(){

      return new EloquentAddHazard(

        new AddHazard

        );

    });

    // Audit Signs Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\AuditSignsInterface',function(){

      return new EloquentAuditSigns(

        new AuditSigns

        );

    });

    // Request Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\RequestInterface',function(){

      return new EloquentRequest(

        new Request

        );

    });

    // JobSync Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\JobSyncInterface',function(){

      return new EloquentJobSync(

        new JobSync

        );

    });

    // Docket Signatures Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\DocketSignaturesInterface',function(){

      return new EloquentDocketSignatures(

        new DocketSignatures

        );

    });

    // Job Site Checklist Item Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\JobSiteCheckListItemInterface',function(){

      return new EloquentJobSiteCheckListItem(

        new JobSiteCheckListItem

        );

    });

    // Site Check List Input Value Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\SiteCheckListInputValueInterface',function(){

      return new EloquentSiteCheckListInputValue(

        new SiteCheckListInputValue

        );

    });

    // Contact Calendar Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\ContactCalendarInterface',function(){

      return new EloquentContactCalendar(

        new ContactCalendar

        );

    });

    // Job Requirements Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\JobRequirementsInterface',function(){

      return new EloquentJobRequirements(

        new JobRequirements

        );

    });

    // Contact Phone Mobile Email Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\ContactPhoneMobileEmailInterface',function(){

      return new EloquentContactPhoneMobileEmail(

        new ContactPhoneMobileEmail

        );

    });

    // JobShiftDetail Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\JobShiftDetailInterface',function(){

      return new EloquentJobShiftDetail(

        new JobShiftDetail

        );

    });

    // AppJobImage Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\JobImageInterface',function(){

      return new EloquentJobImage(

        new JobImage

        );

    });


    // AppVideo Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\VideoInterface',function(){

      return new EloquentVideo(

        new Video

        );

    });

    // AppPreSiteCheckList Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\PreSiteCheckListInterface',function(){

      return new EloquentPreSiteCheckList(

        new PreSiteCheckList

        );

    });

    // AppPreSiteCheckListItem Repository

    $app->bind('App\Modules\dev\Job\Repository\Interfaces\PreSiteCheckListItemInterface',function(){

      return new EloquentPreSiteCheckListItem(

        new PreSiteCheckListItem

        );

    });


  }

}