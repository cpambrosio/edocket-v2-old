<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">

        th {
            font-weight: bold;
        }

    </style>
</head>
<body>
    <table>
        <tr>
            <th>FileName</th>
            <th>EVO_FORM_ID</th>
            <th>FormID</th>
            <th>Subject</th>
            <th>Note</th>
            <th>StartLocation</th>
            <th>EndLocation</th>
            <th>startCapture</th>
            <th>endCapture</th>
        </tr>

        @foreach( $videoData as $videoInfo )

        <tr>
            <td>{{{ isset( $videoInfo['fileName'] ) ? $videoInfo['fileName'] : '' }}}</td>
            <td>{{{ isset( $videoInfo['formId'] ) ? $videoInfo['formId'] : '' }}}</td>
            <td>VIDEO</td>
            <td>{{{ isset( $videoInfo['subject'] ) ? $videoInfo['subject'] : '' }}}</td>
            <td>{{{ isset( $videoInfo['note'] ) ? $videoInfo['note'] : '' }}}</td>
            <td>{{{ isset( $videoInfo['startLocation'] ) ? $videoInfo['startLocation'] : '' }}}</td>
            <td>{{{ isset( $videoInfo['endLocation'] ) ? $videoInfo['endLocation'] : '' }}}</td>
            <td>{{{ isset( $videoInfo['startCapture'] ) ? $videoInfo['startCapture'] : '' }}}</td>
            <td>{{{ isset( $videoInfo['endCapture'] ) ? $videoInfo['endCapture'] : '' }}}</td>
        </tr>

        @endforeach
    </table>
</body>
</html>