<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">

        th {
            font-weight: bold;
        }

    </style>
</head>
<body>
    <table>
        <tr>
            <th>FileName</th>
            <th>EVO_FORM_ID</th>
            <th>DSFOLDER</th>
            <th>JobNo.</th>
            <th>DocketNo.</th>
            <th>JobDateMonth</th>
            <th>JobDateYear</th>
            <th>ClientCode</th>
            <th>ClientName</th>
            <th>DSGroup</th>
            <th>Location1</th>
            <th>Location2</th>
            <th>Depot</th>
            <th>State</th>
            <th>P/Order#</th>
            <th>CLIENTSIGN</th>
            <th>Aftercare</th>
            <th>DOCKETTimeStamp</th>
            <th>DOCKETGeotag</th>

            <!-- Timesheet Rego -->
            <?php $count = 1; ?>

            @foreach( $timesheet as $timesheetInfo )
            
                @if( $timesheetInfo['RegoNo'] != "" )

                    <th>MV Rego#{{ $count }}</th>

                    <?php $count++; ?>

                @endif

            @endforeach

            @if( $count < 5 )

                @while( $count <= 5 )

                    <th>MV Rego#{{ $count }}</th>

                    <?php $count++; ?>

                @endwhile

            @endif


            <!-- Timesheet Contact Code -->
            <?php $count = 1; ?>

            @foreach( $timesheet as $timesheetInfo )

                @if( $count <= 10 )
                
                    <th>ETC#{{ $count }}</th>

                    <?php $count++; ?>

                @endif

            @endforeach

            @if( $count < 10 )

                @while( $count <= 10 )

                <th>ETC#{{ $count }}</th>

                <?php $count++; ?>

                @endwhile

            @endif


            <!-- Timesheet Contact Name -->
            <?php $count = 1; ?>

            @foreach( $timesheet as $timesheetInfo )

                @if( $count <= 10 )
            
                    <th>ETC{{ $count }}A</th>

                    <?php $count++; ?>

                @endif

            @endforeach

            @if( $count < 10 )

                @while( $count <= 10 )

                    <th>ETC{{ $count }}A</th>

                    <?php $count++; ?>

                @endwhile

            @endif

            <!-- Timesheet Shift Type -->
            <?php $count = 1; ?>

            @foreach( $timesheet as $timesheetInfo )

                @if( $count <= 10 )
            
                    <th>ETC{{ $count }}_S</th>

                    <?php $count++; ?>

                @endif

            @endforeach

            @if( $count < 10 )

                @while( $count <= 10 )

                    <th>ETC{{ $count }}_S</th>

                    <?php $count++; ?>

                @endwhile

            @endif

            <!-- Timesheet Attend Depot -->
            <?php $count = 1; ?>

            @foreach( $timesheet as $timesheetInfo )

                @if( $count <= 10 )
            
                    <th>ETC{{ $count }}_D</th>

                    <?php $count++; ?>

                @endif

            @endforeach

            @if( $count < 10 )

                @while( $count <= 10 )

                    <th>ETC{{ $count }}_D</th>

                    <?php $count++; ?>

                @endwhile

            @endif


            <!-- Timesheet Fatique Compliance -->
            <?php $count = 1; ?>

            @foreach( $timesheet as $timesheetInfo )

                @if( $count <= 10 )
            
                    <th>ETC{{ $count }}_C</th>

                    <?php $count++; ?>

                @endif

            @endforeach

            @if( $count < 10 )

                @while( $count <= 10 )

                    <th>ETC{{ $count }}_C</th>

                    <?php $count++; ?>

                @endwhile

            @endif


            <th>ShiftLength</th>

        </tr>
        <tr>
            <td>{{ $fileName }}</td>
            <td>{{ $formId }}</td>
            <td>Job Docket</td>
            <td>{{ $jobNo }}</td>
            <td>{{ $jobID }}</td>
            <td>{{ $jobDateMonth }}</td>
            <td>{{ $jobDateYear  }}</td>
            <td>{{ $clientCode }}</td>
            <td>{{ $clientName }}</td>
            <td>&nbsp;</td>
            <td>{{ $location1 }}</td>
            <td>&nbsp;</td>
            <td>{{ $depot }}</td>
            <td>{{ $state }}</td>
            <td>{{ $orderNo }}</td>
            <td>{{ $clientSign }}</td>
            <td>{{ $afterCare }}</td>
            <td>{{ $swmsTimeStamp }}</td>
            <td>{{ $swmsGeotag }}</td>

            <!-- Timesheet Rego -->
            <?php $count = 1; ?>

            @if( isset( $timesheet ) )

                @foreach( $timesheet as $timesheetInfo )
                
                    @if( $timesheetInfo['RegoNo'] != "" && $count <= 5 )

                        <td>{{ $timesheetInfo['RegoNo'] }}</td>

                        <?php $count++; ?>

                    @endif

                @endforeach

            @endif

            @if( $count < 5 )

                @while( $count <= 5 )

                    <td>&nbsp;</td>

                    <?php $count++; ?>

                @endwhile

            @endif


             <!-- Timesheet Contact Code -->
            <?php $count = 1; ?>

            @if( isset( $timesheet ) )

                @foreach( $timesheet as $timesheetInfo )

                    @if( $count <= 10 )
                
                        <td>{{ $timesheetInfo['ContactCode'] }}</td>

                        <?php $count++; ?>

                    @endif

                @endforeach

            @endif

            @if( $count < 10 )

                @while( $count <= 10 )

                    <td>&nbsp;</td>

                    <?php $count++; ?>

                @endwhile

            @endif

            <!-- Timesheet Contact Name -->
            <?php $count = 1; ?>

            @if( isset( $timesheet ) )

                @foreach( $timesheet as $timesheetInfo )

                    @if( $count <= 10 )
                
                        <td>{{ $timesheetInfo['ContactName'] }}</td>

                        <?php $count++; ?>

                    @endif

                @endforeach

            @endif

            @if( $count < 10 )

                @while( $count <= 10 )

                    <td>&nbsp;</td>

                    <?php $count++; ?>

                @endwhile

            @endif


            <!-- Timesheet Shift Type -->
            <?php $count = 1; ?>

            @if( isset( $timesheet ) )

                @foreach( $timesheet as $timesheetInfo )

                    @if( $count <= 10 )
                
                        <td>{{ $timesheetInfo['ShiftType'] }}</td>

                        <?php $count++; ?>

                    @endif

                @endforeach

            @endif

            @if( $count < 10 )

                @while( $count <= 10 )

                    <td>&nbsp;</td>

                    <?php $count++; ?>

                @endwhile

            @endif


            <!-- Timesheet Attend Depot -->
            <?php $count = 1; ?>

            @if( isset( $timesheet ) )

                @foreach( $timesheet as $timesheetInfo )

                    @if( $count <= 10 )
                
                        <td>{{ $timesheetInfo['AttendDepot'] }}</td>

                        <?php $count++; ?>

                    @endif

                @endforeach

            @endif

            @if( $count < 10 )

                @while( $count <= 10 )

                    <td>&nbsp;</td>

                    <?php $count++; ?>

                @endwhile

            @endif


            <!-- Timesheet FatigueCompliance -->
            <?php $count = 1; ?>

            @if( isset( $timesheet ) )

                @foreach( $timesheet as $timesheetInfo )

                    @if( $count <= 10 )
                
                        <td>{{ $timesheetInfo['FatigueCompliance'] }}</td>

                        <?php $count++; ?>

                    @endif

                @endforeach

            @endif

            @if( $count < 10 )

                @while( $count <= 10 )

                    <td>&nbsp;</td>

                    <?php $count++; ?>

                @endwhile

            @endif

            <td>{{ $shiftLength }}</td>

        </tr>
    </table>
</body>
</html>