<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">

        th {
            font-weight: bold;
        }

    </style>
</head>
<body>
    <table>
        <tr>
            <th>FileName</th>
            <th>FormID</th>
            <th>JobNo.</th>
            <th>DocketNo.</th>
            <th>JobDateMonth</th>
            <th>JobDateYear</th>
            <th>ClientCode</th>
            <th>ClientName</th>
            <th>Location1</th>
            <th>Location2</th>
            <th>Depot</th>
            <th>State</th>
            <th>P/Order#</th>
            <th>CLIENTSIGN</th>
            <th>Aftercare</th>
            <th>SWMSTimeStamp</th>
            <th>SWMSUser</th>
            <th>SQMSGeotag</th>

            <!-- Timesheet Rego -->
            <?php $count = 1; ?>

            @foreach( $timesheet as $timesheetInfo )
            
            @if( $timesheetInfo['RegoNo'] != "" )

            <th>MV Rego#{{ $count }}</th>

            <?php $count++; ?>

            @endif

            @endforeach

            <!-- Requirements -->
            <?php $count = 1; ?>

            @foreach( $requirement as $requirementInfo )
            
            <th>Requirement {{ $count }}</th>

            <?php $count++; ?>

            @endforeach


            <!-- Timesheet Contact Code -->
            <?php $count = 1; ?>

            @foreach( $timesheet as $timesheetInfo )
            
            <th>ETC#{{ $count }}</th>

            <?php $count++; ?>

            @endforeach


            <!-- Timesheet Contact Name -->
            <?php $count = 1; ?>

            @foreach( $timesheet as $timesheetInfo )
            
            <th>ETC{{ $count }}A</th>

            <?php $count++; ?>

            @endforeach


            <!-- Timesheet Shift Type -->
            <?php $count = 1; ?>

            @foreach( $timesheet as $timesheetInfo )
            
            <th>ETC{{ $count }}_S</th>

            <?php $count++; ?>

            @endforeach

            <!-- Timesheet Attend Depot -->
            <?php $count = 1; ?>

            @foreach( $timesheet as $timesheetInfo )
            
            <th>ETC{{ $count }}_D</th>

            <?php $count++; ?>

            @endforeach


            <!-- Site Audit Road Config -->
            <?php $count = 1; ?>

            @foreach( $siteaudit['Normal_Road_Configuration'] as $roadConfigInfo )
            
            <th>R_CONFIG_{{ $count }} D</th>

            <?php $count++; ?>

            @endforeach


            <!-- Site Audit Onsite Condition -->
            <?php $count = 1; ?>

            @foreach( $siteaudit['Onsite_Conditions'] as $conditionInfo )
            
            <th>CONDTN_{{ $count }} D</th>

            <?php $count++; ?>

            @endforeach


            <!-- Site Audit Traffic Control Requirements -->
            @foreach( $siteaudit['Traffic_Control_Requirements'] as $conditionInfo )
            
            <th>CONDTN_{{ $count }} D</th>

            <?php $count++; ?>

            @endforeach


            <th>SWMS 1</th>
            <th>SWMS 1C</th>

            <th>SWMS 2</th>
            <th>SWMS 2C</th>

            <th>SWMS 3</th>
            <th>SWMS 3C</th>

            <th>SWMS 4</th>
            <th>SWMS 4C</th>

            <th>SWMS 5</th>
            <th>SWMS 5C</th>

            <th>SWMS 6</th>
            <th>SWMS 6C</th>

            <th>SWMS 7</th>
            <th>SWMS 7C</th>

            <th>SWMS 8</th>
            <th>SWMS 8C</th>

            <th>SWMS 9</th>
            <th>SWMS 9C</th>

            <th>SWMS 10</th>
            <th>SWMS 10C</th>

            <th>SWMS 11</th>
            <th>SWMS 11C</th>

            <th>SWMS 12</th>
            <th>SWMS 12C</th>

            <th>SWMS 13</th>

            <!-- Site Audit Traffic Control Requirements -->
            <?php $count = 1; ?>

            @foreach( $additionalHazards as $hazardInfo )
            
            <th>SWMS 13C{{ $count }}</th>

            <?php $count++; ?>

            <th>SWMS 13C{{ $count }}</th>

            <?php $count++; ?>

            <th>SWMS 13C{{ $count }}</th>

            <?php $count++; ?>

            <th>SWMS 13C{{ $count }}</th>

            <?php $count++; ?>

            @endforeach

            <th>TIME ERECTED</th>
            <th>TIME COLLECTED</th>
            <th>SIGNCHECK 1</th>
            <th>SIGNCHECK 2</th>
            <th>SIGNCHECK 3</th>
            <th>SIGNCHECK 4</th>

            <!-- Job Images -->
            <?php $count = 1; ?>

            @foreach( $jobImages as $imageInfo )
            
            <th>JOBIMAGE_{{ $count }}</th>

            <?php $count++; ?>

            @endforeach

        </tr>
        <tr>

            <td>{{ $fileName }}</td>
            <td>{{ $formId }}</td>
            <td>{{ $jobNo }}</td>
            <td>{{ $jobID }}</td>
            <td>{{ $jobDateMonth }}</td>
            <td>{{ $jobDateYear }}</td>
            <td>{{ $clientCode }}</td>
            <td>{{ $clientName }}</td>
            <td>{{ $location1 }}</td>
            <td>&nbsp;</td>
            <td>{{ $depot }}</td>
            <td>{{ $state }}</td>
            <td>{{ $orderNo }}</td>
            <td>{{ $clientSign }}</td>
            <td>{{ $afterCare }}</td>
            <td>{{ $swmsTimeStamp }}</td>
            <td>{{ $swmsUser }}</td>
            <td>{{ $swmsGeotag }}</td>

            <!-- Timesheet Rego -->
            @foreach( $timesheet as $timesheetInfo )
            
            @if( $timesheetInfo['RegoNo'] != "" )

            <td>{{ $timesheetInfo['RegoNo'] }}</td>

            @endif

            @endforeach


            <!-- Requirements -->
            @foreach( $requirement as $requirementInfo )
            
            <td>{{ $requirementInfo['Notes'] }}</td>

            @endforeach


            <?php $tcCount = 0; ?>

             <!-- Timesheet Contact Code -->
            @foreach( $timesheet as $timesheetInfo )

                  @if( $tcCount <= 1 )
            
                        <td>{{ $timesheetInfo['ContactCode'] }}</td>

                  @else

                        <td></td>

                  @endif


                  <?php $tcCount++; ?>

            @endforeach


            <?php $tcCount = 0; ?>

            <!-- Timesheet Contact Name -->
            @foreach( $timesheet as $timesheetInfo )

                  @if( $tcCount <= 1 )
            
                        <td>{{ $timesheetInfo['ContactName'] }}</td>

                  @else

                        <td></td>

                  @endif

                  <?php $tcCount++; ?>
            
            @endforeach


            <?php $tcCount = 0; ?>

            <!-- Timesheet Shift Type -->
            @foreach( $timesheet as $timesheetInfo )

                  @if( $tcCount <= 1 )
            
                         <td>{{ $timesheetInfo['ShiftType'] }}</td>

                  @else

                        <td></td>

                  @endif

                  <?php $tcCount++; ?>

            @endforeach


            <?php $tcCount = 0; ?>

            <!-- Timesheet Attend Depot -->
            @foreach( $timesheet as $timesheetInfo )

                  @if( $tcCount <= 1 )
            
                        <td>{{ $timesheetInfo['AttendDepot'] }}</td>

                  @else

                        <td></td>

                  @endif

                  <?php $tcCount++; ?>

            @endforeach

            <!-- Site Audit Road Config -->
            @foreach( $siteaudit['Normal_Road_Configuration'] as $key => $answer )
            
            <td>{{ $answer }}</td>

            @endforeach

            <!-- Site Audit Onsite Condition -->
            @foreach( $siteaudit['Onsite_Conditions'] as $key => $answer )
            
            <td>{{ $answer }}</td>

            @endforeach

            <!-- Site Audit Traffic Control Requirements -->
            @foreach( $siteaudit['Traffic_Control_Requirements'] as $key => $answer )
            
            <td>{{ $answer }}</td>

            @endforeach


            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['All_control_measures_as_per_SWMS_in_place'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['All_control_measures_as_per_SWMS_in_place_Note'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Signage_erected_as_per_SWMS,_Manual_diagram,_Traffic_Control_Plan_-_TCP_No.'] }}</td>

            @if( $siteaudit['Control_Measures_as_per_SWMS']['Signage_erected_as_per_SWMS,_Manual_diagram,_Traffic_Control_Plan_-_TCP_No.'] == "Yes" )

                <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['If_Yes,_please_enter_the_appropriate_diagram_/_TCP_No.'] }}</td>

            @else

                <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Signage_erected_as_per_SWMS,_Manual_diagram,_Traffic_Control_Plan_-_TCP_No._Note'] }}</td>

            @endif

            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Has_conflicting_signage_been_covered_or_removed'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Has_conflicting_signage_been_covered_or_removed_Note'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Are_signs_securely_mounted_and_visible_to_traffic'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Are_signs_securely_mounted_and_visible_to_traffic_Note'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Traffic_Controller_Ahead_/_Prepare_to_stop_sign_erected'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Traffic_Controller_Ahead_/_Prepare_to_stop_sign_erected_Note'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['PPE_is_being_worn_as_instructed_and_as_per_SWMS'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['PPE_is_being_worn_as_instructed_and_as_per_SWMS_Note'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Stop_slow_bat_and_hand_signals_to_be_used_to_control_traffic'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Stop_slow_bat_and_hand_signals_to_be_used_to_control_traffic_Note'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Each_TC_maintains_an_escape_route_at_all_times'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Each_TC_maintains_an_escape_route_at_all_times_Note'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['TC_to_stand_facing_traffic_and_outside_projected_travel_path'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['TC_to_stand_facing_traffic_and_outside_projected_travel_path_Note'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Access_provided_for_cyclists,_pedestrians,_wheelchairs_and_driveways'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Access_provided_for_cyclists,_pedestrians,_wheelchairs_and_driveways_Note'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Maintain_safe_distance_from_all_plant_and_equipment'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Maintain_safe_distance_from_all_plant_and_equipment_Note'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Is_there_sufficient_room_to_queue_stopped_vehicles'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Is_there_sufficient_room_to_queue_stopped_vehicles_Note'] }}</td>
            <td>{{ $siteaudit['Control_Measures_as_per_SWMS']['Environment_-_Is_there_any_environmental_risks'] }}</td>

            <!-- Additional Hazard -->
            @foreach( $additionalHazards as $hazardInfo )
            
            <td>{{ $hazardInfo['AddHazard'] }}</td>
            <td>{{ $hazardInfo['InitialRisk'] }}</td>
            <td>{{ $hazardInfo['ControlMeasures'] }}</td>
            <td>{{ $hazardInfo['ResidualRisk'] }}</td>

            @endforeach

            <td>{{ date('H:i',strtotime($signageAudit['TimeErected'])) }}</td>
            <td>{{ date('H:i',strtotime($signageAudit['TimeCollected'])) }}</td>
            <td>{{ date('H:i',strtotime($signageAudit['TimeChecked1'])) }}</td>
            <td>{{ date('H:i',strtotime($signageAudit['TimeChecked2'])) }}</td>
            <td>{{ date('H:i',strtotime($signageAudit['TimeChecked3'])) }}</td>
            <td>{{ date('H:i',strtotime($signageAudit['TimeChecked4'])) }}</td>

            <!-- Job Images -->
            @foreach( $jobImages as $imageInfo )
            
            <td>{{ $imageInfo['JobImage'] }}</td>

            @endforeach

        </tr>
    </table>
</body>
</html>