<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">

        th {
            font-weight: bold;
        }

    </style>
</head>
<body>
    <table>
        <tr>
            <th>JobNo.</th>
            <th>DocketNo.</th>
            <th>RegNo</th>
            <th>ContactCode</th>
            <th>ContactName</th>
            <th>ShiftType</th>
            <th>AttendDepot</th>
            <th>JobStartDateTime</th>
            <th>JobFinishDateTime</th>
            <th>TravelStartDateTime</th>
            <th>TravelStartDateTimeEnd</th>
            <th>TravelFinishDateTimeStart</th>
            <th>TravelFinishDateTime</th>
            <th>BreakStartDateTime</th>
            <th>BreakFinishDateTime</th>
            <th>TravelStartDistance</th>
            <th>TravelFinishDistance</th>
            <th>FatigueCompliance</th>
            <th>TraveledKilometers</th>
        </tr>

        @if( isset( $timesheet ) )

            @foreach( $timesheet as $timesheetInfo )

            <tr>

                <td>{{ $jobNo }}</td>
                <td>{{ $jobID }}</td>
                <td>{{ $timesheetInfo['RegoNo'] }}</td>
                <td>{{ $timesheetInfo['ContactCode'] }}</td>
                <td>{{ $timesheetInfo['ContactName'] }}</td>
                <td>{{ $timesheetInfo['ShiftType'] }}</td>
                <td>{{ $timesheetInfo['AttendDepot'] }}</td>
                <td>{{ $timesheetInfo['JobStartDateTime'] }}</td>
                <td>{{ $timesheetInfo['JobFinishDateTime'] }}</td>
                <td>{{ $timesheetInfo['TravelStartDateTime'] }}</td>
                <td>{{ $timesheetInfo['TravelStartDateTimeEnd'] }}</td>
                <td>{{ $timesheetInfo['TravelFinishDateTimeStart'] }}</td>
                <td>{{ $timesheetInfo['TravelFinishDateTime'] }}</td>
                <td>{{ $timesheetInfo['BreakStartDateTime'] }}</td>
                <td>{{ $timesheetInfo['BreakFinishDateTime'] }}</td>
                <td>{{ $timesheetInfo['TravelStartDistance'] }}</td>
                <td>{{ $timesheetInfo['TravelFinishDistance'] }}</td>
                <td>{{ $timesheetInfo['FatigueCompliance'] }}</td>
                <td>{{ $timesheetInfo['TraveledKilometers'] }}</td>

            </tr>

            @endforeach

        @endif

    </table>
</body>
</html>