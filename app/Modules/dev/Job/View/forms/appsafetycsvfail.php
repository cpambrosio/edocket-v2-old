<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">

        th {
            font-weight: bold;
        }

    </style>
</head>
<body>
    <table>
        <tr>
            <th>FileName</th>
            <th>EVO_FORM_ID</th>
            <th>DSFOLDER</th>
            <th>JobNo.</th>
            <th>DocketNo.</th>
            <th>JobDateMonth</th>
            <th>JobDateYear</th>
            <th>ClientCode</th>
            <th>ClientName</th>
            <th>DSGroup</th>
            <th>Location1</th>
            <th>Location2</th>
            <th>Depot</th>
            <th>State</th>
            <th>P/Order#</th>
            <th>CLIENTSIGN</th>
            <th>Aftercare</th>
            <th>SWMSTimeStamp</th>
            <th>SWMSUser</th>
            <th>SWMSGeotag</th>

            <!-- Timesheet Rego -->
            <?php $count = 1; ?>

            @foreach( $timesheet as $timesheetInfo )
            
                @if( $timesheetInfo['RegoNo'] != "" )

                    <th>MV Rego#{{ $count }}</th>

                    <?php $count++; ?>

                @endif

            @endforeach

            @if( $count < 5 )

                @while( $count <= 5 )

                    <th>MV Rego#{{ $count }}</th>

                    <?php $count++; ?>

                @endwhile

            @endif


            <!-- Timesheet Contact Code -->
            <?php $count = 1; ?>

            @foreach( $timesheet as $timesheetInfo )

                @if( $count <= 10 )
                
                    <th>ETC#{{ $count }}</th>

                    <?php $count++; ?>

                @endif

            @endforeach

            @if( $count < 10 )

                @while( $count <= 10 )

                <th>ETC#{{ $count }}</th>

                <?php $count++; ?>

                @endwhile

            @endif


            <!-- Timesheet Contact Name -->
            <?php $count = 1; ?>

            @foreach( $timesheet as $timesheetInfo )

                @if( $count <= 10 )
            
                    <th>ETC{{ $count }}A</th>

                    <?php $count++; ?>

                @endif

            @endforeach

            @if( $count < 10 )

                @while( $count <= 10 )

                    <th>ETC{{ $count }}A</th>

                    <?php $count++; ?>

                @endwhile

            @endif

            <!-- Site Audit Road Config -->
            <th>R_CONFIG_1 D</th>
            <th>R_CONFIG_1 D2</th>
            <th>R_CONFIG_2 D</th>
            <th>R_CONFIG_2 D2</th>
            <th>R_CONFIG_2 D3</th>
            <th>R_CONFIG_2 D4</th>
            <th>R_CONFIG_2 D5</th>
            <th>R_CONFIG_3 D</th>
            <th>R_CONFIG_4 D</th>
            <th>R_CONFIG_5 D</th>

            <!-- Site Audit Onsite Condition -->
            <?php /* $count = 1; ?>

            @foreach( $siteaudit['Onsite_Conditions'] as $conditionInfo )
            
            <th>CONDTN_{{ $count }} D</th>

            <?php $count++; ?>

            @endforeach

            <?php */ ?>

            <th>CONDTN_1 D</th>

            <th>CONDTN_2 D</th>


            <!-- Site Audit Traffic Control Requirements -->
            <?php /* ?>
            @foreach( $siteaudit['Traffic_Control_Requirements'] as $conditionInfo )
            
            <th>CONDTN_{{ $count }} D</th>

            <?php $count++; ?>

            @endforeach
            <?php */ ?>

            <th>CONDTN_1 D</th>

            <th>CONDTN_2 D</th>

            <th>CONDTN_3 D</th>

            <!-- Site Audit SWMS -->
            <th>SWMS 1</th>
            <th>SWMS 1C</th>

            <th>SWMS 2</th>
            <th>SWMS 2C</th>

            <th>SWMS 3</th>
            <th>SWMS 3C</th>

            <th>SWMS 4</th>
            <th>SWMS 4C</th>

            <th>SWMS 5</th>
            <th>SWMS 5C</th>

            <th>SWMS 6</th>
            <th>SWMS 6C</th>

            <th>SWMS 7</th>
            <th>SWMS 7C</th>

            <th>SWMS 8</th>
            <th>SWMS 8C</th>

            <th>SWMS 9</th>
            <th>SWMS 9C</th>

            <th>SWMS 10</th>
            <th>SWMS 10C</th>

            <th>SWMS 11</th>
            <th>SWMS 11C</th>

            <th>SWMS 12</th>
            <th>SWMS 12C</th>

            <th>SWMS 13</th>

            <!-- Site Audit Additional Hazard -->

            <th>SWMS 13.1a</th>
            <th>SWMS 13.1b</th>

            <th>SWMS 13.2a</th>
            <th>SWMS 13.2b</th>

            <th>SWMS 13.3a</th>
            <th>SWMS 13.3b</th>

            <th>SWMS 13.4a</th>
            <th>SWMS 13.4b</th>

            <th>SWMS 13.5a</th>
            <th>SWMS 13.5b</th>

            <th>TIME ERECTED</th>
            <th>WHO ERECTED</th>
            <th>TIME COLLECTED</th>
            <th>WHO COLLECTED</th>
            <th>SIGNCHECK 1</th>
            <th>SIGNCHECK 2</th>
            <th>SIGNCHECK 3</th>
            <th>SIGNCHECK 4</th>
            <th>ShiftLength</th>

        </tr>
        <tr>
            <td>{{{ isset( $fileName ) ? $fileName : '' }}}</td>
            <td>{{{ isset( $formId ) ? $formId : '' }}}</td>
            <td>SAFETY DOCKET</td>
            <td>{{{ isset( $jobNo ) ? $jobNo : '' }}}</td>
            <td>{{{ isset( $jobID ) ? $jobID : '' }}}</td>
            <td>{{{ isset( $jobDateMonth ) ? $jobDateMonth : '' }}}</td>
            <td>{{{ isset( $jobDateYear ) ? $jobDateYear : '' }}}</td>
            <td>{{{ isset( $clientCode ) ? $clientCode : '' }}}</td>
            <td>{{{ isset( $clientName ) ? $clientName : '' }}}</td>
            <td>&nbsp;</td>
            <td>{{{ isset( $location1 ) ? $location1 : '' }}}</td>
            <td>&nbsp;</td>
            <td>{{{ isset( $depot ) ? $depot : '' }}}</td>
            <td>{{{ isset( $state ) ? $state : '' }}}</td>
            <td>{{{ isset( $orderNo ) ? $orderNo : '' }}}</td>
            <td>{{{ isset( $clientSign ) ? $clientSign : '' }}}</td>
            <td>{{{ isset( $afterCare ) ? $afterCare : '' }}}</td>
            <td>{{{ isset( $swmsTimeStamp ) ? $swmsTimeStamp : '' }}}</td>
            <td>{{{ isset( $swmsUser ) ? $swmsUser : '' }}}</td>
            <td>{{{ isset( $swmsGeotag ) ? $swmsGeotag : '' }}}</td>

            <!-- Timesheet Rego -->
            <?php $count = 1; ?>

            @if( isset( $timesheet ) )

                @foreach( $timesheet as $timesheetInfo )
                
                    @if( $timesheetInfo['RegoNo'] != "" && $count <= 5 )

                        <td>{{ $timesheetInfo['RegoNo'] }}</td>

                        <?php $count++; ?>

                    @endif

                @endforeach

            @endif

            @if( $count < 5 )

                @while( $count <= 5 )

                    <td>&nbsp;</td>

                    <?php $count++; ?>

                @endwhile

            @endif



            <!-- Timesheet Contact Code -->
            <?php $count = 1; ?>

            @if( isset( $timesheet ) )

                @foreach( $timesheet as $timesheetInfo )

                    @if( $count <= 10 )
                
                        <td>{{ $timesheetInfo['ContactCode'] }}</td>

                        <?php $count++; ?>

                    @endif

                @endforeach

            @endif

            @if( $count < 10 )

                @while( $count <= 10 )

                    <td>&nbsp;</td>

                    <?php $count++; ?>

                @endwhile

            @endif


            <!-- Timesheet Contact Name -->
            <?php $count = 1; ?>

            @if( isset( $timesheet ) )

                @foreach( $timesheet as $timesheetInfo )

                    @if( $count <= 10 )
                
                        <td>{{ $timesheetInfo['ContactName'] }}</td>

                        <?php $count++; ?>

                    @endif

                @endforeach

            @endif

            @if( $count < 10 )

                @while( $count <= 10 )

                    <td>&nbsp;</td>

                    <?php $count++; ?>

                @endwhile

            @endif

            <!-- Site Audit Road Config -->
            <td>
                {{{ isset( $siteaudit['Type_of_Road'] ) ? $siteaudit['Type_of_Road'] : '' }}}
            </td>

            <td>
                {{{ isset( $siteaudit['No._of_lanes_(Multilane_only)'] ) ? $siteaudit['No._of_lanes_(Multilane_only)'] : '' }}}
            </td>

            <?php 

                $roadConfig = [];

                if( isset( $siteaudit['Road_Configuration'] ) ){

                    $roadConfig = explode('|',$siteaudit['Road_Configuration']);
                    
                }

                $count = 1; 
            ?>

            @foreach( $roadConfig as $roadConfigInfo )

                @if( $count <= 5 )

                    <td>{{ $roadConfigInfo }}</td>

                    <?php $count++; ?>

                @endif

            @endforeach

            @if( $count < 5 )

                @while( $count <= 5 )

                    <td>&nbsp;</td>

                    <?php $count++; ?>

                @endwhile

            @endif

            <td>
                {{{ isset( $siteaudit['Road_Surface'] ) ? $siteaudit['Road_Surface'] : '' }}}
            </td>

            <td>
                {{{ isset( $siteaudit['Normal_Road_Speed_Limit'] ) ? $siteaudit['Normal_Road_Speed_Limit'] : '' }}}
            </td>

            <td>
                {{{ isset( $siteaudit['Signs_/_Signals'] ) ? $siteaudit['Signs_/_Signals'] : '' }}}
            </td>


            <!-- Site Audit Onsite Condition -->
{{--             @if( isset($siteaudit['Onsite_Conditions']) )
                @foreach( $siteaudit['Onsite_Conditions'] as $key => $answer )
                
                    <td>{{ $answer }}</td>

                @endforeach
            @endif --}}

            <td>
                {{{ isset( $siteaudit['Visibility'] ) ? $siteaudit['Visibility'] : '' }}}
            </td>

            <td>
                {{{ isset( $siteaudit['Weather'] ) ? $siteaudit['Weather'] : '' }}}
            </td>


            <!-- Site Audit Traffic Control Requirements -->
{{--             @if( isset($siteaudit['Traffic_Control_Requirements']) )
                @foreach( $siteaudit['Traffic_Control_Requirements'] as $key => $answer )
                
                    <td>{{ $answer }}</td>

                @endforeach
            @endif --}}

            <td>
                {{{ isset( $siteaudit['Traffic_Control'] ) ? $siteaudit['Traffic_Control'] : '' }}}
            </td>

            <td>
                {{{ isset( $siteaudit['Lanes_Closed'] ) ? $siteaudit['Lanes_Closed'] : '' }}}
            </td>

            <td>
                {{{ isset( $siteaudit['Workers_Clearance_(Speed_reduced_to)'] ) ? $siteaudit['Workers_Clearance_(Speed_reduced_to)'] : '' }}}
            </td>



            <td>
                {{{ isset( $siteaudit['All_control_measures_as_per_SWMS_in_place'] ) ? $siteaudit['All_control_measures_as_per_SWMS_in_place'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['All_control_measures_as_per_SWMS_in_place_Note'] ) ? $siteaudit['All_control_measures_as_per_SWMS_in_place_Note'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Signage_erected_as_per_SWMS,_Manual_diagram,_Traffic_Control_Plan_-_TCP_No.'] ) ? $siteaudit['Signage_erected_as_per_SWMS,_Manual_diagram,_Traffic_Control_Plan_-_TCP_No.'] : '' }}}
            </td>


            @if( $siteaudit['Signage_erected_as_per_SWMS,_Manual_diagram,_Traffic_Control_Plan_-_TCP_No.'] == "Yes" )

                <td>
                {{{ isset( $siteaudit['If_Yes,_please_enter_the_appropriate_diagram_/_TCP_No.'] ) ? $siteaudit['If_Yes,_please_enter_the_appropriate_diagram_/_TCP_No.'] : '' }}}
                </td>

            @else

                <td>
                {{{ isset( $siteaudit['Signage_erected_as_per_SWMS,_Manual_diagram,_Traffic_Control_Plan_-_TCP_No._Note'] ) ? $siteaudit['Signage_erected_as_per_SWMS,_Manual_diagram,_Traffic_Control_Plan_-_TCP_No._Note'] : '' }}}
                </td>

            @endif


            <td>
                {{{ isset( $siteaudit['Has_conflicting_signage_been_covered_or_removed'] ) ? $siteaudit['Has_conflicting_signage_been_covered_or_removed'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Has_conflicting_signage_been_covered_or_removed_Note'] ) ? $siteaudit['Has_conflicting_signage_been_covered_or_removed_Note'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Are_signs_securely_mounted_and_visible_to_traffic'] ) ? $siteaudit['Are_signs_securely_mounted_and_visible_to_traffic'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Are_signs_securely_mounted_and_visible_to_traffic_Note'] ) ? $siteaudit['Are_signs_securely_mounted_and_visible_to_traffic_Note'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Traffic_Controller_Ahead_/_Prepare_to_stop_sign_erected'] ) ? $siteaudit['Traffic_Controller_Ahead_/_Prepare_to_stop_sign_erected'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Traffic_Controller_Ahead_/_Prepare_to_stop_sign_erected_Note'] ) ? $siteaudit['Traffic_Controller_Ahead_/_Prepare_to_stop_sign_erected_Note'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['PPE_is_being_worn_as_instructed_and_as_per_SWMS'] ) ? $siteaudit['PPE_is_being_worn_as_instructed_and_as_per_SWMS'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['PPE_is_being_worn_as_instructed_and_as_per_SWMS_Note'] ) ? $siteaudit['PPE_is_being_worn_as_instructed_and_as_per_SWMS_Note'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Stop_slow_bat_and_hand_signals_to_be_used_to_control_traffic'] ) ? $siteaudit['Stop_slow_bat_and_hand_signals_to_be_used_to_control_traffic'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Stop_slow_bat_and_hand_signals_to_be_used_to_control_traffic_Note'] ) ? $siteaudit['Stop_slow_bat_and_hand_signals_to_be_used_to_control_traffic_Note'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Each_TC_maintains_an_escape_route_at_all_times'] ) ? $siteaudit['Each_TC_maintains_an_escape_route_at_all_times'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Each_TC_maintains_an_escape_route_at_all_times_Note'] ) ? $siteaudit['Each_TC_maintains_an_escape_route_at_all_times_Note'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['TC_to_stand_facing_traffic_and_outside_projected_travel_path'] ) ? $siteaudit['TC_to_stand_facing_traffic_and_outside_projected_travel_path'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['TC_to_stand_facing_traffic_and_outside_projected_travel_path_Note'] ) ? $siteaudit['TC_to_stand_facing_traffic_and_outside_projected_travel_path_Note'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Access_provided_for_cyclists,_pedestrians,_wheelchairs_and_driveways'] ) ? $siteaudit['Access_provided_for_cyclists,_pedestrians,_wheelchairs_and_driveways'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Access_provided_for_cyclists,_pedestrians,_wheelchairs_and_driveways_Note'] ) ? $siteaudit['Access_provided_for_cyclists,_pedestrians,_wheelchairs_and_driveways_Note'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Maintain_safe_distance_from_all_plant_and_equipment'] ) ? $siteaudit['Maintain_safe_distance_from_all_plant_and_equipment'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Maintain_safe_distance_from_all_plant_and_equipment_Note'] ) ? $siteaudit['Maintain_safe_distance_from_all_plant_and_equipment_Note'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Is_there_sufficient_room_to_queue_stopped_vehicles'] ) ? $siteaudit['Is_there_sufficient_room_to_queue_stopped_vehicles'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Is_there_sufficient_room_to_queue_stopped_vehicles_Note'] ) ? $siteaudit['Is_there_sufficient_room_to_queue_stopped_vehicles_Note'] : '' }}}
            </td>
            <td>
                {{{ isset( $siteaudit['Environment_-_Is_there_any_environmental_risks'] ) ? $siteaudit['Environment_-_Is_there_any_environmental_risks'] : '' }}}
            </td>


            <!-- Additional Hazard -->
            <?php $count = 1; ?>

            @if( isset( $additionalHazards ) )

                @foreach( $additionalHazards as $hazardInfo )
                
                    <td>{{ $hazardInfo['AddHazard'] }}</td>
                    <td>{{ $hazardInfo['InitialRisk'] }}</td>
                    {{--<td>{{ $hazardInfo['ControlMeasures'] }}</td>
                    <td>{{ $hazardInfo['ResidualRisk'] }}</td> --}}

                    <?php $count++; ?>

                @endforeach

            @endif

            @if( $count < 5 )

                @while( $count <= 5 )

                <td>&nbsp;</td>
                <td>&nbsp;</td>

                <?php $count++; ?>

                @endwhile

            @endif

            <td>{{{ isset( $signageAudit['TimeErected'] ) ? date('H:i',strtotime($signageAudit['TimeErected'])) : '' }}}</td>
            <td>{{{ isset( $signageAudit['ErectedBy'] ) ? $signageAudit['ErectedBy'] : '' }}}</td>
            <td>{{{ isset( $signageAudit['TimeCollected'] ) ? date('H:i',strtotime($signageAudit['TimeCollected'])) : '' }}}</td>
            <td>{{{ isset( $signageAudit['CollectedBy'] ) ? $signageAudit['CollectedBy'] : '' }}}</td>
            <td>{{{ isset( $signageAudit['TimeChecked1'] ) ? date('H:i',strtotime($signageAudit['TimeChecked1'])) : '' }}}</td>
            <td>{{{ isset( $signageAudit['TimeChecked2'] ) ? date('H:i',strtotime($signageAudit['TimeChecked2'])) : '' }}}</td>
            <td>{{{ isset( $signageAudit['TimeChecked3'] ) ? date('H:i',strtotime($signageAudit['TimeChecked3'])) : '' }}}</td>
            <td>{{{ isset( $signageAudit['TimeChecked4'] ) ? date('H:i',strtotime($signageAudit['TimeChecked4'])) : '' }}}</td>
            <td>{{ $shiftLength }}</td>
        </tr>
    </table>
</body>
</html>