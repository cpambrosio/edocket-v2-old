<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">

        th {
            font-weight: bold;
        }

    </style>
</head>
<body>
    <table>
        <tr>
            <th>FileName</th>
            <th>EVO_FORM_ID</th>
            <th>DSFOLDER</th>
            <th>JobNo.</th>
            <th>DocketNo.</th>
            <th>JobDateMonth</th>
            <th>JobDateYear</th>
            <th>ClientCode</th>
            <th>ClientName</th>
            <th>DSGroup</th>
            <th>Location1</th>
            <th>Location2</th>
            <th>Depot</th>
            <th>State</th>
            <th>P/Order#</th>
            <th>JOBIMAGE_Time</th>
            <th>JOBIMAGE_Geo</th>
        </tr>

        @foreach( $jobImageData as $jobImageDataInfo )

        <tr>
            <td>{{{ isset( $jobImageDataInfo['fileName'] ) ? $jobImageDataInfo['fileName'] : '' }}}</td>
            <td>{{{ isset( $jobImageDataInfo['formId'] ) ? $jobImageDataInfo['formId'] : '' }}}</td>
            <td>IMAGES</td>
            <td>{{{ isset( $jobImageDataInfo['jobNo'] ) ? $jobImageDataInfo['jobNo'] : '' }}}</td>
            <td>{{{ isset( $jobImageDataInfo['jobID'] ) ? $jobImageDataInfo['jobID'] : '' }}}</td>
            <td>{{{ isset( $jobImageDataInfo['jobDateMonth'] ) ? $jobImageDataInfo['jobDateMonth'] : '' }}}</td>
            <td>{{{ isset( $jobImageDataInfo['jobDateYear'] ) ? $jobImageDataInfo['jobDateYear'] : '' }}}</td>
            <td>{{{ isset( $jobImageDataInfo['clientCode'] ) ? $jobImageDataInfo['clientCode'] : '' }}}</td>
            <td>{{{ isset( $jobImageDataInfo['clientName'] ) ? $jobImageDataInfo['clientName'] : '' }}}</td>
            <td>&nbsp;</td>
            <td>{{{ isset( $jobImageDataInfo['location1'] ) ? $jobImageDataInfo['location1'] : '' }}}</td>
            <td>&nbsp;</td>
            <td>{{{ isset( $jobImageDataInfo['depot'] ) ? $jobImageDataInfo['depot'] : '' }}}</td>
            <td>{{{ isset( $jobImageDataInfo['state'] ) ? $jobImageDataInfo['state'] : '' }}}</td>
            <td>{{{ isset( $jobImageDataInfo['orderNo'] ) ? $jobImageDataInfo['orderNo'] : '' }}}</td>
            <td>{{{ isset( $jobImageDataInfo['jobImageTime'] ) ? $jobImageDataInfo['jobImageTime'] : '' }}}</td>
            <td>{{{ isset( $jobImageDataInfo['jobImageGeo'] ) ? $jobImageDataInfo['jobImageGeo'] : '' }}}</td>
        </tr>

        @endforeach
    </table>
</body>
</html>