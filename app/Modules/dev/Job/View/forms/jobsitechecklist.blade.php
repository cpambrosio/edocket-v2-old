<html>
<head>
  <meta charset="utf-8">
  <style>
    @page { margin: 50px 20px; }
    body { padding: 5em 0em; }
    .header { position: fixed; top: -50px; left: 0px; right: 0px; height: 100px; padding: 1.5em 0px; text-align: center; display:block; }
    .footer { position: fixed; bottom: 10px; border-top:1px solid #000; padding:15px 20px; display: block; background-color: #3C3D3D; color:#fff; }
  </style>
</head>
<body style="font-family:'Helvetica';">
  <div class="header">

    <!-- Job Details -->
      <table style="width:100%;" cellspacing="0" cellpadding="0">
        <tr>
          <td style="text-align:center;">
            <img src="{{ public_path('image\logo\evolution-traffic-control-logo-lowres.png') }}" style="height:50px; display:block;" />
          </td>
          <td cellpadding="10" style="background-color:#00AEEF; padding:15px; border-bottom-right-radius: 2em;">

            <table style="width:100%;">
              <tr>
                <td cellspacing="0" cellpadding="0" style="color:#fff;"><strong>Evo Docket eForm</strong></td>
                <td cellspacing="0" cellpadding="0" style="color:#fff;"><strong>{{ $jobId }}</strong></td>
              </tr>
              <tr>
                <td cellspacing="0" cellpadding="0" style="font-size:12px; color:#fff;"><span style="font-weight:bold">Date:</span> {{ date('d F Y') }}</td>
                <td cellspacing="0" cellpadding="0" style="font-size:12px; color:#fff;"><span style="font-weight:bold">Client:</span> {{ $client }}</td>
              </tr>
              <tr>
                <td cellspacing="0" cellpadding="0" style="font-size:12px; color:#fff;"><span style="font-weight:bold">Client Order No:</span> {{ $orderNumber }}</td>
                <td cellspacing="0" cellpadding="0" style="font-size:12px; color:#fff;"><span style="font-weight:bold">Location:</span> {{ $location }}</td>
              </tr>
            </table>

          </td>
        </tr>
      </table>
      <!-- End of Job Details -->
  </div>

  <div class="footer">

    <table style="width:100%;" cellspacing="0" cellpadding="0">
      <tr>
        <td style="width:40%; text-align:left; font-size: 10px; font-weight:bold;">Evo Docket eForm  / {{ $jobId }}</td>
        <td style="width:50%; text-align:center; font-size: 10px;">&nbsp;</td>
        <td style="width:50%; text-align:right; font-size: 12px; font-weight:bold;">DTMCD002</td>
      </tr>
    </table>

  </div>



 <table style="width:100%;">
      <tr>
        <td style="background-color:#00AEEF">
          <p style="padding:10px 30px; margin:0px; font-size:12px; font-weight:bold;"><span style="color:#fff;">SITE SPECIFIC RISK ASSESSMENT</span></p>
        </td>
      </tr>
    </table>


    <!-- Site Specific Risk Assessment -->
    <table style="width:100%; margin:0px; padding:10px 0px;">


      <tr> 
        <td valign="top" style="width:30%;">

          <!-- Normal Road Configuration and Onsite Conditions -->
          <table style="width:100%;">
            <tr>
              <td style="font-size:12px; font-weight:bold; color:#fff; background-color:#3C3D3D; padding:5px;">Normal Road Configuration</td>
            </tr>

            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Type of Road:</span> {{ $siteaudit['Normal_Road_Configuration']['Type_of_Road'] }}</td>
            </tr>
            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">No. of lanes (Multilane only):</span> {{ $siteaudit['Normal_Road_Configuration']['No._of_lanes_(Multilane_only)'] }}</td>
            </tr>
            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Road Configuration:</span> {{ $siteaudit['Normal_Road_Configuration']['Road_Configuration'] }}</td>
            </tr>
            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Road Surface:</span> {{ $siteaudit['Normal_Road_Configuration']['Road_Surface'] }}</td>
            </tr>
            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Normal Road Speed Limit:</span> {{ $siteaudit['Normal_Road_Configuration']['Normal_Road_Speed_Limit'] }}</td>
            </tr>
            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Signs / Signals:</span> {{ $siteaudit['Normal_Road_Configuration']['Signs_/_Signals'] }}</td>
            </tr>


          </table>
          <!-- End of Normal Road Configuration and Onsite Conditions -->

          <br />

          <!-- Onsite Conditions -->
          <table style="width:100%;">
            <tr>
              <td style="font-size:12px; font-weight:bold; color:#fff; background-color:#3C3D3D; padding:5px;">Onsite Conditions</td>
            </tr>

            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Visibility:</span> {{ $siteaudit['Onsite_Conditions']['Visibility'] }}</td>
            </tr>
            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Weather:</span> {{ $siteaudit['Onsite_Conditions']['Weather'] }}</td>
            </tr>



          </table>
          <!-- End of Onsite Conditions -->

          <br />

          <!-- Traffic Control Requirements -->
          <table style="width:100%;">
            <tr>
              <td style="font-size:12px; font-weight:bold; color:#fff; background-color:#3C3D3D; padding:5px;">Traffic Control Requirements</td>
            </tr>

            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Traffic Control:</span> {{ $siteaudit['Traffic_Control_Requirements']['Traffic_Control'] }}</td>
            </tr>
            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Lanes Closed:</span> {{ $siteaudit['Traffic_Control_Requirements']['Lanes_Closed'] }}</td>
            </tr>

            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Workers Clearance (Speed reduced to):</span> {{ $siteaudit['Traffic_Control_Requirements']['Workers_Clearance_(Speed_reduced_to)'] }}</td>
            </tr>

          </table>
          <!-- End of Traffic Control Requirements -->

        </td>
        <td valign="top" style="width:70%;">

          <!-- Control Measures as per SWMS -->
          <table style="width:100%;">
            <tr>
              <td style="font-size:12px; font-weight:bold; color:#fff; background-color:#3C3D3D; padding:5px;">Control Measures as per SWMS</td>
            </tr>

            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">All control measures as per SWMS in place:</span> 
                {{ $siteaudit['Control_Measures_as_per_SWMS']['All_control_measures_as_per_SWMS_in_place'] }}
              </td>
            </tr>

            <tr>
              <td style="font-size:10px;">
                - {{ $siteaudit['Control_Measures_as_per_SWMS']['All_control_measures_as_per_SWMS_in_place_Note'] }}
              </td>
            </tr>
          
            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Signage erected as per SWMS, Manual diagram, Traffic Control Plan - TCP No.:</span> 
                {{ $siteaudit['Control_Measures_as_per_SWMS']['Signage_erected_as_per_SWMS,_Manual_diagram,_Traffic_Control_Plan_-_TCP_No.'] }}
              </td>
            </tr>

            <tr>
              <td style="font-size:10px;">

                @if( $siteaudit['Control_Measures_as_per_SWMS']['Signage_erected_as_per_SWMS,_Manual_diagram,_Traffic_Control_Plan_-_TCP_No.'] == "Yes" )

                  - {{ $siteaudit['Control_Measures_as_per_SWMS']['If_Yes,_please_enter_the_appropriate_diagram_/_TCP_No.'] }}

                @else

                  - {{ $siteaudit['Control_Measures_as_per_SWMS']['Signage_erected_as_per_SWMS,_Manual_diagram,_Traffic_Control_Plan_-_TCP_No._Note'] }} 

                @endif
                
              </td>
            </tr>

            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Has conflicting signage been covered or removed:</span> 
                {{ $siteaudit['Control_Measures_as_per_SWMS']['Has_conflicting_signage_been_covered_or_removed'] }}
              </td>
            </tr>

            <tr>
              <td style="font-size:10px;">
                - {{ $siteaudit['Control_Measures_as_per_SWMS']['Has_conflicting_signage_been_covered_or_removed_Note'] }}
              </td>
            </tr>


            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Are signs securely mounted and visible to traffic:</span> 
              {{ $siteaudit['Control_Measures_as_per_SWMS']['Are_signs_securely_mounted_and_visible_to_traffic'] }}
            </td>
            </tr>
            <tr>
              <td style="font-size:10px;">
                - {{ $siteaudit['Control_Measures_as_per_SWMS']['Are_signs_securely_mounted_and_visible_to_traffic_Note'] }}
              </td>
            </tr>

            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Traffic Controller Ahead / Prepare to stop sign erected:</span> 
                {{ $siteaudit['Control_Measures_as_per_SWMS']['Traffic_Controller_Ahead_/_Prepare_to_stop_sign_erected'] }}
              </td>
            </tr>
            <tr>
              <td style="font-size:10px;">
                - {{ $siteaudit['Control_Measures_as_per_SWMS']['Traffic_Controller_Ahead_/_Prepare_to_stop_sign_erected_Note'] }}
              </td>
            </tr>

            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">PPE is being worn as instructed and as per SWMS:</span> 
                {{ $siteaudit['Control_Measures_as_per_SWMS']['PPE_is_being_worn_as_instructed_and_as_per_SWMS'] }}
              </td>
            </tr>
            <tr>
              <td style="font-size:10px;">
                - {{ $siteaudit['Control_Measures_as_per_SWMS']['PPE_is_being_worn_as_instructed_and_as_per_SWMS_Note'] }}
              </td>
            </tr>

            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Stop slow bat and hand signals to be used to control traffic:</span> 
                {{ $siteaudit['Control_Measures_as_per_SWMS']['Stop_slow_bat_and_hand_signals_to_be_used_to_control_traffic'] }}
              </td>
            </tr>
            <tr>
              <td style="font-size:10px;">- {{ $siteaudit['Control_Measures_as_per_SWMS']['Stop_slow_bat_and_hand_signals_to_be_used_to_control_traffic_Note'] }}</td>
            </tr>

            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Each TC maintains an escape route at all times:</span> 
                {{ $siteaudit['Control_Measures_as_per_SWMS']['Each_TC_maintains_an_escape_route_at_all_times'] }}
              </td>
            </tr>
            <tr>
              <td style="font-size:10px;">- {{ $siteaudit['Control_Measures_as_per_SWMS']['Each_TC_maintains_an_escape_route_at_all_times_Note'] }}</td>
            </tr>

            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">TC to stand facing traffic and outside projected travel path:</span> 
                {{ $siteaudit['Control_Measures_as_per_SWMS']['TC_to_stand_facing_traffic_and_outside_projected_travel_path'] }}
              </td>
            </tr>
            <tr>
              <td style="font-size:10px;">- {{ $siteaudit['Control_Measures_as_per_SWMS']['TC_to_stand_facing_traffic_and_outside_projected_travel_path_Note'] }}</td>
            </tr>
            
            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Access provided for cyclists, pedestrians, wheelchairs and driveways:</span> 
                {{ $siteaudit['Control_Measures_as_per_SWMS']['Access_provided_for_cyclists,_pedestrians,_wheelchairs_and_driveways'] }}
              </td>
            </tr>
            <tr>
              <td style="font-size:10px;">
                - {{ $siteaudit['Control_Measures_as_per_SWMS']['Access_provided_for_cyclists,_pedestrians,_wheelchairs_and_driveways_Note'] }}
              </td>
            </tr>

            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Maintain safe distance from all plant and equipment:</span> 
                  {{ $siteaudit['Control_Measures_as_per_SWMS']['Maintain_safe_distance_from_all_plant_and_equipment'] }}
              </td>
            </tr>
            <tr>
              <td style="font-size:10px;">
                - {{ $siteaudit['Control_Measures_as_per_SWMS']['Maintain_safe_distance_from_all_plant_and_equipment_Note'] }}
              </td>
            </tr>

            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Is there sufficient room to queue stopped vehicles:</span> 
                {{ $siteaudit['Control_Measures_as_per_SWMS']['Is_there_sufficient_room_to_queue_stopped_vehicles'] }}
              </td>
            </tr>
            <tr>
              <td style="font-size:10px;">
                - {{ $siteaudit['Control_Measures_as_per_SWMS']['Is_there_sufficient_room_to_queue_stopped_vehicles_Note'] }}
              </td>
            </tr>

            <tr>
              <td style="font-size:10px;"><span style="font-weight:bold;">Environment - Is there any environmental risks:</span> 
                {{ $siteaudit['Control_Measures_as_per_SWMS']['Environment_-_Is_there_any_environmental_risks'] }}
              </td>
            </tr>


          </table>
          <!-- End of Control Measures as per SWMS -->
          
        </td>
      </tr>

    </table>
    <!-- End of Site Specific Risk Assessment -->

    <!-- Additional Hazards Identified -->
    <table style="width:100%;">
      <tr>
        <td style="font-size:12px; font-weight:bold; color:#fff; background-color:#3C3D3D; padding:5px;" colspan="3">
          Additional Hazards Identified
        </td>
      </tr>

      <?php $hazardCount = 0; ?>

      @foreach( $additionalHazards as $hazardInfo )


      <tr>

        <!-- Any additional hazards identified -->
        <td style="font-size:10px; padding:5px;"><span style="font-weight:bold;">Any additional hazards identified:</span> {{ $hazardInfo['AddHazard'] }}</td>
        <!-- End of Any additional hazards identified -->

        <!-- Initial Risk -->
        <td style="font-size:10px; padding:5px;" align="right"><span style="font-weight:bold;">Initial Risk:</span> {{ $hazardInfo['InitialRisk'] }}</td>
        <!-- End of Initial Risk -->

        <!-- Residual Risk -->
        <td style="font-size:10px; padding:5px;" align="right"><span style="font-weight:bold;">Residual Risk:</span> {{ $hazardInfo['ResidualRisk'] }}</td>
        <!-- End of Residual Risk -->

      </tr>
      <tr>

        <!-- Control measures to minimise risk -->
        <td colspan="3" style="font-size:10px; padding:5px;"><span style="font-weight:bold;">Control measures to minimise risk:</span>{{ $hazardInfo['ControlMeasures'] }}</td>
        <!-- End of Control measures to minimise risk -->

      </tr>

        <?php $hazardCount++; ?>


      @endforeach


      @if( $hazardCount == 0 )

        <tr>
          <td colspan="3" style="font-size:12px; text-align:center; color:#000; padding:15px;">No Additional Hazard Available</td>
        </tr>


      @endif


    </table>
    <!-- End of Additional Hazards Identified -->


    <br />


    <table style="width:100%;">
      <tr>
        <td style="background-color:#00AEEF">
          <p style="padding:10px 30px; margin:0px; font-size:12px; font-weight:bold;"><span style="color:#fff;">SIGNAGE AUDIT</span></p>
        </td>
      </tr>
    </table>


    <table style="width:100%;">
      <tr>

        <!-- Evolution Rego No. -->
        {{-- <td style="font-size:10px;"><span style="font-weight:bold;">Evolution Rego No.:</span> {{ $signageAudit['RegNo'] }}</td> --}}
        <!-- End of Evolution Rego No. -->

        <!-- Direction -->
        <td style="font-size:10px;"><span style="font-weight:bold;">Direction:</span> 
          @if( isset( $signageAudit['Direction'] ) ) {{ $signageAudit['Direction'] }} @endif
        </td>
        <!-- End of Direction -->

        <!-- Local Landmark -->
        <td style="font-size:10px;"><span style="font-weight:bold;">Local Landmark:</span> 
          @if( isset( $signageAudit['LocalLandmark'] ) ) {{ $signageAudit['LocalLandmark'] }} @endif
        </td>
        <!-- End of Local Landmark -->

      </tr>
    </table>



     <!-- Slow Lane -->
    <table cellpadding="10" style="width:100%; margin:10px 0px;">
      <tr>
        <td style="font-size:12px; font-weight:bold; color:#000; padding:5px;">Slow Lane</td>
      </tr>
      <tr>
        <td>
          <table style="width:100%;">
            <tr>

              <?php 

                $count = 0; 
                $hcount = 0;
                $totalLane = 0;

              ?>


              @foreach( $signageAudit['SlowLane'] as $slowLaneInfo )

                @if( $slowLaneInfo['Metres'] != "" || $slowLaneInfo['Qty'] != "" || $slowLaneInfo['AfterCareMetres'] != "" || $slowLaneInfo['AfterCareQty'] != ""  )

                  @if( $count > 2 )

                    <?php 

                      $count = 0;
                      $hcount++;

                     ?>

                    </tr>
                    <tr>

                  @endif

                  <td style="width:25%;">
                    <table style="width:100%;">

                      <?php if( $hcount == 0 ){ ?>

                        <tr>
                          <th style="font-size:10px; width:40%;">&nbsp;</th>
                          <th style="font-size:10px; width:5%;">&nbsp;</th>
                          <th style="font-size:10px; width:25%; text-align:center;">METRES</th>
                          <th style="font-size:10px; width:5%;">&nbsp;</th>
                          <th style="font-size:10px; width:25%; text-align:center;">QTY</th>
                        </tr>

                      <?php } ?>

                      <tr>
                        <td rowspan="2" style="solid #000; width:40%; height:0px; text-align:center; padding:0px; margin:0px;">
                          <img src="{{ public_path($slowLaneInfo['Src']) }}" style="height:30px; width:30px; padding:0px !important; margin:0px !important;" />
                        </td>
                        <td style="font-size:10px; width:5%; text-align:center;">&nbsp;</td>
                        <td style="font-size:10px; width:25%; text-align:center; border-bottom:1px solid #000;">{{ $slowLaneInfo['Metres'] }}</td>
                        <td style="font-size:10px; width:5%; text-align:center; font-weight:bold;">x</td>
                        <td style="font-size:10px; width:25%; text-align:center; border-bottom:1px solid #000;">{{ $slowLaneInfo['Qty'] }}</td>
                      </tr>
                      <tr>
                        <td style="font-size:10px; width:5%; text-align:center; font-weight:bold;">A:</td>
                        <td style="font-size:10px; width:25%; text-align:center; font-weight:bold; border-bottom:1px solid #000;">{{ $slowLaneInfo['AfterCareMetres'] }}</td>
                        <td style="font-size:10px; width:5%; text-align:center; font-weight:bold;">x</td>
                        <td style="font-size:10px; width:25%; text-align:center; font-weight:bold; border-bottom:1px solid #000;">{{ $slowLaneInfo['AfterCareQty'] }}</td>
                      </tr>
                    </table>
                  </td>

                  <?php 

                    $count++; 
                    $totalLane++;

                  ?>

                @endif

              @endforeach

              @if( $totalLane > 0 && $count <= 2 )

                @while( $count <= 2 )

                  <td style="width:25%;">&nbsp;</td>

                  <?php $count++ ?>

                @endwhile

              @endif

              @if( $totalLane == 0 )

                <td style="width:100%;">
                    <table style="width:100%;">
                      <tr>
                        <td style="font-size:12px; text-align:center; color:#000; padding:15px;">No Signage Available for Slow Lane</td>
                      </tr>
                    </table>
                </td>

              @endif

            </tr>
          </table>
        </td>
      </tr>

    </table>
    <!-- End of Slow Lane -->




    <!-- Fast Lane -->
    <table cellpadding="10"  style="width:100%; margin:10px 0px;">
      <tr>
        <td style="font-size:12px; font-weight:bold; color:#000; ">Fast Lane</td>
      </tr>
      <tr>
        <td>
          <table style="width:100%;">
            <tr>

      <?php 

        $count = 0; 
        $hcount = 0;
        $totalLane = 0;

      ?>

      @foreach( $signageAudit['FastLane'] as $fastLaneInfo )

        @if( $fastLaneInfo['Metres'] != "" || $fastLaneInfo['Qty'] != "" || $fastLaneInfo['AfterCareMetres'] != "" || $fastLaneInfo['AfterCareQty'] != ""  )

          @if( $count > 2 )

            <?php 

              $count = 0;
              $hcount++;

             ?>

             </tr>
            <tr>

          @endif

          <td style="width:25%;">
            <table style="width:100%;">

              <?php if( $hcount == 0 ){ ?>

                <tr>
                  <th style="font-size:10px; width:40%;">&nbsp;</th>
                  <th style="font-size:10px; width:5%;">&nbsp;</th>
                  <th style="font-size:10px; width:25%; text-align:center;">METRES</th>
                  <th style="font-size:10px; width:5%;">&nbsp;</th>
                  <th style="font-size:10px; width:25%; text-align:center;">QTY</th>
                </tr>

              <?php } ?>

              <tr>
                <td rowspan="2" style="solid #000; width:40%; height:0px; text-align:center; padding:0px; margin:0px;">
                  <img src="{{ public_path($fastLaneInfo['Src']) }}" style="height:30px; width:30px; padding:0px !important; margin:0px !important;" />
                </td>
                <td style="font-size:10px; width:5%; text-align:center;">&nbsp;</td>
                <td style="font-size:10px; width:25%; text-align:center; border-bottom:1px solid #000;">{{ $fastLaneInfo['Metres'] }}</td>
                <td style="font-size:10px; width:10%; text-align:center; font-weight:bold;">x</td>
                <td style="font-size:10px; width:25%; text-align:center; border-bottom:1px solid #000;">{{ $fastLaneInfo['Qty'] }}</td>
              </tr>
              <tr>
                <td style="font-size:10px; width:5%; text-align:center; font-weight:bold;">A:</td>
                <td style="font-size:10px; width:25%; text-align:center; font-weight:bold; border-bottom:1px solid #000;">{{ $fastLaneInfo['AfterCareMetres'] }}</td>
                <td style="font-size:10px; width:10%; text-align:center; font-weight:bold;">x</td>
                <td style="font-size:10px; width:25%; text-align:center; font-weight:bold; border-bottom:1px solid #000;">{{ $fastLaneInfo['AfterCareQty'] }}</td>
              </tr>
            </table>
          </td>

          <?php 

            $count++; 
            $totalLane++;


          ?>

        @endif

      @endforeach

      @if( $totalLane > 0 && $count <= 2 )

        @while( $count <= 2 )

          <td style="width:25%;">&nbsp;</td>

          <?php $count++ ?>

        @endwhile

      @endif

      @if( $totalLane == 0 )

        <td colspan="3" style="font-size:12px; text-align:center; color:#000; padding:15px;">No Signage Available for Fast Lane</td>


      @endif

            </tr>
          </table>
        </td>
      </tr>

    </table>
    <!-- End of Fast Lane -->

    <br />

    <table style="width:100%;">
      <tr>
        <td style="text-align:right; font-size:10px;">Time Erected:</td>
        <td style="font-size:10px; text-align:left;">
          @if( isset( $signageAudit['TimeErected'] ) && $signageAudit['TimeErected'] != "" ) {{ date('Hi',$signageAudit['TimeErected']) }} @endif
        </td>
        <td style="text-align:right; font-size:10px;">Time Collected:</td>
        <td style="font-size:10px; text-align:left;">
          @if( isset( $signageAudit['TimeCollected'] ) && $signageAudit['TimeCollected'] != "" ) {{ date('Hi',$signageAudit['TimeCollected']) }} @endif
        </td>
        <td style="text-align:right; font-size:10px;">Time Checked 1:</td>
        <td style="font-size:10px; text-align:left;">
          @if( isset( $signageAudit['TimeChecked1'] ) && $signageAudit['TimeChecked1'] != "" ) {{ date('Hi',$signageAudit['TimeChecked1']) }} @endif
        </td>
      </tr>
      <tr>
        <td style="text-align:right; font-size:10px;">Time Checked 2:</td>
        <td style="font-size:10px; text-align:left;">
          @if( isset( $signageAudit['TimeChecked2'] ) && $signageAudit['TimeChecked2'] != "" ) {{ date('Hi',$signageAudit['TimeChecked2']) }} @endif
        </td>
        <td style="text-align:right; font-size:10px;">Time Checked 3:</td>
        <td style="font-size:10px; text-align:left;">
          @if( isset( $signageAudit['TimeChecked3'] ) && $signageAudit['TimeChecked3'] != "" ) {{ date('Hi',$signageAudit['TimeChecked3']) }} @endif
        </td>
        <td style="text-align:right; font-size:10px;">Time Checked 4:</td>
        <td style="font-size:10px; text-align:left;">
          @if( isset( $signageAudit['TimeChecked4'] ) && $signageAudit['TimeChecked4'] != "" ) {{ date('Hi',$signageAudit['TimeChecked4']) }} @endif
        </td>
      </tr>
    </table>


    <br />



    <?php 

    if( count($tcSignature) ): ?>

    <table style="width:100%;">
      <tr>

        <?php $count = 0; ?>

        @foreach( $tcSignature as $tcInfo )

        <?php 

          if( $count > 3 ){

            $count = 0;

        ?>

          </tr><tr>          

        <?php

          } 

        ?>

        <td style="text-align:center;">
          <table style="width:100%;">
            <tr>
                <td style="text-align:center; width:25%;">
                  <img src="{{ public_path('image\tc_attachment\\'.$tcInfo['Src']) }}" style="height:100px; padding:0px !important; margin:0px !important;" />
                </td>
            </tr>
            <tr>
              <td style="border-top:1px solid #000; text-align:center; font-size:10px;">TC{{ $count+1 }}: {{ $tcInfo['TCName'] }}</td>
            </tr>
          </table>
        </td>

          <?php $count++; ?>

        @endforeach

        @if( $count <= 3 )

          @while( $count <= 3 )

            <td style="width:25%;">
              &nbsp;
            </td>

            <?php $count++; ?>

          @endwhile

        @endif

      </tr>
    </table>

    <?php endif; ?>




</body>
</html>