
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>

    <table>
      <tr>
        <td><strong>Job Information</strong></td>
      </tr>
      <tr>
        <td>Job No. :</td>
        <td>{{ $jobID }}</td>
      </tr>
      <tr>
        <td>Company Name :</td>
        <td>{{ $companyName }}</td>
      </tr>
      <tr>
        <td>Address :</td>
        <td>{{ $address }}</td>
      </tr>
      <tr>
        <td>Date Started :</td>
        <td>{{ $dateStarted }}</td>
      </tr>
      <tr>
        <td>Depot :</td>
        <td>{{ $depot }}</td>
      </tr>
      <tr>
        <td>Authorized By :</td>
        <td>{{ $authorized }}</td>
      </tr>
    </table>

    <br />

    <table>
      <tr>
        <td><strong>Timesheet Information</strong></td>
      </tr>
    </table>

    @foreach ($tcData as $tc)

    <br />

    <table>
      <tr>
        <td>Name :</td>
        <td>{{ $tc['fullname'] }}</td>
      </tr>
      <tr>
        <td>Start Travel :</td>
        <td>{{ $tc['startTravel'] }}</td>
      </tr>
      <tr>
        <td>Start Onsite :</td>
        <td>{{ $tc['startOnsite'] }}</td>
      </tr>
      <tr>
        <td>Start Meal :</td>
        <td>{{ $tc['startMeal'] }}</td>
      </tr>
      <tr>
        <td>Finish Meal :</td>
        <td>{{ $tc['finishMeal'] }}</td>
      </tr>
      <tr>
        <td>Finish Onsite :</td>
        <td>{{ $tc['finishOnsite'] }}</td>
      </tr>
      <tr>
        <td>Finish Travel :</td>
        <td>{{ $tc['finishTravel'] }}</td>
      </tr>
    </table>

    @endforeach

  </body>
</html>