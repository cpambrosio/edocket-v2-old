
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>

    <p>Hi {{ $mail['fullname'] }},</p>

    <p>Job No. {{ $mail['jobID'] }} has been sent. Please see the submitted information below:</p>

    <br />

    <table>
      <tr>
        <td><strong>Job Information</strong></td>
      </tr>
      <tr>
        <td>Job No. :</td>
        <td>{{ $mail['jobID'] }}</td>
      </tr>
      <tr>
        <td>Company Name :</td>
        <td>{{ $mail['companyName'] }}</td>
      </tr>
      <tr>
        <td>Address :</td>
        <td>{{ $mail['address'] }}</td>
      </tr>
      <tr>
        <td>Date Started :</td>
        <td>{{ $mail['dateStarted'] }}</td>
      </tr>
      <tr>
        <td>Depot :</td>
        <td>{{ $mail['depot'] }}</td>
      </tr>
      <tr>
        <td>Authorized By :</td>
        <td>{{ $mail['authorized'] }}</td>
      </tr>
    </table>

    <br />

    <table>
      <tr>
        <td><strong>Timesheet Information</strong></td>
      </tr>
    </table>

    @foreach ($mail['tcData'] as $tc)

    <br />

    <table>
      <tr>
        <td>Name :</td>
        <td>{{ $tc['fullname'] }}</td>
      </tr>
      <tr>
        <td>Start Travel :</td>
        <td>{{ $tc['startTravel'] }}</td>
      </tr>
      <tr>
        <td>Start Onsite :</td>
        <td>{{ $tc['startOnsite'] }}</td>
      </tr>
      <tr>
        <td>Start Meal :</td>
        <td>{{ $tc['startMeal'] }}</td>
      </tr>
      <tr>
        <td>Finish Meal :</td>
        <td>{{ $tc['finishMeal'] }}</td>
      </tr>
      <tr>
        <td>Finish Onsite :</td>
        <td>{{ $tc['finishOnsite'] }}</td>
      </tr>
      <tr>
        <td>Finish Travel :</td>
        <td>{{ $tc['finishTravel'] }}</td>
      </tr>
    </table>

    @endforeach

  </body>
</html>