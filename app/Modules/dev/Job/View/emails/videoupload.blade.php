
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>

    <p>Hi,</p>

    <p>A video was successfully uploaded. Please see the information below:</p>

    <br />

    <table>
      <tr>
        <td><strong>Subject :</strong></td>
      </tr>
      <tr>
        <td>{{ $subject }}</td>
      </tr>
      <tr>
        <td><strong>Note :</strong></td>
      </tr>
      <tr>
        <td>{{ $note }}</td>
      </tr>

      @if( $link != "" )

      <tr>
        <td><strong>Video Link :</strong></td>
      </tr>
      <tr>
        <td>{{ $link }}</td>
      </tr>

      @endif


      <tr>
        <td><strong>Start Capture :</strong></td>
      </tr>
      <tr>
        <td>{{ $startCapture }}</td>
      </tr>
      <tr>
        <td><strong>End Capture :</strong></td>
      </tr>
      <tr>
        <td>{{ $endCapture }}</td>
      </tr>
      <tr>
        <td><strong>Start Location :</strong></td>
      </tr>
      <tr>
        <td>
          <a href="http://maps.google.com/?q={{ $startLocation }}" target="_blank">
            <img border="0" src="http://maps.googleapis.com/maps/api/staticmap?center={{ $startLocation }}&zoom=16&size=400x400&
markers=color:red|label:S|{{ $startLocation }}">
          </a>
        </td>
      </tr>
      <tr>
        <td><strong>End Location :</strong></td>
      </tr>
      <tr>
        <td>
          <a href="http://maps.google.com/?q={{ $endLocation }}" target="_blank">
            <img border="0" src="http://maps.googleapis.com/maps/api/staticmap?center={{ $endLocation }}&zoom=16&size=400x400&
markers=color:red|label:E||{{ $endLocation }}">
          </a>
        </td>
      </tr>
    </table>

  </body>
</html>