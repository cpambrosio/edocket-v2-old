
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>

    <p>Hi {{ $fullname }},</p>

    <p>Job No. {{ $jobID }} has been sent. Please see the submitted information below:</p>

    <br />

    <table>
      <tr>
        <td><strong>Job Information</strong></td>
      </tr>
      <tr>
        <td>Job No. :</td>
        <td>{{ $jobID }}</td>
      </tr>
      <tr>
        <td>Company Name :</td>
        <td>{{ $companyName }}</td>
      </tr>
      <tr>
        <td>Address :</td>
        <td>{{ $address }}</td>
      </tr>
      <tr>
        <td>Date Started :</td>
        <td>{{ $dateStarted }}</td>
      </tr>
      <tr>
        <td>Depot :</td>
        <td>{{ $depot }}</td>
      </tr>
      <tr>
        <td>Authorized By :</td>
        <td>{{ $authorized }}</td>
      </tr>
    </table>

    <br />

    <table>
      <tr>
        <td><strong>Timesheet Information</strong></td>
      </tr>
      <tr>
        <td>Start Travel :</td>
        <td>{{ $startTravel }}</td>
      </tr>
      <tr>
        <td>Start Onsite :</td>
        <td>{{ $startOnsite }}</td>
      </tr>
      <tr>
        <td>Start Meal :</td>
        <td>{{ $startMeal }}</td>
      </tr>
      <tr>
        <td>Finish Meal :</td>
        <td>{{ $finishMeal }}</td>
      </tr>
      <tr>
        <td>Finish Onsite :</td>
        <td>{{ $finishOnsite }}</td>
      </tr>
      <tr>
        <td>Finish Travel :</td>
        <td>{{ $finishTravel }}</td>
      </tr>
    </table>

    <p>This is just a test email notification. Do not reply.</p>

  </body>
</html>