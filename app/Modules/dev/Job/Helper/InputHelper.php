<?php namespace App\Modules\dev\Job\Helper;


class InputHelper
{


  public function __construct()
  {

  }

  public function filterInput($input){

    foreach( $input as $inputKey => $inputInfo ){

      $input[$inputKey] = str_replace('\n', '', $input[$inputKey]);
      $input[$inputKey] = str_replace('\"', '"', $input[$inputKey]);
      $input[$inputKey] = str_replace('\/', '/', $input[$inputKey]);

    }

    return $input;

  }



}