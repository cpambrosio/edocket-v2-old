<?php namespace App\Modules\dev\Job\Helper;


class JobHelper
{


  public function __construct()
  {

  }

  public function extract_unit($string, $start, $end){

    $pos = stripos($string, $start);

    $str = substr($string, $pos);

    $str_two = substr($str, strlen($start));

    $second_pos = stripos($str_two, $end);

    $str_three = substr($str_two, 0, $second_pos);

    $unit = trim($str_three); // remove whitespaces

    return $unit;
  }

  public function arraySorter(&$array, $field, $desc = 0)
  {

    usort($array,function($a, $b) use($field,$desc)
    {

      $t1 = $a[$field];
      $t2 = $b[$field];

      if( $desc == 0 )
      {
        return ($t1 < $t2) ? -1 : 1;
      }
      else
      {
        return ($t1 > $t2) ? -1 : 1;
      }

    });

    return $array;

  }

  public function uuidv4()
  {

    $data = openssl_random_pseudo_bytes(16);

      assert(strlen($data) == 16);

      $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
      $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

      return strtoupper(vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4)));
  }



}