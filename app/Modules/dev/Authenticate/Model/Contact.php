<?php namespace App\Modules\dev\Authenticate\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class Contact extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'Contact';

	/**
     * The connection name for the model.
     *
     * @var string
     */
	protected $connection = 'manstatsql';

}