<?php namespace App\Modules\dev\Authenticate\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class Token extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'App_Token';

	/**
     * The connection name for the model.
     *
     * @var string
     */
	protected $connection = 'sqlsrv';


}