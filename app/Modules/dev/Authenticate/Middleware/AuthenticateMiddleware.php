<?php namespace App\Modules\dev\Authenticate\Middleware;

use DB;
use Input;
use Response;
use Request;
use Config;
use App;
use Carbon\Carbon;
use DateTime;
use Closure;

class AuthenticateMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		//Set maximum execution time
		set_time_limit(600);

		$apiToken       = Request::header('X-Auth-Token');
	    $userToken      = DB::table('App_Token')->where('ApiToken', $apiToken)->first();

	    //check if the token exists else check if it is expired or not
	    if(count($userToken) == 0){

	      $response = [
	      	'status' => 'false', 
	      	'message' => 'API Token is invalid or doesn\'t exist'
	      ];
	      return Response::json($response,401);


	    }else{
	      //create an instance of dateTime
	      $expirationDate = Carbon::instance(new DateTime($userToken->ExpiredAt));
	      //if the "current date" is GTE(Greater Than or Equal) to the "expiration date"
	      //return true else false
	      $isExpired = (Carbon::now()->gte($expirationDate)) ? true : false;
	      if($isExpired){

	        $response = [
	        	'status' => 'false', 
	        	'message' => 'Expired API Token'
	        ];

	        return Response::json($response,401);
	      }
	    }


		$response =  $next($request);

		return $response;

	}

}
