<?php namespace App\Modules\dev\Authenticate\Provider;

use Illuminate\Support\ServiceProvider;

use App\Modules\dev\Authenticate\Gateway\AuthenticateGateway;

use App\Modules\dev\Authenticate\Services\TokenServices;

use App\Modules\dev\Authenticate\Repository\Eloquent\EloquentOperator;

use App\Modules\dev\Authenticate\Repository\Eloquent\EloquentToken;

use App\Modules\dev\Authenticate\Repository\Eloquent\EloquentContact;

use App\Modules\dev\Authenticate\Model\Operator;

use App\Modules\dev\Authenticate\Model\Token;

use App\Modules\dev\Authenticate\Model\Contact;

use App\Modules\dev\Authenticate\Model\AccountInfo;


class AuthenticateServiceProvider extends ServiceProvider

{

  public function register()

  {

    $app = $this->app;

    // Contact Repository

    $app->bind('App\Modules\dev\Authenticate\Repository\Interfaces\ContactInterface',function(){

      return new EloquentContact(

        new Contact

        );

    });


    // AppToken Repository

    $app->bind('App\Modules\dev\Authenticate\Repository\Interfaces\TokenInterface',function(){

      return new EloquentToken(

        new Token

        );

    });
    

  }

}