<?php namespace App\Modules\dev\Authenticate\Provider;

use App\Modules\dev\Authenticate\Request\AuthenticateValidator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AuthenticateValidatorServiceProvider extends ServiceProvider{

    public function boot()
    {

        //Custom Authenticate Validation

        Validator::resolver(function($translator, $data, $rules, $messages)
        {

            return new AuthenticateValidator($translator, $data, $rules, $messages);
            
        });

    }

    public function register()
    {
    }
}