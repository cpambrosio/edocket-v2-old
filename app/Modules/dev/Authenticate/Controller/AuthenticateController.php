<?php namespace App\Modules\dev\Authenticate\Controller;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\dev\Authenticate\Gateway\AuthenticateGateway;
use App\Modules\dev\Authenticate\Request\LoginRequest;

use Illuminate\Http\Request;
use DB, Input, Response;

class AuthenticateController extends Controller {

	protected $authenticateGateway;

	function __construct(AuthenticateGateway $authenticateGateway){

		$this->authenticateGateway = $authenticateGateway;

	}

	function login(LoginRequest $request){

		$responseData = $this->authenticateGateway->login($request->all());

		$statusCode = $responseData['statusCode'];
		unset($responseData['statusCode']);
		$response = $responseData;

		return Response::json($response, $statusCode);

	}

}
