<?php namespace App\Modules\dev\Authenticate\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Authenticate\Repository\Interfaces\TokenInterface;

class EloquentToken implements TokenInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $token;


  public function __construct( Model $token )
  {

    $this->token   = $token;

  }


  /**
   * Find user by initials
   *
   * @return mixed
   */

  public function create($data)
  {

    return $this->token->insert($data);

  }

  public function find($apiToken){

    return $this->token
                ->where('App_Token.ApiToken','=',$apiToken)
                ->first()
                ->toArray();

  }


}