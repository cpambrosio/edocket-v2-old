<?php namespace App\Modules\dev\Authenticate\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Modules\dev\Authenticate\Repository\Interfaces\ContactInterface;

use DB;

class EloquentContact implements ContactInterface

{

  /**
   * Eloquent model
   *
   * @var Illuminate\Database\Eloquent\Model
   */

  protected $contact;


  public function __construct( Model $contact )
  {

    $this->contact   = $contact;

  }



  /**
   * Find user by contact code
   *
   * @return mixed
   */

  public function findAccount($code)
  {

    $query = $this->contact
                ->join('App_AccountInfo as AccountInfo','AccountInfo.ContactID','=','Contact.ContactID')
                ->where('Contact.ContactCode','=',$code)
                ->first()->toArray();

  }

  public function countAcountByCode($code){

    return $this->contact
                ->join('App_AccountInfo as AccountInfo','AccountInfo.ContactID','=','Contact.ContactID')
                ->where('Contact.ContactCode','=',$code)
                ->count();

  }

  public function findByCode($code){

    return $this->contact
                ->where('ContactCode','=',$code)
                ->first()
                ->toArray();

  }

  public function countByCode($code){

    return $this->contact
                ->where('ContactCode','=',$code)
                ->count();

  }

  public function findByContactReportData($id, $jobId){

    $fields = [
      "Contact.ContactID",
      "Contact.ContactCode",
      "Contact.AttacheCode",
      DB::raw("(RTRIM(LTRIM(Contact.FirstName))+' '+RTRIM(LTRIM(Contact.LastName))) as ContactName"),
      "Lookup.Description"
    ];

    $count = $this->contact
                ->select($fields)
                ->join("ContactCalendar","Contact.ContactID","=","ContactCalendar.ContactID")
                ->join("JobShiftDetail", function($join)
                    {
                        $join->on("JobShiftDetail.JobID", "=", "ContactCalendar.JobID")
                             ->on("JobShiftDetail.ContactID","=","ContactCalendar.ContactID")
                             ->on("JobShiftDetail.ShiftID","=","ContactCalendar.ShiftInfoID");
                    })
                ->join("Lookup","Lookup.LookupID","=","JobShiftDetail.JobStartType")
                ->where('Contact.ContactID','=',$id)
                ->where('ContactCalendar.JobID','=',$jobId)
                ->count();


    if( $count > 0 ){

      return $this->contact
                ->select($fields)
                ->join("ContactCalendar","Contact.ContactID","=","ContactCalendar.ContactID")
                ->join("JobShiftDetail", function($join)
                    {
                        $join->on("JobShiftDetail.JobID", "=", "ContactCalendar.JobID")
                             ->on("JobShiftDetail.ContactID","=","ContactCalendar.ContactID")
                             ->on("JobShiftDetail.ShiftID","=","ContactCalendar.ShiftInfoID");
                    })
                ->join("Lookup","Lookup.LookupID","=","JobShiftDetail.JobStartType")
                ->where('Contact.ContactID','=',$id)
                ->where('ContactCalendar.JobID','=',$jobId)
                ->first()
                ->toArray();


    }
    else{

      return false;

    }

  }

  /**
   * Find user by token
   *
   * @return mixed
   */

  public function findByToken($token)
  {

    return $this->contact
                ->join('App_Token', 'App_Token.ContactID', '=', 'Contact.ContactID')
                ->where('App_Token.ApiToken','=',$token)->first();

  }


}