<?php namespace App\Modules\dev\Authenticate\Repository\Interfaces;

interface ContactInterface
{

  public function countAcountByCode($code);

  public function findAccount($code);

  public function findByCode($code);

  public function findByToken($token);

  public function countByCode($code);

  public function findByContactReportData($id, $jobId);

}