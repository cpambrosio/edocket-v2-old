<?php namespace App\Modules\dev\Authenticate\Repository\Interfaces;

interface TokenInterface
{
  /**
   * Returns all collection data handled by the model
   *
   * @return mixed
   */
  public function create($data);

  public function find($apiToken);


}