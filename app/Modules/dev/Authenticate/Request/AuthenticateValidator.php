<?php namespace App\Modules\dev\Authenticate\Request;

use \Illuminate\Validation\Validator;

class AuthenticateValidator extends Validator{

	/**
	 * Parse the data and hydrate the files array.
	 *
	 * @param  array   $data
	 * @param  string  $arrayKey
	 * @return array
	 */
	protected function parseData(array $data, $arrayKey = null)
	{
		if (is_null($arrayKey))
		{
			$this->files = array();
		}

		foreach ($data as $key => $value)
		{
			$key = ($arrayKey) ? "$arrayKey.$key" : $key;

			// If this value is an instance of the HttpFoundation File class we will
			// remove it from the data array and add it to the files array, which
			// we use to conveniently separate out these files from other data.
			if ($value instanceof File)
			{
				$this->files[$key] = $value;

				unset($data[$key]);
			}
			elseif (is_array($value))
			{

				$this->parseData($value, $key);
			}
			//If value was a json, convert json to array
			elseif( is_string($value) && is_array(json_decode($value)) && ( json_last_error() == JSON_ERROR_NONE ) ){

				$data[$key] = json_decode($data[$key],true);
				$value = json_decode($value,true);

				$this->parseData($value, $key);

			}

		}

		return $data;
	}

	protected function validateMicrotime($attribute, $value)
	{

		$formatValue = date('Y-m-d H:i:s',$value / 1000);


		if( $formatValue == '1970-01-01 00:00:00' ){

			return false;

		}

		if( $this->validateDate($attribute,$formatValue) ){

			return true;

		}

		return false;
	}

	protected function validateJson($attribute, $value)
	{

		if( is_string($value) && is_array(json_decode($value)) && ( json_last_error() == JSON_ERROR_NONE ) ){

			return true;

		}

		return false;
	}

	protected function validateRequiredMulti($attribute, $value)
	{

		$multi = json_decode($value);

		if( !$this->checkMultiContent($multi) ){

			return false;

		}

		return true;
	}

	protected function checkMultiContent($array){

		if(is_array($array) || is_object($array)){

			foreach($array as $arrayInfo){

				if( !$this->checkMultiContent($arrayInfo) ){

					return false;

				}

			}

			return true;

		}
		else{

			if( $array == "" ){

				return false;

			}
			else{

				return true;

			}

		}
	}


	protected function validateGeotag($attribute, $value)
	{

		if( !preg_match('/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/', $value) ){

			return false;

		}

		return true;

	}
}