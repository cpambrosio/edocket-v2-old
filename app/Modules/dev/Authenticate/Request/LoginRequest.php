<?php namespace App\Modules\dev\Authenticate\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class LoginRequest extends FormRequest {


	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'etccode' => 'required',
			'password' => 'required'
		];
	}

	/**
	 * Get the proper failed validation response for the request. This is optional.
	 *
	 * @param  array  $errors
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function response(array $errors)
	{


		//Make all error messages inline
		$message = [];

		foreach( $errors as $field => $errorInfo ){

			$message[] = $field.':'.implode('|', $errorInfo);

		}

		$errors = implode('~', $message);

		$message = [
	        'status' => false,
	        'message' => $errors
      	];

		return new JsonResponse($message, 200);

	}

}
