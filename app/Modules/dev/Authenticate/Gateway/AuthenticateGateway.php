<?php namespace App\Modules\dev\Authenticate\Gateway;

use App\Modules\dev\Job\Services\ManstatServices;
use App\Modules\dev\Authenticate\Services\TokenServices;
use App\Modules\dev\Authenticate\Services\ContactServices;

use Config;
use Carbon\Carbon;
use Hash;


class AuthenticateGateway
{

  protected $tokenServices;
  protected $contactServices;
  protected $manstatServices;

  public function __construct(
    TokenServices $tokenServices,
    ContactServices $contactServices,
    ManstatServices $manstatServices)
  {

    $this->tokenServices  = $tokenServices;
    $this->contactServices  = $contactServices;
    $this->manstatServices = $manstatServices;

  }

  /**
     * Get token to accessed the rest of the api.
     *
     * @param data The data provided which consist of contact code and password
     *
     * @return token
     */
  function login($data){

    $user = array();

    // //check if account exist
    $countAccount = $this->contactServices->count($data['etccode']);
    $countContact = $this->manstatServices->countUserByCode($data['etccode']);

    if( $countContact === false ){

      $response = [
        'status' => false, 
        'statusCode' => 500,
        'message' => 'Unable to connect to ManStat. Please try again.'
      ];

      return $response;

    }

    if( $countAccount == 0 && $countContact == 0 ){

      $response = [
        'status' => false, 
        'statusCode' => 422,
        'message' => 'User account doesn\'t exist'
      ];

      return $response;

    }

    $contactID = $this->manstatServices->getUserID($data['etccode']);
    $contactInfo = $this->contactServices->find($data['etccode']);

    $defaultPassword = date('dmY',strtotime($contactInfo['DOB']));

    if( $countContact > 0 && ( $data['password'] == $defaultPassword) ){

      $token = base64_encode(microtime().'-'.$contactInfo['ContactID']. '-' .Config::get('evolution.key'));

      $data = [
        'ContactID'     => $contactInfo['ContactID'],
        'ApiToken'   => $token,
        'ExpiredAt'  => Carbon::now()->addDays(Config::get('evolution.expiration')),
        'CreatedAt'  => Carbon::now(),
        'UpdatedAt'  => Carbon::now(),
      ];

      $tokenResult = $this->tokenServices->create($data);

      $response = [
          'status' => true, 
          'statusCode' => 200,
          'token' => $token
      ];

      return $response;

    }
    else{

      $response = [
          'status' => false, 
          'statusCode' => 422,
          'message' => 'Invalid User Account / Password'
        ];

        return $response;

    }


  }

}