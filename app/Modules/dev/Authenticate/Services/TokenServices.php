<?php namespace App\Modules\dev\Authenticate\Services;

use App\Modules\dev\Authenticate\Repository\Interfaces\TokenInterface as TokenRepository;

use Config;
use Carbon\Carbon;
use Hash;
use Request;


class TokenServices
{

  protected $tokenRepository;

  public function __construct(
    TokenRepository $tokenRepository)
  {

    $this->tokenRepository  = $tokenRepository;

  }


  function info(){

    $apiToken = Request::header('X-Auth-Token');
    $token = $this->tokenRepository->find($apiToken);

    return $token;

  }

  function create($data){

    $result = $this->tokenRepository->create($data);

    return $result;

  }

}