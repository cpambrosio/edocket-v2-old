<?php namespace App\Modules\dev\Authenticate\Services;

use App\Modules\dev\Authenticate\Repository\Interfaces\ContactInterface as ContactRepository;

use Config;
use Carbon\Carbon;
use Hash;
use Request;


class ContactServices
{

  protected $contactRepository;

  public function __construct(
    ContactRepository $contactRepository)
  {

    $this->contactRepository  = $contactRepository;

  }


  function info(){

    $apiToken = Request::header('X-Auth-Token');
    $token = $this->contactRepository->find($apiToken);

    return $token;

  }

  function count($code){

    $result = $this->contactRepository->countAcountByCode($code);

    return $result;

  }

  function find($code){

    $result = $this->contactRepository->findByCode($code);

    return $result;

  }

  function findByToken($token){

    $result = $this->contactRepository->findByToken($token);

    return $result;

  }

  function findByCurrentToken(){

    $apiToken = Request::header('X-Auth-Token');
    $result = $this->contactRepository->findByToken($apiToken);

    return $result;

  }

  public function findByContactReportData($id, $jobId){

    $result = $this->contactRepository->findByContactReportData($id, $jobId);

    return $result;

  }

}