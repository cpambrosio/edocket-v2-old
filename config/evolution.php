<?php 

use Illuminate\Support\Str;

return array(

  /*
  |--------------------------------------------------------------------------
  | Token Expiration
  |--------------------------------------------------------------------------
  |
  | Here you may specify the number of minutes that you wish the token
  | to be allowed to remain idle before it expires.
  |
  */

 'expiration' => 3,// days

 /*
  |--------------------------------------------------------------------------
  | Token Encryption Key
  |--------------------------------------------------------------------------
  |
  | This key is used by the Illuminate encrypter service and should be set
  | to a random, 32 character string, otherwise these encrypted strings
  | will not be safe. Please do this before deploying an application!
  |
  */

  'key' => Str::random(32),


     /*
  |--------------------------------------------------------------------------
  | ManStat Address
  |--------------------------------------------------------------------------
  |
  | The host url of main manstat api
  |
  */

  'manstatAddress' => 'www.manstat.com:9998',

       /*
  |--------------------------------------------------------------------------
  | ManStat Username
  |--------------------------------------------------------------------------
  |
  | The username to access the main manstat api
  |
  */

  'manstatUsername' => 'ManstatMobileUser',

         /*
  |--------------------------------------------------------------------------
  | ManStat Password
  |--------------------------------------------------------------------------
  |
  | The password to access the main manstat api
  |
  */

  'manstatPassword' => 'Pass4ManstatMobileUser',

   /*
  |--------------------------------------------------------------------------
  | Activation Code
  |--------------------------------------------------------------------------
  |
  | This code is used to determine what database to be access
  |
  */

  //'activationCode' => '62ECB482', // <-- Manstat Test Server
  'activationCode' => '44ECB48A', // <-- Evolution Test Server
  //'activationCode' => '42ECB486', // <-- Evolution Test Server with data visibility
  // 'activationCode' => '24ECB48A', // <-- Evolution Test Server VPN
  //'activationCode' => '62ECB486', // <-- Evolution Production Server

  

  /*
  |--------------------------------------------------------------------------
  | Activation User
  |--------------------------------------------------------------------------
  |
  | This user used to for activation
  |
  */

  'activation_user' => 'ManstatIS', // <-- Evolution Test Server
  // 'activation_user' => 'ManstatIS', // <-- Evolution Test Server VPN


    /*
  |--------------------------------------------------------------------------
  | Docushare User
  |--------------------------------------------------------------------------
  |
  | The user to be used in docushare
  |
  */

  // 'docushareUser' => 'OBPTestUser',
  // 'docushareUser' => 'ManstatIS',
  'docushareUser' => 'Admin',
  //'docushareUser' => 'appuser',

      /*
  |--------------------------------------------------------------------------
  | Docushare Password
  |--------------------------------------------------------------------------
  |
  | The password to be used in docushare
  |
  */

  // 'docusharePassword' => 'Carbon16',
  // 'docusharePassword' => 'Pass4MIS!',
  'docusharePassword' => 'jaKe0864',
  //'docusharePassword' => 'Div246ulge',


        /*
  |--------------------------------------------------------------------------
  | Docushare Domain
  |--------------------------------------------------------------------------
  |
  | The domain to be used in docushare
  |
  */

  'docushareDomain' => 'DocuShare',
  // 'docushareDomain' => 'corp.ermg.com.au',

   /*
  |--------------------------------------------------------------------------
  | Site Assessment Path
  |--------------------------------------------------------------------------
  |
  | The path where the Site Assessment eForm will be stored
  |
  */

  'siteAssessmentPath' => '',
  // 'siteAssessmentPath' => 'C:/Users/user-2/Documents/eDocket Files Watchfolder/Site Assessment/',


      /*
  |--------------------------------------------------------------------------
  | Enable Docushare Watch Folder
  |--------------------------------------------------------------------------
  |
  | True or False
  |
  */

  'watchFolderEnable' => true,


   /*
  |--------------------------------------------------------------------------
  | Docushare Watch Folder FTP Host
  |--------------------------------------------------------------------------
  |
  | Host for Watch Folder FTP
  |
  */

  'watchFolderHost' => 'ftp2.ermg.com.au',


   /*
  |--------------------------------------------------------------------------
  | Docushare Watch Folder FTP Username
  |--------------------------------------------------------------------------
  |
  | Username for Watch Folder FTP
  |
  */

  'watchFolderUsername' => 'ftpuser',


   /*
  |--------------------------------------------------------------------------
  | Docushare Watch Folder FTP Password
  |--------------------------------------------------------------------------
  |
  | Password for Watch Folder FTP
  |
  */

  'watchFolderPassword' => 'XbEpM9Htm@',


     /*
  |--------------------------------------------------------------------------
  | Generate File Path
  |--------------------------------------------------------------------------
  |
  | Path where generated files saved
  |
  */

  'generatedFilePath' => public_path('generated files').'/',


       /*
  |--------------------------------------------------------------------------
  | Enable Uploading on ManStat
  |--------------------------------------------------------------------------
  |
  | True or False
  |
  */

  'uploadManstat' => true,


         /*
  |--------------------------------------------------------------------------
  | Evolution admin email
  |--------------------------------------------------------------------------
  |
  | Email address of the admin to be send an email
  |
  */

  // 'evolutionEmail' => 'appdata@evolutiontraffic.com.au',
  'evolutionEmail' => 'cpambrosio@straightarrow.com.ph',
  


         /*
  |--------------------------------------------------------------------------
  | Enable Job Sync
  |--------------------------------------------------------------------------
  |
  | True or False
  |
  */

  'jobSyncEnable' => true,


       /*
  |--------------------------------------------------------------------------
  | Enable Uploading on ManStat Dev
  |--------------------------------------------------------------------------
  |
  | True or False
  |
  */

  'uploadManstatDev' => true,
  


         /*
  |--------------------------------------------------------------------------
  | Enable Job Sync Dev
  |--------------------------------------------------------------------------
  |
  | True or False
  |
  */

  'jobSyncEnableDev' => false,


           /*
  |--------------------------------------------------------------------------
  | Google API Server Key
  |--------------------------------------------------------------------------
  |
  | Key use for All Google API
  |
  */

  //'googleApiKey' => 'AIzaSyBW3cR-DtShLTrCms_uZtxAeDXCx47i0Po', // Localhost
   'googleApiKey' => 'AIzaSyCTW4awBHQMG7SD5VmlB7bVoati7d_fVy4', // Dev Server


             /*
  |--------------------------------------------------------------------------
  | Default Timezone
  |--------------------------------------------------------------------------
  |
  | Key use for All Google API
  |
  */

  'defaultTimezone' => 'Australia/Brisbane',


             /*
  |--------------------------------------------------------------------------
  | Acceptable image extension files
  |--------------------------------------------------------------------------
  |
  | List of image extension files
  |
  */

  'imageExtension' => ["jpg","jpeg","png","jpe","jpeg","JPG","JPEG","PNG","JPE","JPEG"],


               /*
  |--------------------------------------------------------------------------
  | Job expiration delay hours
  |--------------------------------------------------------------------------
  |
  | No. of hours the job to be removed after its end date
  |
  */

  'expirationDelayHours' => 24,


               /*
  |--------------------------------------------------------------------------
  | Acceptable video extension files
  |--------------------------------------------------------------------------
  |
  | List of video extension files
  |
  */

  'videoExtension' => ['mov','3gp','mp4','wmv','mkv','flv'],

                 /*
  |--------------------------------------------------------------------------
  | Video upload attachment size limit
  |--------------------------------------------------------------------------
  |
  | Limit size for attaching video in megabytes
  |
  */

  'videoAttachmentLimit' => 30,




);
