<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');



Route::group(['prefix' => 'development', 'namespace' => 'App\Modules\dev'],function(){

	Route::group(['prefix' => 'auth', 'namespace' => 'Authenticate\Controller'],function(){

		Route::post('login','AuthenticateController@login');
	
	});

	Route::group(['namespace' => 'Job\Controller', 'middleware' => ['authenticate']],function(){
	
		Route::get('job','JobController@getRelatedJobs');
		Route::post('job/timesheet','JobController@getTimesheetEmployees');
		Route::get('job/requirements','JobController@getRequirements');
		Route::get('job/docket_attachment','JobController@getDocketAttachments');
		Route::get('job/checklist','JobController@getSiteCheckList');
		Route::get('job/tc_attachment','JobController@getTCAttachment');
		Route::get('job/signage_audit','JobController@getSignageAudit');
		Route::get('job/sign','JobController@getSign');
		Route::get('job/carriage_way','JobController@getCarriageWay');
		Route::get('job/direction','JobController@getDirection');

		Route::post('job/update','JobController@updateJob'); 
		Route::post('job/save_tcsignature','JobController@saveTCAttachment');
		Route::post('job_all','JobController@getAllJobData');

		Route::delete('job/delete_extjob/{id}','JobController@deleteExtensionJob');
		
		Route::get('job/job_sync/{id}','JobController@countJobSync');

		Route::post('job/generate_job_docket_report','JobController@generateJobDocketReport'); 
		Route::post('job/generate_app_safety_report','JobController@generateAppSafetyReport'); 
		Route::post('job/generate_job_image_report','JobController@generateJobImageReport'); 

		Route::get('raw','JobController@returnAll');

		Route::post('job/upload_job_image','JobController@uploadJobImage');

		Route::post('job/view_link','JobController@viewDocketLink');

		Route::get('job/notes','JobController@getNotes');

		Route::get('job/job_report/{type}/{id}','JobController@checkJobReport'); 

		Route::get('job/truncate_docket_attachment','JobController@truncateDocketAttachments');

		Route::get('job/check_connectivity','JobController@checkConnectivity');

		Route::post('job/generate_fail_safe_report','JobController@generateFailSafeReport');

		Route::get('job/job_expire/{id}','JobController@checkJobExpiration');

		Route::post('upload_video','JobController@uploadVideo');

		Route::delete('job/job_sync/{id}','JobController@clearJobSync');

	});

});






